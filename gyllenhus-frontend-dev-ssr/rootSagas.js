import { all } from 'redux-saga/effects';
import createSagaMiddleware from 'redux-saga';
import menuListSaga from './containers/MenuList/saga'
import myAccountSaga from './containers/MyAccountWidget/saga'
import loginModelSaga from './containers/LoginModal/saga'
import forgotPasswordModelSaga from './containers/ForgotPasswordModal/saga'
import priceListSaga from './containers/PriceList/saga'
import footerListSaga from './containers/FooterList/saga'
import homePageSaga from './containers/HomePage/saga'
import registerSaga from './containers/RegisterPage/saga'
import cartPageSaga from './containers/CartPage/saga'
import searchProductSaga from './containers/Search/saga';


export const sagaMiddleware = createSagaMiddleware();

export default function* rootSaga() {
    yield all([
        menuListSaga(),
        myAccountSaga(),
        loginModelSaga(),
        forgotPasswordModelSaga(),
        priceListSaga(),
        footerListSaga(),
        homePageSaga(),
        registerSaga(),
        cartPageSaga(),
        searchProductSaga(),

    ]);
}
