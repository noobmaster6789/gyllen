import { LOAD_MENU, LOAD_MENU_ERROR, LOAD_MENU_SUCCESS } from './constants';

export function loadMenu(name) {
  return {
    type: LOAD_MENU,
    name,
  };
}

export function menuLoaded(menus) {
  return {
    type: LOAD_MENU_SUCCESS,
    menus,
  };
}

export function menuLoadingError(error) {
  return {
    type: LOAD_MENU_ERROR,
    error,
  };
}
