/*
 * MenuList Messages
 *
 * This contains all the text for the MenuList container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({});
