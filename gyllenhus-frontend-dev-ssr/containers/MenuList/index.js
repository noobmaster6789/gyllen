/**
 *
 * MenuList
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { createStructuredSelector } from 'reselect';
import { loadMenu } from './actions';
import {
  makeSelectMenuListData,
  makeSelectMenuListError,
  makeSelectMenuListLoading,
} from '../App/selectors';
import ListItem from '../../components/ListItem';
import List from '../../components/List';
import MenuListItem from '../../components/MenuListItem';
import { showLoginModal } from '../App/actions';

/**
 * @return {null}
 */
export function MenuList(props) {
  const {error, data, onClickShowLoginModal } = props;

  if (error !== false) {
    const ErrorComponent = () => (
      <ListItem item="Something went wrong cannot load menu. Please try again!" />
    );
    return <List component={ErrorComponent} />;
  }
  if (data !== false) {
    return <ul className="menu">
      <List component={MenuListItem} items={data} />
      <li className="on-mobile account-group">
        <a href="#" onClick={onClickShowLoginModal}>Login</a>
        <a className="button" href="/register">Register</a>
      </li> </ul>
  }
  return null;
}

MenuList.propTypes = {
  name: PropTypes.string.isRequired,
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  onLoadMenu: PropTypes.func,
  onClickShowLoginModal : PropTypes.func
};

function mapDispatchToProps(dispatch) {
  return {
    onClickShowLoginModal: () => dispatch(showLoginModal()),
  };
}

const withConnect = connect(
  createStructuredSelector({
    loading: makeSelectMenuListLoading(),
    error: makeSelectMenuListError(),
  }),
  mapDispatchToProps,
);

export default compose(withConnect)(MenuList);
