import { takeLatest, call, put, select } from 'redux-saga/effects';
import Cookie from 'js-cookie'
import request from '../../utils/request';
import { LOAD_MENU } from './constants';
import { menuLoaded, menuLoadingError } from './actions';
import { makeSelectMenuListName } from '../App/selectors';

const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
export function* getMenu() {
  const menuEndpoint = process.env.REACT_APP_API_GET_MENU;
  const requestURL = `${apiUrl}/${menuEndpoint}`;
  try {
    const language = Cookie.get('locale')
    const menuName = yield select(makeSelectMenuListName());
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({ language, menuName }),
    });
    if (response.code === 'gyllenhus_get_menu_success') {
      yield put(menuLoaded(response.data.menus));
    } else {
      yield put(menuLoadingError(response));
    }
  } catch (e) {
    yield put(menuLoadingError(e));
  }
}
// Individual exports for testing
export default function* menuListSaga() {
  yield takeLatest(LOAD_MENU, getMenu);
}
