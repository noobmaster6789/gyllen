import { takeLatest, call, put, select } from 'redux-saga/effects';
import request from '../../utils/request';
import { SUBMIT_LOGIN_FORM } from './constants';
import {
  makeSelectLoginEmail,
  makeSelectLoginKeepMeLoggedIn,
  makeSelectLoginPassword,
  makeSelectJwtToken,
} from '../App/selectors';

import {
  loadUserByJwtToken,
  loginError,
  loginSuccess,
  loadUserInfo,
  loadUserInfoSuccess,
  loadIndexes,
  logoutSuccess,
} from '../App/actions';
import {
  loadGoldProducts,
  loadSilverProducts,
  loadSpecialOffers,
  loadPlatinumProducts,
  loadPalladiumProducts,
} from '../HomePage/actions';
import { LOAD_USER_INFO, USER_LOGOUT } from '../App/constants';
import { reloadDeliverySetting } from '../CartPage/actions';

const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
const loginEndpoint = process.env.REACT_APP_API_LOGIN_ENDPOINT;
const getUserInfoEndpoint = process.env.REACT_APP_API_GET_USER_INFO_ENDPOINT;
const authUrl = `${apiUrl}/${loginEndpoint}`;
const userInfoUrl = `${apiUrl}${getUserInfoEndpoint}`;

export function* postLogin() {
  try {
    const email = yield select(makeSelectLoginEmail());
    const password = yield select(makeSelectLoginPassword());
    const keepMeLoggedIn = yield select(makeSelectLoginKeepMeLoggedIn());
    // post data
    const response = yield call(request, authUrl, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({ email, password, keepMeLoggedIn }),
    });
    if (response.code === 'gyllenhus_login_success') {
      yield put(loginSuccess(response.data.token));
      yield put(loadUserByJwtToken(response.data.token));
      yield put(loadUserInfo());
      yield put(loadGoldProducts());
      yield put(loadSilverProducts());
      yield put(loadSpecialOffers());
      yield put(loadPlatinumProducts());
      yield put(loadPalladiumProducts());
    } else {
      yield put(loginError(response));
    }
  } catch (error) {
    yield put(loginError(error));
  }
}

export function* getUserInfo() {
  try {
    const token = yield select(makeSelectJwtToken());
    // post data
    const response = yield call(request, userInfoUrl, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }),
    });
    if (response.code === 'gyllenhus_get_user_info_success') {
      yield put(loadUserInfoSuccess(response.data.userType));
      yield put(loadIndexes());
      yield put(reloadDeliverySetting());
    }
  } catch (error) { }
}

export function* userLogout() {
  yield put(logoutSuccess());
  yield put(loadIndexes());
  yield put(loadGoldProducts());
  yield put(loadSilverProducts());
  yield put(loadSpecialOffers());
  yield put(loadPlatinumProducts());
  yield put(loadPalladiumProducts());
  yield put(reloadDeliverySetting());
}

// Individual exports for testing
export default function* loginModalSaga() {
  yield takeLatest(SUBMIT_LOGIN_FORM, postLogin);
  yield takeLatest(LOAD_USER_INFO, getUserInfo);
  yield takeLatest(USER_LOGOUT, userLogout);
}
