/*
 * LoginModal Constants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */
export const CHANGE_EMAIL = 'gyllenhus/LoginModal/CHANGE_EMAIL';
export const CHANGE_PASSWORD = 'gyllenhus/LoginModal/CHANGE_PASSWORD';
export const CHANGE_KEEP_ME_LOGIN = 'gyllenhus/LoginModal/CHANGE_KEEP_ME_LOGIN';
export const SUBMIT_LOGIN_FORM = 'gyllenhus/LoginModal/SUBMIT_LOGIN_FORM';
export const LOGIN_SUCCESS = 'gyllenhus/LoginModal/LOGIN_SUCCESS';
export const LOGIN_ERROR = 'gyllenhus/LoginModal/LOGIN_ERROR';
export const SHOW_LOGIN_MODAL = 'gyllenhus/LoginModal/SHOW_LOGIN_MODAL';
