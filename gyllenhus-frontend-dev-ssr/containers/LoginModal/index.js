/**
 *
 * Login Modal
 *
 */

import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import {
  makeSelectIsLoggedIn,
  makeSelectLoginLoading,
  makeSelectLoginEmail,
  makeSelectLoginError,
  makeSelectLoginKeepMeLoggedIn,
  makeSelectLoginPassword,
  makeSelectLoginShowModal,
} from '../App/selectors';
import {
  changeEmail,
  changeKeepMeLogin,
  changePassword,
  showLoginModal,
  submitLogin,
  showForgotPasswordModal,
} from '../App/actions';

import messages from './messages';
import useOutsideClick from '../../utils/useOutsideClick';

const key = 'loginModel';

export function LoginModal(props) {

  const ref = useRef();

  useOutsideClick(ref, () => {
    if (showModal) {
      onClickCloseModal();
    }
  });

  const {
    showModal,
    isLoggedIn,
    loading,
    error,
    email,
    password,
    keepMeLoggedIn,
    onChangeEmail,
    onChangePassword,
    onChangeKeepMeLogin,
    onSubmitForm,
    onClickCloseModal,
    onClickOpenForgotModal,
  } = props;
  return (
    <div className={`modal login ${!isLoggedIn && showModal ? 'active' : ''}`} ref={ref}>
      <div className="card">
        <div className="head">
          <h2><FormattedMessage {...messages.login} /></h2>
          <a href="#" onClick={e => { onClickOpenForgotModal(); e.preventDefault()}}>
            <FormattedMessage {...messages.forgotPassword} />
          </a>
        </div>
        <form className="body" onSubmit={onSubmitForm}>
          <div className="form-group">
            <input
              type="email"
              id="email"
              className={`input-text ${email !== '' ? 'has-value' : ''}`}
              required="required"
              autoComplete="off"
              value={email}
              onChange={onChangeEmail} />
            <div className="placeholder">Email</div>
          </div>
          <div className="form-group">
            <input
              type="password"
              id="password"
              className={`input-text ${password !== '' ? 'has-value' : ''}`}
              required="required"
              value={password}
              autoComplete="new-password"
              onChange={onChangePassword}
            />
            <div className="placeholder">Password</div>
          </div>
          <label htmlFor="keep-loggedin" className="checkbox">
            <input
              type="checkbox"
              name="keep-login"
              id="keep-loggedin"
              value={1}
              checked={keepMeLoggedIn}
              onChange={onChangeKeepMeLogin} />
            <span className="tick">
              <i />
            </span>
            <span className="text">Keep me logged in</span>
          </label>
          <div style={{ color: 'red' }}>
            {(!loading && error && error.message) || null}
          </div>
          <div className="button-group">
            <button
              className="button is-blank"
              type="button"
              onClick={onClickCloseModal}>
              Cancel
              </button>
            <button className="button" type="submit">
              Login
              </button>
          </div>
        </form>
      </div>
    </div>
  );
}

LoginModal.propTypes = {
  showModal: PropTypes.bool,
  isLoggedIn: PropTypes.bool,
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  keepMeLoggedIn: PropTypes.bool,
  onChangeEmail: PropTypes.func,
  onChangePassword: PropTypes.func,
  onChangeKeepMeLogin: PropTypes.func,
  onSubmitForm: PropTypes.func,
  onClickCloseModal: PropTypes.func,
  onClickOpenForgotModal: PropTypes.func,
};

function mapDispatchToProps(dispatch) {
  return {
    onChangeEmail: evt => dispatch(changeEmail(evt.target.value)),
    onChangePassword: evt => dispatch(changePassword(evt.target.value)),
    onChangeKeepMeLogin: evt => dispatch(changeKeepMeLogin(evt.target.checked)),
    onSubmitForm: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(submitLogin());
    },
    onClickCloseModal: () => dispatch(showLoginModal(false)),
    onClickOpenForgotModal: () => dispatch(showForgotPasswordModal(true)),
  };
}

const withConnect = connect(
  createStructuredSelector({
    showModal: makeSelectLoginShowModal(),
    isLoggedIn: makeSelectIsLoggedIn(),
    loading: makeSelectLoginLoading(),
    error: makeSelectLoginError(),
    email: makeSelectLoginEmail(),
    password: makeSelectLoginPassword(),
    keepMeLoggedIn: makeSelectLoginKeepMeLoggedIn(),
  }),
  mapDispatchToProps,
);


export default compose(
  withConnect,
)(LoginModal);
