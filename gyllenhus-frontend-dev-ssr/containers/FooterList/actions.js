/*
 *
 * FooterList actions
 *
 */

import {
  LOAD_FOOTER_LIST,
  LOAD_FOOTER_LIST_SUCCESS,
  LOAD_FOOTER_LIST_ERROR,
} from './constants';

export function loadFooterList() {
  return {
    type: LOAD_FOOTER_LIST,
  };
}

export function footerListLoaded(customerSupport, menuFooter1, menuFooter2) {
  return {
    type: LOAD_FOOTER_LIST_SUCCESS,
    customerSupport,
    menuFooter1,
    menuFooter2,
  };
}

export function footerListLoadingError(error) {
  return {
    type: LOAD_FOOTER_LIST_ERROR,
    error,
  };
}
