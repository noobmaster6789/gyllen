/*
 * FooterList Messages
 *
 * This contains all the text for the FooterList container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  customerSupport: {
    id: `${scope}.customerSupport`,
    defaultMessage: 'Customer Support',
  },
  information: {
    id: `${scope}.information`,
    defaultMessage: 'Information',
  },
  aboutUs: {
    id: `${scope}.aboutUs`,
    defaultMessage: 'About Us',
  },
  weAccept: {
    id: `${scope}.weAccept`,
    defaultMessage: 'We Accept',
  },
});
