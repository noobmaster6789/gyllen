import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import Cookie from 'js-cookie'
import { footerListLoaded, footerListLoadingError } from './actions';
import { LOAD_FOOTER_LIST } from './constants';

const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
export function* getFooter() {
  const footerEndpoint = process.env.REACT_APP_API_GET_FOOTER;
  const requestUrl = `${apiUrl}/${footerEndpoint}`;
  try {
    const language = Cookie.get('locale')
    const response = yield call(request, requestUrl, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_footer_success') {
      const { customerSupport, menuFooter1, menuFooter2 } = response.data;

      const menuFooter1Children =
        menuFooter1.length > 0 && typeof menuFooter1[0].children !== 'undefined'
          ? menuFooter1[0].children
          : [];
      const menuFooter2Children =
        menuFooter2.length > 0 && typeof menuFooter2[0].children !== 'undefined'
          ? menuFooter2[0].children
          : [];

      yield put(
        footerListLoaded(
          customerSupport,
          menuFooter1Children,
          menuFooter2Children,
        ),
      );
    } else {
      yield put(footerListLoadingError(response));
    }
  } catch (e) {
    yield put(footerListLoadingError(e));
  }
}
// Individual exports for testing
export default function* footerListSaga() {
  yield takeLatest(LOAD_FOOTER_LIST, getFooter);
}
