/*
 *
 * FooterList reducer
 *
 */
import produce from 'immer';
import {
  LOAD_FOOTER_LIST,
  LOAD_FOOTER_LIST_SUCCESS,
  LOAD_FOOTER_LIST_ERROR,
} from './constants';

export const initialState = {
  loading: false,
  error: false,
  data: {
    customerSupport: false,
    menuFooter1: false,
    menuFooter2: false,
  },
};

/* eslint-disable default-case, no-param-reassign */
const footerListReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case LOAD_FOOTER_LIST:
        draft.loading = true;
        draft.error = false;
        draft.data.customerSupport = false;
        draft.data.menuFooter1 = false;
        draft.data.menuFooter2 = false;
        break;
      case LOAD_FOOTER_LIST_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.data.customerSupport = action.customerSupport;
        draft.data.menuFooter1 = action.menuFooter1;
        draft.data.menuFooter2 = action.menuFooter2;
        break;
      case LOAD_FOOTER_LIST_ERROR:
        draft.loading = false;
        draft.data.customerSupport = false;
        draft.data.menuFooter1 = false;
        draft.data.menuFooter2 = false;
        draft.error = action.error;
        break;
    }
  });

export default footerListReducer;
