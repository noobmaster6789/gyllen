import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the footerList state domain
 */

const selectFooterList = state => state.footerList || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by FooterList
 */

const makeSelectFooterListLoading = () =>
  createSelector(
    selectFooterList,
    footerListState => footerListState.loading,
  );

const makeSelectFooterListError = () =>
  createSelector(
    selectFooterList,
    footerListState => footerListState.error,
  );
const makeSelectFooterListDataCustomerSupport = () =>
  createSelector(
    selectFooterList,
    footerListState =>
      footerListState.data && footerListState.data.customerSupport,
  );

const makeSelectFooterListDataMenuFooter1 = () =>
  createSelector(
    selectFooterList,
    footerListState => footerListState.data && footerListState.data.menuFooter1,
  );

const makeSelectFooterListDataMenuFooter2 = () =>
  createSelector(
    selectFooterList,
    footerListState => footerListState.data && footerListState.data.menuFooter2,
  );

export {
  selectFooterList,
  makeSelectFooterListLoading,
  makeSelectFooterListError,
  makeSelectFooterListDataCustomerSupport,
  makeSelectFooterListDataMenuFooter1,
  makeSelectFooterListDataMenuFooter2,
};
