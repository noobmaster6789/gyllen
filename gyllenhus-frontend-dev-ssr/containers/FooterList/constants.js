/*
 *
 * FooterList constants
 *
 */

export const LOAD_FOOTER_LIST = 'app/FooterList/LOAD_FOOTER_LIST';
export const LOAD_FOOTER_LIST_SUCCESS = 'app/FooterList/LOAD_FOOTER_LIST_SUCCESS';
export const LOAD_FOOTER_LIST_ERROR = 'app/FooterList/LOAD_FOOTER_LIST_ERROR';
