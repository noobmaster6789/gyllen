/**
 *
 * FooterList
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import {
  makeSelectFooterListDataCustomerSupport,
  makeSelectFooterListDataMenuFooter1,
  makeSelectFooterListDataMenuFooter2,
  makeSelectFooterListError,
  makeSelectFooterListLoading,
} from './selectors';

import messages from './messages';
import { loadFooterList } from './actions';
import LoadingIndicator from '../../components/LoadingIndicator';
import ListItem from '../../components/ListItem';
import List from '../../components/List';
import Copyright from '../../components/Copyright';
import CustomerSupport from '../../components/CustomerSupport';
import FooterMenu from '../../components/FooterMenu';

const REACT_APP_API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT
const swishImage = `${REACT_APP_API_ENDPOINT}/static/images/swish.png`;
const visaImage = `${REACT_APP_API_ENDPOINT}/static/images/visa.png`;
const masterCardImage = `${REACT_APP_API_ENDPOINT}/static/images/mastercard.png`;
const securityConnectImage = `${REACT_APP_API_ENDPOINT}/static/images/ssl-secure-connection.png`;

export function FooterList({
  loading,
  error,
  dataFooter,
  data: { customerSupport, menuFooter1, menuFooter2 },
  onLoadFooterList,
}) {
  if (dataFooter && dataFooter != undefined) {
    customerSupport = dataFooter.customerSupport;
    menuFooter1 = dataFooter.menuFooter1Children;
    menuFooter2 = dataFooter.menuFooter2Children;
  }
  useEffect(() => {
    if (!loading && !error && !dataFooter) onLoadFooterList();
  }, []);

  if (loading) return <LoadingIndicator />;
  if (error !== false) {
    const ErrorComponent = () => <ListItem item={error.message} />;
    return <List component={ErrorComponent} />;
  }
  return (
    <footer className="footer">
      <div className="wrapper">
        <div className="col-1">
          <h2>
            <FormattedMessage {...messages.customerSupport} />
          </h2>
          {customerSupport !== undefined && (
            <CustomerSupport {...customerSupport} />
          )}
        </div>
        <div className="col-2">
          <h2>
            <FormattedMessage {...messages.information} />
          </h2>
          <FooterMenu items={menuFooter1} />
        </div>
        <div className="col-3">
          <h2>
            <FormattedMessage {...messages.aboutUs} />
          </h2>
          <FooterMenu items={menuFooter2} />
        </div>
        <div className="col-4">
          <h2>
            <FormattedMessage {...messages.weAccept} />:
          </h2>
          <img className="payment-badges" src={visaImage} alt="visa" />
          <img
            className="payment-badges"
            src={masterCardImage}
            alt="master card"
          />
          <img
            className="payment-badges"
            src={swishImage}
            alt="swish payment"
          />

          <img
            className="ssl-badges"
            src={securityConnectImage}
            alt="security connect"
          />
        </div>
      </div>
      <Copyright />
    </footer>
  );
}

FooterList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  data: PropTypes.shape({
    customerSupport: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
    menuFooter1: PropTypes.oneOfType([PropTypes.bool, PropTypes.array]),
    menuFooter2: PropTypes.oneOfType([PropTypes.bool, PropTypes.array]),
  }),
  onLoadFooterList: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  loading: makeSelectFooterListLoading(),
  error: makeSelectFooterListError(),
  data: createStructuredSelector({
    customerSupport: makeSelectFooterListDataCustomerSupport(),
    menuFooter1: makeSelectFooterListDataMenuFooter1(),
    menuFooter2: makeSelectFooterListDataMenuFooter2(),
  }),
});

function mapDispatchToProps(dispatch) {
  return {
    onLoadFooterList: () => dispatch(loadFooterList()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(FooterList);
