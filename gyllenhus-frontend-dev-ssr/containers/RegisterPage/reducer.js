/*
 *
 * RegisterPage reducer
 *
 */
import produce from 'immer';
import {
  SUBMIT_REGISTER_FORM,
  REGISTER_ERROR,
  CHANGE_ACCOUNT_TYPE,
  CHANGE_FULL_NAME,
  CHANGE_ADDRESS,
  CHANGE_ID_NUMBER,
  CHANGE_POSTAL_CODE,
  CHANGE_EMAIL,
  CHANGE_CITY,
  CHANGE_PHONE,
  CHANGE_PASSPHRASE,
  CHANGE_RE_PASSPHRASE,
  REGISTER_USER_SUCCESS,
  CHANGE_ACCEPT_PRIVACY_POLICY,
  CHANGE_SUBSCRIBE,
  RESET_FORM_INPUT,
} from './constants';
import { USER_ROLE_PERSONAL } from '../App/constants';

export const initialState = {
  loading: false,
  error: false,
  user: false,
  accountType: USER_ROLE_PERSONAL,
  fullName: '',
  address: '',
  idNumber: '',
  postalCode: '',
  email: '',
  city: '',
  phone: '',
  passphrase: '',
  rePassphrase: '',
  acceptPrivacyPolicy: false,
  subscribeStatus: false,
};

/* eslint-disable default-case, no-param-reassign */
const registerPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SUBMIT_REGISTER_FORM:
        draft.loading = true;
        draft.error = false;
        draft.user = false;
        break;
      case CHANGE_ACCOUNT_TYPE:
        draft.accountType = action.accountType;
        break;
      case CHANGE_FULL_NAME:
        draft.fullName = action.fullName;
        break;
      case CHANGE_ADDRESS:
        draft.address = action.address;
        break;
      case CHANGE_ID_NUMBER:
        draft.idNumber = action.idNumber;
        break;
      case CHANGE_POSTAL_CODE:
        draft.postalCode = action.postalCode;
        break;
      case CHANGE_EMAIL:
        draft.email = action.email;
        break;
      case CHANGE_CITY:
        draft.city = action.city;
        break;
      case CHANGE_PHONE:
        draft.phone = action.phone;
        break;
      case CHANGE_PASSPHRASE:
        draft.passphrase = action.passphrase;
        break;
      case CHANGE_RE_PASSPHRASE:
        draft.rePassphrase = action.rePassphrase;
        break;
      case CHANGE_ACCEPT_PRIVACY_POLICY:
        draft.acceptPrivacyPolicy = action.checked;
        break;
      case CHANGE_SUBSCRIBE:
        draft.subscribeStatus = action.checked;
        break;
      case REGISTER_USER_SUCCESS:
        draft.user = action.user;
        draft.loading = false;
        draft.error = false;
        localStorage.setItem('jwtToken', action.user.token);
        break;
      case REGISTER_ERROR:
        draft.error = action.error;
        draft.loading = false;
        draft.user = false;
        break;
      case RESET_FORM_INPUT:
        draft.accountType = state.accountType;
        draft.fullName = '';
        draft.email = '';
        draft.postalCode = '';
        draft.address = '';
        draft.city = '';
        draft.phone = '';
        draft.idNumber = '';
        draft.passphrase = '';
        draft.rePassphrase = '';
        draft.acceptPrivacyPolicy = false;
        draft.subscribeStatus = false;
        break;
    }
  });

export default registerPageReducer;
