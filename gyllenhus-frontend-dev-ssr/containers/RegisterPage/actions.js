/*
 *
 * RegisterPage actions
 *
 */

import {
  CHANGE_FULL_NAME,
  CHANGE_ADDRESS,
  CHANGE_ID_NUMBER,
  CHANGE_POSTAL_CODE,
  CHANGE_EMAIL,
  CHANGE_CITY,
  CHANGE_PHONE,
  CHANGE_PASSPHRASE,
  CHANGE_RE_PASSPHRASE,
  CHANGE_ACCOUNT_TYPE,
  REGISTER_ERROR,
  REGISTER_USER_SUCCESS,
  SUBMIT_REGISTER_FORM,
  CHANGE_ACCEPT_PRIVACY_POLICY,
  CHANGE_SUBSCRIBE,
  RESET_FORM_INPUT,
} from './constants';

export function changeAccountType(accountType) {
  return {
    type: CHANGE_ACCOUNT_TYPE,
    accountType,
  };
}
export function changeFullName(fullName) {
  return {
    type: CHANGE_FULL_NAME,
    fullName,
  };
}

export function changeAddress(address) {
  return {
    type: CHANGE_ADDRESS,
    address,
  };
}

export function changeIdNumber(idNumber) {
  return {
    type: CHANGE_ID_NUMBER,
    idNumber,
  };
}

export function changePostalCode(postalCode) {
  return {
    type: CHANGE_POSTAL_CODE,
    postalCode,
  };
}

export function changeEmail(email) {
  return {
    type: CHANGE_EMAIL,
    email,
  };
}

export function changeCity(city) {
  return {
    type: CHANGE_CITY,
    city,
  };
}

export function changePhone(phone) {
  return {
    type: CHANGE_PHONE,
    phone,
  };
}

export function changePassphrase(passphrase) {
  return {
    type: CHANGE_PASSPHRASE,
    passphrase,
  };
}

export function changeRePassphrase(rePassphrase) {
  return {
    type: CHANGE_RE_PASSPHRASE,
    rePassphrase,
  };
}

export function submitRegisterForm() {
  return {
    type: SUBMIT_REGISTER_FORM,
  };
}
export function registerLoadingError(error) {
  return {
    type: REGISTER_ERROR,
    error,
  };
}

export function loadRegisterUserSuccess(user) {
  return {
    type: REGISTER_USER_SUCCESS,
    user,
  };
}

export function changeAcceptPrivacyPolicy(checked) {
  return {
    type: CHANGE_ACCEPT_PRIVACY_POLICY,
    checked,
  };
}

export function changeSubscribeStatus(checked) {
  return {
    type: CHANGE_SUBSCRIBE,
    checked,
  };
}

export function resetFormInput() {
  return {
    type: RESET_FORM_INPUT,
  };
}
