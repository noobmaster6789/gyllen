import { call, select, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import { SUBMIT_REGISTER_FORM } from './constants';
import { loadRegisterUserSuccess, registerLoadingError } from './actions';
import {
  makeSelectRegisterAccountType,
  makeSelectRegisterAddress,
  makeSelectRegisterCity,
  makeSelectRegisterEmail,
  makeSelectRegisterFullName,
  makeSelectRegisterIdNumber,
  makeSelectRegisterPassphrase,
  makeSelectRegisterPhone,
  makeSelectRegisterPostalCode,
  makeSelectSubscribeStatus,
} from './selectors';
import { loadUserByJwtToken } from '../App/actions';

const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
const registerEndpoint = process.env.REACT_APP_API_REGISTER_ENDPOINT;
const registerUrl = `${apiUrl}/${registerEndpoint}`;

export function* registerUser() {
  try {
    const fullName = yield select(makeSelectRegisterFullName());
    const address = yield select(makeSelectRegisterAddress());
    const username = yield select(makeSelectRegisterEmail());
    const email = yield select(makeSelectRegisterEmail());
    const postalCode = yield select(makeSelectRegisterPostalCode());
    const password = yield select(makeSelectRegisterPassphrase());
    const role = yield select(makeSelectRegisterAccountType());
    const city = yield select(makeSelectRegisterCity());
    const phone = yield select(makeSelectRegisterPhone());
    const idNumber = yield select(makeSelectRegisterIdNumber());
    const isSubscribe = yield select(makeSelectSubscribeStatus());
    const response = yield call(request, registerUrl, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        fullName,
        address,
        email,
        username,
        postalCode,
        password,
        role,
        city,
        phone,
        idNumber,
        isSubscribe,
      }),
    });

    if (response.code === 'gyllenhus_register_success') {
      yield put(loadRegisterUserSuccess(response.data));
      yield put(loadUserByJwtToken(response.data.token));
      window.location.href = ("/thank-you")
    } else {
      yield put(registerLoadingError(response));
    }
  } catch (err) {
    yield put(registerLoadingError(err));
  }
}

export default function* registerSaga() {
  yield takeLatest(SUBMIT_REGISTER_FORM, registerUser);
}
