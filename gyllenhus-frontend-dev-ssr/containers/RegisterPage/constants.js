/*
 *
 * RegisterPage constants
 *
 */

export const SUBMIT_REGISTER_FORM = 'app/RegisterPage/SUBMIT_REGISTER_FORM';
export const REGISTER_USER_SUCCESS = 'app/RegisterPage/REGISTER_USER_SUCCESS';
export const REGISTER_ERROR = 'app/RegisterPage/REGISTER_ERROR';
export const CHANGE_ACCOUNT_TYPE = 'app/RegisterPage/CHANGE_ACCOUNT_TYPE';
export const CHANGE_FULL_NAME = 'app/RegisterPage/CHANGE_FULL_NAME';
export const CHANGE_ADDRESS = 'app/RegisterPage/CHANGE_ADDRESS';
export const CHANGE_ID_NUMBER = 'app/RegisterPage/CHANGE_ID_NUMBER';
export const CHANGE_POSTAL_CODE = 'app/RegisterPage/CHANGE_POSTAL_CODE';
export const CHANGE_EMAIL = 'app/RegisterPage/CHANGE_EMAIL';
export const CHANGE_PHONE = 'app/RegisterPage/CHANGE_PHONE';
export const CHANGE_CITY = 'app/RegisterPage/CHANGE_CITY';
export const CHANGE_PASSPHRASE = 'app/RegisterPage/CHANGE_PASSPHRASE';
export const CHANGE_RE_PASSPHRASE = 'app/RegisterPage/CHANGE_RE_PASSPHRASE';
export const CHANGE_ACCEPT_PRIVACY_POLICY =
  'app/RegisterPage/CHANGE_ACCEPT_PRIVACY_POLICY';
export const CHANGE_SUBSCRIBE = 'app/RegisterPage/CHANGE_SUBSCRIBE';
export const RESET_FORM_INPUT = 'app/RegisterPage/RESET_FORM_INPUT';
