import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the registerPage state domain
 */

const selectRegister = state => state.registerPage || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by RegisterPage
 */
const makeSelectRegisterLoading = () =>
  createSelector(
    selectRegister,
    registerState => registerState.loading,
  );
const makeSelectRegisterError = () =>
  createSelector(
    selectRegister,
    registerState => registerState.error,
  );
const makeSelectRegisterAccountType = () =>
  createSelector(
    selectRegister,
    registerState => registerState.accountType,
  );
const makeSelectRegisterFullName = () =>
  createSelector(
    selectRegister,
    registerState => registerState.fullName,
  );
const makeSelectRegisterAddress = () =>
  createSelector(
    selectRegister,
    registerState => registerState.address,
  );
const makeSelectRegisterIdNumber = () =>
  createSelector(
    selectRegister,
    registerState => registerState.idNumber,
  );
const makeSelectRegisterPostalCode = () =>
  createSelector(
    selectRegister,
    registerState => registerState.postalCode,
  );

const makeSelectRegisterEmail = () =>
  createSelector(
    selectRegister,
    registerState => registerState.email,
  );
const makeSelectRegisterCity = () =>
  createSelector(
    selectRegister,
    registerState => registerState.city,
  );
const makeSelectRegisterPhone = () =>
  createSelector(
    selectRegister,
    registerState => registerState.phone,
  );

const makeSelectRegisterPassphrase = () =>
  createSelector(
    selectRegister,
    registerState => registerState.passphrase,
  );
const makeSelectRegisterRePassphrase = () =>
  createSelector(
    selectRegister,
    registerState => registerState.rePassphrase,
  );

const makeSelectAcceptPrivacyPolicy = () =>
  createSelector(
    selectRegister,
    registerState => registerState.acceptPrivacyPolicy,
  );
const makeSelectSubscribeStatus = () =>
  createSelector(
    selectRegister,
    registerState => registerState.subscribeStatus,
  );

const makeSelectRegisterUser = () =>
  createSelector(
    selectRegister,
    registerState => registerState.user,
  );

export {
  selectRegister,
  makeSelectRegisterLoading,
  makeSelectRegisterError,
  makeSelectRegisterUser,
  makeSelectRegisterAccountType,
  makeSelectRegisterAddress,
  makeSelectRegisterCity,
  makeSelectRegisterFullName,
  makeSelectRegisterEmail,
  makeSelectRegisterIdNumber,
  makeSelectRegisterPhone,
  makeSelectRegisterPostalCode,
  makeSelectRegisterPassphrase,
  makeSelectRegisterRePassphrase,
  makeSelectAcceptPrivacyPolicy,
  makeSelectSubscribeStatus,
};
