/*
 * RegisterPage Messages
 *
 * This contains all the text for the RegisterPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  register: {
    id: `${scope}.register`,
    defaultMessage: 'Register',
  },
  email: {
    id: `${scope}.email`,
    defaultMessage: 'Email',
  },
  fullName: {
    id: `${scope}.fullName`,
    defaultMessage: 'Full Name',
  },
  address: {
    id: `${scope}.address`,
    defaultMessage: 'Address',
  },
  idNumber: {
    id: `${scope}.personalIdNumber`,
    defaultMessage: 'Personal Id Number',
  },
  postalCode: {
    id: `${scope}.postalCode`,
    defaultMessage: 'Postal Code',
  },
  city: {
    id: `${scope}.city`,
    defaultMessage: 'City',
  },
  phone: {
    id: `${scope}.phone`,
    defaultMessage: 'Phone',
  },
  business: {
    id: `${scope}.business`,
    defaultMessage: `Business`,
  },
  subscriber: {
    id: `${scope}.subscriber`,
    defaultMessage: 'Subscriber',
  },
  passphrase: {
    id: `${scope}.passphrase`,
    defaultMessage: 'Passphrase',
  },
  rePassphrase: {
    id: `${scope}.rePassphrase`,
    defaultMessage: 'Re-enter passphrase',
  },
  passphraseNotMatch: {
    id: `${scope}.passphraseNotMatch`,
    defaultMessage: 'Passphrase and re-enter passphrase do not match!',
  },
  personalAccount: {
    id: `${scope}.personalAccount`,
    defaultMessage: 'Private Account',
  },
  businessAccount: {
    id: `${scope}.businessAccount`,
    defaultMessage: 'Business Account',
  },
  registerPersonalAccount: {
    id: `${scope}.registerPersonalAccount`,
    defaultMessage: 'Open Private Account',
  },
  registerBusinessAccount: {
    id: `${scope}.registerBusinessAccount`,
    defaultMessage: 'Open Business Account',
  },
  receiveSpecialOffers: {
    id: `${scope}.receiveSpecialOffers`,
    defaultMessage:
      'I would like to receive special offers and market analysis',
  },
  agreePrivacyPolicy: {
    id: `${scope}.agreePrivacyPolicy`,
    defaultMessage: 'I agree to Gyllenhus {privacy}',
  },
  privacyPolicy: {
    id: `${scope}.privacyPolicy`,
    defaultMessage: 'Privacy Policy',
  },
  choosePassphrase: {
    id: `${scope}.choosePassphrase`,
    defaultMessage: 'Choose a Passphrase',
  },
  pleaseAgreePrivacyPolicy: {
    id: `${scope}.pleaseAgreePrivacyPolicy`,
    defaultMessage:
      'Please indicate that you have read and agree to the Terms and Conditions and Privacy Policy',
  },
  idNumberError: {
    id: `${scope}.idNumberError`,
    defaultMessage: 'Please enter a valid personal id number.',
  },
  regiesterTitleIntro: {
    id: `${scope}.regiesterTitleIntro`,
    defaultMessage: 'Register an account and experience the full potential of our platform',
  },
  leadingMarketPrices: {
    id: `${scope}.leadingMarketPrices`,
    defaultMessage: 'Leading Market Prices',
  },
  checkStatusTheOrders: {
    id: `${scope}.checkStatusTheOrders`,
    defaultMessage: 'Check the Status of Your Orders',
  },
  specialsAndDiscounts: {
    id: `${scope}.specialsAndDiscounts`,
    defaultMessage: 'Special Offers and Discounts',
  },
  
});
