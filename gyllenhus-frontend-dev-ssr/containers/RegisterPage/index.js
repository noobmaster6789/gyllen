/**
 *
 * RegisterPage
 *
 */

import PropTypes from 'prop-types';
import React, { memo, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import { USER_ROLE_BUSINESS, USER_ROLE_PERSONAL } from '../App/constants';
import { makeSelectIsLoggedIn } from '../App/selectors';
import { changeAcceptPrivacyPolicy, changeAccountType, changeAddress, changeCity, changeEmail, changeFullName, changeIdNumber, changePassphrase, changePhone, changePostalCode, changeRePassphrase, changeSubscribeStatus, registerLoadingError, resetFormInput, submitRegisterForm } from './actions';
import messages from './messages';
import reducer from './reducer';
import saga from './saga';
import { makeSelectAcceptPrivacyPolicy, makeSelectRegisterAccountType, makeSelectRegisterAddress, makeSelectRegisterCity, makeSelectRegisterEmail, makeSelectRegisterError, makeSelectRegisterFullName, makeSelectRegisterIdNumber, makeSelectRegisterLoading, makeSelectRegisterPassphrase, makeSelectRegisterPhone, makeSelectRegisterPostalCode, makeSelectRegisterRePassphrase, makeSelectRegisterUser, makeSelectSubscribeStatus } from './selectors';
import Router from 'next/router'


const idNumberRegex = /(^(((20)((0[0-9])|(1[0-1])))|(([1][^0-8])?\d{2}))((0[1-9])|1[0-2])((0[1-9])|(2[0-9])|(3[01]))[-]?\d{4}$)/;
const REACT_APP_API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT
const leadingMarketPricesImage = `${REACT_APP_API_ENDPOINT}/static/images/leading-market-prices.png`;
const checkStatusTheOrdersImage = `${REACT_APP_API_ENDPOINT}/static/images/check-order-status.png`;
const specialDiscountsImage = `${REACT_APP_API_ENDPOINT}/static/images/special-discounts.png`;

export function RegisterPage(props) {
  //useInjectReducer({ key: 'registerPage', reducer });
  //useInjectSaga({ key: 'registerPage', saga });
  useEffect(() => {
    const { dispatch } = props;
    dispatch(resetFormInput());
    const jwtToken = localStorage.getItem('jwtToken');
    if (jwtToken) {
      window.location.href = ("/thank-you")
    }
    // if (jwtToken || isLoggedIn) {
    //   // [TODO] inform successful registration and redirect to my account page.
    //   return <Redirect to="/" />;
    // }

  }, []);


  const {
    isLoggedIn,
    loading,
    error,
    user,
    accountType,
    fullName,
    address,
    idNumber,
    postalCode,
    email,
    city,
    phone,
    passphrase,
    rePassPhrase,
    acceptPrivacyPolicy,
    subscribeStatus,
    onSubmitRegisterForm,
    onChangeEmail,
    onChangeFullName,
    onChangeAddress,
    onChangeAccountType,
    onChangePostalCode,
    onChangeIdNumber,
    onChangeCity,
    onChangePhone,
    onChangePassphrase,
    onChangeRePassphrase,
    onChangeAcceptPrivacyPolicy,
    onChangeSubscribeStatus,
    dispatch,

  } = props;
  const submitHandle = evt => {
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    if (!idNumberRegex.test(props.idNumber)) {
      dispatch(
        registerLoadingError({
          message: <FormattedMessage {...messages.idNumberError} />,
        }),
      );
      return;
    }
    if (acceptPrivacyPolicy && String(passphrase) === String(rePassPhrase)) {
      onSubmitRegisterForm(evt);
    } else {
      let newError = {};
      if (passphrase !== rePassPhrase) {
        newError = {
          message: <FormattedMessage {...messages.passphraseNotMatch} />,
        };
      } else if (!acceptPrivacyPolicy) {
        newError = {
          message: <FormattedMessage {...messages.pleaseAgreePrivacyPolicy} />,
        };
      }

      dispatch(registerLoadingError(newError));
    }
  };




  return (
    <React.Fragment>
      <Helmet>
        <title>Register</title>
        <meta name="description" content="" />
      </Helmet>
      <div className="wrapper user-portal">
        <div className="half signup">
          <nav className="tab">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a
              className={accountType === USER_ROLE_PERSONAL ? 'active' : ''}
              href="#"
              onClick={() => onChangeAccountType(USER_ROLE_PERSONAL)}
            >
              <FormattedMessage {...messages.personalAccount} />
            </a>
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a
              href="#"
              className={accountType === USER_ROLE_BUSINESS ? 'active' : ''}
              onClick={() => onChangeAccountType(USER_ROLE_BUSINESS)}
            >
              <FormattedMessage {...messages.businessAccount} />
            </a>
          </nav>
          <form className="form-register" onSubmit={submitHandle}>
            <h2>Personal Details</h2>
            <div className="form-wrap">
              <div className="form-group one-two">
                <input
                  type="text"
                  id="full-name"
                  className={`input-text ${fullName !== '' ? 'has-value' : ''}`}
                  required="required"
                  autoComplete="new-full-name"
                  value={fullName}
                  onChange={onChangeFullName}
                />
                <div className="placeholder">
                  <FormattedMessage {...messages.fullName} />
                </div>
              </div>
              <div className="form-group one-two">
                <input
                  type="text"
                  id="address"
                  className={`input-text ${address !== '' ? 'has-value' : ''}`}
                  required="required"
                  autoComplete="off"
                  value={address}
                  onChange={onChangeAddress}
                />
                <div className="placeholder">
                  <FormattedMessage {...messages.address} />
                </div>
              </div>
              <div className="form-group one-two">
                <input
                  type="text"
                  id="personal-id-number"
                  className={`input-text ${idNumber !== '' ? 'has-value' : ''}`}
                  autoComplete="new-id-number"
                  value={idNumber}
                  onChange={onChangeIdNumber}
                  pattern="[0-9\-]*"
                  maxLength={11}
                  required="required"
                />
                <div className="placeholder">
                  <FormattedMessage {...messages.idNumber} />
                </div>
              </div>
              <div className="form-group one-two">
                <input
                  type="text"
                  id="postal-code"
                  className={`input-text ${
                    postalCode !== '' ? 'has-value' : ''
                    }`}
                  autoComplete="new-postal-code"
                  value={postalCode}
                  maxLength={32}
                  pattern="[a-zA-Z0-9-]+"
                  onChange={onChangePostalCode}
                />
                <div className="placeholder">
                  <FormattedMessage {...messages.postalCode} />
                </div>
              </div>
              <div className="form-group one-two">
                <input
                  type=""
                  id="email"
                  className={`input-text ${email !== '' ? 'has-value' : ''}`}
                  required="required"
                  autoComplete="new-email"
                  value={email}
                  onChange={onChangeEmail}
                />
                <div className="placeholder">
                  <FormattedMessage {...messages.email} />
                </div>
              </div>
              <div className="form-group one-two">
                <input
                  type="text"
                  id="city"
                  className={`input-text ${city !== '' ? 'has-value' : ''}`}
                  autoComplete="new-city"
                  value={city}
                  onChange={onChangeCity}
                />
                <div className="placeholder">
                  <FormattedMessage {...messages.city} />
                </div>
              </div>
              <div className="form-group one-two">
                <input
                  type="tel"
                  id="phone"
                  className={`input-text ${phone !== '' ? 'has-value' : ''}`}
                  autoComplete="new-phone"
                  value={phone}
                  onChange={onChangePhone}
                  pattern="[0-9\+]*"
                />
                <div className="placeholder">
                  <FormattedMessage {...messages.phone} />
                </div>
              </div>
            </div>
            <h2>
              <FormattedMessage {...messages.choosePassphrase} />
            </h2>
            <div className="form-wrap">
              <div className="form-group one-two">
                <input
                  type="password"
                  id="passphrase"
                  className={`input-text ${
                    passphrase !== '' ? 'has-value' : ''
                    }`}
                  required="required"
                  value={passphrase}
                  autoComplete="new-password"
                  minLength={6}
                  maxLength={30}
                  onChange={onChangePassphrase}
                />
                <div className="placeholder">
                  <FormattedMessage {...messages.passphrase} />
                </div>
              </div>
            </div>
            <div className="form-wrap">
              <div className="form-group one-two">
                <input
                  type="password"
                  id="rePassphrase"
                  className={`input-text ${
                    rePassPhrase !== '' ? 'has-value' : ''
                    }`}
                  required="required"
                  value={rePassPhrase}
                  autoComplete="new-password"
                  onChange={onChangeRePassphrase}
                />
                <div className="placeholder">
                  <FormattedMessage {...messages.rePassphrase} />
                </div>
              </div>
            </div>

            <div className="checkbox-group">
              <label htmlFor="privacy" className="checkbox">
                <input
                  type="checkbox"
                  name="privacy"
                  id="privacy"
                  value={acceptPrivacyPolicy}
                  checked={acceptPrivacyPolicy}
                  onChange={onChangeAcceptPrivacyPolicy}
                />
                <span className="tick">
                  <i />
                </span>
                <span className="text">
                  <FormattedMessage
                    {...messages.agreePrivacyPolicy}
                    values={{
                      privacy: (
                        // eslint-disable-next-line jsx-a11y/anchor-is-valid
                        <a href="#">
                          <FormattedMessage {...messages.privacyPolicy} />
                        </a>
                      ),
                    }}
                  />
                </span>
              </label>
              <label htmlFor="offer" className="checkbox">
                <input
                  type="checkbox"
                  name="subscribe"
                  id="offer"
                  value={1}
                  checked={subscribeStatus}
                  onChange={onChangeSubscribeStatus}
                />
                <span className="tick">
                  <i />
                </span>
                <span className="text">
                  <FormattedMessage {...messages.receiveSpecialOffers} />
                </span>
              </label>
            </div>
            <div className="alert error" style={{ color: 'red' }}>
              <span>{(!loading && error && error.message) || null}</span>
            </div>
            <br />
            <button className="button" type="submit" disabled={loading}>
              <i className="fas fa-sign-in-alt" />{' '}
              {(accountType === USER_ROLE_PERSONAL) ?
                <FormattedMessage {...messages.registerPersonalAccount} />
                : <FormattedMessage {...messages.registerBusinessAccount} />
              }
            </button>
            <hr />
          </form>
        </div>
        <div className="half intro">
          <h1><FormattedMessage {...messages.regiesterTitleIntro} /></h1>
          <div class="grid-symbol-text">
            <div>
              <img src={leadingMarketPricesImage} />
              <p><FormattedMessage {...messages.leadingMarketPrices} /></p>
            </div>
            <div>
              <img src={checkStatusTheOrdersImage} />
              <p><FormattedMessage {...messages.checkStatusTheOrders} /> </p>
            </div>
            <div>
              <img src={specialDiscountsImage} />
              <p><FormattedMessage {...messages.specialsAndDiscounts} /></p>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

RegisterPage.propTypes = {
  isLoggedIn: PropTypes.bool,
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  user: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  accountType: PropTypes.oneOf([USER_ROLE_PERSONAL, USER_ROLE_BUSINESS]),
  fullName: PropTypes.string.isRequired,
  address: PropTypes.string,
  // eslint-disable-next-line consistent-return
  idNumber(props, propName, componentName) {
    const value = props[propName];
    if (value === '' || value.length < 13) {
      return null;
    }
    if (!idNumberRegex.test(value)) {
      return new Error(
        `Invalid prop ${propName} supplied to ${componentName}. Validation failed.`,
      );
    }
  },
  postalCode: PropTypes.string,
  email: PropTypes.string.isRequired,
  city: PropTypes.string,
  phone: PropTypes.string,
  passphrase: PropTypes.string.isRequired,
  rePassPhrase: PropTypes.string.isRequired,
  acceptPrivacyPolicy: PropTypes.bool.isRequired,
  subscribeStatus: PropTypes.bool,
  onChangeAccountType: PropTypes.func,
  onChangeFullName: PropTypes.func,
  onChangeAddress: PropTypes.func,
  onChangeIdNumber: PropTypes.func,
  onChangePostalCode: PropTypes.func,
  onChangeEmail: PropTypes.func,
  onChangeCity: PropTypes.func,
  onChangePhone: PropTypes.func,
  onChangePassphrase: PropTypes.func,
  onChangeRePassphrase: PropTypes.func,
  onChangeAcceptPrivacyPolicy: PropTypes.func,
  onChangeSubscribeStatus: PropTypes.func,
  onSubmitRegisterForm: PropTypes.func,
  dispatch: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  isLoggedIn: makeSelectIsLoggedIn(),
  loading: makeSelectRegisterLoading(),
  error: makeSelectRegisterError(),
  user: makeSelectRegisterUser(),
  accountType: makeSelectRegisterAccountType(),
  fullName: makeSelectRegisterFullName(),
  address: makeSelectRegisterAddress(),
  idNumber: makeSelectRegisterIdNumber(),
  postalCode: makeSelectRegisterPostalCode(),
  email: makeSelectRegisterEmail(),
  city: makeSelectRegisterCity(),
  phone: makeSelectRegisterPhone(),
  passphrase: makeSelectRegisterPassphrase(),
  rePassPhrase: makeSelectRegisterRePassphrase(),
  acceptPrivacyPolicy: makeSelectAcceptPrivacyPolicy(),
  subscribeStatus: makeSelectSubscribeStatus(),
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeAccountType: type => dispatch(changeAccountType(type)),
    onChangeFullName: evt => dispatch(changeFullName(evt.target.value)),
    onChangeAddress: evt => dispatch(changeAddress(evt.target.value)),
    onChangeIdNumber: evt => {
      if (evt.target.validity.valid || evt.target.value === '') {
        dispatch(changeIdNumber(evt.target.value));
      }
    },
    onChangePostalCode: evt => {
      if (evt.target.validity.valid) {
        dispatch(changePostalCode(evt.target.value));
      }
    },
    onChangeEmail: evt => dispatch(changeEmail(evt.target.value)),
    onChangeCity: evt => dispatch(changeCity(evt.target.value)),
    onChangePhone: evt => {
      if (evt.target.validity.valid) {
        dispatch(changePhone(evt.target.value));
      }
    },
    onChangePassphrase: evt => dispatch(changePassphrase(evt.target.value)),
    onChangeRePassphrase: evt => dispatch(changeRePassphrase(evt.target.value)),
    onSubmitRegisterForm: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(submitRegisterForm());
    },
    dispatch,
    onChangeAcceptPrivacyPolicy: evt =>
      dispatch(changeAcceptPrivacyPolicy(evt.target.checked)),
    onChangeSubscribeStatus: evt =>
      dispatch(changeSubscribeStatus(evt.target.checked)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect
)(RegisterPage);
