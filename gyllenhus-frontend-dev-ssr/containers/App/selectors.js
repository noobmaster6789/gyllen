/**
 * The global state selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectGlobal = state => state.global || initialState;

const makeSelectIndexes = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.indexes,
  );

const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading,
  );

const makeSelectError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.error,
  );

const makeSelectCurrency = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.currency,
  );

const makeSelectGoldPrice = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.goldPrice,
  );
const makeSelectSilverPrice = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.silverPrice,
  );

const makeSelectPlatinumPrice = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.platinumPrice,
  );
const makeSelectPalladiumPrice = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.palladiumPrice,
  );

const makeSelectCountDownTime = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.countdown,
  );

const makeSelectIsLoggedIn = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.isLoggedIn,
  );

const makeSelectJwtToken = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.jwtToken,
  );

const makeSelectLoginLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loginUser && globalState.loginUser.loading,
  );

const makeSelectLoginError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loginUser && globalState.loginUser.error,
  );
const makeSelectLoginEmail = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loginUser && globalState.loginUser.email,
  );

const makeSelectLoginPassword = () =>
  createSelector(
    selectGlobal,
    globaState => globaState.loginUser && globaState.loginUser.password,
  );

const makeSelectLoginKeepMeLoggedIn = () =>
  createSelector(
    selectGlobal,
    globalState =>
      globalState.loginUser && globalState.loginUser.keepMeLoggedIn,
  );

const makeSelectLoginShowModal = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loginUser && globalState.loginUser.showModal,
  );

const makeSelectForgotPasswordModal = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.showForgotPasswordModal,
  );

const makeSelectForgotPasswordEmail = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.emailForgot,
  );

const makeSelectForgotMessageError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.messageError,
  );
const makeSelectForgotSuccess = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.forgotSuccess,
  );

const makeSelectForgotPasswordButtonLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading,
  );

const makeSelectMenuListLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.menu && globalState.menu.loading,
  );

const makeSelectMenuListError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.menu && globalState.menu.error,
  );

const makeSelectMenuListData = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.menu && globalState.menu.data,
  );

const makeSelectMenuListName = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.menu && globalState.menu.name,
  );

const makeSelectDelayTime = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.delayTime,
  );

const makeSelectOpenPrice = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.openPrices,
  );

const makeSelectGoldRealtime = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.goldPriceRealtime,
  );

const makeSelectSilverRealtime = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.silverPriceRealtime,
  );

const makeSelectUserInfo = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loginUser.info,
  );

const makeSelectPriceRealtimeCache = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.priceRealtime,
  );

const makeSelectRefreshTime = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.refreshTime,
  );

const makeSelectCollapse = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.collapse,
  );

const makeSelectPreviousUpdateTime = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.previousUpdateTime,
  );

const makeSelectTextSearch = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.textSearch,
  );

const makeSelectSearchResult = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.searchResult,
  );

const makeSelectSearchActive = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.searchActive,
  );

export {
  selectGlobal,
  makeSelectLoading,
  makeSelectError,
  makeSelectIndexes,
  makeSelectCurrency,
  makeSelectGoldPrice,
  makeSelectSilverPrice,
  makeSelectPlatinumPrice,
  makeSelectPalladiumPrice,
  makeSelectCountDownTime,
  makeSelectIsLoggedIn,
  makeSelectJwtToken,
  makeSelectLoginEmail,
  makeSelectLoginPassword,
  makeSelectLoginError,
  makeSelectLoginLoading,
  makeSelectLoginKeepMeLoggedIn,
  makeSelectLoginShowModal,
  makeSelectForgotPasswordModal,
  makeSelectForgotPasswordEmail,
  makeSelectForgotMessageError,
  makeSelectForgotSuccess,
  makeSelectForgotPasswordButtonLoading,
  makeSelectMenuListLoading,
  makeSelectMenuListError,
  makeSelectMenuListData,
  makeSelectMenuListName,
  makeSelectDelayTime,
  makeSelectOpenPrice,
  makeSelectGoldRealtime,
  makeSelectSilverRealtime,
  makeSelectUserInfo,
  makeSelectPriceRealtimeCache,
  makeSelectRefreshTime,
  makeSelectCollapse,
  makeSelectPreviousUpdateTime,
  makeSelectTextSearch,
  makeSelectSearchResult,
  makeSelectSearchActive
};
