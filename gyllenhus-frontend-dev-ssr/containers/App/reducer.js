/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import { find } from 'lodash';
import Cookie from 'js-cookie'

// import { act } from 'react-testing-library';
import {
  LOAD_INDEXES,
  LOAD_INDEXES_SUCCESS,
  LOAD_INDEXES_ERROR,
  CHANGE_CURRENCY,
  LOAD_GOLD_PRICE,
  LOAD_SILVER_PRICE,
  LOAD_PLATINUM_PRICE,
  LOAD_PALLADIUM_PRICE,
  COUNTDOWN_TIME,
  LOAD_USER_BY_JWT_TOKEN,
  JWT_TOKEN_VALID,
  // USER_LOGOUT,
  USER_LOGOUT_SUCCESS,
  JWT_TOKEN_INVALID,
  RESET_JWT_TOKEN,
  UPDATE_JWT_TOKEN,
  CHANGE_DELAY_TIME,
  // LOAD_OPEN_PRICE,
  // LOAD_REALTIME_PRICE,
  LOAD_REALTIME_PRICE_SUCCESS,
  LOAD_OPEN_PRICE_SUCCESS,
  LOAD_USER_INFO_SUCCESS,
  LOAD_PRICE_LIST_SETTINGS,
  LOAD_SERVER_TIMESTAMP,
  CHANGE_COLLAPSE,
} from './constants';
import {
  CHANGE_EMAIL,
  CHANGE_KEEP_ME_LOGIN,
  CHANGE_PASSWORD,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  SUBMIT_LOGIN_FORM,
  SHOW_LOGIN_MODAL,
} from '../LoginModal/constants';
import {
  LOAD_MENU,
  LOAD_MENU_ERROR,
  LOAD_MENU_SUCCESS,
} from '../MenuList/constants';
import { CHANGE_TEXT_SEARCH, SEARCH_RESULT_SUCCESS, SEARCH_RESULT_EMPTY, CHANGE_SEARCH_ACTIVE } from '../Search/constants';
import { SHOW_FORGOT_PASSWORD_MODAL, CHANGE_FORGOT_PASSWORD_EMAIL, FORGOT_PASSWORD_ERROR, FORGOT_PASSWORD_SUCCESS, SUBMIT_FORGOT_PASSWORD } from '../ForgotPasswordModal/constants';

/* The initial state of the app */
export const initialState = {
  delayTime: 2000,
  refreshTime: 0,
  previousUpdateTime: 0,
  loading: false,
  error: false,
  indexes: false,
  goldPrice: null,
  silverPrice: null,
  platinumPrice: null,
  palladiumPrice: null,
  openPrices: null,
  priceRealtime: null,
  goldPriceRealtime: null,
  silverPriceRealtime: null,
  quantity: 1,
  countdown: 0,
  jwtToken: '',
  isLoggedIn: false,
  loginUser: {
    showModal: false,
    loading: false,
    error: false,
    email: '',
    password: '',
    keepMeLoggedIn: false,
    info: null,
  },
  showForgotPasswordModal: false,
  forgotSuccess: false,
  loading: false,
  menu: {
    name: '',
    loading: false,
    error: false,
    data: false,
  },
  textSearch: ''
};

/* eslint-disable default-case, no-param-reassign */
const appReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case LOAD_INDEXES:
        draft.loading = true;
        draft.error = false;
        draft.indexes = false;
        break;

      case LOAD_INDEXES_SUCCESS:
        draft.indexes = action.indexes;
        draft.loading = false;
        break;

      case LOAD_INDEXES_ERROR:
        draft.error = action.error;
        draft.loading = false;
        break;
      case CHANGE_CURRENCY:
        draft.currency = action.currency;
        //localStorage.setItem('currency', action.currency);
        break;
      case LOAD_GOLD_PRICE:
        // eslint-disable-next-line no-case-declarations
        // const golSymbol = `XAU${action.currency}`;
        // eslint-disable-next-line no-case-declarations
        const golSymbol = `XAUSEK`;
        // eslint-disable-next-line no-case-declarations
        const { indexes } = action;
        // eslint-disable-next-line no-case-declarations
        const goldPrice = find(indexes, { symbol: golSymbol });
        draft.goldPrice = goldPrice;
        break;
      case LOAD_SILVER_PRICE:
        // eslint-disable-next-line no-case-declarations
        // const silverSymbol = `XAG${action.currency}`;
        // eslint-disable-next-line no-case-declarations
        const silverSymbol = `XAGSEK`;
        // eslint-disable-next-line no-case-declarations
        const silverPrice = find(action.indexes, { symbol: silverSymbol });
        draft.silverPrice = silverPrice;
        break;
      case LOAD_PLATINUM_PRICE:
        // eslint-disable-next-line no-case-declarations
        // const platinumSymbol = `PLA${action.currency}`;
        // eslint-disable-next-line no-case-declarations
        const platinumSymbol = `PLASEK`;
        // eslint-disable-next-line no-case-declarations
        const platinumPrice = find(action.indexes, {
          symbol: platinumSymbol,
        });
        draft.platinumPrice = platinumPrice;
        break;
      case LOAD_PALLADIUM_PRICE:
        // eslint-disable-next-line no-case-declarations
        // const palladiumSymbol = `PAL${action.currency}`;
        // eslint-disable-next-line no-case-declarations
        const palladiumSymbol = `PALSEK`;
        // eslint-disable-next-line no-case-declarations
        const palladiumPrice = find(action.indexes, {
          symbol: palladiumSymbol,
        });
        draft.palladiumPrice = palladiumPrice;
        break;
      case COUNTDOWN_TIME:
        draft.countdown -= 1;
        break;
      case CHANGE_DELAY_TIME:
        draft.countdown = action.delayTime;
        draft.refreshTime = action.refreshTime;
        break;
      case LOAD_USER_BY_JWT_TOKEN:
        draft.jwtToken = action.jwtToken;
        break;
      case JWT_TOKEN_VALID:
        draft.isLoggedIn = true;
        break;
      case JWT_TOKEN_INVALID:
        localStorage.removeItem('jwtToken');
        draft.isLoggedIn = false;
        break;
      case RESET_JWT_TOKEN:
        draft.jwtToken = action.token;
        break;
      case UPDATE_JWT_TOKEN:
        draft.jwtToken = action.newToken;
        localStorage.setItem('jwtToken', action.newToken);
        break;
      case USER_LOGOUT_SUCCESS:
        localStorage.removeItem('jwtToken');
        localStorage.removeItem('userInfo');
        Cookie.remove('userInfoID')
        draft.jwtToken = '';
        draft.isLoggedIn = false;
        draft.loginUser.showModal = false;
        draft.loginUser.info = null;
        break;
      case CHANGE_EMAIL:
        draft.loginUser.email = action.email;
        break;
      case CHANGE_PASSWORD:
        draft.loginUser.password = action.password;
        break;
      case CHANGE_KEEP_ME_LOGIN:
        draft.loginUser.keepMeLoggedIn = action.checked;
        break;
      case SUBMIT_LOGIN_FORM:
        draft.loginUser.loading = true;
        draft.loginUser.error = false;
        break;
      case LOGIN_SUCCESS:
        draft.jwtToken = action.token;
        draft.isLoggedIn = true;
        draft.loginUser.loading = false;
        draft.loginUser.error = false;
        localStorage.setItem('jwtToken', action.token);
        draft.loginUser.showModal = false;
        break;
      case LOGIN_ERROR:
        draft.jwtToken = '';
        draft.isLoggedIn = false;
        draft.loginUser.loading = false;
        draft.loginUser.error = action.error;
        break;
      case SHOW_LOGIN_MODAL:
        draft.loginUser.showModal = action.show;
        draft.loginUser.email = '';
        draft.loginUser.password = '';
        draft.loginUser.keepMeLoggedIn = false;
        break;
      case SUBMIT_FORGOT_PASSWORD:
        draft.loading = true;
        break;
      case CHANGE_FORGOT_PASSWORD_EMAIL:
        draft.emailForgot = action.email;
        break;
      case SHOW_FORGOT_PASSWORD_MODAL:
        draft.loginUser.showModal = false;
        draft.showForgotPasswordModal = action.show;
        draft.forgotSuccess = false;
        break;
      case FORGOT_PASSWORD_ERROR:
        draft.forgotSuccess = false;
        draft.loading = false;
        draft.messageError = "Email does not exist.";
        break;
      case FORGOT_PASSWORD_SUCCESS:
        draft.messageError = "";
        draft.loading = false;
        draft.forgotSuccess = true;
        break;
      case LOAD_MENU:
        draft.menu.name = action.name;
        draft.menu.loading = true;
        draft.menu.error = false;
        draft.menu.data = false;
        break;
      case LOAD_MENU_SUCCESS:
        draft.menu.loading = false;
        draft.menu.error = false;
        draft.menu.data = action.menus;
        break;
      case LOAD_MENU_ERROR:
        draft.menu.loading = false;
        draft.menu.error = action.error;
        draft.menu.data = false;
        break;
      case LOAD_OPEN_PRICE_SUCCESS:
        draft.openPrices = action.openPrices;
        break;
      case LOAD_REALTIME_PRICE_SUCCESS:
        if (typeof draft.currency === undefined)
          draft.currency = action.currency;
        // eslint-disable-next-line no-case-declarations
        const goldSymbolRealtime = `XAU${draft.currency}`;
        // eslint-disable-next-line no-case-declarations
        const silverSymbolRealTime = `XAG${draft.currency}`;
        // eslint-disable-next-line no-case-declarations
        const goldPriceRealTime = find(action.data, {
          symbol: goldSymbolRealtime,
        });
        // eslint-disable-next-line no-case-declarations
        const silverPriceReaTtime = find(action.data, {
          symbol: silverSymbolRealTime,
        });

        draft.goldPriceRealtime = goldPriceRealTime;
        draft.silverPriceRealtime = silverPriceReaTtime;
        draft.priceRealtime = action.data;
        break;
      case LOAD_USER_INFO_SUCCESS:
        localStorage.setItem('userInfo', JSON.stringify(action.info));
        Cookie.set('userInfoID', action.info.ID)
        draft.loginUser.info = action.info;
        break;
      case LOAD_PRICE_LIST_SETTINGS:
        draft.refreshTime = action.refreshTime;
        draft.previousUpdateTime = action.previousUpdateTime;
        break;
      case LOAD_SERVER_TIMESTAMP:
        const { timestamp } = action;
        const { refreshTime, previousUpdateTime } = draft;
        if (refreshTime > 0) {
          const newCountdown = draft.refreshTime - ((timestamp - previousUpdateTime) % refreshTime) - 1;
          draft.countdown = newCountdown;
        }
        break;
      case CHANGE_TEXT_SEARCH:
        draft.textSearch = action.textSearch;
        draft.searchResult = [];
        break;
      case SEARCH_RESULT_SUCCESS:
        draft.searchResult = action.products;
        break;
      case SEARCH_RESULT_EMPTY:
        draft.searchResult = [];
        break;
      case CHANGE_SEARCH_ACTIVE:
        draft.searchActive = action.value;
        break;
      case CHANGE_COLLAPSE:
        draft.collapse = action.value;
        break;

    }
  });

export default appReducer;
