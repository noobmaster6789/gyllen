/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import Header from 'components/Header';
import TradingViewPage from 'containers/TradingViewPage/Loadable';

import LoginModal from 'containers/LoginModal';
import GlobalStyle from '../../global-styles';
import FooterList from '../FooterList';

export default function App() {
  const protectedComponent = component => (
    <div>
      <Header />
      {component}
      <FooterList />
    </div>
  );

  return (
    <React.Fragment>
      <Helmet
        titleTemplate="%s | Expert on the value that endures"
        defaultTitle="Gyllenhus"
      >
        <meta name="description" content="A Gyllenhus application" />
      </Helmet>
      <Switch>
        <Route
          exact
          path="/trading-view"
          render={() => protectedComponent(<TradingViewPage />)}
        />
      </Switch>
      <LoginModal />
      <GlobalStyle />
    </React.Fragment>
  );
}
