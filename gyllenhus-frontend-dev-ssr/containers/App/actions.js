/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */
import {
  LOAD_INDEXES,
  LOAD_INDEXES_ERROR,
  LOAD_INDEXES_SUCCESS,
  CHANGE_CURRENCY,
  LOAD_GOLD_PRICE,
  LOAD_SILVER_PRICE,
  LOAD_PALLADIUM_PRICE,
  LOAD_PLATINUM_PRICE,
  COUNTDOWN_TIME,
  LOAD_USER_BY_JWT_TOKEN,
  JWT_TOKEN_VALID,
  USER_LOGOUT,
  JWT_TOKEN_INVALID,
  RESET_JWT_TOKEN,
  UPDATE_JWT_TOKEN,
  CHANGE_DELAY_TIME,
  LOAD_OPEN_PRICE,
  LOAD_OPEN_PRICE_SUCCESS,
  LOAD_REALTIME_PRICE,
  LOAD_REALTIME_PRICE_SUCCESS,
  LOAD_USER_INFO,
  LOAD_USER_INFO_SUCCESS,
  USER_LOGOUT_SUCCESS,
  LOAD_PRICE_LIST_SETTINGS,
  LOAD_SERVER_TIMESTAMP,
  CHANGE_COLLAPSE,
} from './constants';
import {
  CHANGE_EMAIL,
  CHANGE_KEEP_ME_LOGIN,
  CHANGE_PASSWORD,
  LOGIN_SUCCESS,
  SUBMIT_LOGIN_FORM,
  LOGIN_ERROR,
  SHOW_LOGIN_MODAL,
} from '../LoginModal/constants';
import { SHOW_FORGOT_PASSWORD_MODAL, CHANGE_FORGOT_PASSWORD_EMAIL, SUBMIT_FORGOT_PASSWORD, FORGOT_PASSWORD_ERROR, FORGOT_PASSWORD_SUCCESS } from '../ForgotPasswordModal/constants';

/**
 * Load the indexes, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_INDEXES
 */
export function loadIndexes() {
  return {
    type: LOAD_INDEXES,
  };
}

/**
 * Dispatched when the repositories are loaded by the request saga
 *
 * @param  {array} indexes The gold price data
 *
 * @return {object}      An action object with a type of LOAD_INDEXES_SUCCESS passing the indexes
 */
export function indexesLoaded(indexes) {
  return {
    type: LOAD_INDEXES_SUCCESS,
    indexes,
  };
}

/**
 * Dispatched when loading the indexes fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of LOAD_INDEXES_ERROR passing the error
 */
export function indexLoadingError(error) {
  return {
    type: LOAD_INDEXES_ERROR,
    error,
  };
}

/**
 * Dispatched when user change currency
 * @param {string} currency the currency name
 * @return an action with a type of CHANGE_CURRENCY passing the data
 * */
export function changeCurrency(currency = 'SEK') {
  return {
    type: CHANGE_CURRENCY,
    currency,
  };
}

export function loadGoldPrice(indexes, currency) {
  return {
    type: LOAD_GOLD_PRICE,
    indexes,
    currency,
  };
}

export function loadSilverPrice(indexes, currency) {
  return {
    type: LOAD_SILVER_PRICE,
    indexes,
    currency,
  };
}

export function loadPlatinumPrice(indexes, currency) {
  return {
    type: LOAD_PLATINUM_PRICE,
    indexes,
    currency,
  };
}

export function loadPalladiumPrice(indexes, currency) {
  return {
    type: LOAD_PALLADIUM_PRICE,
    indexes,
    currency,
  };
}

export function loadOpenPrice() {
  return {
    type: LOAD_OPEN_PRICE,
  };
}

export function loadedOpenPrice(openPrices) {
  return {
    type: LOAD_OPEN_PRICE_SUCCESS,
    openPrices,
  };
}

export function loadPriceRealtime() {
  return {
    type: LOAD_REALTIME_PRICE,
  };
}

export function loadedPriceRealtime(data, currency="SEK") {
  return {
    type: LOAD_REALTIME_PRICE_SUCCESS,
    data,
    currency
  };
}

export function changeCollapse(value) {
  return {
    type: CHANGE_COLLAPSE,
    value
  };
}

export function changeCountDownTime(countdown) {
  return {
    type: COUNTDOWN_TIME,
    countdown,
  };
}

export function changeDelayTime(refreshTime, delayTime) {
  return {
    type: CHANGE_DELAY_TIME,
    refreshTime,
    delayTime,
  };
}

export function loadUserByJwtToken(jwtToken) {
  return {
    type: LOAD_USER_BY_JWT_TOKEN,
    jwtToken,
  };
}

export function jwtTokenValid({ code, data }) {
  return {
    type: JWT_TOKEN_VALID,
    code,
    data,
  };
}

export function jwTokenInvalid() {
  return {
    type: JWT_TOKEN_INVALID,
  };
}

export function resetJwtToken(token) {
  return {
    type: RESET_JWT_TOKEN,
    token,
  };
}

export function updateJwtToken(newToken) {
  return {
    type: UPDATE_JWT_TOKEN,
    newToken,
  };
}
export function logout() {
  return {
    type: USER_LOGOUT,
  };
}

export function logoutSuccess() {
  return {
    type: USER_LOGOUT_SUCCESS,
  };
}

export function changeEmail(email) {
  return {
    type: CHANGE_EMAIL,
    email,
  };
}

export function changePassword(password) {
  return {
    type: CHANGE_PASSWORD,
    password,
  };
}

export function changeKeepMeLogin(checked) {
  return {
    type: CHANGE_KEEP_ME_LOGIN,
    checked,
  };
}

export function submitLogin() {
  return {
    type: SUBMIT_LOGIN_FORM,
  };
}

export function loginSuccess(token) {
  return {
    type: LOGIN_SUCCESS,
    token,
  };
}

export function loginError(error) {
  return {
    type: LOGIN_ERROR,
    error,
  };
}

export function showLoginModal(show = true) {
  return {
    type: SHOW_LOGIN_MODAL,
    show,
  };
}

export function showForgotPasswordModal(show = true) {
  return {
    type: SHOW_FORGOT_PASSWORD_MODAL,
    show,
  };
}

export function changeEmailForgot(email) {
  return {
    type: CHANGE_FORGOT_PASSWORD_EMAIL,
    email,
  };
}

export function submitForgotPassword() {
  return {
    type: SUBMIT_FORGOT_PASSWORD,
  };
}

export function forgotPasswordError() {
  return {
    type: FORGOT_PASSWORD_ERROR,
  };
}

export function forgotPasswordSuccess() {
  return {
    type: FORGOT_PASSWORD_SUCCESS,
  };
}

export function loadUserInfo() {
  return {
    type: LOAD_USER_INFO,
  };
}

export function loadUserInfoSuccess(info) {
  return {
    type: LOAD_USER_INFO_SUCCESS,
    info,
  };
}

export function loadPriceListSettings(refreshTime, previousUpdateTime) {
  return {
    type: LOAD_PRICE_LIST_SETTINGS,
    refreshTime,
    previousUpdateTime
  };
}

export function loadServerTimeStamp(timestamp) {
  return {
    type: LOAD_SERVER_TIMESTAMP,
    timestamp
  };
}