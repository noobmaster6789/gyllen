/**
 *
 * ProductListItem
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { createStructuredSelector } from 'reselect';
import Brand from '../../components/Brand';
import ProductName from '../../components/ProductName';
import Img from '../../components/Img';
import ProductWeight from '../../components/ProductWeight';
import {
  makeSelectCurrency,
  makeSelectGoldPrice,
  makeSelectSilverPrice,
  makeSelectPalladiumPrice,
  makeSelectPlatinumPrice,
} from '../../containers/App/selectors';
import { FormattedMessage } from 'react-intl';
import ButtonGroup from './ButtonGroup';
import QuantityInput from '../../components/QuantityInput';

import messages from './messages';
import Router from 'next/router';
import { getBuySellPriceProduct } from '../../utils/commons';
import { buyAddToCart, sellAddToCart } from '../CartPage/actions';

const MAX_QUANTITY = 999;
export class ProductListItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = { quantity: 1, animationClassBuySell: "", isFirstLoaded: true };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeQuantity = this.handleChangeQuantity.bind(this);
    this.handleBuy = this.handleBuy.bind(this);
    this.handleSell = this.handleSell.bind(this);

  }

  handleChange(event) {
    let { value } = event.target;
    value = Number(value);
    this.setState({ quantity: this.getQuantity(value) });
  }

  handleChangeQuantity(value) {
    let { quantity } = this.state;
    quantity = Number(quantity);
    if (quantity + value <= 0) {
      quantity = 1;
    } else
    if (quantity + value > 999) {
      quantity = MAX_QUANTITY;
    } else quantity = quantity + value;
    this.setState({ quantity: quantity });
  }

  getQuantity(value) {
    let quantity = isNaN(value) ? 1 : Number(value);
    quantity = quantity > 0 ? quantity : 1;
    quantity = quantity > MAX_QUANTITY ? MAX_QUANTITY : quantity;
    return quantity;
  }

  getValuePrice(isBuyType) {
    const {
      item: {
        product,
        images,
        productPurity,
        productVat,
        productWeight,
        typeProduct,
        typeBrands,
        productBuyMargin,
        productSellMargin,
      },
      currency,
      goldPrice,
      silverPrice,
      platinumPrice,
      palladiumPrice
    } = this.props;

    return getBuySellPriceProduct(typeProduct, goldPrice, silverPrice, platinumPrice, palladiumPrice,
      productWeight, (isBuyType ? productBuyMargin : productSellMargin), productPurity, productVat);
  }

  handleSell() {
    if (!this.props.goldPrice || !this.props.silverPrice || !this.props.palladiumPrice)
      return;
    let { quantity } = this.state;
    this.props.onSellAddToCart(this.props, quantity);
  }

  handleBuy() {
    if (!this.props.goldPrice || !this.props.silverPrice || !this.props.palladiumPrice)
      return;
    let { quantity } = this.state;
    this.props.onBuyAddToCart(this.props, quantity);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.goldPrice !== this.props.goldPrice) {
      if (this.state.isFirstLoaded) {
        this.setState({ isFirstLoaded: false });
      } else { 
      this.setState({ animationClassBuySell: "blink" });
      setTimeout(() => {
        this.setState({ animationClassBuySell: "" });
      }, 1000);
    }
  }
}

render() {
  const {
    item: {
      product,
      images,
      productPurity,
      productVat,
      productWeight,
      typeProduct,
      typeBrands,
      productBuyMargin,
      productSellMargin,
    },
    currency,
    goldPrice,
    silverPrice,
    platinumPrice,
    palladiumPrice,
  } = this.props;

  let buyPrice = null;
  let sellPrice = null;
  if (
    goldPrice &&
    silverPrice &&
    platinumPrice &&
    palladiumPrice &&
    typeof goldPrice === 'object' &&
    typeof silverPrice === 'object' &&
    typeof platinumPrice === 'object' &&
    typeof palladiumPrice === 'object'
  ) {

    if (typeProduct === 'platinum') {
      sellPrice = platinumPrice.price;
      buyPrice = platinumPrice.price;
    } else if (typeProduct === 'palladium') {
      sellPrice = palladiumPrice.price;
      buyPrice = palladiumPrice.price;
    } else if (typeProduct === 'gold') {
      sellPrice = goldPrice.price;
      buyPrice = goldPrice.price;
    } else if (typeProduct === 'silver') {
      sellPrice = silverPrice.price;
      buyPrice = silverPrice.price;
    }
  }
  let imageUrl = null;
  if (typeof images !== 'undefined' && images.url) {
    imageUrl = images.url;
  }
  return (
    <div className={`item ${this.state.animationClassBuySell}`}>
      {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
      <a href="#" className="image">
        <Img src={imageUrl || ''} alt={product.title} />
      </a>
      <div className="info">
        <div className="group">
          <a href={`/products/${product.slug}`}>
            <ProductName {...productWeight} title={product.title} />
          </a>
        </div>
        <div className="group">
          {typeof typeBrands !== 'undefined' ? (
            <Brand items={typeBrands} />
          ) : null}
          <div className="condition">Brand Name</div>
          <div className="detail">
            <p>Dimension: 4mm x 3mm x 1mm</p>
            <p>Purity: 99,99% (24k)</p>
            <p>Fine weight: 1000 gram (32,15 troy oz)</p>
            <p>Country of origin: Switzerland</p>
            <p>
              Packaging: Encased in an interim package provided by Gyllenhus
              </p>
          </div>
        </div>
      </div>
      <ButtonGroup className="button-group">
        <button className="sell" type="button" onClick={this.handleSell}>
          <span className="text">
            <FormattedMessage {...messages.sell} />
          </span>
          <span className="percent">-1.00%</span>
          <span className="price">
            <ProductWeight
              {...productWeight}
              currency={currency}
              price={sellPrice}
              margin={productSellMargin}
              productPurity={productPurity}
              productVat={productVat}
            />
          </span>
        </button>
        <QuantityInput
          value={this.state.quantity}
          onChangeHandle={this.handleChange}
          onChangeQuantity={this.handleChangeQuantity}
        />
        <button className="buy" type="button" onClick={this.handleBuy}>
          <span className="text">
            <FormattedMessage {...messages.buy} />
          </span>
          <span className="price">
            <ProductWeight
              {...productWeight}
              currency={currency}
              price={buyPrice}
              margin={productBuyMargin}
              productPurity={productPurity}
              productVat={productVat}
            />
          </span>
          <span className="percent">+2.00%</span>
        </button>
      </ButtonGroup>
    </div>
  );
}
}

ProductListItem.propTypes = {
  item: PropTypes.shape({
    product: PropTypes.object,
    images: PropTypes.any,
    productPurity: PropTypes.number,
    productVat: PropTypes.number,
    productWeight: PropTypes.object,
    typeProduct: PropTypes.string,
    typeBrands: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.array,
      PropTypes.object,
    ]),
  }),
  currency: PropTypes.string,
  goldPrice: PropTypes.object,
  silverPrice: PropTypes.object,
  platinumPrice: PropTypes.object,
  palladiumPrice: PropTypes.object,
  dispatch: PropTypes.func,
  countdown: PropTypes.number,
  onBuyAddToCart: PropTypes.func,
  onSellAddToCart: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({

});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onBuyAddToCart: (item, quantity) => dispatch(buyAddToCart(item, quantity)),
    onSellAddToCart: (item, quantity) => dispatch(sellAddToCart(item, quantity)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(ProductListItem);
