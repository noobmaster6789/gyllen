/*
 *
 * CartPage constants
 *
 */

export const CHANGE_TRANSPORT_INSURANCE = 'app/CartPage/CHANGE_TRANSPORT_INSURANCE';
export const RELOAD_DELIVERY_SETTING = 'app/CartPage/RELOAD_DELIVERY_SETTING';
export const SHOW_HIDE_DROPDOWN = 'app/CartPage/SHOW_HIDE_DROPDOWN';
export const HIDE_TRANSPORT_DROPDOWN = 'app/CartPage/HIDE_TRANSPORT_DROPDOWN';
export const SHOW_HIDE_DIFFERENT_ADDRESS = 'app/CartPage/SHOW_HIDE_DIFFERENT_ADDRESS';
export const CHANGE_TAB = 'app/CartPage/CHANGE_TAB';
export const BUY_ADD_TO_CART = 'app/CartPage/BUY_ADD_TO_CART';
export const SELL_ADD_TO_CART = 'app/CartPage/SELL_ADD_TO_CART';
export const CHANGE_QTT = 'app/CartPage/CHANGE_QTT';
export const CHANGE_TOTAL = 'app/CartPage/CHANGE_TOTAL';
export const REMOVE_PRODUCT = 'app/CartPage/REMOVE_PRODUCT';
export const LOAD_CART_DATA = 'app/CartPage/LOAD_CART_DATA';
export const CHANGE_DISCOUNT = 'app/CartPage/CHANGE_DISCOUNT';
export const CHECK_DISCOUNT_CODE = 'app/CartPage/CHECK_DISCOUNT_CODE';
export const GET_COUPON_SUCCESS = 'app/CartPage/GET_COUPON_SUCCESS';
export const GET_COUPON_ERROR = 'app/CartPage/GET_COUPON_ERROR';
export const REMOVE_COUPON = 'app/CartPage/REMOVE_COUPON';
export const CHANGE_MESSAGE = 'app/CartPage/CHANGE_MESSAGE';
export const CHANGE_BILLING_ADDRESS = 'app/CartPage/CHANGE_BILLING_ADDRESS';
export const CHANGE_BILLING_COMPANY = 'app/CartPage/CHANGE_BILLING_COMPANY';
export const CHANGE_BILLING_NAME = 'app/CartPage/CHANGE_BILLING_NAME';
export const CHANGE_FULL_NAME = 'app/CartPage/CHANGE_FULL_NAME';
export const CHANGE_BILLING_ORG_NUMBER = 'app/CartPage/CHANGE_BILLING_ORG_NUMBER';
export const CHANGE_BILLING_POSTAL_CODE = 'app/CartPage/CHANGE_BILLING_POSTAL_CODE';
export const CHANGE_BILLING_CITY = 'app/CartPage/CHANGE_BILLING_CITY';
export const CHANGE_BILLING_EMAIL = 'app/CartPage/CHANGE_BILLING_EMAIL';
export const CHANGE_BILLING_PHONE = 'app/CartPage/CHANGE_BILLING_PHONE';
export const CONFIRM_ORDER = 'app/CartPage/CONFIRM_ORDER';
export const CONFIRM_ORDER_SUCCESS = 'app/CartPage/CONFIRM_ORDER_SUCCESS';
export const CONFIRM_ORDER_ERROR = 'app/CartPage/CONFIRM_ORDER_ERROR';
export const TOGGLE_ACTIVE = 'app/CartPage/TOGGLE_ACTIVE';
export const CHANGE_SHIPPING_ADDRESS = 'app/CartPage/CHANGE_SHIPPING_ADDRESS';
export const CHANGE_SHIPPING_POSTAL_CODE = 'app/CartPage/CHANGE_SHIPPING_POSTAL_CODE';
export const CHANGE_SHIPPING_CITY = 'app/CartPage/CHANGE_SHIPPING_CITY';
export const CHANGE_TERMS_CONDITIONS = 'app/CartPage/CHANGE_TERMS_CONDITIONS';
export const CHANGE_SHIPPING_COUNTRY = 'app/CartPage/CHANGE_SHIPPING_COUNTRY';
export const CHANGE_USE_SHIPPING_ADDRESS = 'app/CartPage/CHANGE_USE_SHIPPING_ADDRESS';
export const GET_ORDER_SUCCESS = 'app/CartPage/GET_ORDER_SUCCESS';
export const GET_ORDER_ERROR = 'app/CartPage/GET_ORDER_ERROR';
export const SELECT_PAYMENT_METHOD = 'app/CartPage/SELECT_PAYMENT_METHOD';
export const UPDATE_PRICE = 'app/CartPage/UPDATE_PRICE';
export const UPDATE_PRICE_SUCCESS = 'app/CartPage/UPDATE_PRICE_SUCCESS';
export const UPDATE_PRICE_ERROR = 'app/CartPage/UPDATE_PRICE_ERROR';
export const CHANGE_BANK = 'app/CartPage/CHANGE_BANK';
export const CHANGE_CLEARING_NUMBER = 'app/CartPage/CHANGE_CLEARING_NUMBER';
export const CHANGE_ACCOUNT_NUMBER = 'app/CartPage/CHANGE_ACCOUNT_NUMBER';

