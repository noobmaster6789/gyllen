/*
 *
 * CartPage actions
 *
 */

import {
  CHANGE_TRANSPORT_INSURANCE, SHOW_HIDE_DROPDOWN, CHANGE_TAB, CHANGE_QTT, CHANGE_TOTAL, REMOVE_PRODUCT, CHANGE_DISCOUNT, CHECK_DISCOUNT_CODE, GET_COUPON_SUCCESS, GET_COUPON_ERROR,
  CHANGE_MESSAGE, CHANGE_BILLING_ADDRESS, CHANGE_BILLING_COMPANY, CHANGE_BILLING_ORG_NUMBER, CHANGE_BILLING_POSTAL_CODE, CHANGE_BILLING_CITY,
  CHANGE_BILLING_EMAIL, CHANGE_BILLING_PHONE, CONFIRM_ORDER, TOGGLE_ACTIVE, CONFIRM_ORDER_SUCCESS, CONFIRM_ORDER_ERROR, SHOW_HIDE_DIFFERENT_ADDRESS,
  CHANGE_SHIPPING_ADDRESS, CHANGE_SHIPPING_POSTAL_CODE, CHANGE_SHIPPING_COUNTRY, CHANGE_SHIPPING_CITY,
  CHANGE_USE_SHIPPING_ADDRESS,
  GET_ORDER_SUCCESS, UPDATE_PRICE, UPDATE_PRICE_SUCCESS,
  GET_ORDER_ERROR, SELECT_PAYMENT_METHOD, LOAD_CART_DATA, BUY_ADD_TO_CART, SELL_ADD_TO_CART, RELOAD_DELIVERY_SETTING, CHANGE_BANK, CHANGE_CLEARING_NUMBER, CHANGE_ACCOUNT_NUMBER, REMOVE_COUPON, CHANGE_TERMS_CONDITIONS, CHANGE_FULL_NAME, CHANGE_BILLING_NAME, HIDE_TRANSPORT_DROPDOWN
} from './constants';

export function changetransportAndInsurance(value) {
  return {
    type: CHANGE_TRANSPORT_INSURANCE,
    value,
  };
}

export function reloadDeliverySetting() {
  return {
    type: RELOAD_DELIVERY_SETTING,
  };
}

export function showHideDropdown() {
  return {
    type: SHOW_HIDE_DROPDOWN
  };
}

export function hideTransportDropdown() {
  return {
    type: HIDE_TRANSPORT_DROPDOWN
  };
}

export function showHideDifferentAddress(value) {
  return {
    type: SHOW_HIDE_DIFFERENT_ADDRESS,
    value
  };
}


export function changeTab(value) {
  return {
    type: CHANGE_TAB,
    value
  };
}

export function buyAddToCart(item, quantity) {
  return {
    type: BUY_ADD_TO_CART,
    item,
    quantity
  };
}

export function sellAddToCart(item, quantity) {
  return {
    type: SELL_ADD_TO_CART,
    item,
    quantity
  };
}

export function changeQuantity(typeBuySell, id, qtt) {
  return {
    type : CHANGE_QTT,
    typeBuySell,
    id,
    qtt
  }
}


export function changeTotal(typeBuySell) {
  return {
    type : CHANGE_TOTAL,
    typeBuySell
  }
}

export function remove(typeBuySell, id) {
  return {
    type : REMOVE_PRODUCT,
    typeBuySell,
    id
  }
}

export function loadCartData(deliverySetting) {
  return {
    type: LOAD_CART_DATA,
    deliverySetting
  };
}

export function changeDiscount(discount){
  return {
    type : CHANGE_DISCOUNT,
    discount
  }
}

export function checkDiscountCode(discountCode){
  return {
    type : CHECK_DISCOUNT_CODE,
    discountCode
  }
}

export function removeCoupon() {
  return {
    type : REMOVE_COUPON
  }
}

export function getCouponSuccess(coupon) {
  return {
    type : GET_COUPON_SUCCESS,
    coupon
  }
}

export function getCouponError(description) {
  return {
    type : GET_COUPON_ERROR,
    description
  }
}

export function changeMessage(message) {
  return {
    type: CHANGE_MESSAGE,
    message
  }
}

export function changeBillingName(billingName) {
  return {
    type: CHANGE_BILLING_NAME,
    billingName
  }
}

export function changeBillingCompany(billingCompany) {
  return {
    type: CHANGE_BILLING_COMPANY,
    billingCompany
  }
}

export function changeBillingAddress(billingAddress) {
  return {
    type: CHANGE_BILLING_ADDRESS,
    billingAddress
  }
}

export function changeBillingOrganizationNumber(billingOrganizationNumber) {
  return {
    type: CHANGE_BILLING_ORG_NUMBER,
    billingOrganizationNumber
  }
}

export function changeBillingPostalCode(billingPostalCode) {
  return {
    type: CHANGE_BILLING_POSTAL_CODE,
    billingPostalCode
  }
}

export function changeBillingCity(billingCity) {
  return {
    type: CHANGE_BILLING_CITY,
    billingCity
  }
}

export function changeBillingEmail(billingEmail) {
  return {
    type: CHANGE_BILLING_EMAIL,
    billingEmail
  }
}

export function changeBillingPhone(billingPhone) {
  return {
    type: CHANGE_BILLING_PHONE,
    billingPhone
  }
}

export function changeFullName(fullName) {
  return {
    type: CHANGE_FULL_NAME,
    fullName
  }
}

export function confirmOrder(data) {
  return {
    type: CONFIRM_ORDER,
    data
  }
}

export function onChangeClient(value) {
  return {
    type: TOGGLE_ACTIVE,
    value
  };
}

export function confirmOrderSuccess(order) {
  return {
    type : CONFIRM_ORDER_SUCCESS,
    order
  }
}

export function confirmOrderError(response) {
  return {
    type : CONFIRM_ORDER_ERROR
  }
}

export function changeShippingAddress(shippingAddress) {
  return {
    type : CHANGE_SHIPPING_ADDRESS,
    shippingAddress
  }
}

export function changeShippingPostalCode(shippingPostalCode) {
  return {
    type : CHANGE_SHIPPING_POSTAL_CODE,
    shippingPostalCode
  }
}

export function changeShippingCity(shippingCity) {
  return {
    type : CHANGE_SHIPPING_CITY,
    shippingCity
  }
}

export function changeTermsConditions(termsConditions) {
  return {
    type : CHANGE_TERMS_CONDITIONS,
    termsConditions
  }
}

export function changeShippingCountry(shippingCountry) {
  return {
    type : CHANGE_SHIPPING_COUNTRY,
    shippingCountry
  }
}

export function usingShippingAddress(value) {
  return {
    type : CHANGE_USE_SHIPPING_ADDRESS,
    value
  }
}

export function getOrderSuccess(data, isBuy) {
  return {
    type : GET_ORDER_SUCCESS,
    data,
    isBuy
  }
}

export function getOrderError(message) {
  return {
    type : GET_ORDER_ERROR,
    message
  }
}

export function selectPatmentMethod(value) {
  return {
    type : SELECT_PAYMENT_METHOD,
    value
  }
}

export function getupdatedPrice(data) {
  return {
    type : UPDATE_PRICE,
    data
  }
}

export function updatedPriceSuccess(data) {
  return {
    type : UPDATE_PRICE_SUCCESS,
    data
  }
}

export function changeBank(bank) {
  return {
    type : CHANGE_BANK,
    bank
  }
}
export function changeClearingNumber(clearingNumber) {
  return {
    type : CHANGE_CLEARING_NUMBER,
    clearingNumber
  }
}
export function changeAccountNumber(accountNumber) {
  return {
    type : CHANGE_ACCOUNT_NUMBER,
    accountNumber
  }
}

