import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the registerPage state domain
 */

const selectCart = state => state.cartPage || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by RegisterPage
 */
const makeSelectTransportAndInsurance = () =>
  createSelector(
    selectCart,
    cartState => cartState.transportAndInsurance,
  );


const makeSelectIsShowHide = () =>
  createSelector(
    selectCart,
    cartState => cartState.isShowHideDropdown,
  );

const makeSelectIsDifferentAddress = () =>
  createSelector(
    selectCart,
    cartState => cartState.isDifferentAddress,
  );

const makeSelectIsBuyActive = () =>
  createSelector(
    selectCart,
    cartState => cartState.isBuyTabActive,
  );

const makeSelectTotalBuyQuantity = () =>
  createSelector(
    selectCart,
    cartState => cartState.totalBuyQuantity,
  );

const makeSelectTotalSellQuantity = () =>
  createSelector(
    selectCart,
    cartState => cartState.totalSellQuantity,
  );


const makeSelectTotalBuyToPay = () =>
  createSelector(
    selectCart,
    cartState => cartState.totalBuyToPay,
  );

const makeSelectTotalSellToPay = () =>
  createSelector(
    selectCart,
    cartState => cartState.totalSellToPay,
  );



const makeSelectTotalBuyWitoutTax = () =>
  createSelector(
    selectCart,
    cartState => cartState.totalBuyWithoutTax,
  );

const makeSelectTotalSellWitoutTax = () =>
  createSelector(
    selectCart,
    cartState => cartState.totalSellWithoutTax,
  );

const makeSelectTotalBuyTax = () =>
  createSelector(
    selectCart,
    cartState => cartState.totalBuyTax,
  );


const makeSelectTotalSellTax = () =>
  createSelector(
    selectCart,
    cartState => cartState.totalSellTax,
  );

const makeSelectTotalBuyIncludeTax = () =>
  createSelector(
    selectCart,
    cartState => cartState.totalBuyIncludeTax,
  );

const makeSelectTotalSellIncludeTax = () =>
  createSelector(
    selectCart,
    cartState => cartState.totalSellIncludeTax,
  );

const makeSelectListProduct = () =>
  createSelector(
    selectCart,
    cartState => cartState.listProduct,
  );

const makeSelectCartBuyProducts = () =>
  createSelector(
    selectCart,
    cartState => cartState.cartBuyProducts,
  );


const makeSelectCartSellProducts = () =>
  createSelector(
    selectCart,
    cartState => cartState.cartSellProducts,
  );

const makeSelectDeliveryWitoutVAT = () =>
  createSelector(
    selectCart,
    cartState => cartState.deliveryWithoutVAT,
  );

const makeSelectDeliveryVAT = () =>
  createSelector(
    selectCart,
    cartState => cartState.deliveryVAT,
  );


const makeSelectQuantity = () =>
  createSelector(
    selectCart,
    cartState => cartState.quantity,
  );

const makeSelectDiscount = () =>
  createSelector(
    selectCart,
    cartState => cartState.discount,
  );

const makeSelectValidCoupon = () =>
  createSelector(
    selectCart,
    cartState => cartState.validCoupon,
  );

const makeSelectDiscountTotal = () =>
  createSelector(
    selectCart,
    cartState => cartState.discountTotal,
  );

const makeSelectOrderError = () =>
  createSelector(
    selectCart,
    cartState => cartState.orderError,
  );

const makeSelectDisableSubmitBtn = () =>
  createSelector(
    selectCart,
    cartState => cartState.disableSubmitBtn,
  );

const makeSelectCouponDescription = () =>
  createSelector(
    selectCart,
    cartState => cartState.couponDescription,
  );

const makeSelectIsPrivateActive = () =>
  createSelector(
    selectCart,
    cartState => cartState.isPrivateActive,
  );

const makeSelectMessage = () =>
  createSelector(
    selectCart,
    cartState => cartState.message,
  );
const makeSelectBillingName = () =>
  createSelector(
    selectCart,
    cartState => cartState.billingName,
  );

const makeSelectFullName = () =>
  createSelector(
    selectCart,
    cartState => cartState.fullName,
  );

const makeSelectBillingCompany = () =>
  createSelector(
    selectCart,
    cartState => cartState.billingCompany,
  );

const makeSelectBillingAddress = () =>
  createSelector(
    selectCart,
    cartState => cartState.billingAddress,
  );

const makeSelectBillingOrganizationNumber = () =>
  createSelector(
    selectCart,
    cartState => cartState.billingOrganizationNumber,
  );

const makeSelectBillingPostalCode = () =>
  createSelector(
    selectCart,
    cartState => cartState.billingPostalCode,
  );

const makeSelectBillingCity = () =>
  createSelector(
    selectCart,
    cartState => cartState.billingCity,
  );

const makeSelectBillingEmail = () =>
  createSelector(
    selectCart,
    cartState => cartState.billingEmail,
  );

const makeSelectBillingPhone = () =>
  createSelector(
    selectCart,
    cartState => cartState.billingPhone,
  );

const makeSelectShippingAddress = () =>
  createSelector(
    selectCart,
    cartState => cartState.shippingAddress,
  );

const makeSelectShippingPostalCode = () =>
  createSelector(
    selectCart,
    cartState => cartState.shippingPostalCode,
  );

const makeSelectShippingCity = () =>
  createSelector(
    selectCart,
    cartState => cartState.shippingCity,
  );

const makeSelectShippingCountry = () =>
  createSelector(
    selectCart,
    cartState => cartState.shippingCountry,
  );

const makeSelectUseShippingAddress = () =>
  createSelector(
    selectCart,
    cartState => cartState.useShippingAddress,
  );

const makeSelectTermsConditions = () =>
  createSelector(
    selectCart,
    cartState => cartState.termsConditions,
  );

const makeSelectPaymentMethod = () =>
  createSelector(
    selectCart,
    cartState => cartState.paymentMethod,
  );

const makeSelectGoldPrice = () =>
  createSelector(
    selectCart,
    cartState => cartState.goldPrice,
  );

const makeSelectSilverPrice = () =>
  createSelector(
    selectCart,
    cartState => cartState.silverPrice,
  );

const makeSelectPlatinumPrice = () =>
  createSelector(
    selectCart,
    cartState => cartState.platinumPrice,
  );

const makeSelectPalladiumPrice = () =>
  createSelector(
    selectCart,
    cartState => cartState.palladiumPrice,
  );

const makeSelectBank = () =>
  createSelector(
    selectCart,
    cartState => cartState.bank,
  );

const makeSelectClearingNumber = () =>
  createSelector(
    selectCart,
    cartState => cartState.clearingNumber,
  );

const makeSelectAccountNumber = () =>
  createSelector(
    selectCart,
    cartState => cartState.accountNumber,
  );

export {
  selectCart,
  makeSelectTransportAndInsurance,
  makeSelectIsShowHide,
  makeSelectIsBuyActive,
  makeSelectTotalBuyQuantity,
  makeSelectTotalSellQuantity,
  makeSelectTotalBuyToPay,
  makeSelectTotalSellToPay,
  makeSelectListProduct,
  makeSelectCartBuyProducts,
  makeSelectCartSellProducts,
  makeSelectQuantity,
  makeSelectTotalBuyWitoutTax,
  makeSelectTotalSellWitoutTax,
  makeSelectDiscount,
  makeSelectValidCoupon,
  makeSelectDiscountTotal,
  makeSelectCouponDescription,
  makeSelectTotalBuyTax,
  makeSelectTotalSellTax,
  makeSelectTotalBuyIncludeTax,
  makeSelectTotalSellIncludeTax,
  makeSelectDeliveryWitoutVAT,
  makeSelectDeliveryVAT,
  makeSelectIsPrivateActive,
  makeSelectMessage,
  makeSelectBillingCompany,
  makeSelectBillingAddress,
  makeSelectBillingOrganizationNumber,
  makeSelectBillingPostalCode,
  makeSelectBillingCity,
  makeSelectBillingEmail,
  makeSelectBillingPhone,
  makeSelectBillingName,
  makeSelectFullName,
  makeSelectIsDifferentAddress,
  makeSelectShippingAddress,
  makeSelectShippingPostalCode,
  makeSelectShippingCity,
  makeSelectShippingCountry,
  makeSelectUseShippingAddress,
  makeSelectTermsConditions,
  makeSelectOrderError,
  makeSelectDisableSubmitBtn,
  makeSelectPaymentMethod,
  makeSelectGoldPrice,
  makeSelectSilverPrice,
  makeSelectPlatinumPrice,
  makeSelectPalladiumPrice,
  makeSelectBank,
  makeSelectClearingNumber,
  makeSelectAccountNumber
};
