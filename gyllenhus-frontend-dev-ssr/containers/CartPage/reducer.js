/*
 *
 * RegisterPage reducer
 *
 */
import produce from 'immer';
import {
  CHANGE_TRANSPORT_INSURANCE, SHOW_HIDE_DROPDOWN, CHANGE_TAB, CHANGE_QTT, CHANGE_TOTAL, REMOVE_PRODUCT, CHANGE_DISCOUNT, GET_COUPON_SUCCESS, CHECK_DISCOUNT_CODE, GET_COUPON_ERROR, CHANGE_MESSAGE,
  CHANGE_BILLING_ADDRESS, CHANGE_BILLING_COMPANY, CHANGE_BILLING_ORG_NUMBER, CHANGE_BILLING_POSTAL_CODE, CHANGE_BILLING_CITY, CHANGE_BILLING_EMAIL,
  CHANGE_BILLING_PHONE, CONFIRM_ORDER, TOGGLE_ACTIVE, SHOW_HIDE_DIFFERENT_ADDRESS, CHANGE_SHIPPING_ADDRESS,
  CHANGE_SHIPPING_POSTAL_CODE, CHANGE_SHIPPING_CITY, CHANGE_SHIPPING_COUNTRY, CHANGE_USE_SHIPPING_ADDRESS, GET_ORDER_SUCCESS, GET_ORDER_ERROR, SELECT_PAYMENT_METHOD, LOAD_CART_DATA, BUY_ADD_TO_CART, SELL_ADD_TO_CART,
  UPDATE_PRICE, UPDATE_PRICE_SUCCESS, CHANGE_CLEARING_NUMBER, CHANGE_ACCOUNT_NUMBER, CHANGE_BANK, REMOVE_COUPON, CHANGE_TERMS_CONDITIONS, CHANGE_FULL_NAME, CHANGE_BILLING_NAME, HIDE_TRANSPORT_DROPDOWN
} from './constants';
import { getValuePrice, calculateTotal, reloadBuyCart, reloadSellCart, updateNewPriceToCart } from '../../utils/commons';

const PAYMENT_METHOD_CREDIT_CARD = "credit-card";
export const initialState = {
  isShowHideDropdown: true,
  isBuyTabActive: true,
  quantity: 1,
  totalBuyToPay: 0,
  totalSellToPay: 0,
  totalWithoutTax: 0,
  totalBuyWithoutTax: 0,
  totalSellWithoutTax: 0,
  totalBuyTax: 0,
  totalSellTax: 0,
  totalBuyIncludeTax: 0,
  totalSellIncludeTax: 0,
  deliveryWithoutVAT: 0,
  deliveryVAT: 0,
  isDifferentAddress: false,
  totalBuyQuantity: 0,
  totalSellQuantity: 0,
  orderError: "",
  disableSubmitBtn: false,
  billingName: "",
  billingCompany: "",
  termsConditions : true,
  paymentMethod: PAYMENT_METHOD_CREDIT_CARD
};

/* eslint-disable default-case, no-param-reassign */
const cartPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    const sessionName = action.typeBuySell === "buy" ? "cartBuyItems" : "cartSellItems";
    switch (action.type) {
      case SHOW_HIDE_DROPDOWN:
        draft.isShowHideDropdown = !draft.isShowHideDropdown;
        break;
      case HIDE_TRANSPORT_DROPDOWN:
        draft.isShowHideDropdown = true;
        break;
      case SHOW_HIDE_DIFFERENT_ADDRESS:
        draft.isDifferentAddress = action.value;
        break;
      case CHANGE_TRANSPORT_INSURANCE:
        draft.transportAndInsurance = action.value;
        draft.isShowHideDropdown = true;
        localStorage.setItem('transportInsurance', JSON.stringify(action.value));
        action.typeBuySell = (draft.isBuyTabActive ? "buy" : "sell");
        draft = calculateTotal(action, draft);
        break;
      case CHANGE_TAB:
        draft.isBuyTabActive = action.value
        action.typeBuySell = (draft.isBuyTabActive ? "buy" : "sell");
        draft = calculateTotal(action, draft);
        break;
      case BUY_ADD_TO_CART:
        const propsBuy = action.item;
        const itemBuy = propsBuy.item;
        var cartItemsBuy = JSON.parse(localStorage.getItem('cartBuyItems')) || [];
        var itemCart = { id: itemBuy.product.ID, item: propsBuy, quantity: action.quantity, price: getValuePrice(propsBuy, true) }
        const index = cartItemsBuy.findIndex(e => e.id === itemCart.id);
        if (index !== -1) {
          cartItemsBuy[index].quantity += itemCart.quantity;
          if (cartItemsBuy[index].quantity > 1000) cartItemsBuy[index].quantity = 1000;
        } else cartItemsBuy.push(itemCart);
        localStorage.setItem("cartBuyItems", JSON.stringify(cartItemsBuy));
        draft = reloadBuyCart(draft, cartItemsBuy);
        break;
      case SELL_ADD_TO_CART:
        const propsSell = action.item;
        const itemSell = propsSell.item;
        var cartItemsSell = JSON.parse(localStorage.getItem('cartSellItems')) || [];
        var itemCart = { id: itemSell.product.ID, item: propsSell, quantity: action.quantity, price: getValuePrice(propsSell, false) }
        const indexItemSell = cartItemsSell.findIndex(e => e.id === itemCart.id);
        if (indexItemSell !== -1) {
          cartItemsSell[indexItemSell].quantity += itemCart.quantity;
          if (cartItemsSell[index].quantity > 999) cartItemsSell[index].quantity = 999;
        } else cartItemsSell.push(itemCart);
        localStorage.setItem("cartSellItems", JSON.stringify(cartItemsSell));
        draft = reloadSellCart(draft, cartItemsSell);
        break;
      case CHANGE_QTT:
        var cartProducts = JSON.parse(localStorage.getItem(sessionName));
        const quantity = action.qtt;
        cartProducts = cartProducts.map(el => (el.id === action.id ? { ...el, quantity } : el))
        localStorage.setItem(sessionName, JSON.stringify(cartProducts));
        break;
      case CHANGE_TOTAL:
        draft = calculateTotal(action, draft);
        break;
      case REMOVE_PRODUCT:
        var cartProducts = JSON.parse(localStorage.getItem(sessionName)) || [];
        cartProducts = cartProducts.filter(item => item.id !== action.id)
        if (cartProducts.length == 0) {
          draft.isBuyTabActive = (action.typeBuySell == "sell");
        }
        localStorage.setItem(sessionName, JSON.stringify(cartProducts));
        break;
      case LOAD_CART_DATA:
        if (action.deliverySetting)
          localStorage.setItem("deliverySetting", JSON.stringify(action.deliverySetting));
        draft = calculateTotal(action, draft);
        break;
      case CHANGE_DISCOUNT:
        draft.discount = action.discount;
        break;
      case CHECK_DISCOUNT_CODE:
        break;
      case GET_COUPON_SUCCESS:
        const coupon = action.coupon;
        if (coupon.couponStatus === "active") {
          localStorage.setItem('coupon', JSON.stringify(coupon));
          action.typeBuySell = (draft.isBuyTabActive ? "buy" : "sell");
          draft.validCoupon = true;
          draft = calculateTotal(action, draft);
        }
        break;
      case GET_COUPON_ERROR:
        draft.couponDescription = action.description;
        draft.validCoupon = false;
        break;
      case REMOVE_COUPON:
        localStorage.removeItem('coupon');
        draft.discount = "";
        draft.validCoupon = false;
        draft.couponDescription = "";
        draft = calculateTotal(action, draft);
        break;
      case CHANGE_MESSAGE:
        draft.message = action.message;
        break;
      case CHANGE_BILLING_NAME:
        draft.billingName = action.billingName
        setDataToLocalstorage(draft);
        break;
      case CHANGE_BILLING_COMPANY:
        draft.billingCompany = action.billingCompany
        setDataToLocalstorage(draft);
        break;
      case CHANGE_BILLING_ADDRESS:
        draft.billingAddress = action.billingAddress
        setDataToLocalstorage(draft);
        break;
      case CHANGE_BILLING_ORG_NUMBER:
        draft.billingOrganizationNumber = action.billingOrganizationNumber
        setDataToLocalstorage(draft);
        break;
      case CHANGE_BILLING_POSTAL_CODE:
        draft.billingPostalCode = action.billingPostalCode
        setDataToLocalstorage(draft);
        break;
      case CHANGE_BILLING_CITY:
        draft.billingCity = action.billingCity
        setDataToLocalstorage(draft);
        break;
      case CHANGE_BILLING_EMAIL:
        draft.billingEmail = action.billingEmail
        setDataToLocalstorage(draft);
        break;
      case CHANGE_BILLING_PHONE:
        draft.billingPhone = action.billingPhone
        setDataToLocalstorage(draft);
        break;
      case CHANGE_FULL_NAME:
        draft.fullName = action.fullName;
        setDataToLocalstorage(draft);
        break;
      case TOGGLE_ACTIVE:
        draft.isPrivateActive = action.value
        setDataToLocalstorage(draft);
        break;
      case CONFIRM_ORDER:
        draft.disableSubmitBtn = true;
        setDataToLocalstorage(draft);
        break;
      case CHANGE_SHIPPING_ADDRESS:
        draft.shippingAddress = action.shippingAddress
        setDataToLocalstorage(draft);
        break;
      case CHANGE_SHIPPING_POSTAL_CODE:
        draft.shippingPostalCode = action.shippingPostalCode
        setDataToLocalstorage(draft);
        break;
      case CHANGE_SHIPPING_CITY:
        draft.shippingCity = action.shippingCity
        setDataToLocalstorage(draft);
        break;
      case CHANGE_SHIPPING_COUNTRY:
        draft.shippingCountry = action.shippingCountry
        setDataToLocalstorage(draft);
        break;
      case CHANGE_USE_SHIPPING_ADDRESS:
        draft.useShippingAddress = action.value
        setDataToLocalstorage(draft);
        break;
      case CHANGE_TERMS_CONDITIONS:
        draft.termsConditions = action.termsConditions;
        break;
      case CHANGE_BANK:
        draft.bank = action.bank
        localStorage.setItem('bankPayment', JSON.stringify({
          "bank": draft.bank,
          "clearingNumber": draft.clearingNumber, "accountNumber": draft.accountNumber
        }));
        break;
      case CHANGE_CLEARING_NUMBER:
        draft.clearingNumber = action.clearingNumber
        localStorage.setItem('bankPayment', JSON.stringify({
          "bank": draft.bank,
          "clearingNumber": draft.clearingNumber, "accountNumber": draft.accountNumber
        }));
        break;
      case CHANGE_ACCOUNT_NUMBER:
        draft.accountNumber = action.accountNumber
        localStorage.setItem('bankPayment', JSON.stringify({
          "bank": draft.bank,
          "clearingNumber": draft.clearingNumber, "accountNumber": draft.accountNumber
        }));
        break;
      case GET_ORDER_SUCCESS:
        localStorage.setItem('orderId', JSON.stringify(action.data.data.orderID));
        localStorage.removeItem("coupon");
        localStorage.removeItem("transportInsurance");
        if (action.isBuy) localStorage.removeItem("cartBuyItems")
        else localStorage.removeItem("cartSellItems");
        break;
      case GET_ORDER_ERROR:
        draft.orderError = action.message
        draft.disableSubmitBtn = false;
        break;
      case SELECT_PAYMENT_METHOD:
        draft.paymentMethod = action.value;
        localStorage.setItem('paymentMethod', JSON.stringify({ "type": draft.paymentMethod }));
        break;
      case UPDATE_PRICE:
        break;
      case UPDATE_PRICE_SUCCESS:
        draft.goldPrice = action.data.gold;
        draft.silverPrice = action.data.silver;
        draft.platinumPrice = action.data.platinum;
        draft.palladiumPrice = action.data.palladium;
        updateNewPriceToCart(draft);
        break;
    }

  });

function setDataToLocalstorage(draft) {
  localStorage.setItem('information', JSON.stringify({
    "billingName": draft.billingName,
    "billingCompany": draft.billingCompany, "billingAddress": draft.billingAddress,
    "billingOrganizationNumber": draft.billingOrganizationNumber, "billingPostalCode": draft.billingPostalCode,
    "billingCity": draft.billingCity, "billingEmail": draft.billingEmail, "billingPhone": draft.billingPhone,
    "fullName": draft.fullName, "isPrivateActive": draft.isPrivateActive, "shippingAddress": draft.shippingAddress,
    "shippingPostalCode": draft.shippingPostalCode, "shippingCity": draft.shippingCity, "shippingCountry": draft.shippingCountry,
    "useShippingAddress": draft.useShippingAddress
  }));
}

export default cartPageReducer;
