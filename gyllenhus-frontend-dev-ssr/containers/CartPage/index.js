import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CartProductList from '../../components/CartProductList';
import { createStructuredSelector } from 'reselect';
import {
  makeSelectTransportAndInsurance, makeSelectIsShowHide, makeSelectIsBuyActive, makeSelectListProduct,
  makeSelectTotalBuyWitoutTax, makeSelectTotalSellWitoutTax, makeSelectDiscount, makeSelectCouponDescription, makeSelectTotalBuyTax,
  makeSelectTotalBuyIncludeTax, makeSelectTotalSellTax, makeSelectTotalSellIncludeTax,
  makeSelectIsPrivateActive, makeSelectMessage, makeSelectBillingCompany, makeSelectBillingAddress, makeSelectBillingOrganizationNumber,
  makeSelectBillingPostalCode, makeSelectBillingCity, makeSelectBillingEmail, makeSelectBillingPhone, makeSelectIsDifferentAddress,
  makeSelectShippingAddress, makeSelectShippingPostalCode, makeSelectShippingCity,
  makeSelectUseShippingAddress, makeSelectDiscountTotal,
  makeSelectTotalBuyQuantity, makeSelectTotalSellQuantity, makeSelectOrderError, makeSelectTotalBuyToPay, makeSelectTotalSellToPay,
  makeSelectCartBuyProducts, makeSelectCartSellProducts, makeSelectPaymentMethod, makeSelectGoldPrice, makeSelectSilverPrice, makeSelectPlatinumPrice,
  makeSelectPalladiumPrice,
  makeSelectDeliveryWitoutVAT,
  makeSelectDeliveryVAT,
  makeSelectBank,
  makeSelectClearingNumber,
  makeSelectAccountNumber,
  makeSelectDisableSubmitBtn,
  makeSelectTermsConditions,
  makeSelectValidCoupon,
  makeSelectFullName,
  makeSelectBillingName
} from './selectors';
import {
  showHideDropdown, changetransportAndInsurance, changeTab, changeDiscount, checkDiscountCode, changeMessage, changeBillingCompany,
  changeBillingAddress, changeBillingOrganizationNumber, changeBillingPostalCode, changeBillingCity, changeBillingEmail, changeBillingPhone, onChangeClient,
  confirmOrder, showHideDifferentAddress, changeShippingAddress, changeShippingPostalCode,
  changeShippingCity, usingShippingAddress, selectPatmentMethod, getupdatedPrice, reloadDeliverySetting, removeCoupon, changeTermsConditions, getOrderError, changeFullName, changeBillingName, hideTransportDropdown
} from './actions';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { formatNumberDecimalPurity, roundNumberValuePurity } from '../../utils/formatter';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Currency from '../../components/Currency';
import BankPayment from '../../components/BankPayment';
import Transport from '../../components/Transport';
import BuySellTooltip from '../../components/Tooltip';
import PaymentDesciption from '../../components/PaymentDesciption';
import DeliveryAddress from '../../components/DeliveryAddress';
import BillingAddress from '../../components/BillingAddress';
import useOutsideClick from '../../utils/useOutsideClick';

const idNumberRegex = /(^(((20)((0[0-9])|(1[0-1])))|(([1][^0-8])?\d{2}))((0[1-9])|1[0-2])((0[1-9])|(2[0-9])|(3[01]))[-]?\d{4}$)/;
const DEFAULT_DELIVERY = "PostNORD";
const PERSONAL_DELIVERY = "Personal Delivery";

export function CartPage(props) {
  const {
    defaultCurrency,
    dispatch,
    transportAndInsurance,
    onChangetransportAndInsurance,
    deliverySetting,
    onReloadDeliverySetting,
    deliveryWithoutVAT,
    deliveryVAT,
    isShowHideDropdown,
    onShowHideDropdown,
    onHideTransportDropdown,
    isBuyTabActive,
    onChangeTab,
    cartBuyProducts,
    cartSellProducts,
    totalBuyQuantity,
    totalSellQuantity,
    totalBuyWithoutTax,
    totalSellWithoutTax,
    totalBuyTax,
    totalSellTax,
    totalBuyIncludeTax,
    totalSellIncludeTax,
    transports,
    discount,
    validCoupon,
    onChangeDiscount,
    onCheckDiscountCode,
    onRemoveCoupon,
    couponDescription,
    message,
    onChangeMessage,
    billingName,
    billingCompany,
    billingAddress,
    billingOrganizationNumber,
    billingPostalCode,
    billingCity,
    billingEmail,
    billingPhone,
    onClickConfirmOrder,
    onChangeClient,
    fullName,
    isPrivateActive,
    isDifferentAddress,
    useShippingAddress,
    onShowHideDifferentAddress,
    shippingAddress,
    shippingPostalCode,
    shippingCity,
    termsConditions,
    onChangeTermsConditions,
    banks,
    bank,
    clearingNumber,
    accountNumber,
    discountTotal,
    orderError,
    onSelectPaymentMethod,
    paymentMethod,
    updatePrice,
    disableSubmitBtn,
  } = props;
  const ref = useRef();
  const refDeliveryPopup = useRef();
  var jwtToken = "";
  useEffect(() => {
    jwtToken = localStorage.getItem('jwtToken');
    if (jwtToken) {
      // logged
      onReloadDeliverySetting();

    }
  }, [jwtToken]);

  useEffect(() => {
    const transportInsuranceLocalStorage = JSON.parse(localStorage.getItem("transportInsurance"));
    if (!transportInsuranceLocalStorage) {
      transports.map((transport, i) => {
        if (transport.title === DEFAULT_DELIVERY) {
          onChangetransportAndInsurance(transport);
        }
      })
    }
  }, []);

  useEffect(() => {
    if (!isBuyTabActive && transportAndInsurance && transportAndInsurance.title && transportAndInsurance.title.includes(PERSONAL_DELIVERY)) {
      transports.map((transport, i) => {
        if (transport.title === DEFAULT_DELIVERY) {
          onChangetransportAndInsurance(transport);
        }
      })
    }
  }, [isBuyTabActive]);

  useOutsideClick(ref, () => {
    if (!isShowHideDropdown) {
      onHideTransportDropdown();
    }
  });
  useOutsideClick(refDeliveryPopup, () => {
    if (isDifferentAddress) {
      onShowHideDifferentAddress(false);
    }
  });

  useEffect(() => {
    let interval = setInterval(() => {
      updatePrice();
    }, 300000)
    return () => clearInterval(interval);
  }, []);
  const [isDifferentDelivery, setIsDifferentDelivery] = useState(true);

  const cartBuyProductList = {
    data: cartBuyProducts,
    typeBuySell: (isBuyTabActive ? "buy" : "sell"),
    defaultCurrency,
    dispatch,
  };
  const cartSellProductList = {
    data: cartSellProducts,
    typeBuySell: (isBuyTabActive ? "buy" : "sell"),
    defaultCurrency,
    dispatch,
  };

  if (cartBuyProducts && cartBuyProducts.length == 0 && cartSellProducts && cartSellProducts.length == 0) {
    window.location.href = ("/");
  }

  const submitHandle = (evt) => {
    evt.preventDefault();
    // if (!termsConditions) {
    //   dispatch(getOrderError(<FormattedMessage {...messages.pleaseAgreePrivacyPolicy} />));
    //   return;
    // }
    // if (!transportAndInsurance || !transportAndInsurance.ID || !transportAndInsurance.ID === undefined) {
    //   dispatch(getOrderError(<FormattedMessage {...messages.chooseShippingMethod} />));
    //   return;
    // }
    // if (!paymentMethod || paymentMethod.length == 0) {
    //   dispatch(getOrderError(<FormattedMessage {...messages.selectPaymentMethod} />));
    //   return;
    // }
    // if (isPrivateActive && !idNumberRegex.test(billingOrganizationNumber)) {
    //   dispatch(getOrderError(<FormattedMessage {...messages.idNumberError} />));
    //   return;
    // }

    let payment = { type: paymentMethod };
    if (paymentMethod === "bank-payment" && bank) {
      payment = {
        type: paymentMethod,
        bankName: bank.code,
        clearingNumber: clearingNumber,
        accountNumber: accountNumber
      };
    }

    let data = {
      cartProducts: isBuyTabActive ? cartBuyProducts : cartSellProducts,
      billingAddress: {
        billingName: isPrivateActive ? billingName : billingCompany,
        billingCompany: fullName,
        billingAddress: billingAddress,
        billingOrganizationNumber: billingOrganizationNumber,
        billingPostalCode: billingPostalCode,
        billingCity: billingCity,
        billingEmail: billingEmail,
        billingPhone: billingPhone
      },
      shippingAddress: {
        shippingAddress: useShippingAddress ? shippingAddress : billingAddress,
        shippingPostalCode: useShippingAddress ? shippingPostalCode : billingPostalCode,
        shippingCity: useShippingAddress ? shippingCity : billingCity,
        shippingCountry: 'SE'
      },
      useShippingAddress: useShippingAddress,
      shippingMethod: {
        ID: transportAndInsurance.ID,
        delivery_fee_cost_widthout_vat: roundNumberValuePurity(deliveryWithoutVAT),
        delivery_fee_vat: roundNumberValuePurity(deliveryVAT),
        delivery_fee_cost: roundNumberValuePurity(deliveryWithoutVAT + deliveryVAT)
      },
      coupon: discount,
      paymentMethod: payment,
      message: message,
      isBuyTabActive: isBuyTabActive
    }
    onClickConfirmOrder(data);
  };

  return (
    <React.Fragment>
      <div className="wrapper checkout-page">
        <a className="nav-back" href="/">← <FormattedMessage {...messages.back} /></a>
        <div className="checkout-head">
          <h1><FormattedMessage {...messages.checkout} /></h1>
          <div className="tab-h2">
            <a href="#" onClick={() => onChangeTab(true)} className={`${isBuyTabActive ? "active" : ""} 
              ${!cartBuyProducts || cartBuyProducts.length == 0 ? "on-mobile" : "item"}`}>
              <span><FormattedMessage {...messages.buyOrder} /></span>
              <span className="qty-box">{totalBuyQuantity}</span>
            </a>
            <a href="#" onClick={() => onChangeTab(false)} className={`item ${isBuyTabActive ? "" : "active"}
              ${!cartSellProducts || cartSellProducts.length == 0 ? "on-mobile" : ""}`}>
              <span><FormattedMessage {...messages.sellOrder} /></span>
              <span className="qty-box">{totalSellQuantity}</span>
              <BuySellTooltip isSellActive={!isBuyTabActive} />
            </a>
          </div>
          <a href="#" className="certificate">
            <img src="static/images/ssl-secure-connection.svg" alt="SSL Secure Connection" />
          </a>
        </div>

        <div className="checkout-table">
          <div className="table-head row">
            <div className="cell cell-product"><h2><FormattedMessage {...messages.product} /></h2></div>
            <div className="cell cell-remove"><FormattedMessage {...messages.remove} /></div>
            <div className="cell cell-price"><FormattedMessage {...messages.price} /></div>
            <div className="cell cell-qty"><FormattedMessage {...messages.quantity} /></div>
            <div className="cell cell-ex"><FormattedMessage {...messages.subtotalExVAT} /></div>
            <div className="cell cell-vat"><FormattedMessage {...messages.VAT} /></div>
            <div className="cell cell-sub"><FormattedMessage {...messages.subtotal} /></div>
          </div>
          {isBuyTabActive ?
            <CartProductList {...cartBuyProductList} />
            : <CartProductList {...cartSellProductList} />
          }
          <div className="table-head row">
            <div className="cell cell-product"><h2><FormattedMessage {...messages.transportAndInsurance} /></h2></div>
          </div>
          <div className="cart-item row transport">
            <div className="cell cell-product">
              <a href="#" className="item-img"
                style={{ backgroundImage: `url(${transportAndInsurance.images ? transportAndInsurance.images.url : ""})` }}></a>
              <div className="select" ref={ref}>
                <div className="toggler stroke" onClick={onShowHideDropdown}>
                  {(!transportAndInsurance || !transportAndInsurance.ID || !transportAndInsurance.ID === undefined)
                    ? <FormattedMessage {...messages.deliveryOptionDefault} /> : transportAndInsurance.title} &nbsp;
                  <svg width="7" height="4" viewBox="0 0 7 4" version="1" xmlns="http://www.w3.org/2000/svg">
                    <path fill="#19396E" fillRule="nonzero" d="M3.5 2.636L0.483 0.036 0.483 1.351 3.5 3.981 6.5 1.351 6.5 0.036z"></path></svg></div>
                <div className={`dropdown ${isShowHideDropdown ? '' : 'active'}`}>
                  {
                    transports.map((transport, i) => {
                      if (!(!isBuyTabActive && transport.title.includes(PERSONAL_DELIVERY)))
                        return <div className="item" key={i} onClick={() => onChangetransportAndInsurance(transport)}>{transport.title}</div>
                    })
                  }
                </div>
              </div>
              <div>
                {transportAndInsurance.description}
              </div>
            </div>
            <div className="cell cell-ex">
              <span className="cell-label"><FormattedMessage {...messages.subtotalExVAT} /></span><span>
                {formatNumberDecimalPurity(deliveryWithoutVAT)} <Currency name={defaultCurrency} />
              </span>
            </div>
            <div className="cell cell-vat">
              <span className="cell-label">VAT</span><span>
                {formatNumberDecimalPurity(deliveryVAT)} <Currency name={defaultCurrency} />
              </span></div>
            <div className="cell cell-sub">
              <span className="cell-label"><FormattedMessage {...messages.subtotal} /></span><span>
                {formatNumberDecimalPurity(deliveryWithoutVAT + deliveryVAT)} <Currency name={defaultCurrency} />
              </span></div>
          </div>

          <Transport />

          <div className="table-head row">
            <div className="cell cell-product"><h2><FormattedMessage {...messages.discount} /></h2></div>
          </div>
          <div className="cart-item row voucher">
            <div className="cell cell-product">
              <a href="#" className="item-img" style={{ backgroundImage: `url(static/images/discount@2x.png)` }}></a>
              <div className="form-group-discount">
                <FormattedMessage {...messages.enterVoucherCode}>
                  {(msg) => (<input className="input-text" type="text" name="" placeholder={msg}
                    value={discount} onChange={onChangeDiscount} readOnly={(discount && validCoupon) ? true : false} />)}
                </FormattedMessage>

                <button className="button" onClick={() => ((discount && validCoupon) ? onRemoveCoupon() : onCheckDiscountCode({ discount }))}>
                  {(discount && validCoupon) ? <FormattedMessage {...messages.remove} /> : <FormattedMessage {...messages.apply} />}</button>
              </div>
              <div className="message"> {couponDescription} </div>
            </div>
            <div className="cell cell-sub"><span className="cell-label"><FormattedMessage {...messages.discount} /></span>
              <span>{discount && discountTotal > 0 ? formatNumberDecimalPurity(-discountTotal) : "0"} <Currency name={defaultCurrency} />
              </span>
            </div>
          </div>
          <div className="row cart-total">
            <div className="cell-product">&nbsp;</div>
            <div className="cell cell-ex"><small><FormattedMessage {...messages.total} /> (ex. VAT)</small><div>
              {formatNumberDecimalPurity(isBuyTabActive ? totalBuyWithoutTax : totalSellWithoutTax)} <Currency name={defaultCurrency} />
            </div>
            </div>
            <div className="cell cell-vat"><small>VAT</small><div>
              {formatNumberDecimalPurity(isBuyTabActive ? totalBuyTax : totalSellTax)} <Currency name={defaultCurrency} />
            </div></div>
            <div className="cell cell-sub"><small><FormattedMessage {...messages.total} /></small>
              {formatNumberDecimalPurity(isBuyTabActive ? totalBuyIncludeTax : totalSellIncludeTax)} <Currency name={defaultCurrency} />
            </div>
          </div>

          <form className="form-register" onSubmit={submitHandle}>
            <div className="table-head row">
              <div className="cell cell-product"><h2><FormattedMessage {...messages.selectPaymentMethod} /> </h2></div>
            </div>
            <div className="cart-item row cart-payment">
              <div className="cell cell-payment-method">
                <label htmlFor="payment-method-1" className="radio-box" onClick={() => onSelectPaymentMethod('credit-card')} >
                  <input type="radio" readOnly name="payment-method" id="payment-method-1" checked={paymentMethod === 'credit-card'} />
                  <span><FormattedMessage {...messages.creditCard} /></span>
                </label>
                <label htmlFor="payment-method-2" className="radio-box" onClick={() => onSelectPaymentMethod('bank-payment')} >
                  <input type="radio" readOnly name="payment-method" id="payment-method-2" checked={paymentMethod === 'bank-payment'} />
                  <span><FormattedMessage {...messages.bankPayment} /></span>
                </label>
                <label htmlFor="payment-method-3" className="radio-box" onClick={() => onSelectPaymentMethod('swish')} >
                  <input type="radio" readOnly name="payment-method" id="payment-method-3" checked={paymentMethod === 'swish'} />
                  <span>Swish</span>
                </label>
                <label htmlFor="payment-method-4" className="radio-box" onClick={() => onSelectPaymentMethod('invoice')} >
                  <input type="radio" readOnly name="payment-method" id="payment-method-4" checked={paymentMethod === 'invoice'} />
                  <span><FormattedMessage {...messages.invoice} /></span>
                </label>
              </div>
              <PaymentDesciption paymentMethod={paymentMethod} />
            </div>
            <BankPayment banks={banks} paymentMethod={paymentMethod} isBuyTabActive={isBuyTabActive} />

            <div className="checkout-footer">
              <div className="col billing">
                <div className="table-head row">
                  <div className="cell"><h2><FormattedMessage {...messages.yourInfomation} /></h2></div>
                </div>
                <div className="cart-item row billing">
                  <nav className="tab-wrapper">
                    <ul className="tab">
                      <li className={` ${isPrivateActive ? "active" : ""} `} onClick={() => onChangeClient(true)}><a href="#tab"><FormattedMessage {...messages.privateClient} /></a></li>
                      <li className={` ${isPrivateActive ? "" : "active"} `} onClick={() => onChangeClient(false)}><a href="#tab"><FormattedMessage {...messages.businessClient} /></a></li>
                    </ul>
                  </nav>
                    <div className="tab-content">
                      <BillingAddress isDifferentAddress={false} />
                      {/* <div className="form-group half">
                        <a href="#tab" onClick={() => onShowHideDifferentAddress(true)}>
                          {useShippingAddress ?
                            <FormattedMessage {...messages.differentBillingDeliveryChosen} />
                            : <FormattedMessage {...messages.differentBillingDelivery} />}
                        </a>
                      </div> */}
                    </div>
                </div>
              </div>
              <div className="col note">
                <div className="table-head row">
                  <div className="cell"><h2><FormattedMessage {...messages.messageToGyllenhus} /></h2></div>
                </div>
                <div className="cart-item row order-note">
                  <div className="form-group">
                    <input className={`input-text ${message ? 'has-value' : ''}`} type="text" value={message} onChange={onChangeMessage} />
                    <div className="placeholder"><FormattedMessage {...messages.messageToGyllenhus} /></div>
                  </div>
                </div>
              </div>
              <div className="col review">
                <div className="cart-item row order-review">
                  <h3> {isBuyTabActive ? <FormattedMessage {...messages.totalToPay} />
                    : <FormattedMessage {...messages.totalToRecieve} />}: <strong>
                      {formatNumberDecimalPurity(isBuyTabActive ? totalBuyIncludeTax : totalSellIncludeTax)} <Currency name={defaultCurrency} /></strong>
                  </h3>
                  <button type="submit" disabled={disableSubmitBtn} className="button"><FormattedMessage {...messages.confirmOrder} /></button>
                  <p>
                    <label for="agreed" class="checkbox">
                      <input type="checkbox" name="agreed" id="agreed" type="checkbox" onChange={onChangeTermsConditions}
                        checked={termsConditions} />
                      <span class="tick"><i></i></span>
                      <span class="text"><FormattedMessage {...messages.termConditions} /> <a href="#"><FormattedMessage {...messages.termConditions2} /></a>.</span>
                    </label>
                  </p>
                  <p style={{ color: '#DC143C' }}>
                    {orderError}
                  </p>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>

      <div className={`modal ${isDifferentAddress ? "active" : ""}`} ref={refDeliveryPopup} >
        <div className="card">
          <div className="tab-h2">
            <a href="#" onClick={(e) => { setIsDifferentDelivery(true); e.preventDefault(); }}
              className={`item ${isDifferentDelivery ? "active" : ""}`}>
              <span><FormattedMessage {...messages.delivery} /></span>
            </a>
          </div>
          <DeliveryAddress />
        </div>
      </div>
    </React.Fragment>
  );
}

CartPage.defaultProps = {
  transportAndInsurance: {
    ID: false,
    title: '',
    cost: 0,
    tax: 0
  }
};

CartPage.propTypes = {
  defaultCurrency: PropTypes.string,
  transportAndInsurance: PropTypes.object,
  isShowHideDropdown: PropTypes.bool,
  onChangetransportAndInsurance: PropTypes.func,
  onShowHideDropdown: PropTypes.func,
  dispatch: PropTypes.func,
  isBuyTabActive: PropTypes.bool,
  listProduct: PropTypes.array,
  cartBuyProducts: PropTypes.array,
  cartSellProducts: PropTypes.array,
  onChangeTab: PropTypes.func,
  totalBuyQuantity: PropTypes.number,
  totalSellQuantity: PropTypes.number,
  totalBuyToPay: PropTypes.number,
  totalSellToPay: PropTypes.number,
  totalBuyWithoutTax: PropTypes.number,
  totalSellWithoutTax: PropTypes.number,
  deliveryWithoutVAT: PropTypes.number,
  deliveryVAT: PropTypes.number,
  discount: PropTypes.string,
  validCoupon: PropTypes.bool,
  onChangeDiscount: PropTypes.func,
  onCheckDiscountCode: PropTypes.func,
  onRemoveCoupon: PropTypes.func,
  couponDescription: PropTypes.string,
  message: PropTypes.string,
  onChangeMessage: PropTypes.func,
  totalBuyTax: PropTypes.number,
  totalSellTax: PropTypes.number,
  totalBuyIncludeTax: PropTypes.number,
  totalSellIncludeTax: PropTypes.number,
  isDifferentAddress: PropTypes.bool,
  onShowHideDifferentAddress: PropTypes.func,
  termsConditions: PropTypes.bool,
  onChangeTermsConditions: PropTypes.func,
  discountTotal: PropTypes.number,
  orderError: PropTypes.string,
  disableSubmitBtn: PropTypes.bool,
  bank: PropTypes.object,
  banks: PropTypes.array,
  clearingNumber: PropTypes.string,
  accountNumber: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  transportAndInsurance: makeSelectTransportAndInsurance(),
  isShowHideDropdown: makeSelectIsShowHide(),
  isBuyTabActive: makeSelectIsBuyActive(),
  totalBuyQuantity: makeSelectTotalBuyQuantity(),
  totalSellQuantity: makeSelectTotalSellQuantity(),
  totalBuyToPay: makeSelectTotalBuyToPay(),
  totalSellToPay: makeSelectTotalSellToPay(),
  totalBuyWithoutTax: makeSelectTotalBuyWitoutTax(),
  totalSellWithoutTax: makeSelectTotalSellWitoutTax(),
  totalBuyTax: makeSelectTotalBuyTax(),
  totalSellTax: makeSelectTotalSellTax(),
  totalBuyIncludeTax: makeSelectTotalBuyIncludeTax(),
  totalSellIncludeTax: makeSelectTotalSellIncludeTax(),
  listProduct: makeSelectListProduct(),
  cartBuyProducts: makeSelectCartBuyProducts(),
  cartSellProducts: makeSelectCartSellProducts(),
  deliveryWithoutVAT: makeSelectDeliveryWitoutVAT(),
  deliveryVAT: makeSelectDeliveryVAT(),
  discount: makeSelectDiscount(),
  validCoupon: makeSelectValidCoupon(),
  couponDescription: makeSelectCouponDescription(),
  isPrivateActive: makeSelectIsPrivateActive(),
  message: makeSelectMessage(),
  billingName: makeSelectBillingName(),
  fullName: makeSelectFullName(),
  billingCompany: makeSelectBillingCompany(),
  billingAddress: makeSelectBillingAddress(),
  billingOrganizationNumber: makeSelectBillingOrganizationNumber(),
  billingPostalCode: makeSelectBillingPostalCode(),
  billingCity: makeSelectBillingCity(),
  billingEmail: makeSelectBillingEmail(),
  billingPhone: makeSelectBillingPhone(),
  isDifferentAddress: makeSelectIsDifferentAddress(),
  shippingAddress: makeSelectShippingAddress(),
  shippingPostalCode: makeSelectShippingPostalCode(),
  shippingCity: makeSelectShippingCity(),
  useShippingAddress: makeSelectUseShippingAddress(),
  termsConditions: makeSelectTermsConditions(),
  discountTotal: makeSelectDiscountTotal(),
  orderError: makeSelectOrderError(),
  disableSubmitBtn: makeSelectDisableSubmitBtn(),
  paymentMethod: makeSelectPaymentMethod(),
  goldPrice: makeSelectGoldPrice(),
  silverPrice: makeSelectSilverPrice(),
  platinumPrice: makeSelectPlatinumPrice(),
  palladiumPrice: makeSelectPalladiumPrice(),
  bank: makeSelectBank(),
  clearingNumber: makeSelectClearingNumber(),
  accountNumber: makeSelectAccountNumber(),
});

function mapDispatchToProps(dispatch) {
  return {
    onShowHideDropdown: () => dispatch(showHideDropdown()),
    onHideTransportDropdown: () => dispatch(hideTransportDropdown()),
    onChangetransportAndInsurance: value =>
      dispatch(changetransportAndInsurance(value)),
    onReloadDeliverySetting: () => dispatch(reloadDeliverySetting()),
    dispatch,
    onChangeTab: val => dispatch(changeTab(val)),
    onChangeDiscount: evt => dispatch(changeDiscount(evt.target.value)),
    onCheckDiscountCode: discountCode => dispatch(checkDiscountCode(discountCode)),
    onRemoveCoupon: () => dispatch(removeCoupon()),
    onChangeMessage: data => dispatch(changeMessage(data.target.value)),
    onClickConfirmOrder: data => dispatch(confirmOrder(data)),
    onChangeClient: val => dispatch(onChangeClient(val)),
    onShowHideDifferentAddress: (val) => dispatch(showHideDifferentAddress(val)),
    onUseShippingAddress: (val) => dispatch(usingShippingAddress(val)),
    onChangeTermsConditions: data => dispatch(changeTermsConditions(data.target.checked)),
    onSelectPaymentMethod: val => dispatch(selectPatmentMethod(val)),
    updatePrice: data => dispatch(getupdatedPrice(data)),
  }
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect)
  (CartPage);
