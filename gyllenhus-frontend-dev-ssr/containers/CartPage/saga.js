import { call, select, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import { getPriceList } from '../../utils/loadData';
import Cookie from 'js-cookie'
import {
  CHECK_DISCOUNT_CODE, CONFIRM_ORDER, UPDATE_PRICE, RELOAD_DELIVERY_SETTING
} from './constants';
import {
  getCouponSuccess, getCouponError, getOrderSuccess, getOrderError, updatedPriceSuccess, changeTotal
} from './actions';
import { makeSelectJwtToken } from '../App/selectors';
import validate from 'schema-utils';
import lodash from 'lodash';
import { roundNumberValuePurity } from '../../utils/formatter';

const backendApiUrl = process.env.REACT_APP_BACKEND_API_URL;

/**
 * load popular gold product
 * @return {object}
 */
export function* checkDiscountCode(coupon) {
  const getCouponEndpoint =
    process.env.REACT_APP_API_GET_COUPON;
  const requestURL = `${backendApiUrl}/${getCouponEndpoint}`;
  const couponCode = coupon.discountCode.discount;
  try {
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify({ couponCode }),
    });
    const language = Cookie.get('locale');
    const couponInvalid = language === "en" ? "This coupon is invalid" : "Den här kupongen är ogiltig";

    if (response.code === 'gyllenhus_get_coupon_success' && response.data.coupon) {
      const coupon = response.data.coupon;
      const couponStatus = coupon.couponStatus;
      if (couponStatus === "active") {
        yield put(getCouponSuccess(coupon));
      } else if (couponStatus === "expiry") {
        const couponExpired = language === "en" ? "This coupon has expired" : "Den här kupongen har utgått";
        yield put(getCouponError(couponExpired));
      } else {
        yield put(getCouponError(couponInvalid));
      }
    } else {
      yield put(getCouponError(couponInvalid));
    }
  } catch (e) {
    yield put(getCouponError(couponInvalid));
  }
}


export function* confirmOrder(data) {
  const isBuy = data.data.isBuyTabActive;
  const getOrderEndpoint = isBuy ? process.env.REACT_APP_API_CHECKOUT_BUY : process.env.REACT_APP_API_CHECKOUT_SELL;
  const requestURL = `${backendApiUrl}/${getOrderEndpoint}`;
  const language = Cookie.get('locale')

  const cartProducts = data.data.cartProducts;
  const dataProducts = [];
  for (const [index, value] of cartProducts.entries()) {
    const productVat = value.item.item.productVat;
    const price = value.price;
    const quantity = value.quantity;
    const pricePurity = roundNumberValuePurity(price);
    const subTotal = roundNumberValuePurity(pricePurity * quantity);
    const subTotalExVAT = roundNumberValuePurity(pricePurity / (1 + productVat/100) * quantity);
        
    dataProducts.push({
      "productID": value.id,
      "producerID": value.item.item.typeBrands.length > 0 ? value.item.item.typeBrands[0].ID : "",
      "quantity": quantity,
      "price" : pricePurity,
      "subTotalExVat" : subTotalExVAT,
      "Vat" : (subTotal- subTotalExVAT),
      "subTotal" : subTotal
    })
  }

  const billingAddress = data.data.billingAddress;
  const shippingAddress = data.data.shippingAddress;
  const useShippingAddress = data.data.useShippingAddress;
  const shippingMethod = data.data.shippingMethod;
  const coupon = data.data.coupon;
  const paymentMethod = data.data.paymentMethod;
  const message = data.data.message;

  try {
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify({
        language, dataProducts, billingAddress, shippingAddress, useShippingAddress, 
        coupon, shippingMethod, paymentMethod, message
      })
    });
    if (response.code === 'gyllenhus_buy_success' || response.code === 'gyllenhus_sell_success') {
      yield put(getOrderSuccess(response, isBuy));
      yield call(forwardTo, '/completed-purchase');
      localStorage.setItem('orderId', JSON.stringify(action.data.data.orderID));
    } else {
      yield put(getOrderError(response.message));
    }
  } catch (e) {
    yield put(getOrdererror(response));
  }
}

export function* loadNewPrice() {
  let list = yield call(getPriceList);

  let gold = lodash.find(list, { 'symbol': 'XAUSEK' });
  let silver = lodash.find(list, { 'symbol': 'XAGSEK' });
  let platinum = lodash.find(list, { 'symbol': 'PLASEK' });
  let palladium = lodash.find(list, { 'symbol': 'PALSEK' });

  yield put(updatedPriceSuccess({
    'gold': gold,
    'silver': silver,
    'platinum': platinum,
    'palladium': palladium
  }));
}



export function* getDeliveryCostSettingByUserType() {
  const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
  const getDeliveryCostSetting = process.env.REACT_APP_API_GET_DELIVERY_COST_SETTING;
  const requestURL = `${apiUrl}${getDeliveryCostSetting}`;
  const userInfo = JSON.parse(localStorage.getItem('userInfo'));
  const idUserType = _.get(userInfo, 'ID', 0);
  try {
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify({ userType: idUserType }),
    });
    if (response.code === 'gyllenhus_get_delivery_success') {
      localStorage.setItem("deliverySetting", JSON.stringify(response.data));
      yield put(changeTotal(true));
    }
  } catch (e) {
  }
}

function forwardTo(location) {
  window.location.href = (location);
}

export default function* HomePage() {
  yield takeLatest(CHECK_DISCOUNT_CODE, checkDiscountCode);
  yield takeLatest(CONFIRM_ORDER, confirmOrder);
  yield takeLatest(UPDATE_PRICE, loadNewPrice);
  yield takeLatest(RELOAD_DELIVERY_SETTING, getDeliveryCostSettingByUserType);
}
