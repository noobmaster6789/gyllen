/*
 * MyAccountWidget Messages
 *
 * This contains all the text for the MyAccountWidget container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  bank: {
    id: `${scope}.bank`,
    defaultMessage: 'bank',
  },
  delivery: {
    id: `${scope}.delivery`,
    defaultMessage: 'Delivery',
  },
  expiredCoupon: {
    id: `${scope}.expiredCoupon`,
    defaultMessage: 'My expiredCoupon',
  },
  invalidCoupon: {
    id: `${scope}.invalidCoupon`,
    defaultMessage: 'invalidCoupon',
  },
  pleaseAgreePrivacyPolicy: {
    id: `${scope}.pleaseAgreePrivacyPolicy`,
    defaultMessage:
      'Please indicate that you have read and agree to the Terms and Conditions and Privacy Policy',
  },
  pleaseChooseShippingMethod: {
    id: `${scope}.pleaseChooseShippingMethod`,
    defaultMessage: 'Please choose a shipping method',
  },
  pleaseSelectPaymentMethod: {
    id: `${scope}.pleaseSelectPaymentMethod`,
    defaultMessage: 'Please select a payment method',
  },
  deliveryOptionDefault: {
    id: `${scope}.deliveryOptionDefault`,
    defaultMessage: 'Please select a delivery option',
  },
  back: {
    id: `${scope}.back`,
    defaultMessage: 'Back',
  },
  checkout: {
    id: `${scope}.checkout`,
    defaultMessage: 'Checkout',
  },
  buyOrder: {
    id: `${scope}.buyOrder`,
    defaultMessage: 'Buy Order',
  },
  sellOrder: {
    id: `${scope}.sellOrder`,
    defaultMessage: 'Sell Order',
  },
  product: {
    id: `${scope}.product`,
    defaultMessage: 'Product',
  },
  remove: {
    id: `${scope}.remove`,
    defaultMessage: 'Remove',
  },
  price: {
    id: `${scope}.price`,
    defaultMessage: 'Price',
  },
  quantity: {
    id: `${scope}.quantity`,
    defaultMessage: 'Quantity',
  },
  subtotal: {
    id: `${scope}.subtotal`,
    defaultMessage: 'Subtotal',
  },
  subtotalExVAT: {
    id: `${scope}.subtotalExVAT`,
    defaultMessage: 'Subtotal(ex. VAT)',
  },
  VAT: {
    id: `${scope}.VAT`,
    defaultMessage: 'VAT',
  },
  transportAndInsurance: {
    id: `${scope}.transportAndInsurance`,
    defaultMessage: 'Transport and Insurance',
  },
  total: {
    id: `${scope}.total`,
    defaultMessage: 'Total',
  },
  transportDescription: {
    id: `${scope}.transportDescription`,
    defaultMessage: 'All packages are fully insured against loss, theft and physical damage during transit',
  },
  discount: {
    id: `${scope}.discount`,
    defaultMessage: 'Discount',
  },
  enterVoucherCode: {
    id: `${scope}.enterVoucherCode`,
    defaultMessage: 'Enter Voucher Code',
  },
  apply: {
    id: `${scope}.apply`,
    defaultMessage: 'Apply',
  },
  selectPaymentOption: {
    id: `${scope}.selectPaymentOption`,
    defaultMessage: 'Select Payment Option',
  },
  invoice: {
    id: `${scope}.invoice`,
    defaultMessage: 'Invoice',
  },
  creditCard: {
    id: `${scope}.creditCard`,
    defaultMessage: 'Credit Card',
  },
  bankPayment: {
    id: `${scope}.bankPayment`,
    defaultMessage: 'Bank Payment',
  },
  swish: {
    id: `${scope}.swish`,
    defaultMessage: 'Swish',
  },

  yourInfomation: {
    id: `${scope}.yourInfomation`,
    defaultMessage: 'Your information',
  },
  messageToGyllenhus: {
    id: `${scope}.messageToGyllenhus`,
    defaultMessage: 'Message to Gyllenhus',
  },
  privateClient: {
    id: `${scope}.privateClient`,
    defaultMessage: 'Private Client',
  },
  businessClient: {
    id: `${scope}.businessClient`,
    defaultMessage: 'Business Client',
  },
  name: {
    id: `${scope}.name`,
    defaultMessage: 'Name',
  },
  companyName: {
    id: `${scope}.companyName`,
    defaultMessage: 'Company Name',
  },
  city: {
    id: `${scope}.city`,
    defaultMessage: 'City',
  },
  postalCode: {
    id: `${scope}.postalCode`,
    defaultMessage: 'Postal Code',
  },
  phoneNumber: {
    id: `${scope}.phoneNumber`,
    defaultMessage: 'Phone Number',
  },
  organisationNumber: {
    id: `${scope}.organisationNumber`,
    defaultMessage: 'Organisation Number',
  },
  personalNumber: {
    id: `${scope}.personalNumber`,
    defaultMessage: 'Personal Number',
  },
  differentBillingDelivery: {
    id: `${scope}.differentBillingDelivery`,
    defaultMessage: 'Use different billing/delivery address',
  },
  differentThanBilling: {
    id: `${scope}.differentThanBilling`,
    defaultMessage: 'Separate Delivery Address',
  },
  differentBillingDeliveryChosen: {
    id: `${scope}.differentBillingDeliveryChosen`,
    defaultMessage: 'Other delivery address chosen',
  },
 
  termConditions: {
    id: `${scope}.termConditions`,
    defaultMessage: ' By clicking on the “Confirm Order” you agree with ',
  },
  termConditions2: {
    id: `${scope}.termConditions2`,
    defaultMessage: 'Gyllenhus Terms & Conditions.',
  },
  confirmOrder: {
    id: `${scope}.confirmOrder`,
    defaultMessage: 'Confirm Order',
  },
  specifyBankInformation: {
    id: `${scope}.specifyBankInformation`,
    defaultMessage: 'Specify your bank information for receiving payment. Payment is sent out within x days of Gyllenhus receiving the products.',
  },
  bankName: {
    id: `${scope}.bankName`,
    defaultMessage: 'Bank Name',
  },
  clearingNumber: {
    id: `${scope}.clearingNumber`,
    defaultMessage: 'Clearing Number',
  },
  accountNumber: {
    id: `${scope}.accountNumber`,
    defaultMessage: 'Account Number',
  },
  selectPaymentMethod: {
    id: `${scope}.selectPaymentMethod`,
    defaultMessage: 'Please select a payment method',
  },
  chooseShippingMethod: {
    id: `${scope}.chooseShippingMethod`,
    defaultMessage: 'Please choose a shipping method',
  },
  specifyBankInformationInvoice: {
    id: `${scope}.specifyBankInformationInvoice`,
    defaultMessage: 'Specify your bank information for the invoice. Product is sent out within x days of Gyllenhus receiving the payment.',
  },
  idNumberError: {
    id: `${scope}.idNumberError`,
    defaultMessage: 'Please enter a valid personal id number.',
  },
  totalToPay: {
    id: `${scope}.totalToPay`,
    defaultMessage: 'Total to Pay',
  },
  totalToRecieve: {
    id: `${scope}.totalToRecieve`,
    defaultMessage: 'Total to Recieve',
  },
  billing: {
    id: `${scope}.billing`,
    defaultMessage: 'Billing',
  },
  
});
