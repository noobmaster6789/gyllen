import React, { memo, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import ProductImageSlider from '../../components/ProductImageSlider';
import ProductDetail from '../ProductDetail';
import { makeSelectShowProducer, makeSelecectProducerId } from './selectors';
import ProductSpecifications from '../../components/ProductSpecifications';
import ProductList from '../../components/ProductList';
import { makeSelectGoldPrice, makeSelectSilverPrice, makeSelectPalladiumPrice, makeSelectPlatinumPrice, makeSelectCurrency } from '../App/selectors';
import { loadGoldPrice, loadSilverPrice, loadPlatinumPrice, loadPalladiumPrice } from '../App/actions';
import { getPriceList } from '../../utils/loadData';
import ReactHtmlParser from 'react-html-parser';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

export function ProductPage(props) {
  const {
    defaultCurrency,
    dispatch,
    product,
    showProducer,
    goldPrice,
    silverPrice,
    platinumPrice,
    palladiumPrice,
    onLoadGoldPrice,
    onLoadSilverPrice,
    onLoadPlatinumPrice,
    onLoadPalladiumPrice,
    producerId
  } = props;

  var jwtToken = "";
  useEffect(() => {
    jwtToken = localStorage.getItem('jwtToken');
    if (jwtToken) {
      // logged
    }
  }, [jwtToken]);

  useEffect(() => {
    const jwtToken = localStorage.getItem('jwtToken');
    const authorization = jwtToken && jwtToken !== '' ? `Bearer ${jwtToken}` : '';
    const priceList = getPriceList(null, authorization);
    getPriceList(null, authorization).then(priceList => {
      onLoadGoldPrice(priceList, defaultCurrency);
      onLoadSilverPrice(priceList, defaultCurrency);
      onLoadPlatinumPrice(priceList, defaultCurrency);
      onLoadPalladiumPrice(priceList, defaultCurrency);
    });
  }, []);

  const [tabActive, setTabActive] = useState(0);
  const onChangeTab = (tab, e) => {
    setTabActive(tab);
    e.preventDefault();
  }

  if (!product || product == null) {
    return (
      <React.Fragment>
        404 Not Found
      </React.Fragment>
    );
  }
  const relatedProductList = {
    data: product.productRelated,
    defaultCurrency,
    goldPrice,
    silverPrice,
    platinumPrice,
    palladiumPrice,
    dispatch,
  };
  const detailProduct = {
    item: product,
    defaultCurrency,
    goldPrice,
    silverPrice,
    platinumPrice,
    palladiumPrice,
    dispatch,
  };
  const productImages = product.images;
  const productThumbnails = product.gallery;  
  return (
    <React.Fragment>
      <div className="wrapper product-single product-list list-view">
        <a className="nav-back" href="/">← <FormattedMessage {...messages.back} /></a>
        <div className={`item card item-single ${showProducer ? "show-producer" : ""}`}>
          <ProductImageSlider productImages={productImages} productThumbnails={productThumbnails} />
          <ProductDetail detailProduct={detailProduct} />
        </div>
      </div>
      <div className="wrapper">
        <div className="product-details">
          <nav className="tab-wrapper">
            <ul className="tab">
              <li className={tabActive == 0 ? "active" : ""}><a href="#" onClick={(e) => onChangeTab(0, e)}><FormattedMessage {...messages.specifications} /></a></li>
              <li className={tabActive == 1 ? "active" : ""}><a href="#" onClick={(e) => onChangeTab(1, e)}><FormattedMessage {...messages.productHistory} /></a></li>
              <li className={tabActive == 2 ? "active" : ""}><a href="#" onClick={(e) => onChangeTab(2, e)}><FormattedMessage {...messages.delivery} /></a></li>
            </ul>
          </nav>
          <div className="tab-content">
            {tabActive == 0 ?
              <ProductSpecifications {...product} producerId={producerId} /> : ""}
            {tabActive == 1 ? ReactHtmlParser(product.productHistory) : ""}
            {tabActive == 2 ? ReactHtmlParser(product.productDelivery) : ""}

          </div>
        </div>
      </div>
      <div className="wrapper section popular">
        <div className="section-head">
          <h2 className="title"><FormattedMessage {...messages.relatedProducts} /></h2>
          <a className="link" href="#"><FormattedMessage {...messages.seeAll} /> ›</a>
        </div>
        <div className="product-list grid-view">
          <ProductList {...relatedProductList} />
        </div>
      </div>
    </React.Fragment>
  );
}


ProductPage.propTypes = {
  defaultCurrency: PropTypes.string,
  showProducer: PropTypes.bool,
  goldPrice: PropTypes.object,
  silverPrice: PropTypes.object,
  platinumPrice: PropTypes.object,
  palladiumPrice: PropTypes.object,
  onLoadGoldPrice: PropTypes.func,
  onLoadSilverPrice: PropTypes.func,
  onLoadPlatinumPrice: PropTypes.func,
  onLoadPalladiumPrice: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  showProducer: makeSelectShowProducer(),
  goldPrice: makeSelectGoldPrice(),
  silverPrice: makeSelectSilverPrice(),
  platinumPrice: makeSelectPlatinumPrice(),
  palladiumPrice: makeSelectPalladiumPrice(),
  defaultCurrency: makeSelectCurrency(),
  producerId : makeSelecectProducerId()

});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onLoadGoldPrice: (prices, currency) =>
      dispatch(loadGoldPrice(prices, currency)),
    onLoadSilverPrice: (prices, currency) =>
      dispatch(loadSilverPrice(prices, currency)),
    onLoadPlatinumPrice: (prices, currency) =>
      dispatch(loadPlatinumPrice(prices, currency)),
    onLoadPalladiumPrice: (prices, currency) =>
      dispatch(loadPalladiumPrice(prices, currency)),


  }
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect)
  (ProductPage);
