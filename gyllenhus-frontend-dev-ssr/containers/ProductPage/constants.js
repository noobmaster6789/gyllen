/*
 *
 * Product Page constants
 *
 */

export const CHANGE_BRAND = 'app/ProductPage/CHANGE_BRAND';
export const SHOW_HIDE_PRODUCER = 'app/ProductPage/SHOW_HIDE_PRODUCER';
