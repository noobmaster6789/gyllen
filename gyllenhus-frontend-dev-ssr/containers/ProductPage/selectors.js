import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the registerPage state domain
 */

const selectProduct = state => state.productPage || initialState;

const makeSelectProducer = () =>
  createSelector(
    selectProduct,
    productState => productState.producer,
  );
const makeSelecectProducerId = () =>
  createSelector(
    selectProduct,
    productState => productState.producerId,
  );

const makeSelectShowProducer = () =>
  createSelector(
    selectProduct,
    productState => productState.showProducer,
  );

export {
  makeSelectProducer,
  makeSelectShowProducer,
  makeSelecectProducerId
};
