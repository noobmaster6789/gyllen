/*
 * MyAccountWidget Messages
 *
 * This contains all the text for the MyAccountWidget container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  back: {
    id: `${scope}.back`,
    defaultMessage: 'back',
  },
  seeAll: {
    id: `${scope}.seeAll`,
    defaultMessage: 'See All',
  },
  relatedProducts: {
    id: `${scope}.relatedProducts`,
    defaultMessage: 'Related Products',
  },
  specifications: {
    id: `${scope}.specifications`,
    defaultMessage: 'Specifications',
  },
  productHistory: {
    id: `${scope}.productHistory`,
    defaultMessage: 'Product History',
  },
  delivery: {
    id: `${scope}.delivery`,
    defaultMessage: 'Delivery',
  },
  
});
