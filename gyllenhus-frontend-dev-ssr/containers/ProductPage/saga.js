import { call, select, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import Cookie from 'js-cookie'


const backendApiUrl = process.env.REACT_APP_BACKEND_API_URL;

/**
 * load product by Id
 * @return {object}
 */
export function* getProductById() {
  const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
  const getProductById = ""; //Waiting API ;
  const requestURL = `${apiUrl}${getProductById}`;
  const userInfo = JSON.parse(localStorage.getItem('userInfo'));
  const idUserType = _.get(userInfo, 'ID', 0);
  try {
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify({ userType: idUserType }),
    });
    if (response.code === 'gyllenhus_get_product_success') {
      // Success
    }
  } catch (e) {
  }
}

export default function* ProductPage() {
}
