import { SHOW_HIDE_PRODUCER, CHANGE_BRAND } from "./constants";

/*
 *
 * Product Page actions
 *
 */

export function changeBrand(brandId, brandName) {
  return {
    type: CHANGE_BRAND,
    brandId, brandName
  };
}

export function showHideProducer(showHide) {
  return {
    type: SHOW_HIDE_PRODUCER,
    showHide
  };
}



