/*
 *
 * RegisterPage reducer
 *
 */
import produce from 'immer';
import { SHOW_HIDE_PRODUCER, CHANGE_BRAND } from './constants';

export const initialState = {
  showProducer : false
};

const productPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case CHANGE_BRAND:
        draft.producer = action.brandName;
        draft.producerId = action.brandId;
        draft.showProducer = false;
        break;
      case SHOW_HIDE_PRODUCER:
        draft.showProducer = action.showHide;
        break;
    }
  });

export default productPageReducer;
