/*
 * TradingViewPage Messages
 *
 * This contains all the text for the TradingViewPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.TradingViewPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the TradingViewPage container!',
  },
});
