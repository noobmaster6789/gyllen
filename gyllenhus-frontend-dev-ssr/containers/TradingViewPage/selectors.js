import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the tradingViewPage state domain
 */

const selectTradingViewPageDomain = state =>
  state.tradingViewPage || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by TradingViewPage
 */

const makeSelectTradingViewPage = () =>
  createSelector(
    selectTradingViewPageDomain,
    substate => substate,
  );

export default makeSelectTradingViewPage;
export { selectTradingViewPageDomain };
