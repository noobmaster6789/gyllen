import styled from 'styled-components';

const Wrapper = styled.div`
  & .tab {
    height: 2.5em;
    float: left;
    cursor: pointer;
  }
  & .tab.active {
    background: #f9f9f9;
    color: #3366b7;
    border-bottom-color: #19396e;
  }
  & .pane-legend-line__wrap-description {
    display: none;
  }
  & .clearfix {
    clear: both;
  }
  & label {
    border-radius: 0.25em 0.25em 0 0;
    cursor: pointer;
    display: block;
    float: left;
    font-size: 1em;
    height: 2.5em;
    line-height: 2.5em;
    margin-right: 0.25em;
    padding: 0 1.5em;
    text-align: center;
  }

  #content {
    min-height: 610px;
    width: 100%;
    z-index: 5;
  }

  #content div {
    z-index: -100;
  }
`;

export default Wrapper;
