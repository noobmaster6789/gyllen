/**
 *
 * Asynchronously loads the component for TradingViewPage
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
