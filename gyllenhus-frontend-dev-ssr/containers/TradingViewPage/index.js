/**
 *
 * TradingViewPage
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import makeSelectTradingViewPage from './selectors';
import Wrapper from './Wrapper';
import TradingViewDemo from '../../components/TradingViewDemo';

export class TradingViewPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { 
      tab: 1,
      filter1: 'SEK',
      filter2: 'XAU',
      filter3: 'OZ',
    };

    this.handleChangeTab = this.handleChangeTab.bind(this);
  }

  handleChangeTab(tab) {
    // [TODO] need to limit maximum value by product stock
    this.setState({ tab });
  }

  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>Trading View</title>
          <meta name="description" content="Description of TradingViewPage" />
        </Helmet>
        <Wrapper className="banner wrapper" style={{ height: 'auto' }}>
          <div>
            <div style={{ paddingTop : 20 }}>
            <select style={{ margin: 12 }} value={this.state.filter1} onChange={(value) => this.setState({ filter1: value.target.value })}>
              <option value="SEK">SEK</option>
              <option value="EUR">EUR</option>
              <option value="USD">USD</option>
            </select>
            <select style={{ margin: 12 }} value={this.state.filter2} onChange={(value) => this.setState({ filter2: value.target.value })}>
              <option value="XAU">AU</option>
              <option value="XAG">AG</option>
            </select>
            <select style={{ margin: 12 }} value={this.state.filter3} onChange={(value) => this.setState({ filter3: value.target.value })}>
              <option value="KG">KG</option>
              <option value="OZ">OZ</option>
              <option value="G">G</option>
            </select>
            </div>
            <div className="clearfix" />
            <div id="content">
              <div style={{ display: this.state.tab === 1 ? 'block' : 'none' }}>
                <TradingViewDemo symbol={`${this.state.filter2}${this.state.filter1}-${this.state.filter3}`} containerId="trading-1" />
              </div>
              
            </div>
          </div>
        </Wrapper>
      </React.Fragment>
    );
  }
}

TradingViewPage.propTypes = {};

const mapStateToProps = createStructuredSelector({
  tradingViewPage: makeSelectTradingViewPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(TradingViewPage);
