/**
 *
 * Completed Purchase Page
 *
 */

import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PlaceholderLoading from '../../components/PlaceholderLoading/purchase';


export function PurchasePage(props) {
  const [isLoading, setIsLoading] = useState(true);
  const [orderId, setOrderId] = useState("");
  const [pendingOrder, setPendingOrder] = useState(false);
  const [buyType, setBuyType] = useState(false);

  useEffect(() => {
    setOrderId(localStorage.getItem("orderId") || "");
    const cartBuy = localStorage.getItem("cartBuyItems") || "";
    const cartSell = localStorage.getItem("cartSellItems") || "";
    if (cartBuy && cartBuy.length > 0 && cartBuy != "[]") {
      setPendingOrder(true);
      setBuyType(true);
    } else if (cartSell && cartSell.length > 0 && cartSell != "[]") {
      setPendingOrder(true);
      setBuyType(false);
    }
    setIsLoading(false);
  }, []);
  
  if (isLoading || orderId === "") {
    const loadingPlaceholder = [
      <div class="wrapper">
        <div className="item-empty">
          <PlaceholderLoading />
        </div>
      </div>,
    ];
    return loadingPlaceholder;
  }
  
  return (
    <div className="wrapper user-portal">
      <div>
          <h2> Thank You!</h2>
          <p> Your {buyType ? "Sell" : "Buy"} Order #{orderId} is being processed, you will receive an email confirmation. </p>
          <br/>
          {pendingOrder ? goToPendingOrder(buyType) : goToMyTransaction()}
      </div>
    </div>
  );
}

function goToPendingOrder(isBuy) {
  return ( <div> <a className="button" href="/cart">Continues</a> to finalize the {isBuy? "buy" : "sell"} pending <b> Order.</b>
    </div>)
  ;
}

function goToMyTransaction() {
  return ( <div> <a className="button" href="/">Continues</a> to view <b> My Transactions.</b>
    </div>)
  ;
}

PurchasePage.propTypes = {
};

const mapStateToProps = createStructuredSelector({

});

function mapDispatchToProps(dispatch) {
  return {
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(PurchasePage);
