/**
 *
 * SpecialOfferItem
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { createStructuredSelector } from 'reselect';
import ProductName from '../../components/ProductName';
import Brand from '../../components/Brand';
import ProductWeight from '../../components/ProductWeight';
import {
  makeSelectCurrency,
  makeSelectGoldPrice,
  makeSelectSilverPrice,
  makeSelectPlatinumPrice,
  makeSelectPalladiumPrice,
} from '../App/selectors';
import QuantityInput from '../../components/QuantityInput';

const REACT_APP_API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT
const offerIcon = `${REACT_APP_API_ENDPOINT}/static/images/special-offer.png`

export class SpecialOfferItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { quantity: 1 };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeQuantity = this.handleChangeQuantity.bind(this);
  }

  handleChange(event) {
    let { value } = event.target;
    value = Number(value);
    // [TODO] need to limit maximum value by product stock
    this.setState({ quantity: value > 0 ? value : 1 });
  }

  handleChangeQuantity(value) {
    let { quantity } = this.state;
    quantity = Number(quantity);
    // [TODO] need to limit maximum value by product stock
    this.setState({ quantity: quantity + value > 0 ? quantity + value : 1 });
  }

  render() {
    const {
      item: {
        product,
        images,
        productPurity,
        productVat,
        productWeight,
        typeProduct,
        typeBrands,
        productBuyMargin,
        productSellMargin,
      },
      currency,
      goldPrice,
      silverPrice,
      platinumPrice,
      palladiumPrice,
    } = this.props;
    let buyPrice = null;
    let sellPrice = null;
    if (
      goldPrice &&
      silverPrice &&
      platinumPrice &&
      palladiumPrice &&
      typeof goldPrice === 'object' &&
      typeof silverPrice === 'object' &&
      typeof platinumPrice === 'object' &&
      typeof palladiumPrice === 'object'
    ) {
      if (typeProduct === 'platinum') {
        sellPrice = platinumPrice.price;
        buyPrice = platinumPrice.price;
      } else if (typeProduct === 'palladium') {
        sellPrice = palladiumPrice.price;
        buyPrice = palladiumPrice.price;
      } else if (typeProduct === 'gold') {
        sellPrice = goldPrice.price;
        buyPrice = goldPrice.price;
      } else if (typeProduct === 'silver') {
        sellPrice = silverPrice.price;
        buyPrice = silverPrice.price;
      }
    }
    let imageUrl = null;
    if (typeof images !== 'undefined' && images.url) {
      imageUrl = images.url;
    }
    return (
      <div className="item">
        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <a href="#" className="image">
          <img src={imageUrl} alt={product.title} />
        </a>
        <div className="info">
          <div className="group">
            <ProductName {...productWeight} title={product.title} />
            {/* <div className="product">Silver Bar</div> */}
          </div>
          <div className="group">
            {typeof typeBrands !== 'undefined' ? (
              <Brand items={typeBrands} />
            ) : null}
            <div className="condition">Brand new</div>
            <div className="detail">
              <p>
                <strong>Dimension:</strong> 4mm x 3mm x 1mm
              </p>
              <p>
                <strong>Purity:</strong> 99,99% (24k)
              </p>
              <p>
                <strong>Fine weight:</strong> 1000 gram (32,15 troy oz)
              </p>
              <p>
                <strong>Country of origin:</strong> Switzerland
              </p>
              <p>
                <strong>Packaging:</strong> Encased in an interim package
                provided by Gyllenhus
              </p>
            </div>
            <div className="description">{product.excerpt}</div>
            <div className="offer">
              <div>
                <p className="discount-price">
                  <img
                    className="icon"
                    srcSet={offerIcon}
                    alt="special offer icon"
                  />
                  Price 12000 kr
                </p>
                <p className="discount-percent">Save 20%</p>
              </div>
              <div className="original-price">
                Price
                <ProductWeight
                  {...productWeight}
                  currency={currency}
                  price={buyPrice}
                  margin={0}
                  productPurity={productPurity}
                  productVat={productVat}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="button-group">
          <button className="sell" type="button">
            <span className="text">Sell</span>
            <span className="percent">-1.00%</span>
            <ProductWeight
              {...productWeight}
              currency={currency}
              price={buyPrice}
              margin={productBuyMargin}
              productPurity={productPurity}
              productVat={productVat}
            />
          </button>
          <QuantityInput
            value={this.state.quantity}
            onChangeHandle={this.handleChange}
            onChangeQuantity={this.handleChangeQuantity}
          />
          <button className="buy" type="button">
            <span className="text">Buy</span>
            <ProductWeight
              {...productWeight}
              currency={currency}
              price={sellPrice}
              margin={productSellMargin}
            />
            <span className="percent">+2.00%</span>
          </button>
        </div>
      </div>
    );
  }
}

SpecialOfferItem.propTypes = {
  item: PropTypes.shape({
    product: PropTypes.object,
    images: PropTypes.any,
    productPurity: PropTypes.number,
    productVat: PropTypes.number,
    productWeight: PropTypes.object,
    typeProduct: PropTypes.string,
    typeBrands: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.array,
      PropTypes.object,
    ]),
  }),
  currency: PropTypes.string,
  goldPrice: PropTypes.object,
  silverPrice: PropTypes.object,
  platinumPrice: PropTypes.object,
  palladiumPrice: PropTypes.object,
};
const mapStateToProps = createStructuredSelector({
  currency: makeSelectCurrency(),
  goldPrice: makeSelectGoldPrice(),
  silverPrice: makeSelectSilverPrice(),
  platinumPrice: makeSelectPlatinumPrice(),
  palladiumPrice: makeSelectPalladiumPrice(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(SpecialOfferItem);
