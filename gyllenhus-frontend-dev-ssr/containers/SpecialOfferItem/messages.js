/*
 * SpecialOfferItem Messages
 *
 * This contains all the text for the SpecialOfferItem container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  sell: {
    id: `${scope}.sell`,
    defaultMessage: 'Sell',
  },
  buy: {
    id: `${scope}.buy`,
    defaultMessage: 'Buy',
  },
});
