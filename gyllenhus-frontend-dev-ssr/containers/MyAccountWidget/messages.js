/*
 * MyAccountWidget Messages
 *
 * This contains all the text for the MyAccountWidget container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  login: {
    id: `${scope}.login`,
    defaultMessage: 'Login',
  },
  register: {
    id: `${scope}.register`,
    defaultMessage: 'Register',
  },
  myAccount: {
    id: `${scope}.myAccount`,
    defaultMessage: 'My Account',
  },
  logout: {
    id: `${scope}.logout`,
    defaultMessage: 'Logout',
  },
});
