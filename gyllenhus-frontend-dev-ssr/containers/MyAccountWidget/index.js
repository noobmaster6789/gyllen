/**
 *
 * MyAccountWidget
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { compose } from 'redux';

import { createStructuredSelector } from 'reselect';
import A from './A'

import messages from './messages';
import {
  loadUserByJwtToken,
  logout,
  resetJwtToken,
  showLoginModal,
} from '../App/actions';
import { makeSelectIsLoggedIn, makeSelectJwtToken } from '../App/selectors';

export class MyAccountWidget extends React.PureComponent {
  intervalID = 0;

  componentDidMount() {
    // check jwt token expired
    const { onLoadJwtToken } = this.props;
    const jwtToken = localStorage.getItem('jwtToken');
    if (jwtToken) onLoadJwtToken(jwtToken);
  }

  componentDidUpdate(prevProps) {
    clearInterval(this.intervalID);
    // run reset token in a loop
    function useInterval(callback, token, delay) {
      return setInterval(() => {
        callback(token);
      }, delay);
    }
    const jwtToken = this.props.token;
    // jwt token will expire after some time so we need to update the token
    // if exist jwt token: reset new token
    if (jwtToken) {
      const { onResetToken } = prevProps;
      this.intervalID = useInterval(
        onResetToken,
        jwtToken,
        process.env.REACT_APP_RESET_TOKEN_TIME,
      );
    }
  }

  componentWillUnmount() {
    // need clear interval old interval is running
    clearInterval(this.intervalID);
  }

  render() {
    const { isLoggedIn, onLogout, onClickShowLoginModal } = this.props;
    return (
      <React.Fragment>
        {isLoggedIn ? (
          <React.Fragment>
            <a href="#my-account">
              <span className="cursorspan"><FormattedMessage {...messages.myAccount} /> </span>
            </a>
            <span> &nbsp; </span>
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a href="#" onClick={onLogout}>
              <span className="cursorspan"><FormattedMessage {...messages.logout} /> </span>
            </a>
          </React.Fragment>
        ) : (
            <React.Fragment>
              <div> <span> &nbsp; </span></div>
              <div onClick={onClickShowLoginModal}>
                <a href="#">
                  <span className="cursorspan"><FormattedMessage {...messages.login} /></span>
                </a>
              </div>
              <div>
                <a href='/register'>
                  <span className="cursorspan"><FormattedMessage {...messages.register} /></span>
                </a>
              </div>
            </React.Fragment>
          )}
      </React.Fragment>
    );
  }
}

MyAccountWidget.propTypes = {
  token: PropTypes.string,
  isLoggedIn: PropTypes.bool,
  onLoadJwtToken: PropTypes.func,
  onLogout: PropTypes.func,
  onResetToken: PropTypes.func,
  onClickShowLoginModal: PropTypes.func,
  onReloadIndex: PropTypes.func,
};

function mapDispatchToProps(dispatch) {
  return {
    onLoadJwtToken: jwtToken => dispatch(loadUserByJwtToken(jwtToken)),
    onLogout: () => dispatch(logout()),
    onResetToken: token => dispatch(resetJwtToken(token)),
    onClickShowLoginModal: () => dispatch(showLoginModal()),
  };
}

const withConnect = connect(
  createStructuredSelector({
    token: makeSelectJwtToken(),
    isLoggedIn: makeSelectIsLoggedIn(),
  }),
  mapDispatchToProps,
);

export default compose(
  withConnect,
)(MyAccountWidget);
