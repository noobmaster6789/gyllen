import styled from 'styled-components';
import Link from 'next/link'

const A = styled(Link)`
  vertical-align: baseline;
  padding: 0px;
  border-width: 0px;
  border-style: initial;
  border-color: initial;
  border-image: initial;
  font: inherit;
  margin-left: 4px;
  text-decoration: underline;
`;

export default A;
