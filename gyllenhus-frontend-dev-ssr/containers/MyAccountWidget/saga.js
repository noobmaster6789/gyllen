import { call, select, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import { LOAD_USER_BY_JWT_TOKEN, RESET_JWT_TOKEN } from '../App/constants';
import { makeSelectJwtToken } from '../App/selectors';
import { jwTokenInvalid, jwtTokenValid, updateJwtToken, loadIndexes
       } from '../App/actions';
import {
        loadGoldProducts,
        loadSilverProducts,
        loadSpecialOffers,
      } from '../HomePage/actions';
const apiUrl = process.env.REACT_APP_BACKEND_API_URL;

export function* loadUser() {
  const userInfoEndpoint = process.env.REACT_APP_API_GET_USER_INFO_ENDPOINT;
  const authUrl = `${apiUrl}/${userInfoEndpoint}`;
  const jwtToken = yield select(makeSelectJwtToken());
  try {
    const data = yield call(request, authUrl, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${jwtToken}`,
      }),
    });
    if (data.code === 'gyllenhus_get_user_info_success') {
      yield put(jwtTokenValid(data));
      yield put(loadIndexes());
      yield put(loadGoldProducts());
      yield put(loadSilverProducts());
      yield put(loadSpecialOffers());
    } else {
      yield put(jwTokenInvalid());
    }
  } catch (err) {
    yield put(jwTokenInvalid());
  }
}

export function* resetJwtToken() {
  const resetTokenEndpoint = process.env.REACT_APP_API_RESET_TOKEN_ENDPOINT;
  const resetTokenUrl = `${apiUrl}/${resetTokenEndpoint}`;
  const jwtToken = yield select(makeSelectJwtToken());
  try {
    const response = yield call(request, resetTokenUrl, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${jwtToken}`,
      }),
    });
    if (response.code === 'gyllenhus_reset_token_success') {
      yield put(updateJwtToken(response.data.token));
    } else {
      yield put(jwTokenInvalid());
    }
  } catch (err) {
    yield put(jwTokenInvalid());
  }
}

export default function* myAccountSaga() {
  yield takeLatest(LOAD_USER_BY_JWT_TOKEN, loadUser);
  yield takeLatest(RESET_JWT_TOKEN, resetJwtToken);
}
