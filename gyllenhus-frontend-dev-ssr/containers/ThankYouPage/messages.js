/*
 * ThankYouPage Messages
 *
 * This contains all the text for the ThankYouPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Thank You',
  },
  thankYouMessage: {
    id: `${scope}.thankYouMessage`,
    defaultMessage: 'Thank you for your registration.',
  },
});
