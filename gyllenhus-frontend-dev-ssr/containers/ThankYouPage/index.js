/**
 *
 * ThankYouPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { compose } from 'redux';

import messages from './messages';

export function ThankYouPage() {
  return (
    <div className="wrapper user-portal">
      <div>
        <FormattedMessage {...messages.thankYouMessage} />
      </div>
    </div>
  );
}

ThankYouPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(withConnect)(ThankYouPage);
