/**
 *
 * Asynchronously loads the component for ThankYouPage
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
