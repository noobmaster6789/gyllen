/**
 *
 * ContactUsWidget
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectFooterListDataCustomerSupport } from '../FooterList/selectors';

export function ContactUsWidget({ customerSupport }) {
  const { openDate, tfn } = customerSupport;
  const openDateDefault = "Mon-Sun 09:00 – 21:00";
  const tfnDefault = "08-56 45 56";
  return (
    <React.Fragment>
      <p>{!openDate ? openDateDefault : openDate}
      <span>•</span> {!tfn ? tfnDefault : tfn}</p>
    </React.Fragment>
  );
}

ContactUsWidget.propTypes = {
  customerSupport: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
};

const mapStateToProps = createStructuredSelector({
  customerSupport: makeSelectFooterListDataCustomerSupport(),
});

const withConnect = connect(
  mapStateToProps,
  null,
);

export default compose(withConnect)(ContactUsWidget);
