/**
 *
 * ProductButton
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { createStructuredSelector } from 'reselect';
import ProductWeight from '../../components/ProductWeight';
import {
  makeSelectCurrency,
  makeSelectGoldPrice,
  makeSelectSilverPrice,
  makeSelectPalladiumPrice,
  makeSelectPlatinumPrice,
} from '../../containers/App/selectors';
import { FormattedMessage } from 'react-intl';
import QuantityInput from '../../components/QuantityInput';
import messages from './messages';
import { getBuySellPriceProduct } from '../../utils/commons';
import { buyAddToCart, sellAddToCart } from '../CartPage/actions';

export class ProductButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { quantity: 1, animationClassBuySell: "" };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeQuantity = this.handleChangeQuantity.bind(this);
    this.handleBuy = this.handleBuy.bind(this);
    this.handleSell = this.handleSell.bind(this);

  }

  handleChange(event) {
    let { value } = event.target;
    value = Number(value);
    this.setState({ quantity: value > 0 ? value : 1 });
  }

  handleChangeQuantity(value) {
    let { quantity } = this.state;
    quantity = Number(quantity);
    this.setState({ quantity: quantity + value > 0 ? quantity + value : 1 });
  }

  getValuePrice(isBuyType) {
    const {
      item: {
        productPurity,
        productVat,
        productWeight,
        typeProduct,
        productBuyMargin,
        productSellMargin,
      },
      currency,
      goldPrice,
      silverPrice,
      platinumPrice,
      palladiumPrice
    } = this.props;

    return getBuySellPriceProduct(typeProduct, goldPrice, silverPrice, platinumPrice, palladiumPrice,
      productWeight, (isBuyType ? productBuyMargin : productSellMargin), productPurity, productVat);
  }

  handleSell() {
    if (!this.props.goldPrice || !this.props.silverPrice || !this.props.palladiumPrice)
      return;
    let { quantity } = this.state;
    this.props.onSellAddToCart(this.props, quantity);
  }

  handleBuy() {
    if (!this.props.goldPrice || !this.props.silverPrice || !this.props.palladiumPrice)
      return;
    let { quantity } = this.state;
    this.props.onBuyAddToCart(this.props, quantity);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.goldPrice !== this.props.goldPrice) {
      this.setState({ animationClassBuySell: "blink" });
      setTimeout(() => {
        this.setState({ animationClassBuySell: "" });
      }, 1000);
    }
  }

  render() {
    const {
      item: {
        product,
        images,
        productPurity,
        productVat,
        productWeight,
        typeProduct,
        typeBrands,
        productBuyMargin,
        productSellMargin,
      },
      currency,
      goldPrice,
      silverPrice,
      platinumPrice,
      palladiumPrice,
    } = this.props;
    let buyPrice = null;
    let sellPrice = null;
    if (
      goldPrice &&
      silverPrice &&
      platinumPrice &&
      palladiumPrice &&
      typeof goldPrice === 'object' &&
      typeof silverPrice === 'object' &&
      typeof platinumPrice === 'object' &&
      typeof palladiumPrice === 'object'
    ) {
      if (typeProduct === 'platinum') {
        sellPrice = platinumPrice.price;
        buyPrice = platinumPrice.price;
      } else if (typeProduct === 'palladium') {
        sellPrice = palladiumPrice.price;
        buyPrice = palladiumPrice.price;
      } else if (typeProduct === 'gold') {
        sellPrice = goldPrice.price;
        buyPrice = goldPrice.price;
      } else if (typeProduct === 'silver') {
        sellPrice = silverPrice.price;
        buyPrice = silverPrice.price;
      }
    }

    return (
      <React.Fragment>
        <button className="sell" onClick={this.handleSell}>
          <span className="text">
            <FormattedMessage {...messages.sell} />
          </span>
          <span className="percent">-1.00%</span>
          <span className="price">
            <ProductWeight
              {...productWeight}
              currency={currency}
              price={sellPrice}
              margin={productSellMargin}
              productPurity={productPurity}
              productVat={productVat}
            />
          </span>
        </button>
        <QuantityInput
          value={this.state.quantity}
          onChangeHandle={this.handleChange}
          onChangeQuantity={this.handleChangeQuantity}
        />
        <button className="buy" onClick={this.handleBuy}>
          <span className="text">
            <FormattedMessage {...messages.buy} />
          </span>
          <span className="price">
            <ProductWeight
              {...productWeight}
              currency={currency}
              price={buyPrice}
              margin={productBuyMargin}
              productPurity={productPurity}
              productVat={productVat}
            />
          </span>
          <span className="percent">+2.00%</span>
        </button>
      </React.Fragment>
    );
  }
}

ProductButton.propTypes = {
  item: PropTypes.shape({
    product: PropTypes.object,
    productPurity: PropTypes.number,
    productVat: PropTypes.number,
    productWeight: PropTypes.object,
    typeProduct: PropTypes.string,
  }),
  currency: PropTypes.string,
  goldPrice: PropTypes.object,
  silverPrice: PropTypes.object,
  platinumPrice: PropTypes.object,
  palladiumPrice: PropTypes.object,
  dispatch: PropTypes.func,
  countdown: PropTypes.number,
  onBuyAddToCart: PropTypes.func,
  onSellAddToCart: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  currency: makeSelectCurrency(),
  goldPrice: makeSelectGoldPrice(),
  silverPrice: makeSelectSilverPrice(),
  platinumPrice: makeSelectPlatinumPrice(),
  palladiumPrice: makeSelectPalladiumPrice(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onBuyAddToCart: (item, quantity) => dispatch(buyAddToCart(item, quantity)),
    onSellAddToCart: (item, quantity) => dispatch(sellAddToCart(item, quantity)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(ProductButton);
