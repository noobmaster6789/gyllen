/**
 *
 * ProductName
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { makeSelectShowProducer, makeSelectProducer, makeSelecectProducerId } from '../ProductPage/selectors';
import { showHideProducer, changeBrand } from '../ProductPage/actions';
import { createStructuredSelector } from 'reselect';
import ProductDescription from '../../components/ProductDescription';
import ProductButton from './ProductButton';
import ProductName from '../../components/ProductName';
import { formatNumberDecimalPurity } from '../../utils/formatter';
import ProductPromotion from '../../components/ProductPromotion';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

function ProductDetail({
  producer,
  producerId,
  showProducer,
  onShowHideProducer,
  detailProduct,
  onChangeBrand
}) {
  const {
    item: {
      product,
      productWeight,
      typeProduct,
      typeBrands,
      productBuyMargin,
      productSellMargin,
      productStock,
      productDiscounts,
      typeConditions
    },
    goldPrice,
    silverPrice,
    platinumPrice,
    palladiumPrice,
  } = detailProduct;
  let price = null;

  if (
    goldPrice &&
    silverPrice &&
    platinumPrice &&
    palladiumPrice &&
    typeof goldPrice === 'object' &&
    typeof silverPrice === 'object' &&
    typeof platinumPrice === 'object' &&
    typeof palladiumPrice === 'object'
  ) {
    if (typeProduct === 'platinum') {
      price = platinumPrice.price;
    } else if (typeProduct === 'palladium') {
      price = palladiumPrice.price;
    } else if (typeProduct === 'gold') {
      price = goldPrice.price;
    } else if (typeProduct === 'silver') {
      price = silverPrice.price;
    }
  }
  const troyOunceToGram = 31.1034768;

  useEffect(() => {
    if (!producerId || producerId.length === 0 && productStock && productStock.length > 0) {
      onChangeBrand(productStock[0].brandID, productStock[0].brandName);
    }
  }, []);

  return (
    <React.Fragment>
      <div className="info">
        <ProductPromotion productDiscounts={productDiscounts} />
        <div className="group">
          <ProductName {...productWeight} title={product.title} />
        </div>
        <div className="group">
          <ProductDescription typeBrands={typeBrands} producer={producer} product={product} typeConditions={typeConditions} />
          <div className="list-producer">
            <div className="table">
              <div className="table-head">
                <div className="th"><FormattedMessage {...messages.name} /></div>
                <div className="th"><FormattedMessage {...messages.available} /></div>
              </div>
              {
                productStock ? productStock.map((stock, i) => {
                  if (stock.brandName)
                    return (<div className="tr" key={i} onClick={() => onChangeBrand(stock.brandID, stock.brandName)}>
                      <div className="td">
                        {stock.brandName}
                      </div>
                      <div className="td">
                        {stock.productEnableManagementStockProduct ? stock.productStockQuantity :
                          (stock.productStockStatus === "in-stock" ? "∞" : "0")
                        }
                      </div>
                    </div>
                    )
                }) : ""
              }
            </div>
          </div>

        </div>
      </div>
      <div className="button-group">
        <div className="buyback-info">
          <div className="row">
            <div>
              <div className="data-title"><FormattedMessage {...messages.pricePerGram} /></div>
              <div className="data-number">{price ? formatNumberDecimalPurity(price / troyOunceToGram) : ""}</div>
            </div>
            <div>
              <div className="data-title">PREMIUM</div>
              <div className="data-number">{productBuyMargin}%</div>
            </div>
            <div>
              <div className="data-title">BUYBACK</div>
              <div className="data-number">{productSellMargin}%</div>
            </div>
          </div>
        </div>
        {productStock && productStock.length > 1 ? (
          <div className="select-producer" onClick={() => onShowHideProducer(!showProducer)}>
            <button>
              <span className="icon"><i></i><i></i></span>
              <span><FormattedMessage {...messages.multipleProducers} /> ({productStock ? productStock.length : 0})</span>
            </button>
          </div>) : ""}
        <ProductButton {...detailProduct} />
      </div>
    </React.Fragment>
  );
}

ProductDetail.propTypes = {
  producer: PropTypes.string,
  producerId: PropTypes.string,
  showProducer: PropTypes.bool,
  onShowHideProducer: PropTypes.func
};


const mapStateToProps = createStructuredSelector({
  producer: makeSelectProducer(),
  showProducer: makeSelectShowProducer(),
  producerId: makeSelecectProducerId()

});

function mapDispatchToProps(dispatch) {
  return {
    onChangeBrand: (brandId, brandName) => dispatch(changeBrand(brandId, brandName)),
    onShowHideProducer: showHide => dispatch(showHideProducer(showHide)),

  }
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);


export default compose(
  withConnect)
  (ProductDetail);