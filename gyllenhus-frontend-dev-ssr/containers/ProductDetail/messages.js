/*
 * ProductListItem Messages
 *
 * This contains all the text for the ProductListItem container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  sell: {
    id: `${scope}.sell`,
    defaultMessage: 'Sell',
  },
  buy: {
    id: `${scope}.buy`,
    defaultMessage: 'Buy',
  },
  name: {
    id: `${scope}.name`,
    defaultMessage: 'name',
  },
  available: {
    id: `${scope}.available`,
    defaultMessage: 'Available',
  },
  pricePerGram: {
    id: `${scope}.pricePerGram`,
    defaultMessage: 'PRICE PER GRAM',
  },
  multipleProducers: {
    id: `${scope}.multipleProducers`,
    defaultMessage: 'Multiple Producers',
  },

  
});
