import { CHANGE_TEXT_SEARCH, SEARCH_RESULT_SUCCESS, SEARCH_RESULT_EMPTY, CHANGE_SEARCH_ACTIVE } from "./constants";

/*
 *
 * Search actions
 *
 */


export function changeTextSearch(textSearch) {
  return {
    type: CHANGE_TEXT_SEARCH,
    textSearch,
  };
}

export function getSearchSuccess(products) {
  return {
    type: SEARCH_RESULT_SUCCESS,
    products,
  };
}

export function getSearchResultEmpty() {
  return {
    type: SEARCH_RESULT_EMPTY
  };
}

export function changeSearchActive(value) {
  return {
    type: CHANGE_SEARCH_ACTIVE,
    value
  };
}
