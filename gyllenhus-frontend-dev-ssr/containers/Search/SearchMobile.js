import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectTextSearch, makeSelectSearchResult, makeSelectSearchActive } from '../App/selectors';
import { changeTextSearch, changeSearchActive } from './actions';
import SearchResultList from '../../components/SearchResultList';
import useOutsideClick from '../../utils/useOutsideClick';

/**
 * @return {null}
 */
function SearchMobile(props) {
  const {
    textSearch,
    onChangeTextSearch,
    searchResult,
    searchActive,
    onChangeSearchActive,
  } = props;

  const ref = useRef();
  useOutsideClick(ref, () => {
    if (searchActive) {
      onChangeSearchActive(false);
    }
  });

  return (
    <form className={`search select ${searchActive ? 'active' : ''}`} ref={ref} >
      <div class="form-group">
        <input className={`input-text ${textSearch !== '' ? 'has-value' : ''}`} type="search" name="textSearch" value={textSearch} onChange={onChangeTextSearch} />
        <label class="placeholder">Search</label>
        <div class="dropdown active">
          <SearchResultList listProduct={searchResult} textSearch={textSearch} />
        </div>
      </div>
      <button onClick={e => { onChangeSearchActive(!searchActive); e.preventDefault();}}>
        <svg class="icon-26" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 26 26">
          <path d="M18.7,18c3.3-3.7,3-9.4-0.7-12.7S8.6,2.3,5.3,6s-3,9.4,0.7,12.7c3.4,3.1,8.6,3.1,12,0l4.6,4.6	l0.7-0.7L18.7,18z M4,12c0-4.4,3.6-8,8-8s8,3.6,8,8s-3.6,8-8,8S4,16.4,4,12z" />
          <path class="x" d="M12.8,12l3.2,3.2l-0.7,0.7L12,12.8l-3.2,3.2l-0.7-0.7l3.2-3.2L8.2,8.9l0.7-0.7l3.2,3.2l3.2-3.2l0.7,0.7L12.8,12	z" />
        </svg>
      </button>

    </form >
  );
}

SearchMobile.propTypes = {
  textSearch: PropTypes.string,
  onChangeTextSearch: PropTypes.func,
  searchResult: PropTypes.array,
  searchActive: PropTypes.bool,
  onChangeSearchActive: PropTypes.func

};

const mapStateToProps = createStructuredSelector({
  textSearch: makeSelectTextSearch(),
  searchResult: makeSelectSearchResult(),
  searchActive : makeSelectSearchActive()
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeTextSearch: (evt) => dispatch(changeTextSearch(evt.target.value)),
    onChangeSearchActive : (value) => dispatch(changeSearchActive(value))
  }
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(SearchMobile);
