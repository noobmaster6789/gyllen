import { call, select, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';

import {
  CHANGE_TEXT_SEARCH
} from './constants';
import { getSearchSuccess } from './actions';
import { makeSelectJwtToken } from '../App/selectors';


const backendApiUrl = process.env.REACT_APP_BACKEND_API_URL;

/**
 * search product
 * @return {object}
 */
export function* getSearchResult(keyword) {
  const searchProductEndpoint =
    process.env.REACT_APP_API_SEARCH_PRODUCT;
  const requestURL = `${backendApiUrl}/${searchProductEndpoint}`;
  const paged = 1;
  const per_page = 10;
  const search = keyword.textSearch;
  const token = yield select(makeSelectJwtToken());
  if (search && search.length >= 2) {
    try {
      const response = yield call(request, requestURL, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        }),
        body: JSON.stringify({ paged, per_page, search }),
      });
      if (response.code === 'gyllenhus_search_success' && response.data) {
        yield put(getSearchSuccess(response.data.products));
      }
    } catch (e) {
      console.log('Search Error e: ', e);
      yield put(getSearchResultEmpty());
    }
  }
}


export default function* SearchProductSaga() {
  yield takeLatest(CHANGE_TEXT_SEARCH, getSearchResult);
}
