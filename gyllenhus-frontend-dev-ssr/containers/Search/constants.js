/*
 *
 * Search constants
 *
 */

export const CHANGE_TEXT_SEARCH = 'app/Search/CHANGE_TEXT_SEARCH';
export const SEARCH_RESULT_SUCCESS = 'app/Search/SEARCH_RESULT_SUCCESS';
export const SEARCH_RESULT_EMPTY = 'app/Search/SEARCH_RESULT_EMPTY';
export const CHANGE_SEARCH_ACTIVE = 'app/Search/CHANGE_SEARCH_ACTIVE';

