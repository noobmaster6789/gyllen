/*
 * LocaleToggle Messages
 *
 * This contains all the text for the LanguageToggle component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  en: {
    id: `${scope}.en`,
    defaultMessage: 'ENG',
  },
  se: {
    id: `${scope}.se`,
    defaultMessage: 'SWE',
  },
});
