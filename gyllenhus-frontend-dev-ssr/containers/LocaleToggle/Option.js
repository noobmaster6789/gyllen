/**
 *
 * Locale Option
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

const Option = ({ locale, value, message, intl, onLocaleToggle }) => (
  <li className={locale === value ? 'active' : null}>
    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
    <a href="#" onClick={() => onLocaleToggle(value)}>
      {message ? intl.formatMessage(message) : value}
    </a>
  </li>
);

Option.propTypes = {
  locale: PropTypes.string,
  value: PropTypes.string.isRequired,
  message: PropTypes.object,
  onLocaleToggle: PropTypes.func,
};

export default injectIntl(Option);
