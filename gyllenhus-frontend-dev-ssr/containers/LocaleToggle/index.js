/*
 *
 * LanguageToggle
 *
 */

import React from 'react';
import Cookie from 'js-cookie'
import { useIntl } from 'react-intl'

import Ul from './Ul';
import messages from './messages';
import Option from './Option';

const appLocales = ['en', 'se']

export function LocaleToggle() {
  const { locale } = useIntl()

  const onLocaleToggle = l => {
    Cookie.set('locale', l)
    window.location.reload(true)
  }

  return (
    <Ul className="tab">
      {appLocales.map(value => (
        <Option
          key={value}
          value={value}
          message={messages[value]}
          onLocaleToggle = {onLocaleToggle}
          locale={locale}
        />
      ))}
    </Ul>
  );
}

export default LocaleToggle;
