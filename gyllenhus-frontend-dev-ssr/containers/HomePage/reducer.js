/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import {
  LOAD_OUR_SUPPLIERS,
  LOAD_OUR_SUPPLIERS_ERROR,
  LOAD_OUR_SUPPLIERS_SUCCESS,
  LOAD_GOLD_PRODUCTS,
  LOAD_GOLD_PRODUCTS_ERROR,
  LOAD_GOLD_PRODUCTS_SUCCESS,
  LOAD_SILVER_PRODUCTS,
  LOAD_SILVER_PRODUCTS_ERROR,
  LOAD_SILVER_PRODUCTS_SUCCESS,
  LOAD_SPECIAL_OFFERS,
  LOAD_SPECIAL_OFFERS_ERROR,
  LOAD_SPECIAL_OFFERS_SUCCESS,
  LOAD_BANNER_LIST,
  LOAD_BANNER_LIST_ERROR,
  LOAD_BANNER_LIST_SUCCESS,
  CHANGE_BANNER_ACTIVE_KEY,
  LOAD_PALLADIUM_PRODUCTS,
  LOAD_PALLADIUM_PRODUCTS_SUCCESS,
  LOAD_PALLADIUM_PRODUCTS_ERROR,
  LOAD_PLATINUM_PRODUCTS,
  LOAD_PLATINUM_PRODUCTS_SUCCESS,
  LOAD_PLATINUM_PRODUCTS_ERROR
} from './constants';

// The initial state of the App
export const initialState = {
  loading: false,
  error: false,
  products: false,
  goldProducts: {
    loading: false,
    error: false,
    data: false,
  },
  specialOffers: {
    loading: false,
    error: false,
    data: false,
  },
  ourSuppliers: {
    loading: false,
    error: false,
    data: false,
  },
  silverProducts: {
    loading: false,
    error: false,
    data: false,
  },
  platinumProducts: {
    loading: false,
    error: false,
    data: false,
  },
  palladiumProducts: {
    loading: false,
    error: false,
    data: false,
  },
  bannerList: {
    activeKey: 2,
    loading: false,
    error: false,
    data: false,
    categoryProducts: false,
  },
};

/* eslint-disable default-case, no-param-reassign */
// eslint-disable-next-line no-unused-vars
const homeReducer = (state = initialState, action) =>
  // eslint-disable-next-line no-unused-vars
  produce(state, draft => {
    switch (action.type) {
      case LOAD_GOLD_PRODUCTS:
        draft.goldProducts.loading = true;
        draft.goldProducts.error = false;
        // draft.goldProducts.data = false;
        break;
      case LOAD_GOLD_PRODUCTS_SUCCESS:
        draft.goldProducts.loading = false;
        draft.goldProducts.error = false;
        draft.goldProducts.data = action.products;
        break;
      case LOAD_GOLD_PRODUCTS_ERROR:
        draft.goldProducts.loading = false;
        draft.goldProducts.data = false;
        draft.goldProducts.error = action.error;
        break;
      case LOAD_SPECIAL_OFFERS:
        draft.specialOffers.loading = true;
        draft.specialOffers.error = false;
        // draft.specialOffers.data = false;
        break;
      case LOAD_SPECIAL_OFFERS_SUCCESS:
        draft.specialOffers.loading = false;
        draft.specialOffers.error = false;
        draft.specialOffers.data = action.offers;
        break;
      case LOAD_SPECIAL_OFFERS_ERROR:
        draft.specialOffers.loading = false;
        draft.specialOffers.error = action.error;
        draft.specialOffers.data = false;
        break;
      case LOAD_OUR_SUPPLIERS:
        draft.ourSuppliers.loading = true;
        draft.ourSuppliers.error = false;
        draft.ourSuppliers.data = false;
        break;
      case LOAD_OUR_SUPPLIERS_SUCCESS:
        draft.ourSuppliers.loading = false;
        draft.ourSuppliers.error = false;
        draft.ourSuppliers.data = action.suppliers;
        break;
      case LOAD_OUR_SUPPLIERS_ERROR:
        draft.ourSuppliers.loading = false;
        draft.ourSuppliers.data = false;
        draft.ourSuppliers.error = action.error;
        break;
      case LOAD_SILVER_PRODUCTS:
        draft.silverProducts.loading = true;
        draft.silverProducts.error = false;
        // draft.silverProducts.data = false;
        break;
      case LOAD_SILVER_PRODUCTS_SUCCESS:
        draft.silverProducts.loading = false;
        draft.silverProducts.error = false;
        draft.silverProducts.data = action.products;
        break;
      case LOAD_SILVER_PRODUCTS_ERROR:
        draft.silverProducts.loading = false;
        draft.silverProducts.data = false;
        draft.silverProducts.error = action.error;
        break;
      case LOAD_BANNER_LIST:
        draft.bannerList.loading = true;
        draft.bannerList.error = false;
        draft.bannerList.data = false;
        draft.bannerList.menu = false;
        break;
      case LOAD_BANNER_LIST_SUCCESS:
        draft.bannerList.loading = false;
        draft.bannerList.error = false;
        draft.bannerList.data = action.banners;
        draft.bannerList.categoryProducts = action.categoryProducts;
        break;
      case LOAD_BANNER_LIST_ERROR:
        draft.bannerList.loading = false;
        draft.bannerList.error = action.error;
        draft.bannerList.menu = false;
        draft.bannerList.data = false;
        break;
      case CHANGE_BANNER_ACTIVE_KEY:
        draft.bannerList.activeKey = action.key;
        break;
      case LOAD_PLATINUM_PRODUCTS:
        draft.platinumProducts.loading = true;
        draft.platinumProducts.error = false;
        break;
      case LOAD_PLATINUM_PRODUCTS_SUCCESS:
        draft.platinumProducts.loading = false;
        draft.platinumProducts.error = false;
        draft.platinumProducts.data = action.products;
        break;
      case LOAD_PLATINUM_PRODUCTS_ERROR:
        draft.platinumProducts.loading = false;
        draft.platinumProducts.data = false;
        draft.platinumProducts.error = action.error;
        break;
      case LOAD_PALLADIUM_PRODUCTS:
        draft.palladiumProducts.loading = true;
        draft.palladiumProducts.error = false;
        // draft.goldProducts.data = false;
        break;
      case LOAD_PALLADIUM_PRODUCTS_SUCCESS:
        draft.palladiumProducts.loading = false;
        draft.palladiumProducts.error = false;
        draft.palladiumProducts.data = action.products;
        break;
      case LOAD_PALLADIUM_PRODUCTS_ERROR:
        draft.palladiumProducts.loading = false;
        draft.palladiumProducts.data = false;
        draft.palladiumProducts.error = action.error;
        break;
    }
  });

export default homeReducer;
