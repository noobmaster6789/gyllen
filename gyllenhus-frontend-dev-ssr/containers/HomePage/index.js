/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { FormattedMessage } from 'react-intl';

import {
  makeSelectCountDownTime,
  makeSelectCurrency,
  makeSelectGoldPrice,
  makeSelectSilverPrice,
  makeSelectPlatinumPrice,
  makeSelectPalladiumPrice,
  makeSelectSearchActive,
  makeSelectCollapse,
} from '../App/selectors';

import {
  makeSelectGoldProductsLoading,
  makeSelectGoldProductsError,
  makeSelectGoldProductsData,
  makeSelectSilverProductsLoading,
  makeSelectSilverProductsError,
  makeSelectSilverProductsData,
  makeSelectSpecialOffersLoading,
  makeSelectSpecialOffersError,
  makeSelectSpecialOffersData,
  makeSelectPlatinumData,
  makeSelectPalladiumData
} from './selectors';


import { loadGoldPrice, loadIndexes, loadSilverPrice } from '../App/actions';
import {
  loadOurSuppliers,
  changeBannerActiveKey,
  loadGoldProducts,
  loadSilverProducts,
  loadSpecialOffers,
  loadPlatinumProducts,
  loadPalladiumProducts,

} from './actions';
import ProductList from '../../components/ProductList';
import BannerList from '../../components/BannerList';
import messages from './messages';
import Countdown from '../../components/Countdown';
import SpecialOfferList from '../../components/SpecialOfferList';
import OurSupplierList from '../../components/OurSupplierList';
import { getupdatedPrice } from '../CartPage/actions';

export function HomePage({
  goldProducts,
  goldProductsNext,
  silverProducts,
  silverProductsNext,
  platinumProducts,
  platinumProductsNext,
  palladiumProducts,
  palladiumProductsNext,
  onLoadGoldProduct,
  onLoadSpecialOffer,
  onLoadSilverProduct,
  onLoadPlatinumProduct,
  onLoadPalladiumProducts,
  bannerList,
  defaultCurrency,
  goldPrice,
  silverPrice,
  platinumPrice,
  palladiumPrice,
  countDownTime,
  dispatch,
  specialOffers,
  specialOffersNext,
  ourSuppliers,
  onChangeActiveBanner,
  onReloadIndex,
  updatePrice,
  searchActive,
  goldPriceNext,
  silverPriceNext,
  platinumPriceNext,
  palladiumPriceNext,
  collapse
}) {
  var jwtToken = "";
  useEffect(() => {
    jwtToken = localStorage.getItem('jwtToken');
    if (jwtToken) {
      onLoadGoldProduct();
      onLoadSilverProduct();
      onLoadSpecialOffer();
      onReloadIndex();
      onLoadPlatinumProduct();
      onLoadPalladiumProducts();
    }
  }, [jwtToken]);

  useEffect(() => {
    let interval = setInterval(() => {
      updatePrice();
    }, 300000)
    return () => clearInterval(interval);
  }, []);
  const goldProductsDisplay = goldProducts.data || goldProductsNext;
  const silverProductsDisplay = silverProducts.data || silverProductsNext;
  const specialOffersDisplay = specialOffers.data || specialOffersNext;
  const platinumProductsDisplay = platinumProducts.data || platinumProductsNext;
  const palladiumProductsDisplay = palladiumProducts.data || palladiumProductsNext;


  const goldPriceDisplay = goldPrice || goldPriceNext;
  const silverPriceDisplay = silverPrice || silverPriceNext;
  const platinumPriceDisplay = platinumPrice || platinumPriceNext;
  const palladiumPriceDisplay = palladiumPrice || palladiumPriceNext;


  const goldProductList = {
    data: goldProductsDisplay,
    defaultCurrency,
    goldPrice: goldPriceDisplay,
    silverPrice: silverPriceDisplay,
    platinumPrice: platinumPriceDisplay,
    palladiumPrice: palladiumPriceDisplay,
    dispatch,
  };


  const silverProductList = {
    data: silverProductsDisplay,
    defaultCurrency,
    goldPrice: goldPriceDisplay,
    silverPrice: silverPriceDisplay,
    platinumPrice: platinumPriceDisplay,
    palladiumPrice: palladiumPriceDisplay,
  };
  const platinumProductList = {
    data: platinumProductsDisplay,
    defaultCurrency,
    goldPrice: goldPriceDisplay,
    silverPrice: silverPriceDisplay,
    platinumPrice: platinumPriceDisplay,
    palladiumPrice: palladiumPriceDisplay,
  };
  const palladiumProductList = {
    data: palladiumProductsDisplay,
    defaultCurrency,
    goldPrice: goldPriceDisplay,
    silverPrice: silverPriceDisplay,
    platinumPrice: platinumPriceDisplay,
    palladiumPrice: palladiumPriceDisplay,
  };

  const offers = {
    data: specialOffersDisplay,
    defaultCurrency,
    goldPrice: goldPriceDisplay,
    silverPrice: silverPriceDisplay,
    platinumPrice: platinumPriceDisplay,
    palladiumPrice: palladiumPriceDisplay,
  };

  const suppliers = { data: ourSuppliers };
  const bannerListData = { data: bannerList, onChangeActiveBanner: onChangeActiveBanner, collapse: collapse };
  return (
    <React.Fragment>
      <Helmet>
        <title>Gyllenhus</title>
        <meta name="description" content="Gyllenhus Homepage" />
      </Helmet>
      <BannerList {...bannerListData} />
      <div className="wrapper section">
        <div className="section-head">
          <h2 className="title">
            <FormattedMessage {...messages.popularGoldProducts} />
          </h2>
          <Countdown time={countDownTime} searchActive={searchActive} />
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a className="link" href="#">
            <FormattedMessage {...messages.seeAll} /> ›
          </a>
        </div>
        <div className="product-list grid-view">
          <ProductList {...goldProductList} />
        </div>
      </div>
      <div className="wrapper section">
        <div className="section-head">
          <h2 className="title">
            <FormattedMessage {...messages.popularSilverProducts} />
          </h2>
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a className="link" href="#">
            <FormattedMessage {...messages.seeAll} /> ›
          </a>
        </div>
        <div className="product-list grid-view">
          <ProductList {...silverProductList} />
        </div>
      </div>
      <div className="wrapper section">
        <div className="section-head">
          <h2 className="title">
            <FormattedMessage {...messages.popularPlatinumProducts} />
          </h2>
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a className="link" href="#">
            <FormattedMessage {...messages.seeAll} /> ›
          </a>
        </div>
        <div className="product-list grid-view">
          <ProductList {...platinumProductList} />
        </div>
      </div>

      <div className="wrapper section">
        <div className="section-head">
          <h2 className="title">
            <FormattedMessage {...messages.popularPalladiumProducts} />
          </h2>
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a className="link" href="#">
            <FormattedMessage {...messages.seeAll} /> ›
          </a>
        </div>
        <div className="product-list grid-view">
          <ProductList {...palladiumProductList} />
        </div>
      </div>

      <div className="wrapper section">
        <div className="section-head">
          <h2 className="title">
            <FormattedMessage {...messages.specialOffers} />
          </h2>
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a className="link" href="#">
            <FormattedMessage {...messages.seeAll} /> ›
          </a>
        </div>
        {/* <div className="special-offers product-list list-view">
          <SpecialOfferList {...offers} />
        </div> */}
        <div className="product-list grid-view">
          <ProductList {...offers} />
        </div>
      </div>

      <div className="supplier">
        <div className="wrapper">
          <h3><FormattedMessage {...messages.ourSuppliers} /></h3>
          <OurSupplierList {...suppliers} />
        </div>
      </div>
    </React.Fragment>
  );
}

HomePage.propTypes = {
  defaultCurrency: PropTypes.string,
  goldPrice: PropTypes.object,
  silverPrice: PropTypes.object,
  platinumPrice: PropTypes.object,
  palladiumPrice: PropTypes.object,
  countDownTime: PropTypes.any,
  onLoadGoldProduct: PropTypes.func,
  onLoadSilverProduct: PropTypes.func,
  onLoadPlatinumProduct: PropTypes.func,
  onLoadPalladiumProducts: PropTypes.func,
  onLoadSpecialOffer: PropTypes.func,
  onLoadOurSupplier: PropTypes.func,
  onLoadGoldPrice: PropTypes.func,
  onLoadSilverPrice: PropTypes.func,
  onLoadBannerList: PropTypes.func,
  onChangeActiveBanner: PropTypes.func,
  onReloadIndex: PropTypes.func,
  dispatch: PropTypes.func,
  searchActive: PropTypes.bool
};
const mapStateToProps = createStructuredSelector({
  goldProducts: createStructuredSelector({
    loading: makeSelectGoldProductsLoading(),
    error: makeSelectGoldProductsError(),
    data: makeSelectGoldProductsData(),
  }),
  silverProducts: createStructuredSelector({
    loading: makeSelectSilverProductsLoading(),
    error: makeSelectSilverProductsError(),
    data: makeSelectSilverProductsData(),
  }),
  specialOffers: createStructuredSelector({
    loading: makeSelectSpecialOffersLoading(),
    error: makeSelectSpecialOffersError(),
    data: makeSelectSpecialOffersData(),
  }),
  platinumProducts: createStructuredSelector({
    loading: makeSelectSpecialOffersLoading(),
    error: makeSelectSpecialOffersError(),
    data: makeSelectPlatinumData(),
  }),
  palladiumProducts: createStructuredSelector({
    loading: makeSelectSpecialOffersLoading(),
    error: makeSelectSpecialOffersError(),
    data: makeSelectPalladiumData(),
  }),

  defaultCurrency: makeSelectCurrency(),
  goldPrice: makeSelectGoldPrice(),
  silverPrice: makeSelectSilverPrice(),
  countDownTime: makeSelectCountDownTime(),
  platinumPrice: makeSelectPlatinumPrice(),
  palladiumPrice: makeSelectPalladiumPrice(),
  searchActive: makeSelectSearchActive(),
  collapse: makeSelectCollapse()
});

export function mapDispatchToProps(dispatch) {
  return {
    onLoadGoldProduct: () => dispatch(loadGoldProducts()),
    onLoadGoldPrice: (prices, currency) =>
      dispatch(loadGoldPrice(prices, currency)),
    onLoadSilverPrice: (prices, currency) =>
      dispatch(loadSilverPrice(prices, currency)),
    onLoadSilverProduct: () => dispatch(loadSilverProducts()),
    dispatch,
    onLoadSpecialOffer: () => dispatch(loadSpecialOffers()),
    onLoadOurSupplier: () => dispatch(loadOurSuppliers()),
    onChangeActiveBanner: activeKey =>
      dispatch(changeBannerActiveKey(activeKey)),
    onReloadIndex: () => dispatch(loadIndexes()),
    onLoadPlatinumProduct: () => dispatch(loadPlatinumProducts()),
    onLoadPalladiumProducts: () => dispatch(loadPalladiumProducts()),
    updatePrice: data => dispatch(getupdatedPrice(data)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(HomePage);
