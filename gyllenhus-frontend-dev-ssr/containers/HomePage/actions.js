/*
 * HomePage Actions
 *
 * Actions change things in your application
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */
import {
  LOAD_OUR_SUPPLIERS,
  LOAD_OUR_SUPPLIERS_ERROR,
  LOAD_OUR_SUPPLIERS_SUCCESS,
  LOAD_GOLD_PRODUCTS,
  LOAD_GOLD_PRODUCTS_ERROR,
  LOAD_GOLD_PRODUCTS_SUCCESS,
  LOAD_SILVER_PRODUCTS,
  LOAD_SILVER_PRODUCTS_ERROR,
  LOAD_SILVER_PRODUCTS_SUCCESS,
  LOAD_SPECIAL_OFFERS,
  LOAD_SPECIAL_OFFERS_ERROR,
  LOAD_SPECIAL_OFFERS_SUCCESS,
  LOAD_BANNER_LIST,
  LOAD_BANNER_LIST_ERROR,
  LOAD_BANNER_LIST_SUCCESS,
  CHANGE_BANNER_ACTIVE_KEY,
  LOAD_PALLADIUM_PRODUCTS,
  LOAD_PALLADIUM_PRODUCTS_SUCCESS,
  LOAD_PALLADIUM_PRODUCTS_ERROR,
  LOAD_PLATINUM_PRODUCTS,
  LOAD_PLATINUM_PRODUCTS_SUCCESS,
  LOAD_PLATINUM_PRODUCTS_ERROR
} from './constants';

export function loadGoldProducts() {
  return {
    type: LOAD_GOLD_PRODUCTS,
  };
}

export function goldProductsLoaded(products) {
  return {
    type: LOAD_GOLD_PRODUCTS_SUCCESS,
    products,
  };
}

export function goldProductsLoadingError(error) {
  return {
    type: LOAD_GOLD_PRODUCTS_ERROR,
    error,
  };
}

export function loadSpecialOffers() {
  return {
    type: LOAD_SPECIAL_OFFERS,
  };
}

export function specialOffersLoaded(offers) {
  return {
    type: LOAD_SPECIAL_OFFERS_SUCCESS,
    offers,
  };
}

export function specialOffersLoadingError(error) {
  return {
    type: LOAD_SPECIAL_OFFERS_ERROR,
    error,
  };
}

/**
 * Load our suppliers, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_OUR_SUPPLIERS
 */

export function loadOurSuppliers() {
  return {
    type: LOAD_OUR_SUPPLIERS,
  };
}

/**
 * Dispatched when the repositories are loaded by the request saga
 *
 * @param  {array} suppliers The gold price data
 *
 * @return {object}      An action object with a type of LOAD_OUR_SUPPLIERS_SUCCESS passing the suppliers
 */
export function ourSuppliersLoaded(suppliers) {
  return {
    type: LOAD_OUR_SUPPLIERS_SUCCESS,
    suppliers,
  };
}

/**
 * Dispatched when loading the suppliers fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of LOAD_OUR_SUPPLIERS_ERROR passing the error
 */

export function ourSuppliersLoadingError(error) {
  return {
    type: LOAD_OUR_SUPPLIERS_ERROR,
    error,
  };
}

/**
 * Load silver products, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_SILVER_PRODUCTS
 */

export function loadSilverProducts() {
  return {
    type: LOAD_SILVER_PRODUCTS,
  };
}

/**
 * Dispatched when the repositories are loaded by the request saga
 *
 * @param  {array} products The gold price data
 *
 * @return {object}      An action object with a type of LOAD_SILVER_PRODUCTS_SUCCESS passing the products
 */

export function silverProductsLoaded(products) {
  return {
    type: LOAD_SILVER_PRODUCTS_SUCCESS,
    products,
  };
}

/**
 * Dispatched when loading the products fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of LOAD_SILVER_PRODUCTS_ERROR passing the error
 */
export function silverProductsLoadingError(error) {
  return {
    type: LOAD_SILVER_PRODUCTS_ERROR,
    error,
  };
}
export function loadBannerList() {
  return {
    type: LOAD_BANNER_LIST,
  };
}

export function bannerListLoaded(banners, categoryProducts) {
  return {
    type: LOAD_BANNER_LIST_SUCCESS,
    banners,
    categoryProducts,
  };
}

export function bannerListLoadingError(error) {
  return {
    type: LOAD_BANNER_LIST_ERROR,
    error,
  };
}

export function changeBannerActiveKey(key) {
  return {
    type: CHANGE_BANNER_ACTIVE_KEY,
    key,
  };
}

export function loadPlatinumProducts() {
  return {
    type: LOAD_PLATINUM_PRODUCTS,
  };
}

export function platinumProductsLoaded(products) {
  return {
    type: LOAD_PLATINUM_PRODUCTS_SUCCESS,
    products,
  };
}

export function platinumProductsLoadingError(error) {
  return {
    type: LOAD_PLATINUM_PRODUCTS_ERROR,
    error,
  };
}

export function loadPalladiumProducts() {
  return {
    type: LOAD_PALLADIUM_PRODUCTS,
  };
}

export function palladiumProductsLoaded(products) {
  return {
    type: LOAD_PALLADIUM_PRODUCTS_SUCCESS,
    products,
  };
}

export function palladiumProductsLoadingError(error) {
  return {
    type: LOAD_PALLADIUM_PRODUCTS_ERROR,
    error,
  };
}
