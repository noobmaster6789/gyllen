import { createSelector } from 'reselect';
import initialState from './reducer';

const selectHome = state => state.home || initialState;

const makeSelectGoldProductsLoading = () =>
  createSelector(
    selectHome,
    homeState => homeState.goldProducts && homeState.goldProducts.loading,
  );

const makeSelectGoldProductsError = () =>
  createSelector(
    selectHome,
    homeState => homeState.goldProducts && homeState.goldProducts.error,
  );

const makeSelectGoldProductsData = () =>
  createSelector(
    selectHome,
    homeState => homeState.goldProducts && homeState.goldProducts.data,
  );

const makeSelectSpecialOffersLoading = () =>
  createSelector(
    selectHome,
    homeState => homeState.specialOffers && homeState.specialOffers.loading,
  );

const makeSelectSpecialOffersError = () =>
  createSelector(
    selectHome,
    homeState => homeState.specialOffers && homeState.specialOffers.error,
  );

const makeSelectSpecialOffersData = () =>
  createSelector(
    selectHome,
    homeState => homeState.specialOffers && homeState.specialOffers.data,
  );

const makeSelectOurSuppliersLoading = () =>
  createSelector(
    selectHome,
    homeState => homeState.ourSuppliers && homeState.ourSuppliers.loading,
  );

const makeSelectOurSuppliersError = () =>
  createSelector(
    selectHome,
    homeState => homeState.ourSuppliers && homeState.ourSuppliers.error,
  );

const makeSelectOurSuppliersData = () =>
  createSelector(
    selectHome,
    homeState => homeState.ourSuppliers && homeState.ourSuppliers.data,
  );

const makeSelectSilverProductsLoading = () =>
  createSelector(
    selectHome,
    homeState => homeState.silverProducts && homeState.silverProducts.loading,
  );

const makeSelectSilverProductsError = () =>
  createSelector(
    selectHome,
    homeState => homeState.silverProducts && homeState.silverProducts.error,
  );

const makeSelectSilverProductsData = () =>
  createSelector(
    selectHome,
    homeState => homeState.silverProducts && homeState.silverProducts.data,
  );

const makeSelectBannerListActiveKey = () =>
  createSelector(
    selectHome,
    homeState => homeState.bannerList && homeState.bannerList.activeKey,
  );
const makeSelectBannerListLoading = () =>
  createSelector(
    selectHome,
    homeState => homeState.bannerList && homeState.bannerList.loading,
  );

const makeSelectBannerListError = () =>
  createSelector(
    selectHome,
    homeState => homeState.bannerList && homeState.bannerList.error,
  );

const makeSelectBannerListData = () =>
  createSelector(
    selectHome,
    homeState => homeState.bannerList && homeState.bannerList.data,
  );

const makeSelectBannerListCategoryProducts = () =>
  createSelector(
    selectHome,
    homeState => homeState.bannerList && homeState.bannerList.categoryProducts,
  );

const makeSelectPlatinumData = () =>
  createSelector(
    selectHome,
    homeState => homeState.platinumProducts && homeState.platinumProducts.data,
  );

const makeSelectPalladiumData = () =>
  createSelector(
    selectHome,
    homeState => homeState.palladiumProducts && homeState.palladiumProducts.data,
  );

export {
  selectHome,
  makeSelectGoldProductsError,
  makeSelectGoldProductsLoading,
  makeSelectGoldProductsData,
  makeSelectSpecialOffersData,
  makeSelectSpecialOffersLoading,
  makeSelectSpecialOffersError,
  makeSelectOurSuppliersLoading,
  makeSelectOurSuppliersError,
  makeSelectOurSuppliersData,
  makeSelectSilverProductsLoading,
  makeSelectSilverProductsError,
  makeSelectSilverProductsData,
  makeSelectBannerListLoading,
  makeSelectBannerListError,
  makeSelectBannerListData,
  makeSelectBannerListActiveKey,
  makeSelectBannerListCategoryProducts,
  makeSelectPlatinumData,
  makeSelectPalladiumData
};
