import { call, select, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import Cookie from 'js-cookie'
import {
  LOAD_OUR_SUPPLIERS,
  LOAD_GOLD_PRODUCTS,
  LOAD_SILVER_PRODUCTS,
  LOAD_SPECIAL_OFFERS,
  LOAD_BANNER_LIST,
  LOAD_PLATINUM_PRODUCTS,
  LOAD_PALLADIUM_PRODUCTS
} from './constants';
import {
  ourSuppliersLoaded,
  ourSuppliersLoadingError,
  goldProductsLoaded,
  goldProductsLoadingError,
  silverProductsLoaded,
  silverProductsLoadingError,
  specialOffersLoaded,
  specialOffersLoadingError,
  bannerListLoaded,
  bannerListLoadingError,
  platinumProductsLoaded,
  platinumProductsLoadingError,
  palladiumProductsLoaded,
  palladiumProductsLoadingError
} from './actions';
import { makeSelectJwtToken } from '../App/selectors';

const backendApiUrl = process.env.REACT_APP_BACKEND_API_URL;

/**
 * load popular gold product
 * @return {object}
 */
export function* getGoldProducts() {
  const goldProductEndpoint =
    process.env.REACT_APP_API_GET_POPULAR_GOLD_PRODUCTS;
  const requestURL = `${backendApiUrl}/${goldProductEndpoint}?paged=1&posts_per_page=20`;
  try {
    const jwtToken = yield select(makeSelectJwtToken());
    const authorization = jwtToken !== '' ? `Bearer ${jwtToken}` : '';
    const language = Cookie.get('locale')

    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        authorization,
      }),
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_popular_gold_products_success') {
      yield put(goldProductsLoaded(response.data.products));
    } else {
      yield put(goldProductsLoadingError(response));
    }
  } catch (e) {
    yield put(goldProductsLoadingError(e));
  }
}

/**
 * load special offers
 * @return {object}
 */
export function* getSpecialOffers() {
  const specialOfferEndpoint =
    process.env.REACT_APP_API_GET_SPECIAL_OFFER_ENDPOINT;
  const requestURL = `${backendApiUrl}/${specialOfferEndpoint}`;
  try {
    const jwtToken = yield select(makeSelectJwtToken());
    const language = Cookie.get('locale')
    const authorization = jwtToken !== '' ? `Bearer ${jwtToken}` : '';
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        authorization,
      }),
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_special_offers_success') {
      yield put(specialOffersLoaded(response.data.products));
    }
  } catch (e) {
    yield put(specialOffersLoadingError(e));
  }
}

/**
 * load our suppliers
 * @return {object}
 */

export function* getOurSuppliers() {
  const ourSupplierEndpoint =
    process.env.REACT_APP_API_GET_OUR_SUPPLIER_ENDPOINT;
  const requestURL = `${backendApiUrl}/${ourSupplierEndpoint}`;
  try {
    const jwtToken = yield select(makeSelectJwtToken());
    const language = Cookie.get('locale')
    const authorization = jwtToken !== '' ? `Bearer ${jwtToken}` : '';
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        authorization,
      }),
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_our_suppliers_success') {
      yield put(ourSuppliersLoaded(response.data.suppliers));
    } else {
      yield put(ourSuppliersLoadingError(response));
    }
  } catch (e) {
    yield put(ourSuppliersLoadingError(e));
  }
}

/**
 * load silver products
 * @return {object}
 */
export function* getSilverProducts() {
  const silverProductEndpoint =
    process.env.REACT_APP_API_GET_POPULAR_SILVER_PRODUCTS;
  const requestURL = `${backendApiUrl}/${silverProductEndpoint}`;
  try {
    const jwtToken = yield select(makeSelectJwtToken());
    const language = Cookie.get('locale')
    const authorization = jwtToken !== '' ? `Bearer ${jwtToken}` : '';
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        authorization,
      }),
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_popular_silverproducts_success') {
      yield put(silverProductsLoaded(response.data.products));
    } else {
      yield put(silverProductsLoadingError(response));
    }
  } catch (e) {
    yield put(silverProductsLoadingError(e));
  }
}

export function* getBannerList() {
  const bannerEndpoint = process.env.REACT_APP_API_GET_HOMEPAGE_BANNER_ENDPOINT;
  const requestUrl = `${backendApiUrl}/${bannerEndpoint}`;
  try {
    const language = Cookie.get('locale')

    const response = yield call(request, requestUrl, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_banner_success') {
      const { banners, categoryProducts } = response.data;
      yield put(bannerListLoaded(banners, categoryProducts));
    } else {
      yield put(bannerListLoadingError(response));
    }
  } catch (e) {
    yield put(bannerListLoadingError(e));
  }
}

export function* getPlatinumProducts() {
  const platinumProductEndpoint =
      process.env.REACT_APP_API_GET_POPULAR_PLATINUM_PRODUCTS;
  const requestURL = `${backendApiUrl}/${platinumProductEndpoint}`;
  try {
    const jwtToken = yield select(makeSelectJwtToken());
    const authorization = jwtToken !== '' ? `Bearer ${jwtToken}` : '';
    const language = Cookie.get('locale')

    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        authorization,
      }),
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_popular_platinum_products_success') {
      yield put(platinumProductsLoaded(response.data.products));    
    } else {
      yield put(platinumProductsLoadingError(response));
    }
  } catch (e) {
    yield put(platinumProductsLoadingError(e));
  }
}

export function* getPalladiumProducts() {
  const palladiumProductEndpoint =
      process.env.REACT_APP_API_GET_POPULAR_PALLADIUM_PRODUCTS;
  const requestURL = `${backendApiUrl}/${palladiumProductEndpoint}`;
  try {
    const jwtToken = yield select(makeSelectJwtToken());
    const authorization = jwtToken !== '' ? `Bearer ${jwtToken}` : '';
    const language = Cookie.get('locale')

    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
        authorization,
      }),
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_popular_palladium_products_success') {
      yield put(palladiumProductsLoaded(response.data.products));
    } else {
      yield put(palladiumProductsLoadingError(response));
    }
  } catch (e) {
    yield put(palladiumProductsLoadingError(e));
  }
}

export default function* HomePage() {
  yield takeLatest(LOAD_GOLD_PRODUCTS, getGoldProducts);
  yield takeLatest(LOAD_SILVER_PRODUCTS, getSilverProducts);
  yield takeLatest(LOAD_PALLADIUM_PRODUCTS, getPalladiumProducts);
  yield takeLatest(LOAD_PLATINUM_PRODUCTS, getPlatinumProducts);
  yield takeLatest(LOAD_SPECIAL_OFFERS, getSpecialOffers);
  yield takeLatest(LOAD_OUR_SUPPLIERS, getOurSuppliers);
  yield takeLatest(LOAD_BANNER_LIST, getBannerList);
}
