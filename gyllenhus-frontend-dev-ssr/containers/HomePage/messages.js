/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  popularGoldProducts: {
    id: `${scope}.popularGoldProducts`,
    defaultMessage: 'Popular Gold Products',
  },
  popularSilverProducts: {
    id: `${scope}.popularSilverProducts`,
    defaultMessage: 'Popular Silver Products',
  },
  popularPlatinumProducts: {
    id: `${scope}.popularPlatinumProducts`,
    defaultMessage: 'Popular Platinum Products',
  },
  popularPalladiumProducts: {
    id: `${scope}.popularPalladiumProducts`,
    defaultMessage: 'Popular Palladium Products',
  },
  seeAll: {
    id: `${scope}.seeAll`,
    defaultMessage: 'See All',
  },
  specialOffers: {
    id: `${scope}.specialOffers`,
    defaultMessage: 'Special Offers',
  },
  ourSuppliers: {
    id: `${scope}.ourSuppliers`,
    defaultMessage: 'Our Suppliers',
  },
});
