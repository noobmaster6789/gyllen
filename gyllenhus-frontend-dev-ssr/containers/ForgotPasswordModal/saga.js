import { takeLatest, call, put, select } from 'redux-saga/effects';
import request from '../../utils/request';
import { SUBMIT_FORGOT_PASSWORD } from './constants';
import {
  makeSelectForgotPasswordEmail
} from '../App/selectors';
import {
  forgotPasswordError, forgotPasswordSuccess,
} from '../App/actions';

const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
const forgotEndpoint = process.env.REACT_APP_API_FORGOT_PASSWORD_ENDPOINT;
const forgotUrl = `${apiUrl}/${forgotEndpoint}`;

export function* postForgotPassword() {
  try {
    const email = yield select(makeSelectForgotPasswordEmail());
    const response = yield call(request, forgotUrl, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({ email }),
    });
    if (response.code === 'gyllenhus_update_new_password_success') {
      yield put(forgotPasswordSuccess());
    } else {
      yield put(forgotPasswordError());
    }
  } catch (error) {
    yield put(forgotPasswordError(error));
  }
}
export default function* forgotPasswordModelSaga() {
  yield takeLatest(SUBMIT_FORGOT_PASSWORD, postForgotPassword);
}
