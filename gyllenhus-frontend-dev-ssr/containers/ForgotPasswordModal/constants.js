/*
 * Forgot Password Modal Constants
 */
export const SHOW_FORGOT_PASSWORD_MODAL = 'gyllenhus/ForgotPasswordModal/SHOW_FORGOT_PASSWORD_MODAL';
export const CHANGE_FORGOT_PASSWORD_EMAIL = 'gyllenhus/ForgotPasswordModal/CHANGE_FORGOT_PASSWORD_EMAIL';
export const SUBMIT_FORGOT_PASSWORD = 'gyllenhus/ForgotPasswordModal/SUBMIT_FORGOT_PASSWORD';
export const FORGOT_PASSWORD_ERROR = 'gyllenhus/ForgotPasswordModal/FORGOT_PASSWORD_ERROR';
export const FORGOT_PASSWORD_SUCCESS = 'gyllenhus/ForgotPasswordModal/FORGOT_PASSWORD_SUCCESS';
