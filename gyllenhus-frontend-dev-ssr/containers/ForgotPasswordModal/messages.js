/*
 * ForgotPassword Modal Messages
 *
 * This contains all the text for the LoginModal container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  forgotPassword: {
    id: `${scope}.forgotPassword`,
    defaultMessage: 'Forgot password',
  },
  login: {
    id: `${scope}.login`,
    defaultMessage: 'Login',
  },
  forgotPasswordSuccess : {
    id: `${scope}.forgotPasswordSuccess`,
    defaultMessage : 'We have sent out an email to the address you specified'
  }
});
