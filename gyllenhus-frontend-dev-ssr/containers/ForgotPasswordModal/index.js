/**
 *
 * Login Modal
 *
 */

import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import {
  makeSelectForgotPasswordModal, makeSelectForgotPasswordEmail, makeSelectForgotMessageError, makeSelectForgotSuccess, makeSelectForgotPasswordButtonLoading,
} from '../App/selectors';
import {
  changeEmailForgot, showForgotPasswordModal, submitForgotPassword,
} from '../App/actions';

import messages from './messages';
import useOutsideClick from '../../utils/useOutsideClick';


export function ForgotPasswordModal(props) {
  const {
    showForgotPasswordModal,
    emailForgot,
    onSubmitForm,
    onClickCloseModal,
    onChangeEmail,
    messageError,
    loading,
    forgotSuccess
  } = props;
  const ref = useRef();

  useOutsideClick(ref, () => {
    if (showForgotPasswordModal) {
      onClickCloseModal();
    }
  });

  return (
    <div className={`modal forgot-pass ${showForgotPasswordModal ? 'active' : ''}`} ref={ref}>
      {forgotSuccess ?
        (<div className="card">
          <div className="head">
            <h2><FormattedMessage {...messages.forgotPassword} /></h2>
            <p><FormattedMessage {...messages.forgotPasswordSuccess} /></p>
          </div>
        </div>) :
        (<div className="card">
          <div className="head">
            <h2><FormattedMessage {...messages.forgotPassword} />?</h2>
            <p>Enter your email address and we will send you a link to reset your password.</p>
          </div>
          <form className="body" onSubmit={onSubmitForm}>
            <div className="form-group">
              <input className={`input-text ${emailForgot !== '' ? 'has-value' : ''}`}
                type="email" name="" value={emailForgot} onChange={onChangeEmail} required="required" />
              <div className="placeholder">Email</div>
            </div>
            <p style={{ color: `red` }}> {messageError} </p>
            <div className="button-group" style={{ flexWrap: `inherit` }}>
              <button className="button is-blank" type="button" onClick={onClickCloseModal}>Cancel</button>
              <button className="button" type="submit" disabled={loading}>Send Password Reset Email</button>
            </div>
          </form>
        </div>)}
    </div>
  );
}

ForgotPasswordModal.propTypes = {
  showForgotPasswordModal: PropTypes.bool,
  emailForgot: PropTypes.string.isRequired,
  onChangeEmail: PropTypes.func,
  onSubmitForm: PropTypes.func,
  messageError: PropTypes.string,
  onClickCloseModal: PropTypes.func,
};

function mapDispatchToProps(dispatch) {
  return {
    onChangeEmail: evt => dispatch(changeEmailForgot(evt.target.value)),
    onSubmitForm: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(submitForgotPassword());
    },
    onClickCloseModal: () => dispatch(showForgotPasswordModal(false)),
  };
}

const withConnect = connect(
  createStructuredSelector({
    showForgotPasswordModal: makeSelectForgotPasswordModal(),
    emailForgot: makeSelectForgotPasswordEmail(),
    messageError: makeSelectForgotMessageError(),
    forgotSuccess: makeSelectForgotSuccess(),
    loading: makeSelectForgotPasswordButtonLoading(),
  }),
  mapDispatchToProps,
);


export default compose(
  withConnect,
)(ForgotPasswordModal);
