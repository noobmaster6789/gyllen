import _ from 'lodash';
import { call, select, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';

import {
  CHANGE_CURRENCY,
  COUNTDOWN_TIME,
  LOAD_INDEXES,
  LOAD_OPEN_PRICE,
  LOAD_REALTIME_PRICE,
} from '../App/constants';

import {
  indexesLoaded,
  indexLoadingError,
  loadGoldPrice,
  loadSilverPrice,
  loadPlatinumPrice,
  loadPalladiumPrice,
  loadIndexes,
  changeDelayTime,
  loadedPriceRealtime,
  loadedOpenPrice,
  loadPriceListSettings
} from '../App/actions';
import {
  makeSelectCountDownTime,
  makeSelectCurrency,
  makeSelectUserInfo,
  makeSelectPriceRealtimeCache,
  makeSelectRefreshTime,
} from '../App/selectors';
import {
  loadGoldProducts,
  loadSilverProducts,
  loadSpecialOffers,
} from '../HomePage/actions';

const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
const forexApiQuote = process.env.REACT_APP_API_GET_PRICE_LIST_ENDPOINT;
const forexApiOpenPrice =
  process.env.REACT_APP_API_GET_PRICE_LIST_OPEN_PRICE_ENDPOINT;

const realtimeApiURL = process.env.REACT_APP_FOREX_API_URL;
const forexApiPriceQuoteRealtime =
  process.env.REACT_APP_FOREX_GET_QUOTE_ENDPOINT;

export function* getIndexes() {
  const requestURL = `${apiUrl}${forexApiQuote}`;
  try {
    const userInfo =  JSON.parse(localStorage.getItem('userInfo')) || (yield select(makeSelectUserInfo()));
    const idUserType = _.get(userInfo, 'ID', 0);
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({ userType: idUserType }),
    });
    if (response.code === 'gyllenhus_get_price_list_success') {
      const currency = yield select(makeSelectCurrency());
      const {
        priceList,
        priceListSettings: { previousUpdatedTime, refreshTime },
      } = response.data;
      yield put(indexesLoaded(priceList));
      yield put(loadGoldPrice(priceList, currency));
      yield put(loadSilverPrice(priceList, currency));
      yield put(loadPlatinumPrice(priceList, currency));
      yield put(loadPalladiumPrice(priceList, currency));
      yield put(loadPriceListSettings(refreshTime, previousUpdatedTime));
      // if (Number(previousUpdatedTime) && Number(refreshTime)) {
      //   const currentTime = new Date().getTime();
      //   const timestamp = Math.floor(currentTime / 1000);
      //   const delayTime = refreshTime - (timestamp - previousUpdatedTime);
      //   yield put(changeDelayTime(refreshTime, delayTime));
      // }
    } else {
      yield put(indexLoadingError(response));
    }
  } catch (e) {
    yield put(indexLoadingError(e));
  }
}

export function* getCountDownTime() {
  // const count = yield select(makeSelectCountDownTime());
  // const refresh = yield select(makeSelectRefreshTime());
  // if (count <= 0) {
  //   yield put(changeDelayTime(refresh, refresh));
  //   yield put(loadIndexes());
  //   yield put(loadGoldProducts());
  //   yield put(loadSilverProducts());
  //   yield put(loadSpecialOffers());
  // }
}

export function* getOpenPrice() {
  const requestURL = `${apiUrl}${forexApiOpenPrice}`;
  try {
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
    });
    if (response.code === 'gyllenhus_get_price_list_open_price_success') {
      const openPrices = response.data.getPriceListOpenPrice;
      yield put(loadedOpenPrice(openPrices));
    } else {
      yield put(indexLoadingError(response));
    }
  } catch (e) {
    yield put(indexLoadingError(e));
  }
}

export function* getPriceRealtime() {
  const requestURL = `${realtimeApiURL}/${forexApiPriceQuoteRealtime}`;
  try {
    const response = yield call(request, requestURL, {
      method: 'GET',
    });
    yield put(loadedPriceRealtime(response));
  } catch (e) {
    // console.log(e);
  }
}

export function* changeCurrency() {
  yield put(loadIndexes());
  const dataRealtimeCache = yield select(makeSelectPriceRealtimeCache());
  yield put(loadedPriceRealtime(dataRealtimeCache));
}

export default function* priceListSaga() {
  yield takeLatest(LOAD_INDEXES, getIndexes);
  yield takeLatest(COUNTDOWN_TIME, getCountDownTime);
  yield takeLatest(CHANGE_CURRENCY, changeCurrency);
  yield takeLatest(LOAD_OPEN_PRICE, getOpenPrice);
  yield takeLatest(LOAD_REALTIME_PRICE, getPriceRealtime);
}
