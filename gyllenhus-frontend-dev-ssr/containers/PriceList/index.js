/**
 *
 * PriceList
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { createStructuredSelector } from 'reselect';
import { changeFrequency, updatePriceRealtime } from '../../utils/api';
import {
  makeSelectCurrency,
  makeSelectDelayTime,
  makeSelectError,
  makeSelectGoldPrice,
  makeSelectIndexes,
  makeSelectLoading,
  makeSelectSilverPrice,
  makeSelectOpenPrice,
  makeSelectSilverRealtime,
  makeSelectGoldRealtime,
  makeSelectCountDownTime,
  makeSelectRefreshTime,
  makeSelectCollapse,
} from '../App/selectors';
import {
  changeCountDownTime,
  changeCurrency,
  changeDelayTime,
  loadIndexes,
  loadOpenPrice,
  loadPriceRealtime,
  loadedPriceRealtime,
  changeCollapse,
} from '../App/actions';
import IndexList from '../../components/IndexList';

const key = 'index';

export class PriceList extends React.Component {

  componentDidMount() {
    const {
      loading,
      error,
      indexes,
      openPrices,
      goldPriceRealtime,
      silverPriceRealtime,
      onReloadIndex,
      onChangeCurrency,
      onLoadOpenPrice,
      onLoadPriceRealtime,
      isCollapsed,
      onChangeCollapse
    } = this.props;

    if (!loading && !error && !indexes) {
      onReloadIndex();
    }
    if (!openPrices) {
      onLoadOpenPrice();
    }
    if (!goldPriceRealtime && !silverPriceRealtime) {
      onLoadPriceRealtime();
    }

    // call socket connect function and define
    // an anonymous callback function that
    changeFrequency(() => {
      onReloadIndex();
    });
    updatePriceRealtime((data) => {
      onLoadPriceRealtime(data.data);
    });
    onChangeCollapse(isCollapsed);
  }

  componentWillUnmount() {
    //this.clearTimer();
  }

  render() {
    const {
      loading,
      error,
      gold,
      silver,
      goldPriceRealtime,
      silverPriceRealtime,
      openPrices,
      onChangeCurrency,
      defaultCurrency,
      defaultWeight,
      isCollapsed,
      collapse,
      onChangeCollapse,
      onLoadPriceRealtime,
      priceRealtime,
      openPricesLoad
    } = this.props;

    var goldPrice, silverPrice;
    if (goldPriceRealtime === null || goldPriceRealtime === undefined) {
      switch (defaultCurrency) {
        case "USD":
          goldPrice = priceRealtime[0];
          silverPrice = priceRealtime[3];
          break;
        case "EUR":
          goldPrice = priceRealtime[1];
          silverPrice = priceRealtime[4];
          break;
        default:
          goldPrice = priceRealtime[2];
          silverPrice = priceRealtime[5];
      }
    } else {
      goldPrice = goldPriceRealtime;
      silverPrice = silverPriceRealtime;
    }

    const indexData = {
      loading,
      error,
      gold,
      silver,
      goldPriceRealtime: goldPrice,
      silverPriceRealtime: silverPrice,
      openPrices: openPricesLoad,
      onChangeCurrency,
      defaultCurrency,
      onLoadPriceRealtime,
    };
    return (
      <React.Fragment>
        <IndexList
          {...indexData}
          onChangeCurrency={currency => onChangeCurrency(currency)}
          defaultCurrency={defaultCurrency} defaultWeight={defaultWeight}
          defaultCollapsed={isCollapsed} onChangeCollapse={onChangeCollapse}
        />
      </React.Fragment>
    );
  }
}

PriceList.propTypes = {
  refreshTime: PropTypes.number,
  delayTime: PropTypes.number,
  countdown: PropTypes.number,
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  indexes: PropTypes.oneOfType([PropTypes.bool, PropTypes.array]),
  gold: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  silver: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  openPrices: PropTypes.oneOfType([PropTypes.bool, PropTypes.array]),
  goldPriceRealtime: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  silverPriceRealtime: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  defaultCurrency: PropTypes.string,
  onReloadIndex: PropTypes.func,
  onChangeCurrency: PropTypes.func,
  onChangeCountDownTime: PropTypes.func,
  onChangeDelayTime: PropTypes.func,
  onLoadOpenPrice: PropTypes.func,
  onLoadPriceRealtime: PropTypes.func,
  collapse: PropTypes.bool,
  onChangeCollapse: PropTypes.func
};

function mapDispatchToProps(dispatch) {
  return {
    onReloadIndex: () => dispatch(loadIndexes()),
    onChangeCurrency: currency => dispatch(changeCurrency(currency)),
    onChangeCountDownTime: (countdown, refreshTime) =>
      dispatch(changeCountDownTime(countdown, refreshTime)),
    onChangeDelayTime: (refreshTime, delayTime) =>
      dispatch(changeDelayTime(refreshTime, delayTime)),
    onLoadOpenPrice: () => dispatch(loadOpenPrice()),
    onLoadPriceRealtime: (data) => dispatch(loadedPriceRealtime(data)),
    onChangeCollapse: (val) => dispatch(changeCollapse(val)),
  };
}

const withConnect = connect(
  createStructuredSelector({
    delayTime: makeSelectDelayTime(),
    indexes: makeSelectIndexes(),
    loading: makeSelectLoading(),
    error: makeSelectError(),
    gold: makeSelectGoldPrice(),
    silver: makeSelectSilverPrice(),
    //defaultCurrency: makeSelectCurrency(),
    openPrices: makeSelectOpenPrice(),
    goldPriceRealtime: makeSelectGoldRealtime(),
    silverPriceRealtime: makeSelectSilverRealtime(),
    countdown: makeSelectCountDownTime(),
    refreshTime: makeSelectRefreshTime(),
    collapse: makeSelectCollapse()
  }),
  mapDispatchToProps,
);


export default compose(
  withConnect,
)(PriceList);
