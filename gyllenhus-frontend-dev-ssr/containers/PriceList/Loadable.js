/**
 *
 * Asynchronously loads the component for PriceList
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
