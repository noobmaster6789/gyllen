export const LOAD_INDEXES = 'gyllenhus/App/LOAD_INDEXES';
export const LOAD_INDEXES_SUCCESS = 'gyllenhus/App/LOAD_INDEXES_SUCCESS';
export const LOAD_INDEXES_ERROR = 'gyllenhus/App/LOAD_INDEXES_ERROR';
export const CHANGE_CURRENCY = 'gyllenhus/App/CHANGE_CURRENCY';
export const LOAD_GOLD_PRICE = 'gyllenhus/App/LOAD_GOLD_PRICE';
export const LOAD_SILVER_PRICE = 'gyllenhus/App/LOAD_SILVER_PRICE';
export const LOAD_PLATINUM_PRICE = 'gyllenhus/App/LOAD_PLATINUM_PRICE';
export const LOAD_PALLADIUM_PRICE = 'gyllenhus/App/LOAD_PALLADIUM_PRICE';
export const LOAD_OPEN_PRICE = 'gyllenhus/App/LOAD_OPEN_PRICE';
export const LOAD_OPEN_PRICE_SUCCESS = 'gyllenhus/App/LOAD_OPEN_PRICE_SUCCESS';
export const COUNTDOWN_TIME = 'gyllenhus/App/COUNTDOWN_TIME';
export const LOAD_USER_BY_JWT_TOKEN = 'gyllenhus/App/LOAD_USER_BY_JWT_TOKEN';
export const JWT_TOKEN_VALID = 'gyllenhus/App/JWT_TOKEN_VALID';
export const JWT_TOKEN_INVALID = 'gyllenhus/App/JWT_TOKEN_INVALID';
export const RESET_JWT_TOKEN = 'gyllenhus/App/RESET_JWT_TOKEN';
export const UPDATE_JWT_TOKEN = 'gyllenhus/App/UPDATE_JWT_TOKEN';
export const USER_LOGOUT = 'gyllenhus/App/USER_LOGOUT';
export const USER_LOGOUT_SUCCESS = 'gyllenhus/App/USER_LOGOUT_SUCCESS';
export const USER_ROLE_BUSINESS = 'business';
export const USER_ROLE_PERSONAL = 'personal';
export const CHANGE_DELAY_TIME = 'gyllenhus/App/CHANGE_DELAY_TIME';
export const LOAD_REALTIME_PRICE = 'gyllenhus/App/LOAD_REALTIME_PRICE';
export const LOAD_REALTIME_PRICE_SUCCESS = 'gyllenhus/App/LOAD_REALTIME_PRICE_SUCCESS';
export const LOAD_USER_INFO = 'gyllenhus/App/LOAD_USER_INFO';
export const LOAD_USER_INFO_SUCCESS = 'gyllenhus/App/LOAD_USER_INFO_SUCCESS';

export const CHANGE_EMAIL = 'gyllenhus/LoginModal/CHANGE_EMAIL';
export const CHANGE_PASSWORD = 'gyllenhus/LoginModal/CHANGE_PASSWORD';
export const CHANGE_KEEP_ME_LOGIN = 'gyllenhus/LoginModal/CHANGE_KEEP_ME_LOGIN';
export const SUBMIT_LOGIN_FORM = 'gyllenhus/LoginModal/SUBMIT_LOGIN_FORM';
export const LOGIN_SUCCESS = 'gyllenhus/LoginModal/LOGIN_SUCCESS';
export const LOGIN_ERROR = 'gyllenhus/LoginModal/LOGIN_ERROR';
export const SHOW_LOGIN_MODAL = 'gyllenhus/LoginModal/SHOW_LOGIN_MODAL';


export const LOAD_MENU = 'gyllenhus/MenuList/LOAD_MENU';
export const LOAD_MENU_SUCCESS = 'gyllenhus/MenuList/LOAD_MENU_SUCCESS';
export const LOAD_MENU_ERROR = 'gyllenhus/MenuList/LOAD_MENU_ERROR';


export const LOAD_FOOTER_LIST = 'app/FooterList/LOAD_FOOTER_LIST';
export const LOAD_FOOTER_LIST_SUCCESS = 'app/FooterList/LOAD_FOOTER_LIST_SUCCESS';
export const LOAD_FOOTER_LIST_ERROR = 'app/FooterList/LOAD_FOOTER_LIST_ERROR';


