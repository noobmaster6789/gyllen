import request from './request';
import _ from 'lodash';

const backendApiUrl = process.env.REACT_APP_BACKEND_API_URL;


export const loadBanner = async (language) => {
  const bannerEndpoint = process.env.REACT_APP_API_GET_HOMEPAGE_BANNER_ENDPOINT;
  const requestUrl = `${backendApiUrl}/${bannerEndpoint}`;
  if (!language) language = "se";
  if (language === "se") language = "sv";

  try {
    const response = await request(requestUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_banner_success') {
      return response.data;
    } else return false;
  } catch (e) {
    return false;
  }
}

export const loadGoldProduct = async (language, authorization) => {
  const goldProductEndpoint =
    process.env.REACT_APP_API_GET_POPULAR_GOLD_PRODUCTS;
  const requestURL = `${backendApiUrl}/${goldProductEndpoint}?paged=1&posts_per_page=20`;
  if (!language) language = "se";

  try {
    const response = await request(requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        authorization
      },
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_popular_gold_products_success') {
      return response.data.products;
    } else return false;
  } catch (e) {
    return false;
  }
}

export const loadSilverProduct = async (language, authorization) => {
  const silverProductEndpoint =
    process.env.REACT_APP_API_GET_POPULAR_SILVER_PRODUCTS;
  const requestURL = `${backendApiUrl}/${silverProductEndpoint}`;
  if (!language) language = "se";

  try {
    const response = await request(requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        authorization
      },
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_popular_silverproducts_success') {
      return response.data.products;
    } else return false;
  } catch (e) {
    return false;
  }
}

export const loadPlatinumProduct = async (language, authorization) => {
  const platinumProductEndpoint =
    process.env.REACT_APP_API_GET_POPULAR_PLATINUM_PRODUCTS;
  const requestURL = `${backendApiUrl}/${platinumProductEndpoint}`;
  if (!language) language = "se";

  try {
    const response = await request(requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        authorization
      },
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_popular_platinum_products_success') {
      return response.data.products;
    } else return false;
  } catch (e) {
    return false;
  }
}

export const loadPalladiumProduct = async (language, authorization) => {
  const palladiumProductEndpoint =
    process.env.REACT_APP_API_GET_POPULAR_PALLADIUM_PRODUCTS;
  const requestURL = `${backendApiUrl}/${palladiumProductEndpoint}`;
  if (!language) language = "se";

  try {
    const response = await request(requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        authorization
      },
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_popular_palladium_products_success') {
      return response.data.products;
    } else return false;
  } catch (e) {
    console.log('error: ', e);

    return false;
  }
}

export const loadSpecialOffer = async (language, authorization) => {
  const specialOfferEndpoint =
    process.env.REACT_APP_API_GET_SPECIAL_OFFER_ENDPOINT;
  const requestURL = `${backendApiUrl}/${specialOfferEndpoint}`;
  if (!language) language = "se";

  try {
    const response = await request(requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        authorization
      },
      body: JSON.stringify({ language }),
    });

    if (response.code === 'gyllenhus_get_special_offers_success') {
      return response.data.products;
    } else return false;
  } catch (e) {
    return false;
  }
}

export const getOurSuppliers = async (language) => {
  const ourSupplierEndpoint =
    process.env.REACT_APP_API_GET_OUR_SUPPLIER_ENDPOINT;
  const requestURL = `${backendApiUrl}/${ourSupplierEndpoint}`;
  if (!language) language = "se";

  try {
    const response = await request(requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_our_suppliers_success') {
      return response.data;
    } else return false;
  } catch (e) {
    return false;
  }
}

export const getMenuRequest = async (language, menuName) => {
  const menuEndpoint = process.env.REACT_APP_API_GET_MENU;
  const requestURL = `${backendApiUrl}/${menuEndpoint}`;
  if (!language) language = "se";
  try {
    const response = await request(requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ language, menuName }),
    });
    return response.data.menus;
  } catch (e) {
    return false;
  }
}

export const getPriceRealtime = async () => {
  const realtimeApiURL = process.env.REACT_APP_FOREX_API_URL;
  const forexApiPriceQuoteRealtime =
    process.env.REACT_APP_FOREX_GET_QUOTE_ENDPOINT;
  const requestURL = `${realtimeApiURL}/${forexApiPriceQuoteRealtime}`;
  try {
    const response = await request(requestURL, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      }
    });
    return response;
  } catch (e) {
    return false;
  }
}


export const getOpenPrice = async () => {
  const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
  const forexApiOpenPrice =
    process.env.REACT_APP_API_GET_PRICE_LIST_OPEN_PRICE_ENDPOINT;
  const requestURL = `${apiUrl}${forexApiOpenPrice}`;
  try {
    const response = await request(requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      }
    });
    if (response.code === 'gyllenhus_get_price_list_open_price_success') {
      return response.data.getPriceListOpenPrice;
    } else return false;

  } catch (e) {
    return false;
  }
}

export const getTransports = async (language) => {
  if (!language) language = "en";
  if (language === "se") language = "sv";
  const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
  const getTransportApi = process.env.REACT_APP_API_GET_TRANSPORTS;
  const requestURL = `${apiUrl}${getTransportApi}`;
  try {
    const response = await request(requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_transports_success') {
      return response.data.stransports;
    } else return false;
  } catch (e) {
    return false;
  }
}

export const getBanks = async (language) => {
  if (!language) language = "se";
  const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
  const getTransportApi = process.env.REACT_APP_API_GET_BANKS;
  const requestURL = `${apiUrl}${getTransportApi}`;
  try {
    const response = await request(requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_banks_success') {
      return response.data.banks;
    } else return false;
  } catch (e) {
    return false;
  }
}

export const getDeliveryCostSetting = async () => {
  const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
  const getDeliveryCostSetting = process.env.REACT_APP_API_GET_DELIVERY_COST_SETTING;
  const requestURL = `${apiUrl}${getDeliveryCostSetting}`;
  try {
    const response = await request(requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ 'userType': 0 }),
    });
    if (response.code === 'gyllenhus_get_delivery_success') {
      return response.data;
    } else return false;
  } catch (e) {
    return false;
  }
}

export const getPriceList = async (userInfoID) => {
  const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
  const forexApiQuote = process.env.REACT_APP_API_GET_PRICE_LIST_ENDPOINT;
  const requestURL = `${apiUrl}${forexApiQuote}`;
  try {
    const idUserType = userInfoID ? userInfoID : 0;
    const response = await request(requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ userType: idUserType }),
    });
    if (response.code === 'gyllenhus_get_price_list_success') {
      const {
        priceList
      } = response.data;
      return (priceList);
    } else return (false);
  } catch (e) {
    return (false);
  }
};

export const getFooter = async (language) => {
  const apiUrl = process.env.REACT_APP_BACKEND_API_URL;
  const footerEndpoint = process.env.REACT_APP_API_GET_FOOTER;
  const requestUrl = `${apiUrl}/${footerEndpoint}`;
  try {
    if (!language) language = "se";
    const response = await request(requestUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ language }),
    });
    if (response.code === 'gyllenhus_get_footer_success') {
      const { customerSupport, menuFooter1, menuFooter2 } = response.data;

      const menuFooter1Children =
        menuFooter1.length > 0 && typeof menuFooter1[0].children !== 'undefined'
          ? menuFooter1[0].children
          : [];
      const menuFooter2Children =
        menuFooter2.length > 0 && typeof menuFooter2[0].children !== 'undefined'
          ? menuFooter2[0].children
          : [];

      return {
        customerSupport,
        menuFooter1Children,
        menuFooter2Children,
      }
    }
    return false;
  } catch (e) {
    return false;
  }
}

export const loadProductDetail = async (language, slug) => {
  const menuEndpoint = process.env.REACT_APP_API_GET_PRODUCT_DETAILS;
  const requestURL = `${backendApiUrl}/${menuEndpoint}`;
  if (!language) language = "se";
  try {
    const response = await request(requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ language, slug }),
    });
    if (response.code === 'gyllenhus_get_product_classify_success') {
      return response.data.product;
    } else {
      return false;
    }
  } catch (e) {
    return false;
  }
}