import numeral from 'numeral';
import { round } from 'lodash';

const DEFAULT_DECIMALS_NUMBER_FORMAT_MASK = '0,0.00';
const DECIMALS_NUMBER_FORMAT_MASK = '0,0'


const DEFAULT_DECIMALS_NUMBER = 2;
const DECIMALS_NUMBER = 0;


const convertToFormattedNumber = (value, decimalsNumber, mask) => {
  const roundedNumber = round(value, decimalsNumber);
  return numeral(roundedNumber).format(mask);
};

const convertToFormattedNumberPurity = (value, decimalsNumber, mask) => {
  const roundedNumber = round(value, decimalsNumber);
  return replaceAll((numeral(roundedNumber).format(mask)), ",", " ");
};

const replaceAll = (str, find, replace) => {
  return str.replace(new RegExp(find, 'g'), replace);
}

const formatNumberDefault = (str) => {
  return replaceAll(str, "," , " ").replace(".", ",");
}

export const formatNumberDecimal = (value) => convertToFormattedNumber(value, DEFAULT_DECIMALS_NUMBER, DEFAULT_DECIMALS_NUMBER_FORMAT_MASK);
export const formatNumberDecimalPurity = (value) => convertToFormattedNumberPurity(value, DECIMALS_NUMBER, DECIMALS_NUMBER_FORMAT_MASK);

export const formatInterger = value => numeral(value).format('0,0');
export const formatNumber = value => formatNumberDefault(numeral(value).format('0,0.00'));
export const formatNumber3Decimals = value => formatNumberDefault(numeral(value).format('0,0.000'));
export const toPercent = value => numeral(value).format('0.00%');

export const roundNumberValue = (value, decimalsNumber = DEFAULT_DECIMALS_NUMBER) => round(value, decimalsNumber);
export const roundNumberValuePurity = (value) => round(value, DECIMALS_NUMBER);

export default {};
