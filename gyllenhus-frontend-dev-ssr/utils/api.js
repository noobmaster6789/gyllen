// api/index.js
import io from 'socket.io-client';
const socket = io(process.env.REACT_APP_SOCKET_URL);

// listen for any messages coming through
// of type 'margin-changed' and then trigger the
// callback function

const changeMargin = cb => {
  socket.off('margin-changed');
  socket.on('margin-changed', () => {
    // trigger the callback passed in when
    // our App component calls connect
    cb();
  });
};

// listen for any messages coming through
// of type 'margin-changed' and then trigger the
// callback function

const changeFrequency = cb => {
  socket.off('change-frequency-settings');
  socket.on('change-frequency-settings', () => {
    // trigger the callback passed in when
    // our App component calls connect
    cb();
  });
};

// get price of gold and silver realtime
const updatePriceRealtime = cb => {
  socket.off('update-price-realtime');
  socket.on('update-price-realtime', (data) => {
    // trigger the callback passed in when
    // our App component calls connect
    cb(data);
  });
};

const changeCountdownTime = cb => {
  socket.off('change-countdown');
  socket.on('change-countdown', (data) => {
    const { currentTemp } = data;
    cb(currentTemp);
  });
};

const refreshInternal = cb => {
  socket.off('internal-refresh');
  socket.on('internal-refresh', (data) => {
    cb(data);
  });
};

const updateUserType = cb => {
  socket.off('update-user-type');
  socket.on('update-user-type', (data) => {
    cb(data);
  });
};

export {
  changeMargin,
  changeCountdownTime,
  changeFrequency,
  updatePriceRealtime,
  refreshInternal,
  updateUserType
};
