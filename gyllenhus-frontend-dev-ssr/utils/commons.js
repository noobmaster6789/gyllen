import { roundNumberValuePurity } from "./formatter";

export const updateNewPriceToCart = (draft) => {
  const cartBuyProducts = JSON.parse(localStorage.getItem("cartBuyItems")) || [];
  cartBuyProducts.map(p => {
    const data = {
      item: p.item.item,
      goldPrice: draft.goldPrice,
      silverPrice: draft.silverPrice,
      platinumPrice: draft.platinumPrice,
      palladiumPrice: draft.palladiumPrice
    };
    p.price = getValuePrice(data, true);
    return p;
  });
  localStorage.setItem('cartBuyItems', JSON.stringify(cartBuyProducts));

  const cartSellProducts = JSON.parse(localStorage.getItem("cartSellItems")) || [];
  cartSellProducts.map(p => {
    const data = {
      item: p.item.item,
      goldPrice: draft.goldPrice,
      silverPrice: draft.silverPrice,
      platinumPrice: draft.platinumPrice,
      palladiumPrice: draft.palladiumPrice
    };
    p.price = getValuePrice(data, true);
    return p;
  });
  localStorage.setItem('cartSellItems', JSON.stringify(cartSellProducts));
  return draft = calculateTotal(null, draft);
}

export const calculateTotal = (action, draft) => {
  const cartBuyProducts = JSON.parse(localStorage.getItem("cartBuyItems")) || [];
  if (cartBuyProducts.length == 0) {
    draft.isBuyTabActive = false;
  }
  const totalBuy = cartBuyProducts.reduce(function (p, obj) { return p + obj.price * obj.quantity; }, 0);
  draft.cartBuyProducts = cartBuyProducts;
  draft.totalBuyIncludeTax = totalBuy;
  draft.totalBuyTax = cartBuyProducts.reduce(function (p, obj) {
    return p + (obj.item.item.productVat / 100 * obj.price / (1 + obj.item.item.productVat / 100) * obj.quantity);
  }, 0);
  draft.totalBuyWithoutTax = totalBuy - draft.totalBuyTax;
  draft.totalBuyQuantity = cartBuyProducts.reduce(function (p, obj) {
    return p + obj.quantity
  }, 0);
  draft.totalBuyToPay = draft.totalBuyIncludeTax;

  const cartSellProducts = JSON.parse(localStorage.getItem("cartSellItems")) || [];
  const totalSell = cartSellProducts.reduce(function (p, obj) { return p + obj.price * obj.quantity; }, 0);
  draft.cartSellProducts = cartSellProducts;
  draft.totalSellIncludeTax = totalSell;
  draft.totalSellTax = cartSellProducts.reduce(function (p, obj) {
    return p + (obj.item.item.productVat / 100 * obj.price / (1 + obj.item.item.productVat / 100) * obj.quantity);
  }, 0);
  draft.totalSellWithoutTax = totalSell - draft.totalSellTax;
  draft.totalSellQuantity = cartSellProducts.reduce(function (p, obj) {
    return p + obj.quantity
  }, 0);
  draft.totalSellToPay = draft.totalSellIncludeTax;
  const transportInsurance = JSON.parse(localStorage.getItem("transportInsurance"));
  const deliverySetting = JSON.parse(localStorage.getItem("deliverySetting"));

  // Delivery
  if (transportInsurance && deliverySetting) {
    draft.transportAndInsurance = transportInsurance;
    if (draft.isBuyTabActive) {
      const deliveryBuy = getDelivery(cartBuyProducts, 0, deliverySetting.deliveryCosts, deliverySetting.vatDelivery);

      draft.deliveryWithoutVAT = deliveryBuy.deliveryWithoutVAT;
      draft.deliveryVAT = deliveryBuy.deliveryVAT;

      draft.totalBuyIncludeTax += deliveryBuy.deliveryWithoutVAT + deliveryBuy.deliveryVAT;
      draft.totalBuyWithoutTax += deliveryBuy.deliveryWithoutVAT;
      draft.totalBuyTax += deliveryBuy.deliveryVAT;

    } else {
      const deliverySell = getDelivery(cartSellProducts, 0, deliverySetting.deliveryCosts, deliverySetting.vatDelivery);
      
      draft.deliveryWithoutVAT = deliverySell.deliveryWithoutVAT;
      draft.deliveryVAT = deliverySell.deliveryVAT;

      draft.totalSellIncludeTax += deliverySell.deliveryWithoutVAT + deliverySell.deliveryVAT;
      draft.totalSellWithoutTax += deliverySell.deliveryWithoutVAT;
      draft.totalSellTax += deliverySell.deliveryVAT;

    }
  }
  // Payment Method & Information
  draft = loadPaymentMethod(draft);
  draft = loadInfomation(draft);
  // Coupon
  draft = applyCoupon(action, draft, cartBuyProducts, cartSellProducts);
  return draft;
}

const loadPaymentMethod = (draft) => {
  const paymentMethodStr = localStorage.getItem("paymentMethod");
  try {
    draft.paymentMethod = JSON.parse(paymentMethodStr).type;
    if (draft.paymentMethod === "bank-payment") {
      const bank = JSON.parse(localStorage.getItem("bankPayment"));
      draft.bank = bank.bank ? bank.bank : "";
      draft.clearingNumber = bank.clearingNumber ? bank.clearingNumber : "";
      draft.accountNumber = bank.accountNumber ? bank.accountNumber : "";
    }
  } catch(e) {
    console.log('error: ', e);
  }
  return draft;
}

const loadInfomation = (draft) => {
  try {
    const information = JSON.parse(localStorage.getItem("information"));
    if (information) {
      draft.billingName =  information.billingName;
      draft.billingCompany = information.billingCompany;
      draft.billingAddress = information.billingAddress;
      draft.billingOrganizationNumber = information.billingOrganizationNumber;
      draft.billingPostalCode = information.billingPostalCode;
      draft.billingCity = information.billingCity;
      draft.billingEmail = information.billingEmail;
      draft.billingPhone = information.billingPhone;
      draft.fullName = information.fullName;
      draft.isPrivateActive = information.isPrivateActive;
      draft.shippingAddress = information.shippingAddress;
      draft.shippingPostalCode = information.shippingPostalCode;
      draft.shippingCity = information.shippingCity;
      draft.shippingCountry = information.shippingCountry;
      draft.useShippingAddress = information.useShippingAddress;
    }
  } catch(e) {
    console.log('error: ', e);
  }
  return draft;
}

const applyCoupon = (action, draft, cartBuyProducts, cartSellProducts) => {
  if ( !cartBuyProducts || !cartSellProducts)
    return draft;
  const coupon = JSON.parse(localStorage.getItem("coupon"));
  draft.discountTotal = 0;
  if (action && coupon) {
    draft.discount = coupon.couponCode;
    draft.couponDescription = coupon.couponDescription;
    const couponDiscountType = coupon.couponDiscountType;
    if ( couponDiscountType === "percent" || couponDiscountType === "absolute_value") {
      const couponAmount = coupon.couponAmount || 0;
      const couponProducts = coupon.couponProducts;
      const couponMinimumBuy = coupon.couponMinimumBuy || 0;
      let totalAmount = 0;
      // If coupon apply for some products
      if (couponAmount > 0 && couponProducts && couponProducts.length > 0) {
        if (draft.isBuyTabActive) {
          const filterCouponBuyProduct = cartBuyProducts.filter(p => couponProducts.indexOf(p.id.toString()) != -1);
          totalAmount = filterCouponBuyProduct.reduce(function (p, obj) { return p + obj.price * obj.quantity; }, 0)
        } else {
          const filterCouponSellProduct = cartSellProducts.filter(p => couponProducts.indexOf(p.id.toString()) != -1);
          totalAmount = filterCouponSellProduct.reduce(function (p, obj) { return p + obj.price * obj.quantity; }, 0)
        }
        if (totalAmount >= couponMinimumBuy) {
          draft.discountTotal = (couponDiscountType === "percent" ) ? 
            (couponAmount * totalAmount / 100) : (couponAmount);
        }
      } else {
        // If coupon apply for all products
        if (couponAmount > 0 && draft.totalBuyWithoutTax > couponMinimumBuy) {
          if (draft.isBuyTabActive) {
            draft.discountTotal = coupon.couponAmount * draft.totalBuyWithoutTax / 100;
          } else {
            draft.discountTotal = coupon.couponAmount * draft.totalSellWithoutTax / 100;
          }
        }
      }
    }
    else if (couponDiscountType === "free_delivery") {
      draft.discountTotal = draft.deliveryWithoutVAT;
    }
  }
  if (draft.isBuyTabActive) {
    draft.totalBuyIncludeTax -= draft.discountTotal;
    draft.totalBuyWithoutTax -= draft.discountTotal;
  } else {
    draft.totalSellIncludeTax -= draft.discountTotal;
    draft.totalSellWithoutTax -= draft.discountTotal;
  }
  return draft;
}

export const reloadBuyCart = (draft, cartBuyProducts) => {
  if (cartBuyProducts && cartBuyProducts.length > 0) {
    const total = cartBuyProducts.reduce(function (p, obj) { return p + obj.price * obj.quantity; }, 0);
    draft.cartBuyProducts = cartBuyProducts;
    draft.totalBuyQuantity = cartBuyProducts.reduce(function (p, obj) {
      return p + obj.quantity
    }, 0);
    draft.totalBuyTax = cartBuyProducts.reduce(function (p, obj) {
      return p + (obj.item.item.productVat / 100 * obj.price / (1 + obj.item.item.productVat / 100) * obj.quantity);
    }, 0);
    draft.totalBuyToPay = total;
  }
  return draft;
}

export const reloadSellCart = (draft, cartSellProducts) => {
  if (cartSellProducts && cartSellProducts.length > 0) {
    const total = cartSellProducts.reduce(function (p, obj) { return p + obj.price * obj.quantity; }, 0);
    draft.cartSellProducts = cartSellProducts;
    draft.totalSellQuantity = cartSellProducts.reduce(function (p, obj) {
      return p + obj.quantity
    }, 0);
    draft.totalSellTax = cartSellProducts.reduce(function (p, obj) {
      return p + (obj.item.item.productVat / 100 * obj.price / (1 + obj.item.item.productVat / 100) * obj.quantity);
    }, 0);
    draft.totalSellToPay = total;
  }
  return draft;
}


export const getBuySellPriceProduct = (typeProduct, goldPrice, silverPrice, platinumPrice, palladiumPrice,
  productWeight, productBuyMargin, productPurity, productVat) => {
  let buyPrice = null;
  let sellPrice = null;
  if (
    goldPrice &&
    silverPrice &&
    platinumPrice &&
    palladiumPrice &&
    typeof goldPrice === 'object' &&
    typeof silverPrice === 'object' &&
    typeof platinumPrice === 'object' &&
    typeof palladiumPrice === 'object'
  ) {
    if (typeProduct === 'platinum') {
      sellPrice = platinumPrice.price;
      buyPrice = platinumPrice.price;
    } else if (typeProduct === 'palladium') {
      sellPrice = palladiumPrice.price;
      buyPrice = palladiumPrice.price;
    } else if (typeProduct === 'gold') {
      sellPrice = goldPrice.price;
      buyPrice = goldPrice.price;
    } else if (typeProduct === 'silver') {
      sellPrice = silverPrice.price;
      buyPrice = silverPrice.price;
    }
  }
  return getPriceProduct(buyPrice, false, productWeight.valueGram, productBuyMargin, productPurity, productVat);
}


export const getPriceProduct = (price, isHeader, weight, margin, productPurity, productVat) => {
  const troyOunceToGram = 31.1034768;

  const pricePerGram = price => price / troyOunceToGram;

  let productPrice = price;
  if (isHeader === false) {

    let spotPrice = pricePerGram(price) * weight;
    const marginPercent = Number(margin) ? margin / 100 : 0;
    if (Number(productPurity)) {
      const purity = productPurity === 99.99 ? 1 : productPurity / 100;
      spotPrice *= purity;
    }
    // calculate product price without vat
    const priceWithoutVat = spotPrice + marginPercent * spotPrice;
    productPrice = priceWithoutVat;
    if (Number(productVat)) {
      // product price with vat
      const vatPercent = productVat / 100;
      productPrice = priceWithoutVat + priceWithoutVat * vatPercent;
    }
  } else if (unit === 'g') {
    productPrice = pricePerGram(price);
  }
  return roundNumberValuePurity(productPrice);
}

export const getValuePrice = (data, isBuyType) => {
  const {
    item: {
      product,
      images,
      productPurity,
      productVat,
      productWeight,
      typeProduct,
      typeBrands,
      productBuyMargin,
      productSellMargin,
    },
    currency,
    goldPrice,
    silverPrice,
    platinumPrice,
    palladiumPrice
  } = data;

  return getBuySellPriceProduct(typeProduct, goldPrice, silverPrice, platinumPrice, palladiumPrice,
    productWeight, (isBuyType ? productBuyMargin : productSellMargin), productPurity, productVat);
}


export const getDeliveryCostsByWeight = (weight, userType = 0, deliveryCosts) => {
  let cost = 0;
  if (deliveryCosts && deliveryCosts.length > 0) {
    cost = 0;

    for (let i = 0; i < deliveryCosts.length; i++) {
      if (weight < deliveryCosts[i].weight) {
        cost = deliveryCosts[i].price;
        break;
      }
    }

  }
  return cost;

}

export const getDeliveryCostsByPrice = (price, userType = 0, deliveryCosts) => {
  let cost = 0;
  if (deliveryCosts && deliveryCosts.length > 0) {
    cost = 0;

    for (let i = 0; i < deliveryCosts.length; i++) {
      if (price < deliveryCosts[i].value) {
        cost = deliveryCosts[i].price;
        break;
      }
    }
  }
  return cost;
}

export const getVATDeliveryPercent = (type = 'gold', vatDelivery) => {
  let vatDeliveryPercent = 0;

  switch (type) {
    case 'gold':
      vatDeliveryPercent = vatDelivery.gold;
      break;
    case 'silver':
      vatDeliveryPercent = vatDelivery.silver;
      break;
    case 'accessories':
      vatDeliveryPercent = vatDelivery.accessories;
      break;

    default:
      vatDeliveryPercent = 0;
      break;
  }

  return vatDeliveryPercent;
}

export const getDeliveryFeeWidthoutVat = (vatDeliverySetting, deliveryCosts, totalOrder, totalPriceGold, totalPriceSilver,
  totalPricePlatinum, totalPricePalladium, totalPriceAccessories, totalPriceOtherImpurities) => {

  let deliveryCostGold = 0;
  let deliveryCostSilver = 0;
  let deliveryCostPlatinum = 0;
  let deliveryCostPalladium = 0;
  let deliveryCostAccessories = 0;
  let deliveryCostOtherImpurities = 0;

  if (totalPriceGold > 0) {
    deliveryCostGold = deliveryCosts * ((totalPriceGold) / (totalOrder));
  }

  if (totalPriceSilver > 0) {
    deliveryCostSilver = (deliveryCosts) * ((totalPriceSilver) / (totalOrder));
  }

  if (totalPricePlatinum > 0) {
    deliveryCostPlatinum = (deliveryCosts) * ((totalPricePlatinum) / (totalOrder));
  }

  if (totalPricePalladium > 0) {
    deliveryCostPalladium = (deliveryCosts) * ((totalPricePalladium) / (totalOrder));
  }

  if (totalPriceAccessories > 0) {
    deliveryCostAccessories = (deliveryCosts) * ((totalPriceAccessories) / (totalOrder));
  }

  if (totalPriceOtherImpurities > 0) {
    deliveryCostOtherImpurities = (deliveryCosts) * ((totalPriceOtherImpurities) / (totalOrder));
  }

  const vatDeliveryCostGold = (deliveryCostGold) * getVATDeliveryPercent('gold', vatDeliverySetting);
  const vatDeliveryCostSilver = (deliveryCostSilver) * getVATDeliveryPercent('silver', vatDeliverySetting);
  const vatDeliveryCostPlatinum = (deliveryCostPlatinum) * getVATDeliveryPercent('platinum', vatDeliverySetting);
  const vatDeliveryCostPalladium = (deliveryCostPalladium) * getVATDeliveryPercent('palladium', vatDeliverySetting);
  const vatDeliveryCostAccessories = (deliveryCostAccessories) * getVATDeliveryPercent('accessories', vatDeliverySetting);
  const vatDeliveryCostOtherImpurities = (deliveryCostOtherImpurities) * getVATDeliveryPercent('other_impurities', vatDeliverySetting);

  const deliveryFeeWidthoutVat = (deliveryCosts) - ((vatDeliveryCostGold) + (vatDeliveryCostSilver) + (vatDeliveryCostPlatinum)
    + (vatDeliveryCostPalladium) + (vatDeliveryCostAccessories) + (vatDeliveryCostOtherImpurities));

  return deliveryFeeWidthoutVat;
}


export const getDelivery = (products, userType = 0, deliveryCosts, vatDeliverySetting) => {
  if (products && products.length > 0) {
    let totalOrder = 0;
    let totalOrderWidthOutVAT = 0;
    let totalOrderVAT = 0;

    let totalProductWeight = 0;

    let totalPriceGold = 0;
    let totalPriceSilver = 0;
    let totalPricePlatinum = 0;
    let totalPricePalladium = 0;
    let totalPriceAccessories = 0;
    let totalPriceOtherImpurities = 0;

    products.forEach(function (product) {
      const item = product.item.item;
      const quantity = product.quantity || 0;

      const productWeight = item.productWeight.valueGram;
      totalProductWeight = productWeight * quantity;

      const productType = item.typeProduct;
      const priceVAT = (item.productVat / 100) * product.price;
      const productPrice = product.price + priceVAT;
      const productPriceWidthOutVat = product.price;

      const productTotalPrice = productPrice * quantity;
      const productTotalPriceWidthOutVat = productPriceWidthOutVat * quantity;
      const priceTotalVAT = priceVAT * quantity;

      totalOrder += productTotalPrice;
      totalOrderWidthOutVAT += productTotalPriceWidthOutVat;
      totalOrderVAT += priceTotalVAT;

      switch (productType) {
        case 'gold':
          totalPriceGold += productTotalPrice;
          break;
        case 'silver':
          totalPriceSilver += productTotalPrice;
          break;
        case 'platinum':
          totalPricePlatinum += productTotalPrice;
          break;
        case 'palladium':
          totalPricePalladium += (productTotalPrice);
          break;
        case 'accessories':
          totalPriceAccessories += (productTotalPrice);
          break;
        case 'other_impurities':
          totalPriceOtherImpurities += (productTotalPrice);
          break;

        default:
          break;
      }
    });

    const deliveryCostsByWeight = getDeliveryCostsByWeight(totalProductWeight, userType, deliveryCosts);
    const deliveryCostsByPrice = getDeliveryCostsByPrice(totalOrder, userType, deliveryCosts);

    const deliveryFeeCost = deliveryCostsByWeight > deliveryCostsByPrice ? deliveryCostsByWeight : deliveryCostsByPrice;

    const deliveryFeeWidthoutVat = getDeliveryFeeWidthoutVat(vatDeliverySetting, deliveryFeeCost, totalOrder, totalPriceGold, totalPriceSilver,
      totalPricePlatinum, totalPricePalladium, totalPriceAccessories, totalPriceOtherImpurities);

    const json = {
      'deliveryWithoutVAT': deliveryFeeWidthoutVat,
      'deliveryVAT': (deliveryFeeCost) - (deliveryFeeWidthoutVat),
      'delivery': deliveryFeeCost
    };

    return json;

  } else {
    const json = {
      'deliveryWithoutVAT': 0,
      'deliveryVAT': 0,
      'delivery': 0
    };

    return json;
  }
}

