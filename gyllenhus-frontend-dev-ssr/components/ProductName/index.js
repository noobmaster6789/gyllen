/**
 *
 * ProductName
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

function ProductName({ title, value, unit }) {
  return (
    <React.Fragment>
      <div className="weight">
        {value}
        <i>g</i> {unit}
      </div>{' '}
      <div className="product">{title}</div>
    </React.Fragment>
  );
}

ProductName.propTypes = {
  title: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  unit: PropTypes.string.isRequired,
};

export default ProductName;
