/*eslint-disable */

import { Helmet } from 'react-helmet';

/**
 *
 * TradingViewDemo
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Iframe from 'react-iframe';
// import styled from 'styled-components';

// function TradingViewDemo({ containerId, symbol }) {
//   const url = `${window.location.origin}/static/index.html`
//   const iframe = `<iframe id="${containerId}" src="https://www.tradingview.com/widgetembed/?frameElementId=tradingview_4ba77&amp;symbol=${symbol}&amp;interval=D&amp;hidesidetoolbar=0&amp;symboledit=0&amp;saveimage=1&amp;toolbarbg=f1f3f6&amp;studies=%5B%5D&amp;theme=Light&amp;style=1&amp;timezone=Etc%2FUTC&amp;studies_overrides=%7B%7D&amp;overrides=%7B%7D&amp;enabled_features=%5B%5D&amp;disabled_features=%5B%5D&amp;locale=en&amp;utm_source=www.tradingview.com&amp;utm_medium=widget_new&amp;utm_campaign=chart&amp;utm_term=OANDA%3AXAUUSD" style="width: 100%; height: 610px; margin: 0 !important; padding: 0 !important;" frameborder="0" allowtransparency="true" scrolling="no" allowfullscreen=""></iframe>`;
//   return (
//     <div>
//       <Iframe
//         url={url}
//         width="450px"
//         height="450px"
//         id="myId"
//         className="myClassname"
//         display="initial"
//         position="relative"
//       />
//     </div>
//   );
// }

function getParameterByName(name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  const regex = new RegExp(`[\\?&]${name}=([^&#]*)`);
  const results = regex.exec(location.search);
  return results === null
    ? ''
    : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

export class TradingViewDemo extends React.PureComponent {
  initData = () => {
    const urlLib = `static/charting_library/`;
    const urlChart = `${process.env.REACT_APP_BACKEND_API_URL}/${process.env.REACT_APP_API_CHART}`;

    const widgetOptions = {
      // debug: true,
      fullscreen: true,
      symbol: this.props.symbol,
      interval: 'D',
      container_id: this.props.containerId,

      //	BEWARE: no trailing slash is expected in feed URL
      datafeed: new Datafeeds.UDFCompatibleDatafeed(urlChart, 1000),
      library_path: urlLib,
      locale: getParameterByName('lang') || 'en',

      disabled_features: ['use_localstorage_for_settings'],
      enabled_features: ['study_templates'],
      charts_storage_url: 'http://saveload.tradingview.com',
      charts_storage_api_version: '1.14',
      client_id: 'tradingview.com',
      user_id: 'public_user_id',
      theme: getParameterByName('theme'),
      overrides: {
        'mainSeriesProperties.showCountdown' : true,
      }
    };
    this.widget = (window.tvWidget = new window.TradingView.widget(
      widgetOptions,
    ));

  }
  componentDidMount() {
    this.initData();
  }

  componentDidUpdate() {
    this.widget.chart().setSymbol(this.props.symbol);
  }

  render() {
    return <div id={this.props.containerId} style={{ height: '100%' }} />;
  }
}

TradingViewDemo.propTypes = {
  containerId: PropTypes.string.isRequired,
  symbol: PropTypes.string.isRequired,
};

export default TradingViewDemo;
