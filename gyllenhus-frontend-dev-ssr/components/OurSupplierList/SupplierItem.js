import React from 'react';
import PropTypes from 'prop-types';

/**
 * @return {null}
 */
function SupplierItem({ item, index }) {
  if (item.icon !== undefined && typeof item.icon === 'object') {
    return (
      <React.Fragment>
        <img src={item.icon.url} alt={item.name} />
        {index % 3 === 0 && <div className="line-break" />}
      </React.Fragment>
    );
  }
  return null;
}
SupplierItem.propTypes = {
  item: PropTypes.any,
  index: PropTypes.number,
};

export default SupplierItem;
