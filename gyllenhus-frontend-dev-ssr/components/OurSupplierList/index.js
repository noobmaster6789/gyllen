/**
 *
 * OurSupplierList
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import LoadingIndicator from '../LoadingIndicator';
import ListItem from '../ListItem';
import List from '../List';
import SupplierItem from './SupplierItem';

/**
 * @return {null}
 */
function OurSupplierList({ loading, error, data }) {
  if (loading) {
    return <LoadingIndicator />;
  }
  if (data !== false) {
    
    return (
      <div className="slide">
        <List component={SupplierItem} items={data} />
      </div>
    );
  }
  return null;
}

OurSupplierList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  data: PropTypes.any,
};

export default OurSupplierList;
