import React from 'react';
import PropTypes from 'prop-types';

import ReactHtmlParser from 'react-html-parser';

function BannerItem({ title, background, icon_background, description, symbol, menu, index, activeKey }) {
  return (
    <React.Fragment>
      <div
        className="banner"
        style={{
          display: 'block',
          backgroundImage: `url(${background.url})`,
          width: `100%`
        }}
      >
        <div class="banner-content">
          <div class="symbol">
            {icon_background ? <img src={icon_background.url} /> : ""}
          </div>
          <div class={`text ${activeKey !== 0 ? "is-white" : ""}`}>
            <h1> {ReactHtmlParser(title)} </h1>
            { activeKey == 0 ? (<nav class="quick-links"> {ReactHtmlParser(description)} </nav>) :
              (<p class="quick-links"> {ReactHtmlParser(description)} </p>) }
          </div>
        </div>

      </div>
    </React.Fragment>
  );
}

BannerItem.propTypes = {
  background: PropTypes.object,
  description: PropTypes.string,
  menu: PropTypes.element,
  index: PropTypes.number,
  activeKey: PropTypes.number,
};

export default BannerItem;
