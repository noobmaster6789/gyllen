/**
 *
 * Banner
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Cookie from 'js-cookie'

import BannerMenu from './Menu';
import BannerItem from './Item';
import Siema from 'siema';


function Banner({ items, categoryProducts, activeKey, onChangeActiveBanner, collapse }) {
  const menu = <BannerMenu items={items.categoryProducts} />;
  const [currentIndex, setCurrentIndex] = useState(0);

  const [mySiema, setMySiema] = useState(false);

  useEffect(() => {
    if (window && document && !mySiema) {
      const mySie = new Siema({
        loop: true,
        startIndex: 0,
        onChange: () => {
          setCurrentIndex(mySie.currentSlide);
          onChangeActiveBanner(mySie.currentSlide);
        }
      });
      setMySiema(mySie);
    }
    if (mySiema) {
      setCurrentIndex(activeKey);
      onChangeActiveBanner(activeKey);
      mySiema.goTo(activeKey);
    }
  }, [activeKey]);

  return (
    <React.Fragment>
      <header class={`wrapper siema ${collapse || collapse == "true" ? "less-margin": ""}`}>
        {items.banners.map((item, index) => {
          return (
            <BannerItem
              key={`banner-item-${index}`}
              {...item}
              menu={menu}
              index={index}
              activeKey={currentIndex}
            />);
        })}
      </header>
      <div class="indicator" style={{ position: `relative` }}>
        <button class={`dot ${currentIndex == 0 ? "active" : ""}`} onClick={() => onChangeActiveBanner(0)}></button>
        <button class={`dot ${currentIndex == 1 ? "active" : ""}`} onClick={() => onChangeActiveBanner(1)}></button>
        <button class={`dot ${currentIndex == 2 ? "active" : ""}`} onClick={() => onChangeActiveBanner(2)}></button>
        <button class={`dot ${currentIndex == 3 ? "active" : ""}`} onClick={() => onChangeActiveBanner(3)}></button>
        <button class={`dot ${currentIndex == 4 ? "active" : ""}`} onClick={() => onChangeActiveBanner(4)}></button>
      </div>
    </React.Fragment>
  );
}

Banner.propTypes = {
  items: PropTypes.any,
  categoryProducts: PropTypes.any,
  activeKey: PropTypes.number,
};

export default Banner;
