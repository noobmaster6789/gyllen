import React from 'react';
import PropTypes from 'prop-types';

/**
 * @return {null}
 */
function BannerMenu({ items }) {
  if (typeof items !== 'undefined') {
    return (
      <nav className="quick-links">
        {items.map((item, index) => (
          // eslint-disable-next-line jsx-a11y/anchor-is-valid
          <a key={`quick-link-${index + 1}`} href="#">
            {item.name} ›
          </a>
        ))}
      </nav>
    );
  }
  return null;
}

BannerMenu.propTypes = {
  items: PropTypes.any,
};
export default BannerMenu;
