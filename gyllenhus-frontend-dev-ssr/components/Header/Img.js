import styled from 'styled-components';

const Img = styled.img`
  display: block;
  height: 100%;
`;

export default Img;
