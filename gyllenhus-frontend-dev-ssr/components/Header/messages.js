/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  contactUs: {
    id: `${scope}.contactUs`,
    defaultMessage: 'Contact Us',
  },
});
