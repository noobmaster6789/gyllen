import React from 'react';
import Link from 'next/link'
import { FormattedMessage } from 'react-intl';

import Router from 'next/router'
import NProgress from 'nprogress'
import LocaleToggle from '../../containers/LocaleToggle';
import ContactUsWidget from '../../containers/ContactUsWidget';
import MenuList from '../../containers/MenuList';
import MyAccountWidget from '../../containers/MyAccountWidget';
import PriceList from '../../containers/PriceList';

import NavBar from './NavBar';
import messages from './messages';
import Img from './Img';
import CartHeader from '../CartHeader';
import CartHeaderImage from '../CartHeaderImage';
import Search from '../../containers/Search';
import SearchMobile from '../../containers/Search/SearchMobile';

const REACT_APP_API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT
const logo = `${REACT_APP_API_ENDPOINT}/static/images/logo.png`

Router.onRouteChangeStart = url => NProgress.start()
Router.onRouteChangeComplete = () => NProgress.done()
Router.onRouteChangeError = () => NProgress.done()

export class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isToggleOn: false };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn,
    }));
    try {
      if (!this.state.isToggleOn) {
        if (document.getElementsByClassName("countdown") && document.getElementsByClassName("countdown").length > 0)
          document.getElementsByClassName("countdown")[0].classList.add("hide");
      } else {
        if (document.getElementsByClassName("countdown") && document.getElementsByClassName("countdown").length > 0)
          document.getElementsByClassName("countdown")[0].classList.remove("hide");
      }
    } catch(e){
      console.log('e: ', e);
    }
  }

  componentDidMount() {
    if (this.state.isToggleOn) {
      document.body.classList.add('hide-overflow');
    } else {
      document.body.classList.remove('hide-overflow');
    }
  }

  render() {
    const { openPrices, priceRealtime, menu , deliverySetting, currencyIndex, weightIndex, isCollapsed} = this.props

    return (
      <NavBar className={`header ${this.state.isToggleOn ? 'show-menu' : ''}`}>
        <div className="top wrapper">
          <a className="logo" href="/">
              <Img src={logo} alt="Logo" />
          </a>
          <div className="button-group on-mobile">
            <CartHeaderImage />
            <button
              className="menu-toggle"
              type="button"
              onClick={this.handleClick}>
              <span className="stroke" />
              <span className="stroke" />
              <span className="stroke" />
            </button>
            <SearchMobile /> 
          </div>
        
          <div className="settings">
            <div className="links">
              <LocaleToggle />
              <MyAccountWidget />
            </div>
            <div className="opening-time">
              <ContactUsWidget />
            </div>
          </div>
        </div>
        <PriceList openPricesLoad= {openPrices} priceRealtime={priceRealtime} 
          defaultCurrency={currencyIndex} defaultWeight={weightIndex} isCollapsed={isCollapsed}/>
        <div className="major">
          <div className="wrapper">
            <MenuList name="main-menu" data={menu} />
              
            
            <div className="settings on-mobile">
              <p className="title">
                <FormattedMessage {...messages.contactUs} />
              </p>
              <div className="opening-time">
                <ContactUsWidget />
              </div>
            </div>
            <CartHeader deliverySetting={deliverySetting} />
          </div>
        </div>
      </NavBar>
    );
  }
}

export default Header;
