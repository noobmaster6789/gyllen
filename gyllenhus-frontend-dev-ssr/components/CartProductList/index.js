import React from 'react';
import PropTypes from 'prop-types';
import List from '../List';
import CartProductListItem from '../CartProductListItem';
import PlaceholderLoading from '../PlaceholderLoading/cart';

const REACT_APP_API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT
const productEmpty = `${REACT_APP_API_ENDPOINT}/static/images/product-empty-transparent.png`;

/**
 * @return {null}
 */
function CartProductList({
  data,
  defaultCurrency,
  typeBuySell,
  dispatch,
}) {
  
  if (data !== false && data && data.length > 0 && data!== null) {
    const componentData = {
      typeBuySell: typeBuySell,
      defaultCurrency,
      dispatch,
    };

    return (
      <List
        component={CartProductListItem}
        items={data}
        componentData = {componentData}
      />
    );
  } else {
    const loadingPlaceholder = [
      <div className="item-empty">
        <PlaceholderLoading />
      </div>,
    ];
    return loadingPlaceholder;
  }
  
}

CartProductList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  data: PropTypes.any,
  defaultCurrency: PropTypes.string,
  goldPrice: PropTypes.object,
  silverPrice: PropTypes.object,
  dispatch: PropTypes.func,
  platinumPrice: PropTypes.object,
  palladiumPrice: PropTypes.object,
};

export default CartProductList;
