/*
 * Cart Header Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  totalToPay: {
    id: `${scope}.totalToPay`,
    defaultMessage: 'Total to Pay',
  },
  totalToRecieve: {
    id: `${scope}.totalToRecieve`,
    defaultMessage: 'Total to Recieve',
  },
  
  
});
