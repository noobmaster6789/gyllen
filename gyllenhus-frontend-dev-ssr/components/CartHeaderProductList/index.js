import React from 'react';
import PropTypes from 'prop-types';
import Img from '../../components/Img';
import List from '../List';
import CartHeaderProductListItem from '../CartHeaderProductListItem';
import { formatNumberDecimalPurity } from '../../utils/formatter';
import messages from './messages';
import { FormattedMessage } from 'react-intl';

const REACT_APP_API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT
const productEmpty = `${REACT_APP_API_ENDPOINT}/static/images/product-empty-transparent.png`;

/**
 * @return {null}
 */
function CartHeaderProductList({
  data,
  typeBuySell,
  dispatch,
  title,
  totalToPay
}) {

  if (data !== false && data && data.length > 0 && data !== null) {
    const componentData = {
      typeBuySell: typeBuySell,
      dispatch,
    };

    return (
      <React.Fragment>
        <h2>{title}</h2>
        <List
          component={CartHeaderProductListItem}
          items={data}
          componentData={componentData}
        />
        <div className="row">
          <div className="cart-total">
            <div className="cell">{title === "Buying"? <FormattedMessage {...messages.totalToPay} /> : 
            <FormattedMessage {...messages.totalToRecieve} /> }</div>
            <div className="cell"><strong>{formatNumberDecimalPurity(totalToPay)} kr</strong></div>
          </div>
        </div>
      </React.Fragment>
    );
  } else {
    return (<div> </div>);
  }

}

CartHeaderProductList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  data: PropTypes.any,
  defaultCurrency: PropTypes.string,
  goldPrice: PropTypes.object,
  silverPrice: PropTypes.object,
  dispatch: PropTypes.func,
  platinumPrice: PropTypes.object,
  palladiumPrice: PropTypes.object,
};

export default CartHeaderProductList;
