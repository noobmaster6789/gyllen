/**
 *
 * BannerList
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { makeSelectBillingName, makeSelectFullName, makeSelectBillingCompany, makeSelectBillingAddress, makeSelectBillingOrganizationNumber, makeSelectBillingPostalCode, makeSelectBillingCity, makeSelectBillingEmail, makeSelectBillingPhone, makeSelectIsPrivateActive } from '../../containers/CartPage/selectors';
import { changeBillingName, changeFullName, changeBillingCompany, changeBillingAddress, changeBillingOrganizationNumber, changeBillingPostalCode, changeBillingCity, changeBillingEmail, changeBillingPhone } from '../../containers/CartPage/actions';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import messages from './messages';


function BillingAddress({
  billingName,
  onChangeBillingName,
  fullName,
  onChangeFullName,
  billingCompany,
  onChangeBillingCompany,
  billingAddress,
  onChangeBillingAddress,
  billingOrganizationNumber,
  onChangeBillingOrganizationNumber,
  billingPostalCode,
  onChangeBillingPostalCode,
  billingCity,
  onChangeBillingCity,
  billingEmail,
  onChangeBillingEmail,
  billingPhone,
  onChangeBillingPhone,
  isPrivateActive,
  isDifferentAddress
}) {
  
  return (
    <React.Fragment>
      {isPrivateActive ? (
        <div className={`form-group ${isDifferentAddress? "" : "half"}`}>
          <input className={`input-text ${billingName ? 'has-value' : ''}`}  
            type="text" name="" value={billingName} onChange={onChangeBillingName} />
          <div className="placeholder"> Name</div>
        </div>) :
        <div className={`form-group ${isDifferentAddress? "" : "half"}`}>
          <input className={`input-text ${billingCompany ? 'has-value' : ''}`}  
            type="text" name="" value={billingCompany} onChange={onChangeBillingCompany} />
          <div className="placeholder">
            <FormattedMessage {...messages.companyName} />
          </div>
        </div>}
      {isPrivateActive ? "" : (
        <div className={`form-group ${isDifferentAddress? "" : "half"}`}>
          <input className={`input-text ${fullName ? 'has-value' : ''}`}  
            type="text" name="" value={fullName} onChange={onChangeFullName} />
          <div className="placeholder">Full Name</div>
        </div>)}
      <div className={`form-group ${isDifferentAddress? "" : "half"}`}>
        <input className={`input-text ${billingAddress ? 'has-value' : ''}`}  
          type="text" name="" value={billingAddress} onChange={onChangeBillingAddress} />
        <div className="placeholder">Address</div>
      </div>
      <div className={`form-group ${isDifferentAddress? "" : "half"}`}>
        <input className={`input-text ${billingOrganizationNumber ? 'has-value' : ''}`}
          type="text" name="" value={billingOrganizationNumber} onChange={onChangeBillingOrganizationNumber} />
        <div className="placeholder">
          {isPrivateActive ? <FormattedMessage {...messages.personalNumber} /> : <FormattedMessage {...messages.organisationNumber} />}
        </div>
      </div>
      <div className={`form-group ${isDifferentAddress? "" : "half"}`}>
        <input className={`input-text ${billingPostalCode ? 'has-value' : ''}`}  
          type="text" name="" value={billingPostalCode} onChange={onChangeBillingPostalCode} />
        <div className="placeholder"><FormattedMessage {...messages.postalCode} /></div>
      </div>
      <div className={`form-group ${isDifferentAddress? "" : "half"}`}>
        <input className={`input-text ${billingCity ? 'has-value' : ''}`} 
          type="text" name="" value={billingCity} onChange={onChangeBillingCity} />
        <div className="placeholder"><FormattedMessage {...messages.city} /></div>
      </div>
      <div className={`form-group ${isDifferentAddress? "" : "half"}`}>
        <input className={`input-text ${billingEmail ? 'has-value' : ''}`} 
          type="email" name="" value={billingEmail} onChange={onChangeBillingEmail} />
        <div className="placeholder"><FormattedMessage {...messages.email} /></div>
      </div>
      <div className={`form-group ${isDifferentAddress? "" : "half"}`}>
        <input className={`input-text ${billingPhone ? 'has-value' : ''}`}  
          type="text" name="" value={billingPhone} onChange={onChangeBillingPhone} />
        <div className="placeholder"><FormattedMessage {...messages.phoneNumber} /></div>
      </div>
      {isPrivateActive ? "" : <div className="form-group half" />}
    </React.Fragment>
  );

  return "";
}


BillingAddress.propTypes = {

}

const mapStateToProps = createStructuredSelector({
  billingName: makeSelectBillingName(),
  fullName: makeSelectFullName(),
  billingCompany: makeSelectBillingCompany(),
  billingAddress: makeSelectBillingAddress(),
  billingOrganizationNumber: makeSelectBillingOrganizationNumber(),
  billingPostalCode: makeSelectBillingPostalCode(),
  billingCity: makeSelectBillingCity(),
  billingEmail: makeSelectBillingEmail(),
  billingPhone: makeSelectBillingPhone(),
  isPrivateActive: makeSelectIsPrivateActive(),

});

function mapDispatchToProps(dispatch) {
  return {
    onChangeBillingName: data => dispatch(changeBillingName(data.target.value)),
    onChangeFullName: data => dispatch(changeFullName(data.target.value)),
    onChangeBillingCompany: data => dispatch(changeBillingCompany(data.target.value)),
    onChangeBillingAddress: data => dispatch(changeBillingAddress(data.target.value)),
    onChangeBillingOrganizationNumber: data => dispatch(changeBillingOrganizationNumber(data.target.value)),
    onChangeBillingPostalCode: data => dispatch(changeBillingPostalCode(data.target.value)),
    onChangeBillingCity: data => dispatch(changeBillingCity(data.target.value)),
    onChangeBillingEmail: data => dispatch(changeBillingEmail(data.target.value)),
    onChangeBillingPhone: data => dispatch(changeBillingPhone(data.target.value)),

  }
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect)(BillingAddress);
