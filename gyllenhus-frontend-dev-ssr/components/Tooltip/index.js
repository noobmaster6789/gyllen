/**
 *
 * Transport
 *
 */

import React from 'react';
import messages from './messages';
import { FormattedMessage } from 'react-intl';

function BuySellTooltip({isSellActive}) {
	if (isSellActive) {
		return (
			<div className="tooltip">
				<button className="button-info tooltip-toggler">
					<svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26">
						<g data-name="Group 28039">
							<g data-name="Group 28038">
								<path data-name="Rectangle 1772" transform="translate(12 17)" fill="#044cad" d="M0 0H1.1V2.2H0z" />
								<g data-name="Group 28037">
									<path data-name="Rectangle 1773" fill="none" d="M0 0H26V26H0z" />
									<path data-name="Path 28541" d="M13,4a9,9,0,1,1-9,9,9,9,0,0,1,9-9m0-1A10,10,0,1,0,23,13,10,10,0,0,0,13,3Z" fill="#044cad" />
									<path data-name="Path 28542" d="M12.333,15.686H13.46v-.074a3.148,3.148,0,0,1,1.32-2.555c1.363-1.213,1.8-2.044,1.8-3.42a3.163,3.163,0,0,0-3.456-3.212,3.481,3.481,0,0,0-2.942,1.316,3.959,3.959,0,0,0-.755,1.971h1.1A2.468,2.468,0,0,1,13.1,7.364a2.2,2.2,0,0,1,2.33,2.273,3.325,3.325,0,0,1-1.377,2.724c-1.43,1.269-1.723,2.172-1.723,3.137Z" fill="#044cad" />
								</g>
							</g>
						</g>
					</svg>
				</button>
				<div className="tooltip-message center-align">
					<h3><FormattedMessage {...messages.sellOrders} /> </h3>
					<p><FormattedMessage {...messages.howToSellOrder1} /></p>
					<p><FormattedMessage {...messages.howToSellOrder2} /></p>
					<p><FormattedMessage {...messages.howToSellOrder3} /></p>
				</div>
			</div>
		);
	} else {
		return "";
	}
}


export default BuySellTooltip;
