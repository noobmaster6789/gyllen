/*
 * Tooltip Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  sellOrders: {
    id: `${scope}.sellOrders`,
    defaultMessage: 'Sell Orders',
  },
  howToSellOrder1: {
    id: `${scope}.howToSellOrder1`,
    defaultMessage: '1. Suspendisse luctus, massa ac tincidunt fermentum, dolor lectus maximus erat, id gravida justo nisl et est. Maecenas tristique, est vel porta congue, libero dolor ornare diam, ac sodales diam nibh eu nunc.',
  },
  howToSellOrder2: {
    id: `${scope}.howToSellOrder2`,
    defaultMessage: '2. Fusce tempus sagittis accumsan. Etiam fermentum, urna id congue placerat, orci odio laoreet turpis, sit amet rutrum nunc erat sit amet neque. Curabitur scelerisque scelerisque est, ac pulvinar.',
  },
  howToSellOrder3: {
    id: `${scope}.howToSellOrder3`,
    defaultMessage: '3. Nibh dignissim ac. Morbi ultrices lorem a sapien fringilla facilisis. Aenean metus velit, tempus nec facilisis non, facilisis et dolor. Nullam risus dolor, fermentum vehicula molestie feugiat, porta ac lectus. Praesent at hendrerit felis, eu cursus urna.',
  },
});
