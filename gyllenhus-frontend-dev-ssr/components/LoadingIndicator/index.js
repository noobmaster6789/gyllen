import React from 'react';

import Wrapper from './Wrapper';

const LoadingIndicator = () => <Wrapper />;

export default LoadingIndicator;
