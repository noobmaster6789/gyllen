/**
 *
 * ProductName
 *
 */

import React, { useState } from 'react';
import ProductSpecificationTooltip from '../ProductSpecificationTooltip';
import ReactHtmlParser from 'react-html-parser';
import ProductTechnicals from '../ProductTechnicals';


function ProductSpecifications(props) {
  const {
    productSpecifications,
    productMadeIn,
    typeConditions,
    productTechnicals,
    productPurity,
    typeBrands,
    producerId
  } = props;

  const madeIn = (productMadeIn && productMadeIn.length > 0) ? productMadeIn[0] : false;
  const productMadeInData = madeIn ? {
    image: madeIn.thumbnail.url,
    title: "Made in " + madeIn.name,
    description: madeIn.description
  } : false;

  const productPurityData = productPurity ? {
    image: productPurity.purity_image.url,
    title: productPurity.purity_title,
    description: productPurity.description
  } : false;

  const producer = typeBrands ? typeBrands.filter(function (brand) {
    return brand.ID == producerId;
  }) : false;

  const producedData = (producer && producer.length > 0) ? {
    image: producer[0].thumbnail.url,
    title: "Produced by " + producer[0].name,
    description: producer[0].description
  } : false;

  const productTechnicalData = productTechnicals ? {
    dimensions: productTechnicals.dimensions,
    purity: productTechnicals.purity,
    totalWeight: productTechnicals.total_weight.valueGram,
    fineWeight: productTechnicals.fine_weight,
    unit: productTechnicals.total_weight.unit
  } : false;

  return (
    <React.Fragment>
      <div className="content-two-col">
        <div className="col-left">
          {ReactHtmlParser(productSpecifications)}
        </div>
        <div className="col-right">
          <div className="list-symbol">
            {productMadeInData ?
              <ProductSpecificationTooltip {...productMadeInData} /> : ""}
            {producedData ?
              <ProductSpecificationTooltip {...producedData} /> : ""}
            {
              (typeConditions.length > 0) ? (
                typeConditions.map((item, i) => {
                  if (item.ID && item.thumbnail)
                    return <ProductSpecificationTooltip
                      image={item.thumbnail.url} title={item.name} description={item.description} />
                })) : ""
            }
            {productPurityData ?
              <ProductSpecificationTooltip {...productPurityData} /> : ""}
          </div>
          {productTechnicalData ? <ProductTechnicals {...productTechnicalData} /> : ""}
        </div>
      </div>


    </React.Fragment>
  );
}

export default ProductSpecifications;
