/**
 *
 * ProductName
 *
 */

import React, { useState } from 'react';

function ProductImageSlider(props) {
  const {
    productImages,
    productThumbnails

  } = props;
  if ((!productImages || productImages === undefined || productImages.length == 0)
    && (!productThumbnails || productThumbnails === undefined || productThumbnails.length == 0)) {
    return (
      <React.Fragment>
      </React.Fragment>);
  }
  const [imageSelected, setImageSelected] = useState((productImages ? productImages.url : (productThumbnails[0].sizes.medium_large)));

  const onChangeImage = (image) => {
    setImageSelected(image);
  }
  return (
    <React.Fragment>
      <div className="slider-group">
        <div className="slider-main">
          <div className="slide-item">
            <img src={imageSelected} />
          </div>
        </div>
        <div className="slider-nav">
          {
            productImages ? (<div className="slide-item" key="main" onClick={() => onChangeImage(productImages.url)}>
              <img src={productImages.url} />
            </div>) : ""
          }
          {productThumbnails ?
            productThumbnails.map((image, i) => {
              if (image.sizes.thumbnail && image.sizes.medium_large)
                return <div className="slide-item" key={i} onClick={() => onChangeImage(image.sizes.medium_large)}>
                  <img src={image.sizes.thumbnail} />
                </div>
            }) : ""
          }
        </div>
      </div>
    </React.Fragment>
  );
}

export default ProductImageSlider;
