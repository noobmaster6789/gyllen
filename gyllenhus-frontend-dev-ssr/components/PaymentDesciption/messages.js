/*
 * Payment Description Messages
 *
 */
import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  creditCardPaymentDescription: {
    id: `${scope}.creditCardPaymentDescription`,
    defaultMessage: '',
  },
  bankPaymentDescription: {
    id: `${scope}.bankPaymentDescription`,
    defaultMessage: '',
  },
  swishPaymentDescription: {
    id: `${scope}.swishPaymentDescription`,
    defaultMessage: '',
  },
  invoicePaymentDescription: {
    id: `${scope}.invoicePaymentDescription`,
    defaultMessage: '',
  },

  
});
