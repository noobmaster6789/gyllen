/**
 *
 * PaymentInvoice
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

function PaymentDesciption({ paymentMethod }) {
	const type = () => {

		switch (paymentMethod) {
			case 'credit-card':
				return "Credit Card : ";
			case 'bank-payment':
				return "Direct Bank Payment : ";
			case 'swish':
				return "Swish : ";
			case 'invoice':
				return "Invoice : ";
			default:
				return '';
		}
	}
	const description = () => {
		switch (paymentMethod) {
			case 'credit-card':
				return <FormattedMessage {...messages.creditCardPaymentDescription} />;
			case 'bank-payment':
				return <FormattedMessage {...messages.bankPaymentDescription} />;
			case 'swish':
				return <FormattedMessage {...messages.swishPaymentDescription} />;
			case 'invoice':
				return <FormattedMessage {...messages.invoicePaymentDescription} />;
			default:
				return '';
		}
	}
	return (
		<div className="cell-payment-term">
			<strong> {type()} </strong> {description()}
		</div>
	);

}


export default PaymentDesciption;
