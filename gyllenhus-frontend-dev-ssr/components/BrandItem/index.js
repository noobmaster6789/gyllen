/**
 *
 * BrandItem
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

// import styled from 'styled-components';

function BrandItem({ item }) {
  const { name } = item;
  return <div className="name">{name}</div>;
}

BrandItem.propTypes = {
  item: PropTypes.object,
};

export default BrandItem;
