/**
 *
 * MenuListItem
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';

function MenuListItem({ item }) {
  const { url, title, target } = item;
  return (
    <li>
      <div target={target}>
        <Link href="#"> 
          <a>{title}</a>
        </Link>
      </div>
    </li>
  );
}

MenuListItem.propTypes = {
  item: PropTypes.any,
};

export default MenuListItem;
