/**
 *
 * Countdown
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

function Countdown({ time, searchActive }) {
  const date = new Date(time * 1000);
  // Minutes part from the timestamp
  const minutes = `0${date.getMinutes()}`;
  // Seconds part from the timestamp
  const seconds = `0${date.getSeconds()}`;

  // Will display time in 30:23 format
  const formattedTime = `${minutes.substr(-2)}:${seconds.substr(-2)}`;
  return (
    <div className={`countdown ${searchActive? "hide": ""}`}>
      <div className="inner">
        <strong>{formattedTime}s </strong>
        <FormattedMessage {...messages.priceCountDown} />
      </div>
    </div>
  );
}

Countdown.propTypes = {
  time: PropTypes.any,
};

export default Countdown;
