/*
 * Countdown Messages
 *
 * This contains all the text for the Countdown component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  priceCountDown: {
    id: `${scope}.priceCountDown`,
    defaultMessage: 'until product price update',
  },
});
