/**
 *
 * SpecialOfferList
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import Img from '../../components/Img';
import List from '../List';
import { SpecialOfferItem } from '../../containers/SpecialOfferItem';
import PlaceholderLoading from '../PlaceholderLoading/specialOffers';


const REACT_APP_API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT
const productEmpty = `${REACT_APP_API_ENDPOINT}/static/images/product-empty-transparent.png`;

/**
 * @return {null}
 */
function SpecialOfferList({
  loading,
  error,
  data,
  defaultCurrency,
  goldPrice,
  silverPrice,
  platinumPrice,
  palladiumPrice,
}) {
  if (loading) {
    // return <List component={LoadingIndicator} />;
  }
  if (error !== false) {
    // const ErrorComponent = () => (
    //   <ListItem item="Something went wrong. Please try again!" />
    // );
    // return <List component={ErrorComponent} />;
  }
  if (data !== false && data && data.length > 0) {
    const componentData = {
      currency: defaultCurrency,
      goldPrice,
      silverPrice,
      platinumPrice,
      palladiumPrice,
    };
    return (
      <List
        component={SpecialOfferItem}
        items={data}
        componentData={componentData}
      />
    );
  }
  if (data !== false && data && data.length === 0) {
    return (
      <div className="item-empty">
        <div className="image">
          <Img src={productEmpty} alt="product-empty" />
        </div>
      </div>
    );
  }
  const loadingPlaceholder = [
    <div className="item">
      <PlaceholderLoading />
    </div>,
    <div className="item">
      <PlaceholderLoading />
    </div>,
  ];
  return loadingPlaceholder;
}

SpecialOfferList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  data: PropTypes.any,
  defaultCurrency: PropTypes.string,
  goldPrice: PropTypes.object,
  silverPrice: PropTypes.object,
  platinumPrice: PropTypes.object,
  palladiumPrice: PropTypes.object,
};

export default SpecialOfferList;
