/**
 *
 * Promote
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import PromoteItem from './Item';

function Promote({ items, activeKey, onChangeActiveBanner }) {

  if (typeof items === undefined) {
    return "";
  }

  return (
    <div className="promote wrapper">
      {items.banners.map((item, index) => (
        index !== 0 ?
        <PromoteItem
          key={`promote-item-${index}`}
          {...item}
          index={index}
          activeKey={activeKey}
          onChangeActiveBanner={onChangeActiveBanner}
        /> : ""
      ))}
    </div>
  );
}

Promote.propTypes = {
  items: PropTypes.any,
  activeKey: PropTypes.number,
  onChangeActiveBanner: PropTypes.func,
};

export default Promote;
