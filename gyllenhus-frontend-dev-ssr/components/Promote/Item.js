import React from 'react';
import PropTypes from 'prop-types';

function PromoteItem({ icon, title, index, activeKey, onChangeActiveBanner }) {
  return (
    // eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions
    <div
      onClick={() => onChangeActiveBanner(index)}
      className={activeKey === index ? 'active' : ''}
    >
      {/* eslint-disable-next-line react/no-danger */}
      <svg dangerouslySetInnerHTML={{ __html: icon }} />
      <div>{title}</div>
    </div>
  );
}

PromoteItem.propTypes = {
  icon: PropTypes.any,
  title: PropTypes.string,
  index: PropTypes.number,
  activeKey: PropTypes.number,
  onChangeActiveBanner: PropTypes.func,
};

export default PromoteItem;
