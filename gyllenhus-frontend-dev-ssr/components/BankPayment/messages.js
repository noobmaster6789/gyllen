/*
 * Bank Payment Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  bankName: {
    id: `${scope}.bankName`,
    defaultMessage: 'Bank Name',
  },
  specifyBankInformation: {
    id: `${scope}.specifyBankInformation`,
    defaultMessage: 'Specify your bank information for receiving payment. Payment is sent out within x days of Gyllenhus receiving the products.',
  },
  specifyBankInformationInvoice: {
    id: `${scope}.specifyBankInformationInvoice`,
    defaultMessage: 'Specify your bank information for the invoice. Product is sent out within x days of Gyllenhus receiving the payment.',
  },
  bankDefault: {
    id: `${scope}.bankDefault`,
    defaultMessage: 'Please select your bank',
  },
  clearingNumber: {
    id: `${scope}.clearingNumber`,
    defaultMessage: 'Clearing Number',
  },
  accountNumber: {
    id: `${scope}.accountNumber`,
    defaultMessage: 'Account Number',
  },
  
});
