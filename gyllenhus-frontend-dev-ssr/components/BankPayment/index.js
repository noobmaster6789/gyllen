/**
 *
 * BannerList
 *
 */

import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { makeSelectClearingNumber, makeSelectAccountNumber, makeSelectBank } from '../../containers/CartPage/selectors';
import { changeClearingNumber, changeAccountNumber, changeBank } from '../../containers/CartPage/actions';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import useOutsideClick from '../../utils/useOutsideClick';


function BankPayment({
  paymentMethod,
  banks,
  bank,
  isBuyTabActive,
  clearingNumber,
  accountNumber,
  onChangeBank,
  onChangeClearingNumber,
  onChangeAccountNumber,
}) {
  const ref = useRef();
  const [dropdown, setDropdown] = useState(false);

  useOutsideClick(ref, () => {
    if (dropdown) {
      setDropdown(false);
    }
  });

  if (paymentMethod === "bank-payment" && !isBuyTabActive)
    return (
      <React.Fragment>
        <div className="table-head row">
          <div className="cell cell-product"><h2>Payment detail</h2></div>
        </div>
        <div className="cart-item row bank-detail">
          <div className="cell full-width">
            {isBuyTabActive ?
              <FormattedMessage {...messages.specifyBankInformationInvoice} /> :
              <FormattedMessage {...messages.specifyBankInformation} />
            }
          </div>
          <div className="cell cell-bank-name">
            <div className="form-group">
              <label className="label"><FormattedMessage {...messages.bankName} /> </label>
              <div className="select" ref={ref}>
                <div className="toggler stroke" onClick={() => setDropdown(!dropdown)}>
                  {bank ? bank.name : <FormattedMessage {...messages.bankDefault} />}
                  <svg width="7" height="4" viewBox="0 0 7 4"
                    version="1" xmlns="http://www.w3.org/2000/svg"><path fill="#19396E" fillRule="nonzero" d="M3.5 2.636L0.483 0.036 0.483 1.351 3.5 3.981 6.5 1.351 6.5 0.036z"></path></svg></div>
                <div className={`dropdown ${dropdown ? 'active' : ''}`}>
                  {
                    banks.map((b, i) => {
                      return <div className="item" key={b.code} onClick={() => { onChangeBank(b); setDropdown(false); }}>{b.name}</div>
                    })
                  }
                </div>
              </div>
            </div>
          </div>
          <div className="cell cell-clearing-number">
            <div className="form-group">
              <input className={`input-text ${clearingNumber ? 'has-value' : ''}`}
                type="text" name="" value={clearingNumber} onChange={onChangeClearingNumber} />
              <div className="placeholder"><FormattedMessage {...messages.clearingNumber} /></div>
            </div>
          </div>
          <div className="cell cell-account-number">
            <div className="form-group">
              <input className={`input-text ${accountNumber ? 'has-value' : ''}`}
                type="text" name="" value={accountNumber} onChange={onChangeAccountNumber} />
              <div className="placeholder"><FormattedMessage {...messages.accountNumber} /></div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );

  return "";
}

BankPayment.defaultProps = {
  isShowDropdown: false
};

function showHide(value) {
  return !value;
}

BankPayment.propTypes = {
  banks: PropTypes.array,
  bank: PropTypes.object,
  clearingNumber: PropTypes.string,
  accountNumber: PropTypes.string
}

const mapStateToProps = createStructuredSelector({
  bank: makeSelectBank(),
  clearingNumber: makeSelectClearingNumber(),
  accountNumber: makeSelectAccountNumber()
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeBank: value => dispatch(changeBank(value)),
    onChangeClearingNumber: data => dispatch(changeClearingNumber(data.target.value)),
    onChangeAccountNumber: data => dispatch(changeAccountNumber(data.target.value)),

  }
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect)(BankPayment);
