import React from 'react';
import PropTypes from 'prop-types';

function FooterMenuItem({ item: { title, url, target } }) {
  return (
    <p>
      <a target={target} href={url}>
        {title}
      </a>
    </p>
  );
}

FooterMenuItem.propTypes = {
  item: PropTypes.shape({
    title: PropTypes.string,
    url: PropTypes.string,
    target: PropTypes.string,
  }),
};

export default FooterMenuItem;
