/**
 *
 * FooterMenu
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import FooterMenuItem from './Item';
import List from '../List';

/**
 * @return {null}
 */
function FooterMenu({ items }) {
  if (typeof items !== 'undefined' && items.length > 0) {
    return <List component={FooterMenuItem} items={items} />;
  }
  return null;
}

FooterMenu.propTypes = {
  items: PropTypes.any,
};

export default FooterMenu;
