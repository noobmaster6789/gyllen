/**
 *
 * ProductListItem
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { createStructuredSelector } from 'reselect';
import { makeSelectQuantity } from '../../containers/CartPage/selectors';
import { changeQuantity, changeTotal, remove } from '../../containers/CartPage/actions';
import { formatNumberDecimalPurity } from '../../utils/formatter';
import CartProductName from '../CartProductName';

const MAX_QUANTITY = 999;
export class CartHeaderProductListItem extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangeQuantity = this.handleChangeQuantity.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
  }


  handleChangeQuantity(value) {
    let quantity = this.props.item.quantity;
    quantity = Number(quantity);
    if (quantity + value <= 0) {
      quantity = 1;
    } else
      if (quantity + value > 999) {
        quantity = MAX_QUANTITY;
      } else quantity = quantity + value;
    this.props.onChangeQuantity(this.props.typeBuySell, this.props.item.id, quantity);
    this.props.onChangeTotal(this.props.typeBuySell);
  }

  handleRemove() {
    this.props.onRemove(this.props.typeBuySell, this.props.item.id);
    this.props.onChangeTotal(this.props.typeBuySell);
  }

  render() {

    const {
      item: {
        product,
        productWeight,
      }
    } = this.props.item.item;
    const {
      quantity,
      price,
      currency,
    } = this.props.item;

    return (
      <React.Fragment>
        <div className="row">
          <div className="cart-item">
            <div className="cell cell-product">
              <h3 className="item-title">
                <a href="#">
                  <CartProductName {...productWeight} title={product.title} />
                </a>
              </h3>
            </div>
            <div className="cell cell-remove" onClick={() => this.handleRemove()}><button className="button-blank"><svg xmlns="http://www.w3.org/2000/svg" width="14.7" height="14.7" viewBox="0 0 14.7 14.7"><g><g fill="none" stroke="#19396e"><path data-name="Line 172" d="M0 0L14 14"></path><path d="M0 14L14 0"></path></g></g></svg></button></div>
            <div className="cell cell-qty">
              <div className="qty-box">
                <button className="button-blank" onClick={() => this.handleChangeQuantity(-1)}><svg xmlns="http://www.w3.org/2000/svg" width="8" height="12.2" viewBox="0 0 8 12.2"><path d="M5.27,6.086,0,12.2H2.667L8,6.086,2.667,0H0Z" fill="#3064ba" transform="rotate(180 4 6)"></path></svg></button>
                <input type="text" name="" readOnly className="input-blank" value={quantity} />
                <button className="button-blank" onClick={() => this.handleChangeQuantity(1)}><svg xmlns="http://www.w3.org/2000/svg" width="8" height="12.2" viewBox="0 0 8 12.2"><path d="M5.27,6.115,0,0H2.667L8,6.115,2.667,12.2H0Z" fill="#3064ba"></path></svg></button>
              </div>
            </div>
            <div className="cell cell-sub">{formatNumberDecimalPurity(quantity * price)} kr</div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

CartHeaderProductListItem.propTypes = {
  item: PropTypes.shape({
    product: PropTypes.object,
    images: PropTypes.any,
    productPurity: PropTypes.number,
    productVat: PropTypes.number,
    qty: PropTypes.number,
    productWeight: PropTypes.object,
    typeProduct: PropTypes.string,
    typeBrands: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.array,
      PropTypes.object,
    ]),
  }),
  currency: PropTypes.string,
  goldPrice: PropTypes.object,
  silverPrice: PropTypes.object,
  platinumPrice: PropTypes.object,
  palladiumPrice: PropTypes.object,
  dispatch: PropTypes.func,
  onChangeQuantity: PropTypes.func,
  quantity: PropTypes.number,
  onChangeTotal: PropTypes.func,
  onRemove: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  quantity: makeSelectQuantity()
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeQuantity: (type, id, qtt) => dispatch(changeQuantity(type, id, qtt)),
    onChangeTotal: (type) => dispatch(changeTotal(type)),
    onRemove: (type, id) => dispatch(remove(type, id))

  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(CartHeaderProductListItem);
