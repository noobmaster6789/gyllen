/**
 *
 * BannerList
 *
 */

import React, {useState} from 'react';
import PropTypes from 'prop-types';
import ListItem from '../ListItem';
import List from '../List';
import Banner from '../Banner';
import Promote from '../Promote';

function BannerList({
  loading,
  error,
  data,
  categoryProducts,
  collapse
}) {

  const [activeKey, setActiveKey] = useState(0);

  const onChangeActiveBanner = (key) => {
    setActiveKey(key)
  }

  if (loading) {
    // return <LoadingIndicator />;
  }
  
  if (data !== false) {
    return (
      <React.Fragment>
        <Banner
          items={data}
          categoryProducts={categoryProducts}
          activeKey={activeKey}
          onChangeActiveBanner={onChangeActiveBanner}
          collapse = {collapse}
        />
        <Promote
          items={data}
          activeKey={activeKey}
          onChangeActiveBanner={onChangeActiveBanner}
        />
      </React.Fragment>
    );
  }
  return (
    <React.Fragment>
      <header className="banner wrapper" />
      <div className="promote wrapper" />
    </React.Fragment>
  );
}

BannerList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  data: PropTypes.any,
  categoryProducts: PropTypes.any,
  activeKey: PropTypes.number,
  onChangeActiveBanner: PropTypes.func,
};

export default BannerList;
