/**
 *
 * Copyright
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

function Copyright() {
  return (
    <div className="copyright">
      <FormattedMessage
        {...messages.copyright}
        values={{ year: new Date().getFullYear() }}
      />
    </div>
  );
}

Copyright.propTypes = {};

export default Copyright;
