/*
 * Copyright Messages
 *
 * This contains all the text for the Copyright component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  copyright: {
    id: `${scope}.copyright`,
    defaultMessage: 'Copyright {year} Gyllenhus AB – All Rights Reserved.',
  },
});
