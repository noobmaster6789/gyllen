/*
 * Delivery Messages
 *
 * This contains all the text for the MyAccountWidget container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  delivery: {
    id: `${scope}.delivery`,
    defaultMessage: 'Delivery',
  },
  privateClient: {
    id: `${scope}.privateClient`,
    defaultMessage: 'Private Client',
  },
  businessClient: {
    id: `${scope}.businessClient`,
    defaultMessage: 'Business Client',
  },
  name: {
    id: `${scope}.name`,
    defaultMessage: 'Name',
  },
  companyName: {
    id: `${scope}.companyName`,
    defaultMessage: 'Company Name',
  },
  city: {
    id: `${scope}.city`,
    defaultMessage: 'City',
  },
  postalCode: {
    id: `${scope}.postalCode`,
    defaultMessage: 'Postal Code',
  },
  phoneNumber: {
    id: `${scope}.phoneNumber`,
    defaultMessage: 'Phone Number',
  },
  organisationNumber: {
    id: `${scope}.organisationNumber`,
    defaultMessage: 'Organisation Number',
  },
  personalNumber: {
    id: `${scope}.personalNumber`,
    defaultMessage: 'Personal Number',
  },
  differentBillingDelivery: {
    id: `${scope}.differentBillingDelivery`,
    defaultMessage: 'Use different billing/delivery address',
  },
  differentThanBilling: {
    id: `${scope}.differentThanBilling`,
    defaultMessage: 'Separate Delivery Address',
  },
  billing: {
    id: `${scope}.billing`,
    defaultMessage: 'Billing',
  },
  
});
