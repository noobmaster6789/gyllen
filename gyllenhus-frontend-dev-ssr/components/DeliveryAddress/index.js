/**
 *
 * Delivery Address
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { makeSelectShippingAddress, makeSelectShippingPostalCode, makeSelectShippingCity, makeSelectIsDifferentAddress } from '../../containers/CartPage/selectors';
import { changeShippingAddress, changeShippingPostalCode, changeShippingCity, showHideDifferentAddress, usingShippingAddress } from '../../containers/CartPage/actions';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import messages from './messages';


function DeliveryAddress({
  shippingAddress,
  onChangeShippingAddress,
  shippingPostalCode,
  onChangeShippingPostalCode,
  shippingCity,
  onChangeShippingCity,
  onShowHideDifferentAddress,
  onUseShippingAddress,
  isDifferentAddress
}) {

  const separateDeliveryAddress = (val) => {
    onShowHideDifferentAddress(val);
    onUseShippingAddress(val);
  }

  const submitHandle = (evt) => {
    evt.preventDefault();
    if (isDifferentAddress) {
      onShowHideDifferentAddress(false);
    }
    onUseShippingAddress(true);
    };

  return (
    <React.Fragment>
      <div className="head">
        <div className="form-group-option">
          <label className="switcher" htmlFor="different-billing" onClick={() => separateDeliveryAddress(false)}>
            <input checked="" readOnly type="checkbox" name="different-billing" id="different-billing" />
            <span><i></i></span>
          </label>
          <div className="label"><FormattedMessage {...messages.differentThanBilling} /></div>
        </div>
      </div>
      <form className="body" onSubmit={submitHandle}>
        <div className="form-group">
          <input className={`input-text ${shippingAddress ? 'has-value' : ''}`} 
            type="text" name="" value={shippingAddress} onChange={onChangeShippingAddress} />
          <div className="placeholder">Address</div>
        </div>
        <div className="form-group">
          <input className={`input-text ${shippingPostalCode ? 'has-value' : ''}`} 
            type="text" name="" value={shippingPostalCode} onChange={onChangeShippingPostalCode} />
          <div className="placeholder"><FormattedMessage {...messages.postalCode} /></div>
        </div>
        <div className="form-group">
          <input className={`input-text ${shippingCity ? 'has-value' : ''}`} 
            type="text" name="" value={shippingCity} onChange={onChangeShippingCity} />
          <div className="placeholder"><FormattedMessage {...messages.city} /></div>
        </div>
        <div className="button-group">
          <button className="button" type="submit">Save</button>
        </div>
      </form>
    </React.Fragment>
  );

}

DeliveryAddress.defaultProps = {
  isShowDropdown: false
};

DeliveryAddress.propTypes = {
  
}

const mapStateToProps = createStructuredSelector({
  shippingAddress: makeSelectShippingAddress(),
  shippingPostalCode: makeSelectShippingPostalCode(),
  shippingCity: makeSelectShippingCity(),
  isDifferentAddress: makeSelectIsDifferentAddress(),

});

function mapDispatchToProps(dispatch) {
  return {
    onChangeShippingAddress: data => dispatch(changeShippingAddress(data.target.value)),
    onChangeShippingPostalCode: data => dispatch(changeShippingPostalCode(data.target.value)),
    onChangeShippingCity: data => dispatch(changeShippingCity(data.target.value)),
    onShowHideDifferentAddress: (val) => dispatch(showHideDifferentAddress(val)),
    onUseShippingAddress: (val) => dispatch(usingShippingAddress(val)),

  }
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect)(DeliveryAddress);
