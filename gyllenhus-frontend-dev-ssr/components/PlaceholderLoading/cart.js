import React from 'react';
import ContentLoader from 'react-content-loader';

function PlaceholderLoading() {
  return (
    <ContentLoader height={13} width={200}>
      <rect x="0" y="0" rx="0" ry="0" width="13" height="13" />
      <rect x="14" y="0" rx="0" ry="0" width="93" height="13" />
      <rect x="108" y="0" rx="0" ry="0" width="20" height="13" />
      <rect x="129" y="0" rx="0" ry="0" width="15" height="13" />
      <rect x="145" y="0" rx="0" ry="0" width="22" height="13" />
      <rect x="168" y="0" rx="0" ry="0" width="10" height="13" />
      <rect x="179" y="0" rx="0" ry="0" width="20" height="13" />
    </ContentLoader>
  );
}

export default PlaceholderLoading;
