import React from 'react';
import ContentLoader from 'react-content-loader';

function PlaceholderLoading() {
  return (
    <ContentLoader height={32} width={200}>
      <rect x="0" y="3" rx="0" ry="0" width="25" height="8" />
      <rect x="0" y="12" rx="0" ry="0" width="100" height="5" />
      <rect x="0" y="19" rx="0" ry="0" width="60" height="8" />
      
    </ContentLoader>
  );
}

export default PlaceholderLoading;
