import React from 'react';
import ContentLoader from 'react-content-loader';

function PlaceholderLoading() {
  return (
    <ContentLoader height={300} width={490}>
      <rect x="0" y="0" rx="0" ry="0" width="220" height="300" />
      <rect x="222" y="0" rx="0" ry="0" width="273" height="250" />
      <rect x="222" y="252" rx="0" ry="0" width="273" height="48" />
    </ContentLoader>
  );
}

export default PlaceholderLoading;
