import React from 'react';
import ContentLoader from 'react-content-loader';

function PlaceholderLoading() {
  return (
    <ContentLoader height={395} width={245}>
      <rect x="0" y="0" rx="0" ry="0" width="245" height="300" />
      <rect x="0" y="302" rx="0" ry="0" width="95" height="93" />
      <rect x="97" y="302" rx="0" ry="0" width="51" height="93" />
      <rect x="150" y="302" rx="0" ry="0" width="95" height="93" />
    </ContentLoader>
  );
}

export default PlaceholderLoading;
