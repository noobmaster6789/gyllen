/*
 * Product Technicals Messages
 *
 */
import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  technicals: {
    id: `${scope}.technicals`,
    defaultMessage: 'Technicals',
  },
  dimensions: {
    id: `${scope}.dimensions`,
    defaultMessage: 'Dimensions',
  },
  purity: {
    id: `${scope}.purity`,
    defaultMessage: 'Purity',
  },
  totalWeight: {
    id: `${scope}.totalWeight`,
    defaultMessage: 'Total Weight',
  },
  fineWeight: {
    id: `${scope}.fineWeight`,
    defaultMessage: 'Fine Weight',
  },
});
