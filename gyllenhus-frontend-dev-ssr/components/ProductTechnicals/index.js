/**
 *
 * ProductName
 *
 */

import React from 'react';
import messages from './messages';
import { FormattedMessage } from 'react-intl';


function ProductTechnicals(props) {
  const {
    dimensions,
    purity,
    totalWeight,
    fineWeight,
    unit
  } = props;
  return (
    <React.Fragment>
      <div className="technicals">
        <h2>  <FormattedMessage {...messages.technicals} /></h2>
        <dl>
          <dt> <FormattedMessage {...messages.dimensions} />:</dt>
          <dd> {dimensions}</dd>
          <dt> <FormattedMessage {...messages.purity} />:</dt>
          <dd> {purity}</dd>
          <dt><FormattedMessage {...messages.totalWeight} />:</dt>
          <dd>{totalWeight} {unit}</dd>
          <dt><FormattedMessage {...messages.fineWeight} />:</dt>
          <dd>{fineWeight} {unit}</dd>
        </dl>
      </div>
    </React.Fragment>
  );
}

export default ProductTechnicals;
