/**
 *
 * ProductName
 *
 */

import React from 'react';


function ProductPromotion({ productDiscounts }) {

  if (!productDiscounts || !productDiscounts.productStatusDiscount)
    return (<React.Fragment></React.Fragment>);

  return (
    <React.Fragment>
      <div className="promotion">
        <svg className="icon" xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26"><path data-name="Path 28512" d="M22,4v8.59l-9,9L4.41,13l9-9H22m1-1H13L3,13,13,23,23,13Z" fill="#dcb12a" /><path data-name="Path 28513" d="M13,9.44l.47,1.43.22.69h2.23l-1.22.88-.58.43.22.69L14.81,15l-1.22-.88L13,13.68l-.59.43L11.19,15l.47-1.43.22-.69-.58-.43-1.22-.88h2.23l.22-.69L13,9.44M13,6.2l-1.42,4.36H7l3.71,2.69L9.29,17.61,13,14.92l3.71,2.69-1.42-4.36L19,10.56H14.42Z" fill="#dcb12a" /><path data-name="Path 28514" d="M20.07,4.93a1,1,0,1,1-1,1A1,1,0,0,1,20.07,4.93Z" fill="#dcb12a" /></svg>
        <p>Special Offer: Expires in 2 hours and 10 min</p>
        <p>Curabitur sed aliquam nisl. Morbi nec elit scelerisque, ultricies lorem auctor, efficitur ligula. Etiam id interdum odio, molestie congue augue.</p>
      </div>

    </React.Fragment>
  );
}

export default ProductPromotion;
