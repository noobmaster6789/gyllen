/**
 *
 * ProductName
 *
 */

import React from 'react';
import ReactHtmlParser from 'react-html-parser';

function ProductSpecificationTooltip(props) {
  const {
    image,
    title,
    description
  } = props;
  return (
    <React.Fragment>
      <div className="tooltip">
        <div className="symbol tooltip-toggler">
          <div className="symbol-img"><img src={image ? image : ""} width="29" /></div>
          <div className="symbol-text"> {title} </div>
        </div>
        <div className="tooltip-message center-align">
          <h3>{title}</h3>
          {ReactHtmlParser(description)}
        </div>
      </div>
    </React.Fragment>
  );
}

ProductSpecificationTooltip.propTypes = {

};

export default ProductSpecificationTooltip;
