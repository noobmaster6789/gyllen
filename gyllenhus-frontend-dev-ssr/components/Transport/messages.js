/*
 * Transport Messages
 *
 */
import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  transportDescription: {
    id: `${scope}.transportDescription`,
    defaultMessage: 'All packages are fully insured against loss, theft and physical damage during transit.',
  }
});
