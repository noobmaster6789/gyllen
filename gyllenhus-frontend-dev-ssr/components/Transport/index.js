/**
 *
 * Transport
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

function Transport() {

	return (
		<div className="cart-item row transport">
			<div className="cell cell-product">
				<a href="#" className="item-img" style={{ backgroundImage: `url(static/images//landsfor@2x.png)` }}></a>
				<div className="select">
					<span>Länsförsärkringar</span>
				</div>
				<div className="insurance-text">
					<FormattedMessage {...messages.transportDescription} />
        </div>
			</div>
		</div>
	);

}


export default Transport;
