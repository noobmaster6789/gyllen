import React from 'react';
import PropTypes from 'prop-types';

function Currency({ name }) {
  let currency = null;
  switch (name) {
    case 'EUR':
      currency = '€';
      break;
    case 'USD':
      currency = '$';
      break;
    default:
      currency = 'kr';
      break;
  }
  return currency;
}

Currency.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Currency;
