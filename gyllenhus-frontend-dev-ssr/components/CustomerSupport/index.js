/**
 *
 * CustomerSupport
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

function CustomerSupport({ email, openDate, tfn }) {
  return (
    <React.Fragment>
      <p>{tfn}</p>
      <p>{email}</p>
      <p>{openDate}</p>
    </React.Fragment>
  );
}

CustomerSupport.propTypes = {
  email: PropTypes.string,
  openDate: PropTypes.string,
  tfn: PropTypes.string,
};

export default CustomerSupport;
