import React from 'react';
import PropTypes from 'prop-types';
import ProductPrice from '../ProductPrice';
import Symbol from './Symbol';

function Item({ price, currency, percent, isHeader = false }) {
  return (
    <React.Fragment>
      <span className="percent">{percent}</span>
      <span>
        <ProductPrice
          currency={currency}
          margin={0}
          price={price}
          weight={1}
          isHeader={isHeader}
          unit="oz"
        />{' '}
        <small>
          <Symbol name={currency} unit="oz" />
        </small>
      </span>
      <span>
        <ProductPrice
          currency={currency}
          margin={0}
          price={price}
          weight={1}
          isHeader={isHeader}
          unit="g"
        />{' '}
        <small>
          <Symbol name={currency} unit="g" />
        </small>
      </span>
    </React.Fragment>
  );
}

Item.propTypes = {
  price: PropTypes.any,
  currency: PropTypes.string,
  percent: PropTypes.string,
  isHeader: PropTypes.bool,
};

export default Item;
