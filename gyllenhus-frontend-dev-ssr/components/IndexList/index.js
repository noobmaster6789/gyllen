/**
 *
 * IndexList
 *
 */

import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import Cookie from 'js-cookie'
import _ from 'lodash';
import List from '../List';
import ListItem from '../ListItem';
import { toPercent, formatNumber, formatNumber3Decimals } from '../../utils/formatter';
import useOutsideClick from '../../utils/useOutsideClick';

const DEFAULT_CURRENCY_SEK = "SEK";
const DEFAULT_CURRENCY_USD = "USD";
const DEFAULT_CURRENCY_EUR = "EUR";

const DEFAULT_WEIGHT_KG = "kg";
const DEFAULT_WEIGHT_OZ = "oz";
const DEFAULT_WEIGHT_GRAM = "gram";

/**
 * @return {null}
 */
function IndexList({
  loading,
  error,
  goldPriceRealtime,
  silverPriceRealtime,
  openPrices,
  onChangeCurrency,
  defaultCurrency,
  defaultWeight,
  defaultCollapsed,
  onChangeCollapse
}) {

  if (error !== false) {
    const ErrorComponent = () => (
      <ListItem item="Something went wrong, please try again!" />
    );
    return <List component={ErrorComponent} />;
  }

  useEffect(() => {
    if (defaultCurrency)
      handleChangeCurrency(defaultCurrency);
  }, []);

  if (!goldPriceRealtime || !silverPriceRealtime) {
    return "";
  }
  const refCurrencyDropdown = useRef();
  const refWeightDropdown = useRef();
  const refDropdownMobile = useRef();

  useOutsideClick(refCurrencyDropdown, () => {
    if (isShowCurrencyDropdown) {
      setIsShowCurrencyDropdown(false);
    }
  });
  useOutsideClick(refWeightDropdown, () => {
    if (isShowWeightDropdown) {
      setIsShowWeightDropdown(false);
    }
  });
  useOutsideClick(refDropdownMobile, () => {
    if (isShowDropdownMobile) {
      setIsShowDropdownMobile(false);
    }
  });
  const [currency, setCurrency] = useState(defaultCurrency || process.env.REACT_APP_DEFAULT_CURRENCY);

  const [weight, setWeight] = useState(defaultWeight || DEFAULT_WEIGHT_OZ);

  const troyOunceToGram = 31.1034768;
  const pricePerGram = price => price / troyOunceToGram;

  let percentGoldString = '+ 0%';
  let percentSilverString = '+ 0%';
  let percentGold = 0;
  let percentSilver = 0;

  let valueGoldString = '+ 0';
  let valueSilverString = '+ 0';
  let valueGold = 0;
  let valueSilver = 0;
  const goldID = 'XAU';
  const silverID = 'XAG';
  const gramToKg = (weight === DEFAULT_WEIGHT_GRAM) ? 1 : 1000;
  if (goldPriceRealtime && silverPriceRealtime && openPrices) {
    // get open price on currency
    const openPriceGold = _.find(
      openPrices,
      o => o.symbol === `${goldID}${currency}`,
    );
    const openPriceSilver = _.find(
      openPrices,
      o => o.symbol === `${silverID}${currency}`,
    );

    // calculate value
    valueGold = goldPriceRealtime.price - openPriceGold.openPrice;
    valueGoldString = `${valueGold < 0 ? '- ' : '+ '}${weight === DEFAULT_WEIGHT_OZ ? formatNumber(Math.abs(valueGold)) :
      formatNumber(Math.abs(pricePerGram(valueGold * gramToKg)))}`;

    valueSilver = silverPriceRealtime.price - openPriceSilver.openPrice;
    valueSilverString = `${valueSilver < 0 ? '- ' : '+ '}${weight === DEFAULT_WEIGHT_OZ ? formatNumber3Decimals(Math.abs(valueSilver)) :
      formatNumber3Decimals(Math.abs(pricePerGram(valueSilver * gramToKg)))}`;
    // calculate percent
    percentGold = (goldPriceRealtime.price - openPriceGold.openPrice) / goldPriceRealtime.price;
    percentSilver = (silverPriceRealtime.price - openPriceSilver.openPrice) / silverPriceRealtime.price;
    percentGoldString = `${percentGold < 0 ? '- ' : '+ '}${toPercent(Math.abs(percentGold))}`;
    percentSilverString = `${percentSilver < 0 ? '- ' : '+ '}${toPercent(Math.abs(percentSilver))}`;
  }

  const [isShowCurrencyDropdown, setIsShowCurrencyDropdown] = useState(false);
  const [isShowWeightDropdown, setIsShowWeightDropdown] = useState(false);
  const [isShowDropdownMobile, setIsShowDropdownMobile] = useState(false);
  const [currencyMobile, setCurrencyMobile] = useState(defaultCurrency || process.env.REACT_APP_DEFAULT_CURRENCY);
  const [weightMobile, setWeightMobile] = useState(defaultWeight || DEFAULT_WEIGHT_OZ);
  const [isCollapsed, setIsCollapsed] = useState(defaultCollapsed == "true" || false);
  const [isCollapsedTemp, setIsCollapsedTemp] = useState(defaultCollapsed == "true" || false);

  const handleChangeCurrency = (value) => {
    setCurrency(value);
    Cookie.set('currency', value)
    setCurrencyMobile(value);
    onChangeCurrency(value);
    setIsShowCurrencyDropdown(false);
  }
  const handleChangeWeight = (value) => {
    setWeight(value);
    Cookie.set('weight', value);
    setWeightMobile(value);
    setIsShowWeightDropdown(false);
  }
  const showHideCurrencyDropdownList = (e) => {
    setIsShowCurrencyDropdown(!isShowCurrencyDropdown);
  }
  const showHideWeightDropdownList = (e) => {
    setIsShowWeightDropdown(!isShowWeightDropdown);
  }

  // For Mobile
  const showHideDropdownMobile = (e) => {
    setIsShowDropdownMobile(!isShowDropdownMobile);
  }
  const handleChooseCurrencyMobile = (value) => {
    setCurrencyMobile(value);
  }
  const handleSaveMobile = () => {
    setCurrency(currencyMobile);
    Cookie.set('currency', currencyMobile)
    setWeight(weightMobile);
    Cookie.set('weight', weightMobile);
    setIsCollapsed(isCollapsedTemp);
    Cookie.set('isCollapsed', isCollapsedTemp);
    onChangeCollapse(isCollapsedTemp);
    onChangeCurrency(currencyMobile);
    setIsShowDropdownMobile(false);
  }
  const handleChooseWeightMobile = (value) => {
    setWeightMobile(value);
  }
  const handeChangeCollapsed = () => {
    setIsCollapsedTemp(!isCollapsedTemp);

  }

  const getCurrencyCode = (currency) => {
    if (currency === DEFAULT_CURRENCY_SEK)
      return "kr";
    if (currency === DEFAULT_CURRENCY_USD)
      return "usd";
    if (currency === DEFAULT_CURRENCY_EUR)
      return "eur";
    return "";
  }

  const getWeightCode = (weight) => {
    if (weight === DEFAULT_WEIGHT_GRAM)
      return "g";
    else return weight;
  }

  return (
    <React.Fragment>
      <div className={`price-update ${isCollapsed ? "collapsed" : ""}`}>
        <div className="select" ref={refCurrencyDropdown}>
          <div className="toggler" onClick={() => showHideCurrencyDropdownList()}> {currency} <svg width="7" height="4" viewBox="0 0 7 4" version="1" xmlns="http://www.w3.org/2000/svg"><path fill="#19396E" fillRule="nonzero" d="M3.5 2.636L0.483 0.036 0.483 1.351 3.5 3.981 6.5 1.351 6.5 0.036z" /></svg></div>
          <div className={isShowCurrencyDropdown ? "dropdown active" : "dropdown"}>
            <div className="item" onClick={() => handleChangeCurrency('SEK')}>{DEFAULT_CURRENCY_SEK}</div>
            <div className="item" onClick={() => handleChangeCurrency('USD')}>{DEFAULT_CURRENCY_USD}</div>
            <div className="item" onClick={() => handleChangeCurrency('EUR')}>{DEFAULT_CURRENCY_EUR}</div>
            {/* <div className="item" onClick={() => { setCurrency('GBP');}}>GBP</div> */}
            {/* <div className="item">BTC</div> */}
          </div>

        </div>
        <div className="col-item">
          <div className="major-info">
            <div className="name">Gold <span> {currency}/{(getWeightCode(weight))}</span></div>
            <div className={percentGold > 0 ? "price is-green" : "price is-red"}>{formatNumber(weight === DEFAULT_WEIGHT_OZ ? goldPriceRealtime.price : pricePerGram(goldPriceRealtime.price * gramToKg))}
              <span> {currency}/{(getWeightCode(weight))} </span>
            </div>
          </div>
          <div className={percentGold > 0 ? "minor-info is-green" : "minor-info is-red"} >
            <div className="number">{valueGoldString} {getCurrencyCode(currency)}</div>
            <div className="percent">{percentGoldString}</div>
          </div>
        </div>
        <div className="select on-mobile" ref={refDropdownMobile}>
          <button className="toggler button-white" onClick={() => showHideDropdownMobile()}>
            <i className="dot"></i>
            <i className="dot"></i>
            <i className="dot"></i>
          </button>
          <div className={`dropdown two-col ${isShowDropdownMobile ? 'active' : ''}`}>
            <div className="button-toggle-group">
              <div className="label">Currency</div>
              <label className="button-toggle" htmlFor="sek">
                <input defaultChecked={"SEK" === currencyMobile} type="radio" name="currency" id="sek" />
                <span onClick={() => handleChooseCurrencyMobile("SEK")}> {DEFAULT_CURRENCY_SEK} </span>
              </label>
              <label className="button-toggle" htmlFor="usd">
                <input defaultChecked={"USD" === currencyMobile} type="radio" name="currency" id="usd" />
                <span onClick={() => handleChooseCurrencyMobile("USD")}>{DEFAULT_CURRENCY_USD}</span>
              </label>
              <label className="button-toggle" htmlFor="eur">
                <input defaultChecked={"EUR" === currencyMobile} type="radio" name="currency" id="eur" />
                <span onClick={() => handleChooseCurrencyMobile("EUR")}>{DEFAULT_CURRENCY_EUR}</span>
              </label>

            </div>
            <div className="button-toggle-group">
              <div className="label">Weight</div>
              <label className="button-toggle" htmlFor="kg">
                <input defaultChecked={DEFAULT_WEIGHT_KG === weightMobile} type="radio" name="weight" id="kg" />
                <span onClick={() => handleChooseWeightMobile("kg")}>{DEFAULT_WEIGHT_KG}</span>
              </label>
              <label className="button-toggle" htmlFor="oz">
                <input defaultChecked={DEFAULT_WEIGHT_OZ === weightMobile} type="radio" name="weight" id="oz" />
                <span onClick={() => handleChooseWeightMobile("oz")}>{DEFAULT_WEIGHT_OZ}</span>
              </label>
              <label className="button-toggle" htmlFor="gram">
                <input defaultChecked={DEFAULT_WEIGHT_GRAM === weightMobile} type="radio" name="weight" id="gram" />
                <span onClick={() => handleChooseWeightMobile("gram")}>{DEFAULT_WEIGHT_GRAM}</span>
              </label>

              <div className="label centered">Hide</div>
              <label className="switcher" htmlFor="hide-ticker">
                <input checked={isCollapsedTemp} type="checkbox" name="hide-ticker" id="hide-ticker" />
                <span onClick={() => handeChangeCollapsed()}><i></i></span>
              </label>

              <div className="button" onClick={() => handleSaveMobile()}>Save</div>
            </div>
          </div>

        </div>
        <div className="col-item">
          <div className="major-info">
            <div className="name">Silver <span> {currency}/{getWeightCode(weight)}</span></div>
            <div className={percentSilver > 0 ? "price is-green" : "price is-red"}>{formatNumber3Decimals(weight === DEFAULT_WEIGHT_OZ ? silverPriceRealtime.price : pricePerGram(silverPriceRealtime.price * gramToKg))}
              <span> {currency}/{(getWeightCode(weight))} </span>
            </div>
          </div>
          <div className={percentSilver > 0 ? "minor-info is-green" : "minor-info is-red"} >
            <div className="number">{valueSilverString} {getCurrencyCode(currency)}</div>
            <div className="percent">{percentSilverString}</div>
          </div>
        </div>
        <div className="select" ref={refWeightDropdown}>
          <div className="toggler" onClick={() => showHideWeightDropdownList()}> {weight} <svg width="7" height="4" viewBox="0 0 7 4" version="1" xmlns="http://www.w3.org/2000/svg"><path fill="#19396E" fillRule="nonzero" d="M3.5 2.636L0.483 0.036 0.483 1.351 3.5 3.981 6.5 1.351 6.5 0.036z" /></svg></div>
          <div className={isShowWeightDropdown ? "dropdown active" : "dropdown"}>
            <div className="item" onClick={() => handleChangeWeight('kg')}>{DEFAULT_WEIGHT_KG}</div>
            <div className="item" onClick={() => handleChangeWeight('oz')}>{DEFAULT_WEIGHT_OZ}</div>
            <div className="item" onClick={() => handleChangeWeight('gram')}>{DEFAULT_WEIGHT_GRAM}</div>
          </div>
        </div>



      </div>
    </React.Fragment>
  );
}

IndexList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  gold: PropTypes.any,
  silver: PropTypes.any,
  goldPriceRealtime: PropTypes.any,
  silverPriceRealtime: PropTypes.any,
  openPrices: PropTypes.array,
  onChangeCurrency: PropTypes.func,
  defaultCurrency: PropTypes.string,
};

export default IndexList;
