import React from 'react';
import PropTypes from 'prop-types';

function Symbol({ name, unit }) {
  let currency = null;
  switch (name) {
    case 'EUR':
      currency = 'eur';
      break;
    case 'USD':
      currency = 'usd';
      break;
    default:
      currency = 'kr';
      break;
  }
  return (
    <span>
      {currency}/{unit}
    </span>
  );
}

Symbol.propTypes = {
  name: PropTypes.string.isRequired,
  unit: PropTypes.string.isRequired,
};

export default Symbol;
