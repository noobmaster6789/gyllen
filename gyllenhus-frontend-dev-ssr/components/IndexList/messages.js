/*
 * IndexList Messages
 *
 * This contains all the text for the IndexList container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  liveMarketPrices: {
    id: `${scope}.liveMarketPrices`,
    defaultMessage: 'Live Market Prices',
  },
  sek: {
    id: `${scope}.sek`,
    defaultMessage: 'SEK',
  },
  eur: {
    id: `${scope}.eur`,
    defaultMessage: 'EUR',
  },
  usd: {
    id: `${scope}.usd`,
    defaultMessage: 'USD',
  },
  gold: {
    id: `${scope}.gold`,
    defaultMessage: 'Gold',
  },
  silver: {
    id: `${scope}.silver`,
    defaultMessage: 'Silver',
  },
});
