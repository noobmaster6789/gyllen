import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { round } from 'lodash';
import Currency from '../Currency';
import { formatNumberDecimal } from '../../utils/formatter';

const troyOunceToGram = 31.1034768;
// get price per gram
const pricePerGram = price => price / troyOunceToGram;

/**
 * Product Spot Price = Gram Bid Price of Material * Gram weight of product * Purity Percentage
 Price without VAT = Product Spot price + (Margin Percentage * (Product Spot price))
 * @param price
 * @param weight
 * @param margin
 * @param currency
 * @param productPurity
 * @param productVat
 * @param isHeader
 * @param unit
 * @return {*}
 * @constructor
 */
function ProductPriceCart({
  price,
  weight,
  margin,
  // currency,
  productPurity,
  productVat,
  qty,
  isHeader = false,
  unit = '',
  total
}) {

  let productPrice = price;
  if (isHeader === false) {
    
    let spotPrice = pricePerGram(price) * weight;
    const marginPercent = Number(margin) ? margin / 100 : 0;
    if (Number(productPurity)) {
      const purity = productPurity === 99.99 ? 1 : productPurity / 100;
      spotPrice *= purity;
    }
    // calculate product price without vat
    const priceWithoutVat = spotPrice + marginPercent * spotPrice;
    productPrice = priceWithoutVat;
    if (Number(productVat)) {
      // product price with vat
      const vatPercent = productVat / 100;
      productPrice = priceWithoutVat + priceWithoutVat * vatPercent;
      // productPrice =
      //   currency === 'SEK' ? round(productPrice, 0) : round(productPrice, 2);
    }
  } else if (unit === 'g') {
    productPrice = pricePerGram(price);
  }
  total = productPrice*qty;
  return formatNumberDecimal(productPrice*qty)
        {' '}
        <Currency name="SEK" />;
}

ProductPriceCart.propTypes = {
  price: PropTypes.any,
  weight: PropTypes.number,
  margin: PropTypes.any,
  qty: PropTypes.number,
  productPurity: PropTypes.number,
  productVat: PropTypes.number,
  isHeader: PropTypes.bool,
  unit: PropTypes.string,
};
export default ProductPriceCart;
export { pricePerGram, round };
