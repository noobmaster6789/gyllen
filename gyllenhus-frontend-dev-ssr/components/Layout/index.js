import Head from 'next/head'
import Header from '../Header'
import LoginModal from '../../containers/LoginModal';
import FooterList from '../../containers/FooterList'
import ForgotPasswordModal from '../../containers/ForgotPasswordModal';


class Layout extends React.Component {
  state = {
    token: null
  }

  componentDidMount() {
    const token = localStorage.getItem('customerToken')
    this.setState({
      token
    })
  }

  render() {
    const { children, title = '', openPrices, priceRealtime, menu, deliverySetting,
      dataFooter, currencyIndex, weightIndex, isCollapsed } = this.props

    const { } = this.state

    return (
      <React.Fragment>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta charSet="utf-8" />
          <title>{title}</title>
        </Head>

        <Header deliverySetting={deliverySetting} openPrices={openPrices} priceRealtime={priceRealtime}
          menu={menu} currencyIndex={currencyIndex} weightIndex={weightIndex} isCollapsed={isCollapsed} />
        {children}
        <FooterList dataFooter={dataFooter} />
        <LoginModal />
        <ForgotPasswordModal />
      </React.Fragment>
    )
  }
}

export default Layout
