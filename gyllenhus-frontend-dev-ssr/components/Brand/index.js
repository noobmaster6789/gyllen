/**
 *
 * Brand
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import BrandItem from '../BrandItem';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
/**
 * @return {null}
 */
function Brand({ items }) {
  if (items && items.length > 1) 
    return  <div className="name"><FormattedMessage {...messages.variousProducers} /> </div>;
  else
  return items.map(item => <BrandItem item={item} key={item.ID} />);
}

Brand.propTypes = {
  items: PropTypes.any,
};

export default Brand;
