/*
 * MyAccountWidget Messages
 *
 * This contains all the text for the MyAccountWidget container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  
  variousProducers: {
    id: `${scope}.variousProducers`,
    defaultMessage: 'Various producers',
  },
});
