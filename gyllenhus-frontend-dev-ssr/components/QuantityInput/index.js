/**
 *
 * QuantityInput
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

export function QuantityInput({ value, onChangeHandle, onChangeQuantity }) {
  return (
    <div className="input-amount">
      <button className="up" onClick={() => onChangeQuantity(+1)}><svg xmlns="http://www.w3.org/2000/svg" width="12.2" height="8" viewBox="0 0 12.2 8"><path data-name="arrow-up" d="M5.27,6.115,0,0H2.667L8,6.115,2.667,12.2H0Z" transform="rotate(-90 4 4)" fill="#3064ba" /></svg></button>
      <input
        type="number"
        class={value > 99 ? "smaller" : ""}
        name="quantity"
        style={{ width: `50px` }}
        value={value}
        min={1}
        max={999}
        pattern="[0-9]"
        onChange={onChangeHandle}
      />
      <button className="down" onClick={() => onChangeQuantity(-1)}><svg xmlns="http://www.w3.org/2000/svg" width="12.2" height="8" viewBox="0 0 12.2 8"><path data-name="arrow-down" d="M5.27,6.086,0,12.2H2.667L8,6.086,2.667,0H0Z" transform="rotate(90 6 6)" fill="#3064ba" /></svg></button>

    </div>
  );
}

QuantityInput.propTypes = {
  value: PropTypes.number,
  onChangeHandle: PropTypes.func,
  onChangeQuantity: PropTypes.func,
};

export default QuantityInput;
