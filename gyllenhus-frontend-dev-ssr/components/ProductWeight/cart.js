import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import ProductPriceCart from '../ProductPrice/cart';

function ProductWeightCart({
  valueGram,
  currency,
  price,
  margin,
  productPurity,
  productVat,
  qty, total
}) {
  return (
        price && (
          
            <ProductPriceCart
              weight={Number(valueGram)}
              price={price}
              margin={margin}
              currency={currency}
              productPurity={productPurity}
              productVat={productVat}
              qty = {qty}
              total = {total}
            />         
        ));
}

ProductWeightCart.propTypes = {
  valueGram: PropTypes.number,
  currency: PropTypes.string,
  price: PropTypes.any,
  margin: PropTypes.any,
  productPurity: PropTypes.number,
  productVat: PropTypes.number,
  qty: PropTypes.number,
};

export default ProductWeightCart;
