import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import ProductPrice from '../ProductPrice';

function ProductWeight({
  valueGram,
  currency,
  price,
  margin,
  productPurity,
  productVat,
}) {
  
  return (
        price && (
          
            <ProductPrice
              weight={Number(valueGram)}
              price={price}
              margin={margin}
              currency={currency}
              productPurity={productPurity}
              productVat={productVat}
            />         
        ));
}

ProductWeight.propTypes = {
  valueGram: PropTypes.number,
  currency: PropTypes.string,
  price: PropTypes.any,
  margin: PropTypes.any,
  productPurity: PropTypes.number,
  productVat: PropTypes.number,
};

export default ProductWeight;
