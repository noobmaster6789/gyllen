/**
 * A link to a certain page, an anchor tag
 */

import styled from 'styled-components';
import Link from 'next/link'

const A = styled(Link)`
  color: #41addd;

  &:hover {
    color: #6cc0e5;
  }
`;

export default A;
