/**
 *
 * CartProductName
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

function CartProductName({ title, value, unit }) {
  return (
    <React.Fragment>
        {value} {unit} {title}
    </React.Fragment>
  );
}

CartProductName.propTypes = {
  title: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  unit: PropTypes.string.isRequired,
};

export default CartProductName;
