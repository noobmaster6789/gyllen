/**
 *
 * ProductName
 *
 */

import React from 'react';
import Brand from '../Brand';
import ReactHtmlParser from 'react-html-parser';

function ProductDescription({ typeBrands, product, producer, typeConditions }) {
  const content = (product && product.content) ? product.content : "";
  return (
    <React.Fragment>
      <div className="name">
        {(producer && producer.length > 0) ? producer : typeof typeBrands !== 'undefined' ? (
          <Brand items={typeBrands} />
        ) : null}
      </div>
      <div className="condition">
        {typeConditions && typeConditions.length > 0 ? typeConditions.map(item => item.name + " ") : ""}
      </div>
      <div className="description">
        {ReactHtmlParser(content)}
      </div>
    </React.Fragment>
  );
}

export default ProductDescription;