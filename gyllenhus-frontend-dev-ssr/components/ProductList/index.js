import React from 'react';
import PropTypes from 'prop-types';
import ProductListItem from '../../containers/ProductListItem';
import Img from '../../components/Img';
import List from '../List';
import PlaceholderLoading from '../PlaceholderLoading/product';

const REACT_APP_API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT
const productEmpty = `${REACT_APP_API_ENDPOINT}/static/images/product-empty-transparent.png`;

/**
 * @return {null}
 */
function ProductList({
  loading,
  error,
  data,
  defaultCurrency,
  goldPrice,
  silverPrice,
  platinumPrice,
  palladiumPrice,
  dispatch,
}) {
  if (loading) {
    // return <List component={LoadingIndicator} />;
  }
  if (error !== false) {
    // const ErrorComponent = () => (
    //   <ListItem item="Something went wrong, Please try again!" />
    // );
    // return <List component={ErrorComponent} />;
  }
  if (data !== false && data && data.length > 0) {
    const componentData = {
      currency: defaultCurrency,
      goldPrice,
      silverPrice,
      platinumPrice,
      palladiumPrice,
      dispatch,
    };
    return (
      <List
        component={ProductListItem}
        items={data}
        componentData={componentData}
      />
    );
  }
  if (data !== false && data && data.length === 0) {
    return (
      <div className="item-empty">
        <div className="image">
          <Img src={productEmpty} alt="product-empty" />
        </div>
      </div>
    );
  }
  const loadingPlaceholder = [
    <div className="item-empty">
      <PlaceholderLoading />
    </div>,
    <div className="item-empty">
      <PlaceholderLoading />
    </div>,
    <div className="item-empty">
      <PlaceholderLoading />
    </div>,
  ];
  return loadingPlaceholder;
}

ProductList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  data: PropTypes.any,
  defaultCurrency: PropTypes.string,
  goldPrice: PropTypes.object,
  silverPrice: PropTypes.object,
  dispatch: PropTypes.func,
  platinumPrice: PropTypes.object,
  palladiumPrice: PropTypes.object,
};

export default ProductList;
