import React from 'react';
import PropTypes from 'prop-types';
import List from '../List';
import SearchResultListItem from '../SearchResultListItem';

/**
 * @return {null}
 */
function SearchResultList({
  listProduct,
  textSearch

}) {
  const componentData = {
    textSearch: textSearch
  };
  if (listProduct !== false && listProduct && listProduct.length > 0 && listProduct !== null) {
    return (
      <List
        component={SearchResultListItem}
        items={listProduct}
        componentData={componentData}
      />
    );
  } else {
    return "";
  }
}

SearchResultList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  data: PropTypes.any,
  defaultCurrency: PropTypes.string,
  goldPrice: PropTypes.object,
  silverPrice: PropTypes.object,
  dispatch: PropTypes.func,
  platinumPrice: PropTypes.object,
  palladiumPrice: PropTypes.object,
};

export default SearchResultList;
