import React from 'react';
import ReactHtmlParser from 'react-html-parser';

/**
 * @return {null}
 */
export class SearchResultListItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const findIndex = (string, text) => {
      const strUpcase = string.toUpperCase();
      const textUpcase = text.toUpperCase();
      const position = strUpcase.indexOf(textUpcase);
      return position;
    }

    const getHtmlResult = (title, textSearch) => {
      try {
        const search = textSearch.trim();
        const position = findIndex(product.title, search);
        if (position == -1)
          return title;
        const text = title.substr(position, search.length);
        const result = title.substr(0, position) + "<b>" + text + "</b>" + title.substr(position + search.length, title.length);
        return result;
      } catch (e) {
        return title;
      }
    }

    const { textSearch } = this.props;
    const { product } = this.props.item;
    const title = product.title;

    return (
      <div class="item"><a href={`/products/${product.slug}`}> {ReactHtmlParser(getHtmlResult(title, textSearch))} </a></div>
    );
  }
}

export default SearchResultListItem;
