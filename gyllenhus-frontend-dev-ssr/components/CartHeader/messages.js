/*
 * Cart Header Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'gyllenhus';

export default defineMessages({
  close: {
    id: `${scope}.close`,
    defaultMessage: 'Close',
  },
  toCheckout: {
    id: `${scope}.toCheckout`,
    defaultMessage: 'To Checkout',
  },
  
  
});
