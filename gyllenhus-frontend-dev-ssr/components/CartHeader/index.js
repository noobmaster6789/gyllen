import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectTotalBuyQuantity, makeSelectTotalSellQuantity, makeSelectTotalBuyWitoutTax, makeSelectTotalSellWitoutTax, makeSelectTotalBuyToPay, makeSelectTotalSellToPay, makeSelectCartBuyProducts, makeSelectCartSellProducts, makeSelectIsBuyActive } from '../../containers/CartPage/selectors';
import { formatNumberDecimalPurity } from '../../utils/formatter';
import CartHeaderProductList from '../CartHeaderProductList';
import { loadCartData } from '../../containers/CartPage/actions';
import Search from '../../containers/Search';
import messages from './messages';
import { FormattedMessage } from 'react-intl';

/**
 * @return {null}
 */
function CartHeader(props) {
  const {
    totalBuyQuantity,
    totalSellQuantity,
    totalBuyToPay,
    totalSellToPay,
    cartBuyProducts,
    cartSellProducts,
    dispatch,
    onLoadCartData,
    deliverySetting
  } = props;

  const cartBuyProductList = {
    data: cartBuyProducts,
    typeBuySell: "buy",
    dispatch,
  };
  const cartSellProductList = {
    data: cartSellProducts,
    typeBuySell: "sell",
    dispatch,
  };

  useEffect(() => {
    onLoadCartData(deliverySetting);
  }, []);

  const [mouseHover, setMouseHover] = useState(false);
  const toggleHover = (value) =>  {
    setMouseHover(value);
  }
  
  const isBuyCart = cartBuyProducts && cartBuyProducts.length > 0 && cartBuyProducts !== "[]";
  const isSellCart = cartSellProducts && cartSellProducts.length > 0 && cartSellProducts != "[]";
  const cartEmpty = !isBuyCart && !isSellCart;

  return (
    <div className="button-group">
      <Search />
      <div className={`cart ${cartEmpty ? "" : "has-item"}`} onMouseEnter={() => toggleHover(true)} onMouseLeave={() => toggleHover(false)}  >
        <div className={`cart-tray ${isBuyCart || isSellCart ? "" : "on-mobile"}`}>
          <div className={`tray sell ${isBuyCart ? "" : "on-mobile"}`}>
            <div className="number">{totalBuyQuantity}</div>
            <div className="total">{formatNumberDecimalPurity(totalBuyToPay)}<small> kr</small></div>
          </div>
          <div className={`tray buy ${isSellCart ? "" : "on-mobile"}`}>
            <div className="number">{totalSellQuantity}</div>
            <div className="total">{formatNumberDecimalPurity(totalSellToPay)}<small> kr</small></div>
          </div>
        </div>
        <a href="/cart" onClick={ e => {if (cartEmpty) e.preventDefault()}} >        
        <button>
          <svg className={cartEmpty ? "" : "icon-26"} xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26">
            <g data-name="Group 30310">
              <g data-name="checkout v2">
                <path data-name="Path 28454" d="M8,20a1,1,0,1,1-1,1A1,1,0,0,1,8,20Zm0-1a2,2,0,1,0,2,2A2,2,0,0,0,8,19Z" fill="#19396e" />
                <path data-name="Path 28455" d="M18,20a1,1,0,1,1-1,1A1,1,0,0,1,18,20Zm0-1a2,2,0,1,0,2,2A2,2,0,0,0,18,19Z" fill="#19396e" />
                <path data-name="Path 28456" d="M6.7,6,3.35,2.65l-.7.7L6,6.7V17H22.17L26,6ZM21.46,16H7V7H24.59Z" fill="#19396e" />
                <path data-name="Path 28457" d="M20.5,11.5,17,15l-.75-.75L18.5,12h-9V11h9L16.25,8.75,17,8Z" fill="#19396e" className="arrow" />
              </g>
            </g>
          </svg>
        </button>
        </a>


        <div className="in-cart dropdown" style={{display: mouseHover? "inline" : "none"}}>
          <CartHeaderProductList title="Buying" totalToPay={totalBuyToPay} {...cartBuyProductList} />
          <CartHeaderProductList title="Selling" totalToPay={totalSellToPay} {...cartSellProductList} />


          <div className={`in-cart-footer ${isBuyCart || isSellCart ? "" : "on-mobile"}`}>
            <a href="#" className="button is-blank" onClick={() => toggleHover(false)} ><FormattedMessage {...messages.close} /></a>
            <a href="/cart" className="button"><FormattedMessage {...messages.toCheckout} /></a>
          </div>
          <div className={`in-cart-footer ${isBuyCart || isSellCart ? "on-mobile" : ""}`}>
          <h3> Cart is empty </h3>
          </div>
        </div>
      </div>


    </div>
  );
}


CartHeader.propTypes = {
  totalBuyQuantity: PropTypes.number,
  totalSellQuantity: PropTypes.number,
  totalBuyToPay: PropTypes.number,
  totalSellToPay: PropTypes.number,
  cartBuyProducts: PropTypes.array,
  cartSellProducts: PropTypes.array,
  onLoadCartData: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  totalBuyQuantity: makeSelectTotalBuyQuantity(),
  totalSellQuantity: makeSelectTotalSellQuantity(),
  totalBuyToPay: makeSelectTotalBuyToPay(),
  totalSellToPay: makeSelectTotalSellToPay(),
  cartBuyProducts: makeSelectCartBuyProducts(),
  cartSellProducts: makeSelectCartSellProducts(),

});

function mapDispatchToProps(dispatch) {
  return {
    onLoadCartData: (deliverySetting) => dispatch(loadCartData(deliverySetting)),

  }
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(CartHeader);
