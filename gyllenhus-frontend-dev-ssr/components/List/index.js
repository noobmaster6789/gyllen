import React from 'react';
import PropTypes from 'prop-types';

function List(props) {
  const ComponentToRender = props.component;
  let content = <div />;

  // If we have items, render them
  if (props.items) {
    content = props.items.map((item, index) => (
      // eslint-disable-next-line no-param-reassign
      <ComponentToRender
        item={item}
        {...props.componentData}
        key={`item-${index + 1}`}
        index={index + 1}
      />
    ));
  } else {
    // Otherwise render a single component
    content = <ComponentToRender />;
  }

  return <React.Fragment>{content}</React.Fragment>;
}

List.propTypes = {
  component: PropTypes.any.isRequired,
  items: PropTypes.array,
  componentData: PropTypes.any,
};

export default List;
