const withCss = require('@zeit/next-css');

module.exports = withCss({
  webpack: (config) => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty'
    }
    config.optimization.minimize = false

    return config
  },
  env: {
    // REACT_APP_API_ENDPOINT: "http://167.71.195.173:3000",
    // REACT_APP_FOREX_API_URL: "http://167.71.195.173:8080",
    REACT_APP_API_ENDPOINT: "http://165.22.88.109:3000",
    REACT_APP_FOREX_API_URL: "http://165.22.88.109:8080",
    // REACT_APP_FOREX_API_URL: "http://localhost:8080",

    // #http://localhost:8080
    // REACT_APP_SOCKET_URL: "http://167.71.195.173:8081",
    REACT_APP_SOCKET_URL: "http://165.22.88.109:8081",
    // REACT_APP_SOCKET_URL: "http://localhost:8081",

    REACT_APP_FOREX_GET_QUOTE_ENDPOINT: "api/quote",
    REACT_APP_FOREX_GET_QUOTE_CSV_ENDPOINT: "api/quote-csv",
    REACT_APP_FOREX_GET_PRICE_LIST_ENDPOINT: "api/get-price-list",
    REACT_APP_FOREX_RELOAD_TIME: 18000,
    // REACT_APP_BACKEND_API_URL: "http://167.71.195.173/gyllenhus-wordpress",
    REACT_APP_BACKEND_API_URL: "http://165.22.88.109/gyllenhus-wordpress",
    REACT_APP_BACKEND_API_PRODUCT_ENDPOINT: "api/gyllenhus/v1/products/get-products",
    REACT_APP_API_REGISTER_ENDPOINT: "api/gyllenhus/v1/user/register",
    REACT_APP_API_LOGIN_ENDPOINT: "api/gyllenhus/v1/user/login",
    REACT_APP_API_FORGOT_PASSWORD_ENDPOINT: "api/gyllenhus/v2/user/forgot-password-send-new",
    REACT_APP_API_RESET_TOKEN_ENDPOINT: "api/gyllenhus/v1/token/reset",
    REACT_APP_API_GET_PRICE_LIST_ENDPOINT: "/api/gyllenhus/v1/price/getPriceList",
    REACT_APP_API_GET_PRICE_LIST_OPEN_PRICE_ENDPOINT: "/api/gyllenhus/v1/global/getPriceListOpenPrice",
    REACT_APP_API_GET_USER_INFO_ENDPOINT: "/api/gyllenhus/v2/user/getUserInfo",
    REACT_APP_API_GET_HOMEPAGE_BANNER_ENDPOINT: "api/gyllenhus/v2/pages/frontPage/getBanner",
    REACT_APP_API_GET_SPECIAL_OFFER_ENDPOINT: "api/gyllenhus/v2/pages/frontPage/getSpecialOffers",
    REACT_APP_API_GET_OUR_SUPPLIER_ENDPOINT: "api/gyllenhus/v2/pages/frontPage/getOurSuppliers",
    REACT_APP_API_GET_POPULAR_SILVER_PRODUCTS: "api/gyllenhus/v2/pages/frontPage/getPopularSilverProducts",
    REACT_APP_API_GET_POPULAR_PLATINUM_PRODUCTS: "api/gyllenhus/v2/pages/frontPage/getPopularPlatinumProducts",
    REACT_APP_API_GET_POPULAR_PALLADIUM_PRODUCTS: "api/gyllenhus/v2/pages/frontPage/getPopularPalladiumProducts",
    REACT_APP_API_GET_POPULAR_GOLD_PRODUCTS: "api/gyllenhus/v2/pages/frontPage/getPopularGoldProducts",
    REACT_APP_API_GET_FOOTER: "/api/gyllenhus/v1/global/getFooter",
    REACT_APP_API_GET_MENU: "api/gyllenhus/v1/menu/getMenu",
    REACT_APP_API_GET_TRANSPORTS: "/api/gyllenhus/v1/transports/getTransports",
    REACT_APP_API_GET_BANKS : "/api/gyllenhus/v1/global/getBanks",
    REACT_APP_API_GET_DELIVERY_COST_SETTING: "/api/gyllenhus/v1/delivery/getDeliveryCostSetting",
    REACT_APP_API_CHART: "api/gyllenhus/v1/chart",
    REACT_APP_API_GET_COUPON: "api/gyllenhus/v1/coupons/getCounpons",
    REACT_APP_API_CHECKOUT_BUY: "api/gyllenhus/v1/checkout/buy",
    REACT_APP_API_CHECKOUT_SELL: "api/gyllenhus/v1/checkout/sell",
    REACT_APP_API_GET_PRODUCT_DETAILS: "api/gyllenhus/v1/products/getProductDetails/",
    REACT_APP_API_SEARCH_PRODUCT: "api/gyllenhus/v1/pages/search/",
    REACT_APP_API_PORT: 5100,
    REACT_APP_DEFAULT_CURRENCY: "SEK",
    DEFAULT_LOCALE: "se",
    REACT_APP_RESET_TOKEN_TIME: 90000,
  }
});
