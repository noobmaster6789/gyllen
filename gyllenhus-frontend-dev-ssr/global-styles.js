import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
/* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
*/
/* line 6, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_reset.scss */
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed,
figure, figcaption, footer, header, hgroup,
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font-size: 100%;
  font: inherit;
  vertical-align: baseline;
}

/* HTML5 display-role reset for older browsers */
/* line 27, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_reset.scss */
article, aside, details, figcaption, figure,
footer, header, hgroup, menu, nav, section {
  display: block;
}

/* line 31, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_reset.scss */
body {
  line-height: 1;
}

/* line 34, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_reset.scss */
ol, ul {
  list-style: none;
}

/* line 37, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_reset.scss */
blockquote, q {
  quotes: none;
}

/* line 40, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_reset.scss */
blockquote:before, blockquote:after,
q:before, q:after {
  content: '';
  content: none;
}

/* line 45, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_reset.scss */
table {
  border-collapse: collapse;
  border-spacing: 0;
}

/*
  Name: KIT
  Date: May 2019
  description: >-
  A cool website
  Version: 1.0
  Author: tien pham
  Autor URI: https://tienpham.me
*/
/* Body */
/* line 14, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
html {
  font-size: 62.5%;
}

/* line 18, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
body {
  color: #19396e;
  font-size: 1.5em;
  /* currently ems cause chrome bug misinterpreting rems on body element */
  font-family: "acumin-pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
  line-height: 1.25;
  margin: 0;
  padding: 0;
  text-align: left;
}

/* line 28, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
* {
  box-sizing: border-box;
}

/* Headings */
/* line 31, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
h1, h2, h3, h4, h5, h6 {
  font-family: "acumin-pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
}

/* line 34, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
h1 {
  font-size: 4.4rem;
}

/* line 35, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
h2 {
  font-size: 2.4rem;
}

/* line 36, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
h3 {
  font-size: 1.5rem;
}

/* line 37, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
h4 {
  font-size: 1.3rem;
}

/* line 38, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
h5 {
  font-size: 1.2em;
}

/* line 39, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
h6 {
  font-size: 1rem;
}

/* line 41, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
h2, h3, h4, h5, h6 {
  font-weight: 500;
  line-height: 1.1;
  margin-bottom: .8em;
}

/* Anchors */
/* line 48, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
a {
  outline: 0;
  color: inherit;
}

/* line 49, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
a img {
  border: 0;
  text-decoration: none;
}

/* line 56, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
img {
  max-width: 100%;
}

/* Paragraphs */
/* line 61, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
p {
  margin-bottom: 1.143em;
}

/* line 62, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
* p:last-child {
  margin-bottom: 0;
}

/* line 64, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
strong, b {
  font-weight: bold;
}

/* line 65, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
em, i {
  font-style: italic;
}

/* line 67, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
::-moz-selection {
  background: #19396e;
  color: #fff;
}

/* line 68, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
::selection {
  background: #19396e;
  color: #fff;
}

/* Lists */
/* line 71, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
ul {
  list-style: outside disc;
  margin: 1em 0 1.5em 1.5em;
}

/* line 76, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
ol {
  list-style: outside decimal;
  margin: 1em 0 1.5em 1.5em;
}

/* line 81, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
dl {
  margin: 0 0 1.5em 0;
}

/* line 82, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
dt {
  font-weight: bold;
}

/* line 83, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
dd {
  margin-left: 1.5em;
}

/* Quotes */
/* line 86, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
blockquote {
  font-style: italic;
}

/* Tables */
/* line 92, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
table {
  margin: .5em auto 1.5em auto;
  width: 98%;
}

/* Thead */
/* line 95, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
thead th {
  padding: .5em .4em;
  text-align: left;
}

/* Tbody */
/* line 99, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
tbody td {
  padding: .5em .4em;
}

/* Tfoot */
/* line 109, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.button {
  padding: 0 2.4rem;
  color: #fff;
  background: #3064ba;
  background: linear-gradient(270deg, #3064ba 0%, #044cad 100%);
  outline: none;
  border: none;
  border-radius: 5px;
  box-shadow: rgba(25, 57, 110, 0.21) 0 3px 5px;
  font-size: 1.5rem;
  line-height: 37px;
  height: 37px;
  cursor: pointer;
  transition: background 0.25s ease-in-out;
}

/* line 123, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.button:hover {
  background: #3064ba;
  background: linear-gradient(270deg, #3064c3 0%, #044cba 100%);
}

/* line 127, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.button.is-blank {
  background: transparent;
  color: #3366b7;
  box-shadow: none;
}

/* line 131, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.button.is-blank:hover {
  background: transparent;
  color: #19396e;
}

/* line 138, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.form-group {
  position: relative;
  padding-top: 1rem;
  margin-bottom: 1rem;
  width: 100%;
}

/* line 145, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.input-text {
  color: #19396e;
  appearance: none;
  -webkit-appearance: none;
  outline: none;
  border: none;
  border-bottom: #bdc4d0 solid 1px;
  font-size: 1.5rem;
  height: 30px;
  padding: 0;
  width: 100%;
  background-color: transparent;
}

/* line 157, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.input-text + .placeholder {
  position: absolute;
  left: 0;
  bottom: 5px;
  font-size: 1.5rem;
  transition: all ease-in-out 0.25s;
  transform-origin: left;
  pointer-events: none;
}

/* line 166, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.input-text:focus {
  border-bottom-color: #19396e;
}

/* line 168, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.input-text:focus + .placeholder {
  bottom: 24px;
  transform: scale(0.7, 0.7);
  color: #8390a5;
}

/* line 175, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.input-text.has-value + .placeholder {
  bottom: 24px;
  transform: scale(0.7, 0.7);
  color: #8390a5;
}

/* line 183, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.checkbox-group {
  position: relative;
  margin-bottom: 3rem;
}

/* line 188, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.checkbox {
  font-size: 1.5rem;
  padding-left: 1.7rem;
  position: relative;
  display: block;
  margin-bottom: 1rem;
  cursor: pointer;
}

/* line 196, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.checkbox input {
  position: absolute;
  width: 0;
  height: 0;
  visibility: hidden;
}

/* line 201, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.checkbox input:checked + .tick i {
  opacity: 1;
}

/* line 205, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.checkbox .tick {
  position: absolute;
  left: 0;
  width: 12px;
  height: 12px;
  top: 3px;
  border: #bdc4d0 solid 1px;
  padding: 0.2rem;
}

/* line 213, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.checkbox .tick i {
  background-color: #19396e;
  display: block;
  width: 100%;
  height: 100%;
  transition: all 0.2s ease-in-out;
  opacity: 0;
}

/* line 224, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_base.scss */
.on-mobile {
  display: none !important;
}

/* line 2, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
html {
  height: 100% !important;
}

/* line 5, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
body {
  background-color: #fafafa;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-direction: column;
  flex-direction: column;
  height: auto;
  min-height: 100%;
}

/* line 14, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.hide-overflow {
  overflow: hidden;
}

/* line 17, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.wrapper {
  max-width: 1140px;
  margin-left: auto;
  margin-right: auto;
  width: 90%;
}

/* line 23, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header {
  padding-top: 1.5rem;
  background-color: #fff;
  position: fixed;
  z-index: 9999;
  top: 0;
  left: 0;
  width: 100%;
  font-size: 1.3rem;
}

/* line 32, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header + * {
  margin-top: 132px;
}

/* line 35, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .top {
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
}

/* line 41, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .menu-toggle {
  background-color: #F2F2F2;
  text-align: center;
}

/* line 44, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .menu-toggle .stroke {
  display: block;
  height: 1px;
  margin-bottom: 8px;
  width: 22px;
  background-color: #19396e;
  position: relative;
  top: 0;
  transition: top 0.2s ease-in-out 0.2s, transform 0.2s ease-in-out 0s;
}

/* line 53, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .menu-toggle .stroke:last-child {
  margin-bottom: 0;
}

/* line 59, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .settings .links {
  display: flex;
}

/* line 61, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .settings .links > a {
  color: #19396e;
  text-decoration: none;
  margin-left: 1rem;
}

/* line 65, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .settings .links > a.strong {
  font-weight: 500;
  text-decoration: underline;
}

/* line 71, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .settings .tab {
  margin-right: 3rem;
  position: relative;
}

/* line 74, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .settings .tab:after {
  content: "";
  display: block;
  height: 100%;
  width: 1px;
  background-color: #f2f2f2;
  right: -20px;
  top: 0;
  position: absolute;
}

/* line 85, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .settings .opening-time {
  text-align: right;
  margin-top: 1.2rem;
  font-weight: 500;
}

/* line 89, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .settings .opening-time p {
  margin-bottom: 0.8rem;
}

/* line 94, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .button-group {
  display: flex;
}

/* line 96, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .button-group button {
  display: block;
  width: 40px;
  height: 40px;
}

/* line 100, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .button-group button:last-child {
  margin-right: 0;
}

/* line 104, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .button-group .cart {
  text-align: right;
  background-color: #fafafa;
  position: relative;
}

/* line 108, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .button-group .cart:before {
  content: "";
  display: block;
  width: 9999px;
  height: 100%;
  background-color: #fafafa;
  z-index: 1;
  position: absolute;
  left: 50%;
  top: 0;
}

/* line 121, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header button {
  -webkit-appearance: none;
  appearance: none;
  border: none;
  border-radius: 0;
  background: transparent;
  outline: none;
  padding: 0;
  display: inline-block;
  cursor: pointer;
}

/* line 131, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header button img {
  width: 26px;
  display: inline-block;
  position: relative;
  z-index: 2;
}

/* line 138, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .major {
  background-color: #f2f2f2;
  position: relative;
  overflow: hidden;
}

/* line 142, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .major .wrapper {
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  height: 40px;
  position: relative;
  z-index: 2;
}

/* line 152, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .major .menu {
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  list-style: none;
  padding: 0;
  margin: 0 2rem 0 0;
  font-size: 1.5rem;
  font-weight: 500;
}

/* line 161, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .major .menu > li {
  margin-right: 4.8rem;
}

/* line 164, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .major .menu a {
  text-decoration: none;
}

/* line 166, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .major .menu a:hover {
  text-decoration: underline;
}

/* line 172, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .logo {
  display: block;
  height: 58px;
}

/* line 175, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.header .logo img {
  display: block;
  height: 100%;
}

/* line 182, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.tab {
  list-style: none;
  padding: 0;
  margin: 0;
  display: flex;
  text-transform: uppercase;
}

/* line 188, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.tab > li {
  color: #888;
  padding-bottom: 0.2rem;
  border-bottom: transparent solid 1px;
  margin-bottom: -1px;
  margin-right: 1.3rem;
}

/* line 194, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.tab > li.active {
  color: #3366b7;
  border-bottom-color: #19396e;
}

/* line 198, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.tab > li:last-child {
  margin-right: 0;
}

/* line 201, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.tab > li a {
  text-decoration: none;
  color: inherit;
}

/* line 208, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.banner {
  position: relative;
  overflow: hidden;
  height: 465px;
  background-size: cover;
  background-position: center;
}

/* line 214, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.banner > img {
  width: auto;
  height: 100%;
  display: block;
  max-width: none;
  position: absolute;
  left: -5%;
  display: none;
}

/* line 223, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.banner .text {
  position: absolute;
  width: 400px;
  max-width: 40%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  top: 0;
  left: 50%;
}

/* line 234, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.banner .text h1 {
  font-family: "minion-pro", "Georgia", "Palatino Linotype", "Book Antiqua", Palatino, serif;
  font-size: 3.8rem;
  line-height: 1.2;
}

/* line 239, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.banner .text .quick-links {
  border-top: #daaf3a solid 2px;
  margin-top: 2.5rem;
  padding-top: 1rem;
  width: 100%;
  font-size: 1.3rem;
  font-weight: 500;
  display: flex;
  justify-content: space-between;
}

/* line 248, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.banner .text .quick-links a {
  text-decoration: none;
  color: #3366b7;
}

/* line 251, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.banner .text .quick-links a:hover {
  color: #19396e;
  text-decoration: underline;
}

/* line 260, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.promote {
  background-color: #f2f2f2;
  padding-top: 1.5rem;
  padding-bottom: 1.5rem;
  font-size: 1.3rem;
  font-weight: 500;
}

/* line 266, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.promote.wrapper {
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  align-items: center;
  padding-left: 2.5rem;
  padding-right: 2.5rem;
}

/* line 273, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.promote.wrapper > div {
  display: flex;
  align-items: center;
  cursor: pointer;
}

/* line 277, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.promote.wrapper > div svg {
  width: 28px;
  height: 28px;
  display: block;
  margin-right: 0.5rem;
}

/* line 283, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.promote.wrapper > div:hover {
  color: #3366b7;
}

/* line 285, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.promote.wrapper > div:hover svg path {
  fill: #daaf3a;
}

/* line 293, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.section {
  padding-top: 3.5rem;
  padding-bottom: 3.5rem;
}

/* line 296, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.section .section-head {
  margin-bottom: 2rem;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  position: relative;
}

/* line 302, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.section .section-head .title {
  margin: 0;
}

/* line 305, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.section .section-head .link {
  font-size: 1.4rem;
  text-decoration: none;
}

/* line 308, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.section .section-head .link:hover {
  text-decoration: underline;
}

/* line 315, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.countdown {
  font-size: 1.2rem;
  padding-bottom: 0.5rem;
  border-bottom: #daaf3a solid 2px;
  position: absolute;
  left: 50%;
  top: 0;
  transform: translateX(-50%);
  color: #7d8eab;
}

/* line 324, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.countdown strong {
  color: #19396e;
}

/* line 330, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view {
  display: flex;
  flex-wrap: wrap;
}

/* line 333, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item {
  width: calc(25% - 22.5px);
  margin-right: 3rem;
  background: #e9e9e9;
  display: flex;
  flex-direction: column;
  position: relative;
  -webkit-box-shadow: 0 0 44px -20px rgba(0, 0, 0, 0.24);
  -moz-box-shadow: 0 0 44px -20px rgba(0, 0, 0, 0.24);
  box-shadow: 0 0 44px -20px rgba(0, 0, 0, 0.24);
  text-align: center;
  margin-bottom: 3rem;
}

.product-list.grid-view .item-empty {
  width: calc(25% - 22.5px);
  margin-right: 3rem;
  background: transparent;
  display: flex;
  flex-direction: column;
  position: relative;
  -webkit-box-shadow: 0 0 44px -20px rgba(0, 0, 0, 0.24);
  -moz-box-shadow: 0 0 44px -20px rgba(0, 0, 0, 0.24);
  box-shadow: none;
  text-align: center;
  margin-bottom: 3rem;
}

/* line 345, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .image {
  display: block;
  margin-top: 5rem;
  margin-bottom: 5rem;
}

/* line 349, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .image > img {
  width: 100%;
  display: block;
}

/* line 354, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .weight, .product-list.grid-view .item .product {
  font-size: 1.8rem;
  font-weight: 700;
  display: inline-block;
}

/* line 359, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .weight i {
  display: none;
}

/* line 362, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .name {
  font-weight: 700;
  font-size: 1.4rem;
  margin-bottom: 0.5rem;
}

/* line 367, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .condition {
  font-size: 1.2rem;
}

/* line 370, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .detail,
.product-list.grid-view .item .percent {
  display: none;
}

/* line 374, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item:nth-child(4n) {
  margin-right: 0;
}

/* line 377, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .info {
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  z-index: 2;
  height: calc(100% - 90px);
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 1.5rem 2rem;
}

/* line 389, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group {
  background: #fff;
  width: 100%;
  height: 90px;
  display: flex;
}

/* line 394, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group .input-amount {
  width: 72px;
  flex-grow: auto;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  text-align: center;
  border-left: #e8e8e8 solid 1px;
  border-right: #e8e8e8 solid 1px;
  position: relative;
}

/* line 404, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group .input-amount:before {
  content: "";
  position: absolute;
  top: 50%;
  left: 0;
  width: 100%;
  height: 1px;
  background-color: #e8e8e8;
  z-index: 3;
}

/* line 414, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group .input-amount input {
  color: #3064ba;
  font-size: 3rem;
  font-weight: 600;
  line-height: 1;
  text-align: center;
  display: block;
  padding: 0 1rem;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 3;
  height: 36px;
  width: 55px;
  max-width: 80%;
  border-radius: 5px;
  background: #fff;
  -webkit-box-shadow: 1px 3px 16px -6px rgba(0, 0, 0, 0.4);
  -moz-box-shadow: 1px 3px 16px -6px rgba(0, 0, 0, 0.4);
  box-shadow: 1px 3px 16px -6px rgba(0, 0, 0, 0.4);
  -webkit-appearance: none;
  outline: none;
  border: none;
  appearance: none;
}

/* line 439, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group .input-amount input::-webkit-inner-spin-button, .product-list.grid-view .item .button-group .input-amount input::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* line 445, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group .input-amount button {
  height: 50%;
  z-index: 2;
  position: relative;
  border: none;
  background: #fff;
  font-weight: 700;
  color: #3064ba;
  font-size: 1.8rem;
  cursor: pointer;
  outline: none;
}

/* line 456, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group .input-amount button:hover {
  background: #f7f7f7;
}

/* line 459, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group .input-amount button.up {
  padding-bottom: 2rem;
}

/* line 462, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group .input-amount button.down {
  padding-top: 2rem;
}

/* line 467, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group > button {
  -webkit-appearance: none;
  appearance: none;
  border: none;
  background: #fff;
  border-radius: 0;
  outline: none;
  padding: 1rem 0.5rem;
  flex-grow: 1;
  cursor: pointer;
}

/* line 478, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group > button.sell:hover {
  background: #fbdede;
  background: linear-gradient(180deg, #fbdede 0%, #fcfdfe 100%);
}

/* line 482, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group > button.sell .text {
  color: #ea6161;
}

/* line 487, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group > button.buy:hover {
  background: #d3def0;
  background: linear-gradient(180deg, #d3def0 0%, #fbfdfe 100%);
}

/* line 491, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group > button.buy .text {
  color: #3064ba;
}

/* line 495, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group > button .text {
  text-transform: uppercase;
  font-weight: 700;
  font-size: 1.8rem;
  display: block;
}

/* line 501, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group > button .price {
  font-size: 1.4rem;
  color: #19396e;
}

/* line 504, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.grid-view .item .button-group > button .price small {
  font-style: normal;
  font-size: 1.2rem;
}

/* line 514, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item {
  position: relative;
  -webkit-box-shadow: 0 0 44px -20px rgba(0, 0, 0, 0.24);
  -moz-box-shadow: 0 0 44px -20px rgba(0, 0, 0, 0.24);
  box-shadow: 0 0 44px -20px rgba(0, 0, 0, 0.24);
  display: flex;
  flex-direction: column;
  background: #fff;
  margin-bottom: 3rem;
}

/* line 523, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .image {
  position: absolute;
  left: 0;
  top: 0;
  width: 48%;
  height: 100%;
  background: #e9e9e9;
  display: flex;
  justify-content: center;
  align-items: center;
}

/* line 533, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .image img {
  max-width: 100%;
  max-height: 100%;
  height: auto;
  display: block;
}

/* line 540, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .info {
  padding-left: 3rem;
  padding-right: 3rem;
  padding-top: 2rem;
  margin-left: 48%;
}

/* line 546, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .detail {
  margin-top: 1.4rem;
  margin-bottom: 1.4rem;
}

/* line 549, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .detail p {
  margin-bottom: 0;
}

/* line 551, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .detail p strong {
  font-weight: 500;
}

/* line 556, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .offer {
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  border-top: #e8e8e8 solid 1px;
  padding-top: 1.4rem;
  padding-bottom: 1.4rem;
  margin-top: 1.4rem;
}

/* line 564, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .offer p {
  margin-bottom: 0;
}

/* line 567, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .offer .icon {
  display: inline-block;
  margin-right: 1rem;
  vertical-align: middle;
}

/* line 572, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .offer .discount-price {
  font-weight: 500;
  font-size: 1.5rem;
  color: #36af63;
}

/* line 577, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .offer .discount-percent {
  padding-left: 30px;
  font-size: 1.3rem;
  font-weight: 500;
  color: #3366b7;
}

/* line 583, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .offer .original-price {
  text-decoration: line-through;
  font-weight: 500;
  font-size: 1.3rem;
}

/* line 589, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .weight, .product-list.list-view .item .product {
  font-size: 1.8rem;
  font-weight: 700;
  display: inline-block;
}

/* line 594, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .weight i {
  display: none;
}

/* line 597, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .name {
  font-weight: 700;
  font-size: 1.4rem;
  margin-top: 1rem;
  margin-bottom: 0.4rem;
}

/* line 603, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .condition {
  font-size: 1.3rem;
  margin-bottom: 1rem;
}

/* line 607, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .description {
  margin-top: 1.5rem;
  margin-bottom: 1.5rem;
  font-size: 1.3rem;
}

/* line 612, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item:nth-child(4n) {
  margin-right: 0;
}

/* line 615, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group {
  background: #fff;
  height: 90px;
  display: flex;
  border-top: #e8e8e8 solid 1px;
  margin-left: 50%;
}

/* line 621, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group .percent {
  display: none;
}

/* line 624, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group .input-amount {
  width: 72px;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  text-align: center;
  border-left: #e8e8e8 solid 1px;
  border-right: #e8e8e8 solid 1px;
  position: relative;
}

/* line 633, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group .input-amount input {
  color: #3064ba;
  font-size: 3rem;
  font-weight: 700;
  line-height: 1;
  text-align: center;
  display: block;
  padding: 1rem;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 3;
  height: 36px;
  width: 55px;
  border-radius: 5px;
  background: #fff;
  -webkit-box-shadow: 1px 3px 16px -6px rgba(0, 0, 0, 0.4);
  -moz-box-shadow: 1px 3px 16px -6px rgba(0, 0, 0, 0.4);
  box-shadow: 1px 3px 16px -6px rgba(0, 0, 0, 0.4);
  -webkit-appearance: none;
  outline: none;
  border: none;
  appearance: none;
}

/* line 657, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group .input-amount input::-webkit-inner-spin-button, .product-list.list-view .item .button-group .input-amount input::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* line 663, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group .input-amount button {
  height: 50%;
  z-index: 2;
  position: relative;
  border: none;
  background: #fff;
  font-weight: 700;
  color: #3064ba;
  font-size: 1.8rem;
  cursor: pointer;
}

/* line 673, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group .input-amount button:hover {
  background: #f7f7f7;
}

/* line 676, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group .input-amount button.up {
  padding-bottom: 2rem;
}

/* line 679, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group .input-amount button.down {
  padding-top: 2rem;
}

/* line 684, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group > button {
  -webkit-appearance: none;
  appearance: none;
  border: none;
  background: #fff;
  border-radius: 0;
  outline: none;
  padding: 1rem;
  flex-grow: 1;
  cursor: pointer;
}

/* line 695, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group > button.sell:hover {
  background: #fbdede;
  background: linear-gradient(180deg, #fbdede 0%, #fcfdfe 100%);
}

/* line 699, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group > button.sell .text {
  color: #ea6161;
}

/* line 704, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group > button.buy:hover {
  background: #d3def0;
  background: linear-gradient(180deg, #d3def0 0%, #fbfdfe 100%);
}

/* line 708, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group > button.buy .text {
  color: #3064ba;
}

/* line 712, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group > button .text {
  text-transform: uppercase;
  font-weight: 700;
  font-size: 1.8rem;
  display: block;
}

/* line 718, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.product-list.list-view .item .button-group > button .price {
  font-size: 1.4rem;
  color: #19396e;
}

/* line 729, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab {
  width: 100%;
  border-bottom: #f2f2f2 solid 1px;
  font-size: 1.3rem;
}

/* line 733, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab h2 {
  color: #425c86;
  font-size: 1.3rem;
  text-transform: none;
  margin: 0 3.9rem 0 0;
}

/* line 739, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab > a {
  display: block;
  text-decoration: none;
  padding-bottom: 0.3rem;
  color: #8390a5;
}

/* line 744, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab > a.active {
  color: #3366b7;
  border-bottom: #19396e solid 1px;
}

/* line 750, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab-content {
  display: table;
  width: 100%;
  padding-top: 0.6rem;
  padding-bottom: 0.7rem;
  font-size: 1.3rem;
}

/* line 756, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab-content .row {
  display: table-row;
  font-weight: 600;
}

/* line 759, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab-content .row > span {
  display: table-cell;
  padding: 0.3rem;
}

/* line 762, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab-content .row > span:last-child {
  text-align: right;
  padding-right: 0;
}

/* line 766, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab-content .row > span:first-child {
  padding-left: 0;
}

/* line 769, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab-content .row > span svg {
  display: inline-block;
  margin-right: 3px;
}

/* line 774, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab-content .row .material, .price-update .tab-content .row .percent {
  font-weight: 500;
}

/* line 777, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab-content .row small {
  font-size: 1.1rem;
  font-weight: 500;
}

/* line 783, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab-content .is-up svg.decrease {
  display: none;
}

/* line 786, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab-content .is-up .percent {
  color: #46c976;
}

/* line 791, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab-content .is-down svg.increase {
  display: none;
}

/* line 794, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.price-update .tab-content .is-down .percent {
  color: #e33a3a;
}

/* line 800, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.special-offers.list-view {
  display: flex;
  flex-wrap: wrap;
}

/* line 803, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.special-offers.list-view .item {
  width: calc(50% - 15px);
  margin-right: 2rem;
  position: relative;
  margin-right: 3rem;
}

/* line 808, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.special-offers.list-view .item .info {
  padding-left: 2rem;
  padding-right: 2rem;
}

/* line 812, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.special-offers.list-view .item .detail {
  display: none;
}

/* line 815, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.special-offers.list-view .item:nth-child(2n) {
  margin-right: 0;
}

/* line 820, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.supplier {
  background-color: #f2f2f2;
  padding: 3rem 0;
}

/* line 823, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.supplier h3 {
  font-size: 1.3rem;
  font-weight: 600;
}

/* line 827, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.supplier .slide {
  display: flex;
  flex-direction: row;
  justify-content: space-between;
}

/* line 831, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.supplier .slide .line-break {
  display: none;
}

/* line 834, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.supplier .slide img {
  height: 78px;
  display: inline-block;
}

/* line 841, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.footer {
  font-size: 1.3rem;
  overflow: hidden;
  background-color: #fff;
}

/* line 845, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.footer .wrapper {
  display: flex;
  flex-wrap: wrap;
}

/* line 848, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.footer .wrapper > div {
  padding-top: 3rem;
  padding-bottom: 3rem;
}

/* line 853, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.footer h2 {
  font-size: 1.3rem;
  font-weight: 600;
  margin-bottom: 1rem;
}

/* line 858, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.footer p {
  margin-bottom: 1rem;
}

/* line 861, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.footer .col-1 {
  background-color: #fafafa;
  padding-right: 8%;
  position: relative;
  width: 20%;
}

/* line 866, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.footer .col-1:before {
  content: "";
  display: block;
  width: 99999px;
  height: 100%;
  right: 100%;
  top: 0;
  position: absolute;
  background-color: #fafafa;
}

/* line 877, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.footer .col-2 {
  padding-left: 8%;
  width: 20%;
  background-color: #fff;
}

/* line 882, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.footer .col-3 {
  padding-left: 8%;
  width: 40%;
  background-color: #fff;
}

/* line 887, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.footer .col-4 {
  width: 20%;
  background-color: #fff;
}

/* line 891, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.footer .payment-badges {
  height: 28px;
  display: inline-block;
  margin-right: 0.5rem;
}

/* line 896, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.footer .ssl-badges {
  height: 27px;
  display: block;
  margin-top: 7.5rem;
}

/* line 901, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.footer .copyright {
  background-color: #f2f2f2;
  text-align: center;
  padding-top: 2rem;
  padding-bottom: 2rem;
  font-size: 1.3rem;
}

/* line 910, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.user-portal {
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  padding-top: 3rem;
  flex: 1 0 auto;
  padding-bottom: 3rem;
  width: 100%;
}

/* line 918, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.user-portal > .half {
  width: 50%;
  background: #fff;
  overflow: auto;
}

/* line 923, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.user-portal .signup {
  padding: 5rem 3rem;
  display: flex;
  flex-direction: column;
  align-items: center;
}

/* line 928, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.user-portal .signup h2 {
  font-size: 1.9rem;
  margin-bottom: 0.5rem;
  margin-top: 2rem;
}

/* line 933, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.user-portal .signup .checkbox-group {
  margin-top: 2rem;
  margin-bottom: 3rem;
}

/* line 938, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.user-portal .intro {
  background-color: #e8e8e8;
}

/* line 941, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.user-portal .tab {
  margin-bottom: 3rem;
  font-size: 1.6rem;
  display: flex;
}

/* line 948, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.tab {
  text-transform: none;
  border-bottom: #f2f2f2 solid 1px;
}

/* line 951, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.tab > a {
  color: #7d8eab;
  font-weight: 500;
  text-transform: none;
  text-decoration: none;
  margin-right: 2.6rem;
  margin-bottom: -1px;
}

/* line 958, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.tab > a.active {
  color: #3366b7;
  border-bottom: #19396e solid 1px;
  padding-bottom: 0.5rem;
}

/* line 963, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.tab > a:last-child {
  margin-right: 0;
}

/* line 969, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.form-wrap {
  display: flex;
  flex-wrap: wrap;
}

/* line 972, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.form-wrap .one-two {
  width: calc(50% - 23px);
  margin-right: 23px;
}

/* line 975, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.form-wrap .one-two:nth-child(2n) {
  margin-right: 0;
}

/* line 981, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.modal {
  position: fixed;
  width: 100%;
  height: 100%;
  z-index: 99999;
  pointer-events: none;
  opacity: 0;
  transition: all .2s ease-in-out;
  visibility: hidden;
}

/* line 990, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.modal.active {
  opacity: 1;
  visibility: visible;
  top: 0;
}

/* line 993, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.modal.active .card {
  margin-top: calc(15% + 10px);
}

/* line 997, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.modal .card {
  padding: 3rem 2.3rem 2rem;
  box-shadow: rgba(25, 57, 110, 0.34) 0 13px 21px;
  background-color: #fff;
  margin-left: auto;
  margin-right: auto;
  width: 302px;
  margin-top: 15%;
  pointer-events: auto;
  transition: all .2s ease-in-out;
}

/* line 1007, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.modal .card .head {
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  align-items: center;
  font-size: 1.5rem;
  margin-bottom: 2rem;
}

/* line 1014, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.modal .card .head h2 {
  margin-bottom: 0.6rem;
  font-weight: 500;
  font-size: 2.5rem;
}

/* line 1019, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.modal .card .head a {
  color: #3366b7;
  text-decoration: none;
}

/* line 1022, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.modal .card .head a:hover {
  text-decoration: underline;
}

/* line 1027, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.modal .card .checkbox {
  margin-top: 2rem;
  margin-bottom: 2rem;
}

/* line 1031, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_layout.scss */
.modal .card .button-group {
  display: flex;
  justify-content: flex-end;
  flex-wrap: wrap;
  margin-top: 3rem;
}

@media all and (max-width: 1140px) {
  /* line 2, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .wrapper {
    padding-left: 2.5rem;
    padding-right: 2.5rem;
    width: 100%;
  }
}

@media all and (max-width: 1024px) {
  /* line 10, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .major .menu > li {
    margin-right: 3.5rem;
  }
  /* line 13, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .promote.wrapper > div {
    text-align: center;
    display: block;
  }
  /* line 16, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .promote.wrapper > div svg {
    margin-right: 0;
    display: inline-block;
  }
}

@media all and (max-width: 768px) {
  /* line 24, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .banner .text {
    max-width: 50%;
    left: 45%;
  }
  /* line 28, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item {
    width: calc(33.3333333% - 6.66667px);
    margin-right: 1rem;
  }
  /* line 31, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item:nth-child(4n) {
    margin-right: 1rem;
  }
  /* line 34, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item:nth-child(3n) {
    margin-right: 0;
  }
  /* line 38, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .popular .product-list.grid-view .item:last-child {
    display: none;
  }
  /* line 44, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item .button-group > button .price {
    font-size: 1.3rem;
  }
  /* line 47, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item .button-group .input-amount {
    width: 60px;
  }
  /* line 51, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .promote.wrapper > div {
    font-size: 1.2rem;
  }
  /* line 54, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .special-offers.list-view .item {
    width: 100%;
    margin-right: 0 !important;
  }
  /* line 57, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .special-offers.list-view .item .image {
    width: 33%;
  }
  /* line 60, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .special-offers.list-view .item .info, .special-offers.list-view .item .button-group {
    margin-left: 33%;
  }
  /* line 64, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header {
    padding-top: 0;
    height: 55px;
  }
  /* line 67, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header + * {
    margin-top: 55px;
  }
  /* line 70, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .top {
    padding: 0;
    background-color: #fff;
    z-index: 1000;
    position: relative;
  }
  /* line 75, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .top .button-group.on-mobile {
    display: flex !important;
  }
  /* line 77, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .top .button-group.on-mobile button {
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;
    width: 55px;
    height: 55px;
  }
  /* line 86, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .top .button-group.on-mobile .cart {
    text-align: center;
    padding-bottom: 2px;
  }
  /* line 89, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .top .button-group.on-mobile .cart:before {
    display: none;
  }
  /* line 95, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .logo {
    margin-left: 2.5rem;
    height: 35px;
    z-index: 1000;
  }
  /* line 100, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .price-update {
    width: 100%;
    margin-top: 1rem;
    background: #fafafa;
    padding: 3rem 2.5rem 1.5rem;
    height: 124px;
    transition: all .2s ease-in-out 0.25s;
    margin-top: -100%;
  }
  /* line 109, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .settings {
    display: none;
  }
  /* line 111, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .settings.on-mobile {
    display: inline-block !important;
    position: absolute;
    right: 25px;
    top: 29px;
    text-align: right;
  }
  /* line 117, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .settings.on-mobile .title {
    color: #7d8eab;
    font-size: 1.3rem;
    font-weight: 400;
  }
  /* line 124, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .major {
    position: relative;
    z-index: 999;
    transform: translateY(-100%);
    transition: all .25s ease-in-out 0s;
    height: calc(100vh - 179px);
    overflow: auto;
  }
  /* line 131, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .major .button-group {
    display: none;
  }
  /* line 134, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .major .wrapper {
    height: auto;
  }
  /* line 137, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .major .menu {
    display: block;
    width: 100%;
    padding: 3rem 0;
    margin: 0;
  }
  /* line 142, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .major .menu > li {
    font-size: 1.9rem;
    margin-bottom: 2.3rem;
  }
  /* line 150, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header.show-menu .menu-toggle .stroke {
    transition: top 0.2s ease-in-out 0s, transform 0.2s ease-in-out 0.2s;
  }
  /* line 152, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header.show-menu .menu-toggle .stroke:first-child {
    top: 9px;
    transform: rotate(45deg);
  }
  /* line 156, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header.show-menu .menu-toggle .stroke:nth-child(2) {
    opacity: 0;
  }
  /* line 159, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header.show-menu .menu-toggle .stroke:last-child {
    top: -9px;
    transform: rotate(-45deg);
  }
  /* line 165, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header.show-menu .price-update {
    margin-top: 0;
    transition-delay: 0s;
  }
  /* line 169, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header.show-menu .major {
    transform: none;
    transition-delay: 0.2s;
  }
  /* line 175, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .supplier .slide {
    flex-wrap: wrap;
  }
  /* line 177, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .supplier .slide .line-break {
    width: 100%;
    display: block;
  }
}

@media all and (max-width: 700px) {
  /* line 185, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .section-head {
    flex-wrap: wrap;
  }
  /* line 187, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .section-head .countdown {
    order: 1;
    position: relative;
    margin-bottom: 3rem;
  }
  /* line 192, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .section-head .title, .section-head .link {
    order: 2;
  }
  /* line 195, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .section-head .title {
    width: 70%;
  }
  /* line 198, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .section-head .link {
    text-align: right;
    width: 30%;
  }
}

@media all and (max-width: 550px) {
  /* line 206, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .header .logo {
    margin-left: 1.4rem;
    margin-top: 0.8rem;
  }
  /* line 210, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .banner {
    position: relative;
  }
  /* line 212, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .banner .text {
    width: 100%;
    max-width: none;
    left: 0;
    padding: 2.5rem 1.4rem;
    bottom: 20px;
    height: auto;
    top: auto;
  }
  /* line 223, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .wrapper {
    padding-left: 1.4rem;
    padding-right: 1.4rem;
  }
  /* line 227, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .promote.wrapper > div {
    text-align: center;
    display: block;
  }
  /* line 230, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .promote.wrapper > div svg {
    margin-right: 0;
    display: inline-block;
  }
  /* line 234, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .promote.wrapper > div div {
    display: none;
  }
  /* line 240, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .section .section-head .title {
    font-size: 1.9rem;
  }
  /* line 243, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .section .section-head .link {
    font-size: 1.3rem;
  }
  /* line 250, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item {
    width: calc(50% - 5px);
  }
  /* line 252, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item .image {
    margin-top: 4rem;
    margin-bottom: 4rem;
  }
  /* line 256, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item .info {
    padding: 1.5rem 1rem;
  }
  /* line 258, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item .info .weight, .product-list.grid-view .item .info .product {
    font-size: 1.3rem;
  }
  /* line 261, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item .info .name {
    font-size: 1.1rem;
  }
  /* line 264, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item .info .condition {
    font-size: 1rem;
  }
  /* line 269, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .popular .product-list.grid-view .item:last-child {
    display: flex;
  }
  /* line 273, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item:nth-child(3n) {
    margin-right: 10px;
  }
  /* line 276, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item:nth-child(2n) {
    margin-right: 0;
  }
  /* line 283, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .special-offers.list-view .item .image {
    width: calc(50% - 10px);
  }
  /* line 286, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .special-offers.list-view .item .info, .special-offers.list-view .item .button-group {
    margin-left: calc(50% - 10px);
  }
  /* line 290, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .supplier .slide {
    flex-wrap: wrap;
  }
  /* line 292, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .supplier .slide img {
    height: 60px;
  }
  /* line 296, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .footer {
    flex-wrap: wrap;
  }
  /* line 298, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .footer .copyright {
    position: relative;
    z-index: 9;
  }
  /* line 302, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .footer .col-1 {
    width: 30%;
    padding-right: 0;
  }
  /* line 305, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .footer .col-1:after {
    content: "";
    display: block;
    width: 100%;
    top: 100%;
    left: 0;
    height: 900px;
    background-color: #fafafa;
    position: absolute;
  }
  /* line 316, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .footer .col-2, .footer .col-3 {
    padding-left: 4%;
    width: 35%;
  }
  /* line 320, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .footer .col-4 {
    flex-grow: 3;
    padding-left: 34%;
  }
  /* line 324, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .footer .ssl-badges {
    display: inline-block;
    margin-top: 0;
  }
}

@media all and (max-width: 340px) {
  /* line 332, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .supplier .slide {
    flex-wrap: wrap;
  }
  /* line 334, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .supplier .slide img {
    height: 50px;
  }
  /* line 339, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .footer .col-1 {
    width: 100%;
  }
  /* line 341, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .footer .col-1:after {
    height: 100%;
    left: 100%;
    width: 20px;
    top: 0;
  }
  /* line 348, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .footer .col-2, .footer .col-3 {
    width: 45%;
    padding-left: 0;
  }
  /* line 352, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .footer .col-2 {
    margin-right: 10%;
  }
  /* line 355, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .footer .col-4 {
    width: 100%;
    padding: 0;
  }
  /* line 363, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item .button-group .input-amount,
  .product-list.list-view .item .button-group .input-amount {
    width: 45px;
  }
  /* line 365, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item .button-group .input-amount input,
  .product-list.list-view .item .button-group .input-amount input {
    font-size: 1.8rem;
    padding: 0.2rem;
    max-width: 90%;
    height: 30px;
  }
  /* line 372, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item .button-group > button,
  .product-list.list-view .item .button-group > button {
    padding-left: 0;
    padding-right: 0;
  }
  /* line 375, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item .button-group > button .text,
  .product-list.list-view .item .button-group > button .text {
    font-size: 1.7rem;
  }
  /* line 378, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item .button-group > button .price,
  .product-list.list-view .item .button-group > button .price {
    font-size: 1.1rem;
  }
  /* line 380, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .product-list.grid-view .item .button-group > button .price small,
  .product-list.list-view .item .button-group > button .price small {
    font-size: 0.9rem;
  }
  /* line 388, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .special-offers.list-view .item .offer {
    padding-bottom: 0;
    flex-wrap: wrap;
  }
  /* line 390, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .special-offers.list-view .item .offer .original-price {
    order: 1;
    padding-left: 3rem;
  }
  /* line 394, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .special-offers.list-view .item .offer > div {
    order: 2;
  }
  /* line 398, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .special-offers.list-view .item .offer .discount-price {
    font-size: 1.3rem;
  }
  /* line 402, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .special-offers.list-view .item .info {
    padding: 1rem;
  }
  /* line 404, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .special-offers.list-view .item .info .weight, .special-offers.list-view .item .info .product, .special-offers.list-view .item .info .name, .special-offers.list-view .item .info .condition {
    display: none;
  }
  /* line 407, /Users/tienpham/Documents/Git/gyllenhus/gyllenhus-html/scss/_responsive.scss */
  .special-offers.list-view .item .info .description {
    margin-top: 0;
    font-size: 1.1rem;
  }
}

/*# sourceMappingURL=../css/main.map */
`;

export default GlobalStyle;
