
import React from "react";
import { connect } from 'react-redux';
import { compose } from 'redux';

import Layout from '../components/Layout'
import {
  getMenuRequest,
  getOpenPrice, getPriceRealtime, getTransports, getDeliveryCostSetting, getBanks, getFooter
} from "../utils/loadData";
import CartPage from "../containers/CartPage";

const Cart = (props) => {
  const { 
    currencyIndex,
    weight,
    isCollapsed
  } = props;
  return (
     <Layout title="Checkout" dataFooter = {props.dataFooter} deliverySetting = {props.deliverySetting} openPrices = {props.openPrices} 
        priceRealtime={props.priceRealtime} menu = {props.menu}
        currencyIndex={currencyIndex} weightIndex={weight} isCollapsed={isCollapsed}>
       <div>
        <CartPage transports={props.transports} banks={props.banks} defaultCurrency="SEK"/>
       </div>
     </Layout>
  )
}

Cart.getInitialProps = async (ctx) => {
  const { req } = ctx
  const { locale } = req || window.__NEXT_DATA__.props;

  const results = await Promise.all([
    getMenuRequest(locale, 'main-menu'),
    getPriceRealtime(),
    getOpenPrice(),
    getTransports(locale),
    getBanks(locale),
    getDeliveryCostSetting(),
    getFooter(locale)
  ])

  const menus = results[0];
  const priceRealtime = results[1];
  const openPrices = results[2];
  const transports = results[3]
  const banks = results[4];
  const deliverySetting = results[5];
  const dataFooter = results[6];

  return {
    menu : menus,
    priceRealtime : priceRealtime,
    openPrices : openPrices,
    transports: transports,
    banks : banks,
    deliverySetting : deliverySetting,
    dataFooter : dataFooter
  };
}

function mapDispatchToProps(dispatch) {
  return {
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(withConnect)(Cart);
