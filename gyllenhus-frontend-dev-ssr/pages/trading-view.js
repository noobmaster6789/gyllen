
import React, { useEffect } from "react";
import { connect } from 'react-redux';
import { compose } from 'redux';

import Layout from '../components/Layout'
import TradingViewPage from '../containers/TradingViewPage/index';
import {
  getMenuRequest,
  getOpenPrice, getPriceRealtime, getFooter
} from "../utils/loadData";

const TradingView = (props) => {
  const { 
    currencyIndex,
    weight,
    isCollapsed
  } = props;
  return (
    <Layout title="Home" dataFooter = {props.dataFooter} openPrices = {props.openPrices} 
      priceRealtime={props.priceRealtime} menu = {props.menu}
      currencyIndex={currencyIndex} weightIndex={weight} isCollapsed={isCollapsed}>
      <TradingViewPage />
    </Layout>
  )
}

TradingView.getInitialProps = async (ctx) => {
  const { req } = ctx
  const { locale } = req || window.__NEXT_DATA__.props;
  const results = await Promise.all([
    getMenuRequest(locale, 'main-menu'),
    getPriceRealtime(),
    getOpenPrice(),
    getFooter(locale)
  ])

  const menus = results[0];
  const priceRealtime = results[1];
  const openPrices = results[2];
  const dataFooter = results[3];

  return {
    menu : menus,
    priceRealtime : priceRealtime,
    openPrices : openPrices,
    dataFooter : dataFooter
  };
}



function mapDispatchToProps(dispatch) {
  return {
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(withConnect)(TradingView);
