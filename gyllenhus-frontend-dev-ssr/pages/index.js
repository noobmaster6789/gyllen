import React, { useState, useEffect } from "react";
import { connect } from 'react-redux';
import { compose } from 'redux';

import Layout from '../components/Layout'
import HomePage from '../containers/HomePage'
import { find } from 'lodash';
import {
  loadGoldProduct, loadSilverProduct, getMenuRequest,
  loadPlatinumProduct, loadPalladiumProduct,
  loadBanner, loadSpecialOffer, getOurSuppliers,
  getOpenPrice, getPriceRealtime, getPriceList, getDeliveryCostSetting, getFooter,
} from "../utils/loadData";

// import { changeMargin, changeCountdownTime, listen } from '../utils/api';
import {
  changeMargin,
  changeCountdownTime,
  changeFrequency,
  updatePriceRealtime,
  refreshInternal,
  updateUserType
} from '../utils/api';
import {
  loadIndexes,
  changeDelayTime,
  loadServerTimeStamp,
  loadGoldPrice,
  loadSilverPrice,
  loadPlatinumPrice,
  loadPalladiumPrice,
  indexesLoaded,
  loadedPriceRealtime,
  loadUserInfo
} from "../containers/App/actions";
import {
  loadGoldProducts,
  loadSilverProducts,
  loadSpecialOffers,
  loadPlatinumProducts,
  loadPalladiumProducts,
  goldProductsLoaded,
  silverProductsLoaded,
  platinumProductsLoaded,
  palladiumProductsLoaded,
  specialOffersLoaded,
} from "../containers/HomePage/actions";
import { createStructuredSelector } from "reselect";
import { makeSelectPreviousUpdateTime, makeSelectRefreshTime, makeSelectCurrency } from "../containers/App/selectors";
import { usePageVisibility } from 'react-page-visibility';

const Index = (props) => {
  const {
    onLoadGoldProduct,
    onLoadSilverProduct,
    onLoadPlatinumProduct,
    onLoadPalladiumProduct,
    onLoadGoldPrice,
    onLoadSilverPrice,
    onLoadPlatinumPrice,
    onLoadPalladiumPrice,
    onLoadIndexes,
    onLoadSpecialOffer,
    onReloadIndex,
    onChangeDelayTime,
    refreshTime,
    previousUpdateTime,
    onChangeCountdownWithTimeStamp,
    currency,
    onLoadPriceRealtime,
    onLoadGoldProductLoaded,
    onLoadSilverProductLoaded,
    onLoadPlatinumProductLoaded,
    onLoadPalladiumProductLoaded,
    onLoadSpecialOfferLoaded,
    onLoadUserInfo,
    currencyIndex,
    weight,
    isCollapsed,
    priceList
  } = props;
  const [prevTime, setPrevTime] = useState(new Date().getTime());
  var isVisible;
  try {
    isVisible = usePageVisibility()
  } catch (e) {
    isVisible = false;
  }


  useEffect(() => {
    const currentTime = new Date().getTime();
    if (isVisible === true && (currentTime - prevTime > 60000)) {
      onLoadGoldProduct();
      onLoadSilverProduct();
      onLoadPlatinumProduct();
      onLoadPalladiumProduct();
      onLoadSpecialOffer();
      onReloadIndex();
    } else {
      setPrevTime(currentTime);
    }
  }, [isVisible]);


  // use localStorage after refresh time > 0
  if (refreshTime !== 0) {
    changeCountdownTime((currentTemp) => {
      const countdown = refreshTime - ((currentTemp - previousUpdateTime) % refreshTime) - 1;
      onChangeCountdownWithTimeStamp(currentTemp);
      const jwtToken = localStorage.getItem('jwtToken');
      const authorization = jwtToken && jwtToken !== '' ? `Bearer ${jwtToken}` : '';
      if (countdown === 4) {
        getPriceList(null, authorization).then(priceList => localStorage.setItem("priceList", JSON.stringify(priceList)));
        loadGoldProduct(null, authorization).then(goldProduct => localStorage.setItem("goldProduct", JSON.stringify(goldProduct)));
        loadSilverProduct(null, authorization).then(silverProduct => localStorage.setItem("silverProduct", JSON.stringify(silverProduct)));
        loadPlatinumProduct(null, authorization).then(platinumProduct => localStorage.setItem("platinumProduct", JSON.stringify(platinumProduct)));
        loadPalladiumProduct(null, authorization).then(palladiumProduct => localStorage.setItem("palladiumProduct", JSON.stringify(palladiumProduct)));
        loadSpecialOffer(null, authorization).then(specialOffer => localStorage.setItem("specialOffer", JSON.stringify(specialOffer)));
      }
      if (countdown === 0) {
        const priceList = JSON.parse(localStorage.getItem('priceList'));
        const goldProduct = JSON.parse(localStorage.getItem('goldProduct'));
        const silverProduct = JSON.parse(localStorage.getItem('silverProduct'));
        const platinumProduct = JSON.parse(localStorage.getItem('platinumProduct'));
        const palladiumProduct = JSON.parse(localStorage.getItem('palladiumProduct'));
        const specialOffer = JSON.parse(localStorage.getItem('specialOffer'));

        onLoadIndexes(priceList);
        onLoadGoldPrice(priceList, currency);
        onLoadSilverPrice(priceList, currency);
        onLoadPlatinumPrice(priceList, currency);
        onLoadPalladiumPrice(priceList, currency);

        onLoadGoldProductLoaded(goldProduct);
        onLoadSilverProductLoaded(silverProduct);
        onLoadPlatinumProductLoaded(platinumProduct);
        onLoadPalladiumProductLoaded(palladiumProduct);
        onLoadSpecialOfferLoaded(specialOffer);
      }
    });

    updateUserType((data) => {
      let userInfo = localStorage.getItem('userInfo');
      userInfo = JSON.parse(userInfo);
      if (userInfo) {
        const { userType } = data;
        const newData = JSON.parse(userType);
        if (parseInt(newData.old) === userInfo.ID) {
          onLoadUserInfo();
          onReloadIndex();
          onLoadGoldProduct();
          onLoadSilverProduct();
          onLoadPlatinumProduct();
          onLoadPalladiumProduct();
          onLoadSpecialOffer();
        }
      }
    });
  }

  changeMargin(() => {
    onLoadGoldProduct();
    onLoadSilverProduct();
    onLoadPlatinumProduct();
    onLoadPalladiumProduct();
    onLoadSpecialOffer();
    onReloadIndex();
  });

  changeFrequency(() => {
    onReloadIndex();
  });

  updatePriceRealtime((data) => {
    onLoadPriceRealtime(data.data, currencyIndex);
  });

  refreshInternal((data) => {
  });
  const golSymbol = `XAUSEK`;
  const silverSymbol = `XAGSEK`;
  const platinumSymbol = `PLASEK`;
  const palladiumSymbol = `PALSEK`;


  const goldPrice = find(priceList, { symbol: golSymbol });;
  const silverPrice = find(priceList, { symbol: silverSymbol });
  const platinumPrice = find(priceList, {symbol: platinumSymbol});
  const palladiumPrice = find(priceList, {symbol: palladiumSymbol});

  return (
    <Layout title="Home" deliverySetting={props.deliverySetting} openPrices={props.openPrices}
      priceRealtime={props.priceRealtime} menu={props.menu} dataFooter={props.dataFooter} 
      currencyIndex={currencyIndex} weightIndex={weight} isCollapsed={isCollapsed}>
      <HomePage goldProductsNext={props.goldProducts} silverProductsNext={props.silverProducts}
        platinumProductsNext={props.platinumProducts} palladiumProductsNext={props.palladiumProducts}
        specialOffersNext={props.specialOffers} bannerList={props.banners}
        ourSuppliers={props.ourSuppliers} goldPriceNext={goldPrice} silverPriceNext={silverPrice}
        platinumPriceNext={platinumPrice} palladiumPriceNext={palladiumPrice} />
    </Layout>
  )
}

Index.getInitialProps = async (ctx) => {
  const { req } = ctx
  const { locale, cookies  } = req || window.__NEXT_DATA__.props;
  const { userInfoID } = cookies;

  const results = await Promise.all([
    loadBanner(locale),
    loadGoldProduct(locale),
    loadSilverProduct(locale),
    loadSpecialOffer(locale),
    getOurSuppliers(locale),
    getMenuRequest(locale, 'main-menu'),
    getPriceRealtime(),
    getOpenPrice(),
    loadPlatinumProduct(locale),
    loadPalladiumProduct(locale),
    getDeliveryCostSetting(),
    getFooter(locale),
    getPriceList(userInfoID)
  ])

  const banners = results[0];
  const goldProducts = results[1];
  const silverProducts = results[2];
  const specialOffers = results[3];
  const ourSuppliers = results[4];
  const menus = results[5];
  const priceRealtime = results[6];
  const openPrices = results[7];
  const platinumProducts = results[8];
  const palladiumProducts = results[9];
  const deliverySetting = results[10];
  const dataFooter = results[11];
  const priceList = results[12];

  return {
    banners: banners ? banners : false,
    goldProducts: goldProducts ? goldProducts : [],
    silverProducts: silverProducts ? silverProducts : [],
    specialOffers: specialOffers ? specialOffers : [],
    ourSuppliers: ourSuppliers ? ourSuppliers.suppliers : false,
    menu: menus,
    priceRealtime: priceRealtime,
    openPrices: openPrices,
    platinumProducts: platinumProducts ? platinumProducts : [],
    palladiumProducts: palladiumProducts ? palladiumProducts : [],
    deliverySetting: deliverySetting,
    dataFooter: dataFooter,
    priceList : priceList
  };
};

function mapDispatchToProps(dispatch) {
  return {
    onLoadUserInfo: () => dispatch(loadUserInfo()),
    onReloadIndex: () => dispatch(loadIndexes()),
    onLoadGoldProduct: () => dispatch(loadGoldProducts()),
    onLoadSpecialOffer: () => dispatch(loadSpecialOffers()),
    onLoadSilverProduct: () => dispatch(loadSilverProducts()),
    onLoadPlatinumProduct: () => dispatch(loadPlatinumProducts()),
    onLoadPalladiumProduct: () => dispatch(loadPalladiumProducts()),
    onLoadGoldProductLoaded: (goldProducts) => dispatch(goldProductsLoaded(goldProducts)),
    onLoadSilverProductLoaded: (silverProducts) => dispatch(silverProductsLoaded(silverProducts)),
    onLoadPlatinumProductLoaded: (platinumProducts) => dispatch(platinumProductsLoaded(platinumProducts)),
    onLoadPalladiumProductLoaded: (palladiumProducts) => dispatch(palladiumProductsLoaded(palladiumProducts)),
    onLoadSpecialOfferLoaded: (specialOffer) => dispatch(specialOffersLoaded(specialOffer)),
    onLoadGoldPrice: (priceList, currency) => dispatch(loadGoldPrice(priceList, currency)),
    onLoadSilverPrice: (priceList, currency) => dispatch(loadSilverPrice(priceList, currency)),
    onLoadPlatinumPrice: (priceList, currency) => dispatch(loadPlatinumPrice(priceList, currency)),
    onLoadPalladiumPrice: (priceList, currency) => dispatch(loadPalladiumPrice(priceList, currency)),
    onLoadIndexes: (priceList, currency) => dispatch(indexesLoaded(priceList)),
    onChangeDelayTime: (refreshTime, delayTime) => dispatch(changeDelayTime(refreshTime, delayTime)),
    onChangeCountdownWithTimeStamp: (timestamp) => dispatch(loadServerTimeStamp(timestamp)),
    onLoadPriceRealtime: (data) => dispatch(loadedPriceRealtime(data))
  };
}

const withConnect = connect(
  createStructuredSelector({
    refreshTime: makeSelectRefreshTime(),
    previousUpdateTime: makeSelectPreviousUpdateTime(),
    currency: makeSelectCurrency()
  }),
  mapDispatchToProps,
);

export default compose(withConnect)(Index);
