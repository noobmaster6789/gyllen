  import React, { useEffect } from "react";
import { connect } from 'react-redux';
import { compose } from 'redux';

import Layout from '../components/Layout'
import {
  loadGoldProduct, loadSilverProduct, getMenuRequest,
  loadBanner, loadSpecialOffer, getOurSuppliers,
  getOpenPrice, getPriceRealtime, getFooter
} from "../utils/loadData";
import { PurchasePage } from "../containers/PurchasePage";

const Purchase = (props) => {
  const {
    currencyIndex,
    weight,
    isCollapsed
  } = props;
  return (
    <Layout title="Purchased Complete" dataFooter = {props.dataFooter} openPrices = {props.openPrices}
      priceRealtime={props.priceRealtime} menu = {props.menu}
      currencyIndex={currencyIndex} weightIndex={weight} isCollapsed={isCollapsed}>
      <div><PurchasePage /></div>
    </Layout>
  )
}

Purchase.getInitialProps = async (ctx) => {
  const { req } = ctx
  const { locale } = req || window.__NEXT_DATA__.props;
  const results = await Promise.all([
    getMenuRequest(locale, 'main-menu'),
    getPriceRealtime(),
    getOpenPrice(),
    getFooter(locale)
  ])

  const menus = results[0];
  const priceRealtime = results[1];
  const openPrices = results[2];
  const dataFooter = results[3];
  return {
    menu : menus,
    priceRealtime : priceRealtime,
    openPrices : openPrices,
    dataFooter : dataFooter
  };
}



export default compose(Purchase);
