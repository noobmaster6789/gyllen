import React, { useEffect } from "react";
import { connect } from 'react-redux';
import { compose } from 'redux';
import { find } from 'lodash';
import lodash from 'lodash';

import Layout from '../components/Layout'
import {
  getMenuRequest, getOpenPrice, getPriceRealtime, getFooter, loadGoldProduct, getPriceList, loadProductDetail
} from "../utils/loadData";
import ProductPage from "../containers/ProductPage";

const Products = (props) => {
  const { 
    currencyIndex,
    weight,
    isCollapsed
  } = props;
  return (
    <Layout title="Product Single" dataFooter={props.dataFooter} openPrices={props.openPrices} 
      priceRealtime={props.priceRealtime} menu={props.menu}
      currencyIndex={currencyIndex} weightIndex={weight} isCollapsed={isCollapsed}>
      <div> <ProductPage product={props.product} /> </div>
    </Layout>
  )
}

Products.getInitialProps = async (ctx) => {
  const { req, query } = ctx
  const { locale } = req || window.__NEXT_DATA__.props;
  const slug = query.id;
  const results = await Promise.all([
    getMenuRequest(locale, 'main-menu'),
    getPriceRealtime(),
    getOpenPrice(),
    getFooter(locale),
    loadProductDetail(locale, slug),
    loadGoldProduct(locale),
  ])
  const menus = results[0];
  const priceRealtime = results[1];
  const openPrices = results[2];
  const dataFooter = results[3];
  const product = results[4];
  
  return {
    menu: menus,
    priceRealtime: priceRealtime,
    openPrices: openPrices,
    dataFooter: dataFooter,
    product: product
  };
}

export default compose(Products);
