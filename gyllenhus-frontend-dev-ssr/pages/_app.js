import React from "react";
import { Provider } from "react-redux";
import App, { Container } from "next/app";
import withRedux from "next-redux-wrapper";
import { IntlProvider, injectIntl } from 'react-intl'
import '../global-styles.css';


import makeStore from '../store';

class MyApp extends App {

  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    const { req } = ctx
    const { locale, messages } = req || window.__NEXT_DATA__.props
    let { currency, weight, isCollapsed } = req.cookies;

    if (typeof currency == 'undefined') {
      currency = "SEK";
    }
    if (typeof weight == 'undefined') {
      weight = "oz";
    }
    if (typeof isCollapsed == 'undefined') {
      isCollapsed = false;
    }

    const now = Date.now()
    return { pageProps, locale, messages, now, currency, weight, isCollapsed }
  }

  render() {
    const { Component, pageProps, store, locale, messages, now, currency, weight, isCollapsed } = this.props;
    const IntlPage = injectIntl(Component)
    return (
      <Provider store={store}>
        <IntlProvider locale={locale} messages={messages} initialNow={now}>
          <IntlPage {...pageProps} currencyIndex={currency} weight={weight} isCollapsed={isCollapsed} />
        </IntlProvider>
      </Provider>
    );
  }

}

export default withRedux(makeStore)(MyApp);