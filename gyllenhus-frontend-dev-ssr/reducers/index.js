
import { combineReducers } from "redux"
import globalReducer from '../containers/App/reducer'
import footerListReducer from '../containers/FooterList/reducer'
import homePageReducer from '../containers/HomePage/reducer'
import registerPageReducer from '../containers/RegisterPage/reducer'
import cartPageReducer from "../containers/CartPage/reducer"
import productPageReducer from "../containers/ProductPage/reducer"

export default combineReducers({
    global: globalReducer,
    myaccount: globalReducer,
    loginModel: globalReducer,
    index: globalReducer,
    footerList: footerListReducer,
    home: homePageReducer,
    registerPage : registerPageReducer,
    cartPage : cartPageReducer,
    productPage : productPageReducer
})